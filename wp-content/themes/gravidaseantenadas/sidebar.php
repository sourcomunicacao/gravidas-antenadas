<div class="profile" style="margin-bottom: 20px;">
  <a href="http://www.gravidaseantenadas.com.br/author/patricia/" title="Saiba mais sobre Patrícia Azevedo">
    <img src="<?php echo ot_get_option("foto_perfil"); ?>" class="img-responsive" style="margin:0">
    <div class="content_profile">
      <h3><?php echo ot_get_option("nome_perfil"); ?></h3>
      <p><?php echo ot_get_option("desc_perfil"); ?></p>
      <strong>Leia mais &raquo;</strong>
    </div>
  </a>
</div>

<?php get_template_part( 'partials/widget', 'shop'); ?>

<div class="publicidade">
  <a href="<?php echo ot_get_option('ad1_papo_antenado_link'); ?>" title="<?php echo ot_get_option('ad1_papo_antenado_titulo'); ?>">
    <img src="<?php echo ot_get_option('ad1_papo_antenado'); ?>" alt="<?php echo ot_get_option('ad1_papo_antenado_titulo'); ?>" class="img-responsive" style="margin-bottom: 15px;">
  </a>
</div>

<div class="publicidade">
  <a href="<?php echo ot_get_option('ad2_papo_antenado_link'); ?>" title="<?php echo ot_get_option('ad2_papo_antenado_titulo'); ?>">
    <img src="<?php echo ot_get_option('ad2_papo_antenado'); ?>" alt="<?php echo ot_get_option('ad2_papo_antenado_titulo'); ?>" class="img-responsive" style="margin-bottom: 15px;">
  </a>
</div>

<div class="publicidade">
  <a href="<?php echo ot_get_option('ad3_papo_antenado_link'); ?>" title="<?php echo ot_get_option('ad3_papo_antenado_titulo'); ?>">
    <img src="<?php echo ot_get_option('ad3_papo_antenado'); ?>" alt="<?php echo ot_get_option('ad3_papo_antenado_titulo'); ?>" class="img-responsive" style="margin-bottom: 15px;">
  </a>
</div>

<div class="publicidade">
  <a href="<?php echo ot_get_option('ad4_papo_antenado_link'); ?>" title="<?php echo ot_get_option('ad4_papo_antenado_titulo'); ?>">
    <img src="<?php echo ot_get_option('ad4_papo_antenado'); ?>" alt="<?php echo ot_get_option('ad4_papo_antenado_titulo'); ?>" class="img-responsive" style="margin-bottom: 15px;">
  </a>
</div>

<div class="publicidade">
  <a href="<?php echo ot_get_option('ad5_papo_antenado_link'); ?>" title="<?php echo ot_get_option('ad5_papo_antenado_titulo'); ?>">
    <img src="<?php echo ot_get_option('ad5_papo_antenado'); ?>" alt="<?php echo ot_get_option('ad5_papo_antenado_titulo'); ?>" class="img-responsive" style="margin-bottom: 15px;">
  </a>
</div>

<div class="publicidade">
  <?php echo ot_get_option('ad_google'); ?>
</div>

<div id="fb-root"></div>
<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<div style="max-width: 260px!important; overflow:hidden!important" class="fb-page hidden-sm" data-href="https://www.facebook.com/gravidaseantenadas" data-width="260" data-height="320" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false">
  <div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/gravidaseantenadas"><a href="https://www.facebook.com/gravidaseantenadas">Gravidas e Antenadas</a></blockquote></div>
</div>