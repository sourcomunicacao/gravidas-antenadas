<?php get_header(); ?>

<section class="content">
  <div class="container">
    <div class="col-sm-8">
      <?php echo do_shortcode('[rev_slider alias="sliderhome"]'); ?>
      <?php //echo do_shortcode("[huge_it_slider id='1']"); ?>
    </div>
    <div class="col-xs-12 col-sm-4 responsive-mobile">
      <div class="profile">
        <a href="http://www.gravidaseantenadas.com.br/author/patricia/" title="Saiba mais sobre Patrícia Azevedo">
          <img src="<?php echo ot_get_option("foto_perfil"); ?>" class="img-responsive">
          <div class="content_profile">
            <h3><?php echo ot_get_option("nome_perfil"); ?></h3>
            <p><?php echo ot_get_option("desc_perfil"); ?></p>
            <strong>Leia mais &raquo;</strong>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>

<section class="ads hidden-xs">
  <div class="container">

    <div class="col-xs-12 col-sm-4">
      <a href="<?php echo ot_get_option('ad1_home_link', ''); ?>" title="<?php echo ot_get_option('ad1_home_titulo', ''); ?>">
        <img src="<?php echo ot_get_option('ad1_home_foto', ''); ?>" class="img-responsive" alt="<?php echo ot_get_option('ad1_home_titulo', ''); ?>">
      </a>
    </div>

    <div class="col-xs-12 col-sm-4">
      <a href="<?php echo ot_get_option('ad2_home_link', ''); ?>" title="<?php echo ot_get_option('ad2_home_titulo', ''); ?>">
        <img src="<?php echo ot_get_option('ad2_home_foto', ''); ?>" class="img-responsive" alt="<?php echo ot_get_option('ad2_home_titulo', ''); ?>">
      </a>
    </div>

    <div class="col-xs-12 col-sm-4">
      <a href="<?php echo ot_get_option('ad3_home_link', ''); ?>" title="<?php echo ot_get_option('ad3_home_titulo', ''); ?>">
        <img src="<?php echo ot_get_option('ad3_home_foto', ''); ?>" class="img-responsive" alt="<?php echo ot_get_option('ad3_home_titulo', ''); ?>">
      </a>
    </div>

  </div>
</section>


<?php
  $count = 1;
  if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
  elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
  else { $paged = 1; }
  $args = array(
    'post_type' => 'post',
    'posts_per_page' => 18,
    'paged' => $paged,
    'page' => $paged,
    // 'meta_query'  => array(
    //   array(
    //     'key'   => 'ocultar_da_home',
    //     'value'     => array(0 => array(0 => "Ocultar na Home" ) ),
    //     //'compare'   => '==',
    //   ),
    // ),
  );
  query_posts( $args );
  if(have_posts()):
?>
<section class="posts">
  <div class="container">

    	<?php while(have_posts()) : the_post(); ?>

  		<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-6 col-md-4'); ?>>
    		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <?php the_post_thumbnail(array('class' => 'img-responsive') ); //400x240 ?>
      	</a>
        <div class="post_content">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <h2><?php the_title(); //print_r(get_metadata('post', $post->ID, 'ocultar_da_home', false)); ?></h2>
          </a>
          <div class="post_excerpt"><?php the_excerpt(); ?></div>
        </div>
  		</div>

			<?php if($count == 9) : ?>

	</div>
</section>

<?php get_template_part( 'partials/section', 'shop'); ?>

<section class="posts continued">
  <div class="container">

			<?php endif; ?>

			<?php

				$count++;

				endwhile;

	      if (function_exists("custom_pagination")) {
	        custom_pagination($the_query->max_num_pages,"",$paged);
	      }

				wp_reset_postdata();

			?>

  </div>
</section>

<?php else:  ?>
    <p><?php _e( 'Desculpe, nenhum post foi encontrado.' ); ?></p>
<?php endif; ?>


<section class="brands">
  <div class="container">
    <div class="col-xs-12 col-sm-12">
      <h2>Parceiros</h2>
    </div>
    <div class="col-md-12 nopadding">

      <div id="nav-02" class="crsl-nav" style="position: relative;">
        <a href="#" class="previous left  carousel-control " style="margin-left:  -10px; margin-top: 60px; z-index:999">&nbsp;</a>
        <a href="#" class="next     right carousel-control " style="margin-right: -10px; margin-top: 60px; z-index:999">&nbsp;</a>
      </div>

      <div class="galerytwo crsl-items" data-navigation="nav-02">
        <div class="crsl-wrap">

          <?php if (ot_get_option( 'parceiro_1_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_1_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_1_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_1_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_2_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_2_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_2_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_2_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_3_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_3_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_3_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_3_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_4_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_4_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_4_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_4_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_5_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_5_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_5_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_5_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_6_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_6_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_6_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_6_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_7_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_7_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_7_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_7_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_8_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_8_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_8_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_8_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_9_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_9_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_9_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_9_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_10_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_10_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_10_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_10_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_11_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_11_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_11_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_11_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

          <?php if (ot_get_option( 'parceiro_12_on_off') == 'on'): ?>
            <div class="crsl-item">
              <a href="<?php echo ot_get_option( 'parceiro_12_link') ?>" class="thumbnail">
                <img src="<?php echo ot_get_option( 'parceiro_12_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_12_nome') ?>" class="img-responsive">
              </a>
            </div>
          <?php endif ?>

        </div>
      </div>

    </div>
  </div>
</section>
<?php get_footer(); ?>
