<?php
/*
 * Template Name: Papo Antenado
 */
  get_header(); 
?>

<div class="container">
	<div class="col-sm-9">
		<section class="papo-antenado">
			<?php $query_papo = new WP_Query(array('posts_per_page' => 6, 'cat' => 3)); while($query_papo->have_posts()) : $query_papo->the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="margin-bottom: 40px">
	  		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	        <?php the_post_thumbnail('papoantenado_thumb', array('class' => 'img-responsive', 'style' => 'margin-bottom: 0;') ); //400x240 ?>
	    	</a>
	    	<div class="post_content">
	        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	        	<h2><?php the_title(); ?></h2>
	        	<?php the_excerpt(); ?>
	        </a>
	      </div>
			</div>
			<?php endwhile; ?>
		</section>
		<hr>
		<section class="nav-menu">
			<div class="navigation">
				<div class="pull-left">
					<?php previous_posts_link('&laquo;  Página anterior') ?>
				</div>
				<div class="pull-right">
					<?php next_posts_link('Próxima página &raquo;') ?>
				</div>
				<?php wp_reset_query(); ?>
			</div>
		</section>
	</div><!-- end-col sm 9 -->
	<div class="col-sm-3">
    <?php get_sidebar(); ?>
  </div><!-- end-col sm 3  -->
</div><!-- end container -->
<?php get_footer(); ?>