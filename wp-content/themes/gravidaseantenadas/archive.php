<?php
/*
*	Template name: Mansory
*/
?>
<?php get_header(); ?>
<style>.post_content p, .post_content h2 { margin-bottom: 0; }</style>
<section class="posts">
	<style type="text/css" media="screen">
/*
		.mgrid-item { width: 31.5%; margin-right: 1%; margin-left: 1%; margin-bottom: 20px; }
		@media (max-width: 767px) { 
			.mgrid-item { width: 45.9%; margin-right: 0.8%; margin-left: 0.8%; margin-bottom: 20px; }
		}
		@media (min-width: 768px) {  }
		@media (min-width: 992px) {  }
*/

	</style>
  <div class="container">
	  <div class="row masonry-container">
    <?php 
	    query_posts('posts_per_page=9&paged='.get_query_var('paged'));
			while (have_posts()): the_post(); 
    ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('col-md-4 col-sm-6 item'); ?>>
  		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php the_post_thumbnail('thumb_post', array('class' => 'img-responsive') ); //400x240 ?>
    	</a>
      <div class="post_content" >
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <h2><?php the_title(); ?></h2>
        </a>
        <div class="post_excerpt" style="height: auto; min-height: 1px; margin-bottom: 0"><?php the_excerpt(); //if( function_exists('zilla_likes') ) zilla_likes(); ?></div>
      </div>
		</div>
 		<?php endwhile; ?>	
	  </div>
		
		<style type="text/css" media="screen">
			.pagination {
clear:both;
padding:20px 0;
position:relative;
font-size:11px;
line-height:13px;
}
 
.pagination span, .pagination a {
display:block;
float:left;
margin: 2px 2px 2px 0;
padding:6px 9px 5px 9px;
text-decoration:none;
width:auto;
height: auto!important;
color:#fff;
background: #555;
}
 
.pagination a:hover{
color:#fff;
background: #3279BB;
}
 
.pagination .current{
padding:6px 9px 5px 9px;
background: #3279BB;
color:#fff;
}
		</style>
	  <?php if (function_exists("pagination")) {
    pagination($additional_loop->max_num_pages);
} ?>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js"></script>
 		<script language="javascript" type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>
		<script>
			//$('.mgrid').masonry({
			//	// options
			//	itemSelector: '.mgrid-item',
			//	//columnWidth: 200
			//});
			var $container = $('.masonry-container');
			$container.imagesLoaded( function () {
			  $container.masonry({
			    columnWidth: '.item',
			    itemSelector: '.item'
			  });   
			});
		</script>
		
  </div>
</section>
<?php get_footer(); ?>