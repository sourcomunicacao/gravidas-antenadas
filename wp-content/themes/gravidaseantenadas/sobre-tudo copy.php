<?php
/*
*	Template name: Sobre-tudo
*/
?>
<?php get_header(); ?>
<style>.post_content p, .post_content h2 { margin-bottom: 0; }</style>
<section class="posts">
  <div class="container">
    <?php 
      $query = new WP_Query( array('posts_per_page' => 299) );
			while ($query->have_posts()): $query->the_post(); 
    ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-4'); ?>>
  		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
        <?php the_post_thumbnail('thumb_post', array('class' => 'img-responsive') ); //400x240 ?>
    	</a>
      <div class="post_content">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <h2><?php the_title(); ?></h2>
        </a>
        <div class="post_excerpt"><?php the_excerpt(); if( function_exists('zilla_likes') ) zilla_likes(); ?></div>
      </div>
		</div>
 		<?php endwhile; ?>
  </div>
</section>
<?php get_footer(); ?>