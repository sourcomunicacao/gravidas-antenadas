<?php get_header(); ?>
<div class="container">
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading" style="height: 100px;">
          <h4>Quer ter sua marca sendo divulgada para um público segmentado? <p>Anuncie no Grávidas e Antenadas!</p></h4>
        </div>
        <div class="panel-body">
          <?php echo do_shortcode('[contact-form-7 id="436" title="Formulário de contato (Anuncie Conosco)"]' ); ?>
        </div>
      </div>
    </div>
    
    
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading" style="height: 100px;">
          <h4><p>Ou fale com a Patrícia:</p></h4>
        </div>
        <div class="panel-body">
          <?php echo do_shortcode('[contact-form-7 id="435" title="Formulário de contato"]' ); ?>
        </div>
      </div>
    </div>
    <script src="http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js"></script>
<script>
	$(document).ready(function(){
		$("#contactForm").validate();
	});
</script>
  </div>
</div>

<?php get_footer(); ?>