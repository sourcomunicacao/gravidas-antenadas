<a href="#topo" id="voltar-topo"><span class="icon-seta fa-2x"></span></a>

<footer>
  <div class="container">
    <div class="footerNav">
      <div class="col-xs-12 col-sm-8">
        <nav>
          <ul class="list-unstyled list-inline nav-responsive menu-footer">
            <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/coroa_footer.png" style="width: auto; height: 84px;"></a></li>
            <li><a href="<?php bloginfo('url'); ?>/sobre-tudo/">Sobre tudo</a></li>
            <li><a href="<?php bloginfo('url'); ?>/papo-antenado/">Papo antenado</a></li>
            <li><a href="<?php bloginfo('url'); ?>/inspiracao/">Inspiração</a></li>
            <li><a href="<?php bloginfo('url'); ?>/moda/">Moda</a></li>
            <li><a href="http://www.mamaeachei.com.br/">Shop</a></li>
            <li><a href="<?php bloginfo('url'); ?>/colunistas/">Colunistas</a></li>
            <li><a href="<?php bloginfo('url'); ?>/contato/">Contato</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-xs-12 col-sm-4 text-right">
        <ul class="list-unstyled social-list social-list-footer">
            <li><a href="https://www.facebook.com/gravidaseantenadas?ref=hl" target="_blank" title="Facebook">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/facebook.png" alt="Facebook" class="img-responsive"></a></li>
            <li><a href="https://twitter.com/patriciacamilo" target="_blank" title="Twitter">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/twitter.png" alt="Twitter" class="img-responsive"></a></li>
            <li><a href="https://www.pinterest.com/patricamilo/" target="_blank" title="Pinterest">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/pinterest.png" alt="Pinterest" class="img-responsive"></a></li>
            <li><a href="https://instagram.com/gravidaseantenadas/" target="_blank" title="Instagram">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/instagram.png" alt="Instagram" class="img-responsive"></a></li>
            <li><a href="https://plus.google.com/u/0/+PatriciaCamilogravidaseantenadas/posts" target="_blank" title="Google+">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/googleplus.png" alt="Google+" class="img-responsive"></a></li>
            <li><a href="#" target="_blank" title="Youtube">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/youtube.png" alt="Youtube" class="img-responsive"></a></li>
            <li><a href="http://www.gravidaseantenadas.com.br/?page_id=70 " target="_blank" title="E-mail">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/email.png" alt="E-mail" class="img-responsive"></a></li>
          </ul>
      </div>
    </div>
    <div class="informativo_footer col-xs-12 col-sm-8 col-sm-offset-2" style="padding:10px; line-height: 20px; font-size: 13px;">
      <?php echo ot_get_option('informativo', ''); ?>
    </div>
    <div class="col-xs-12 copyright">
      <p>&copy;2015 Grávidas e Antenadas. Todos os direitos reservados.</p>
    </div>
  </div>
</footer>

<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.min.js"></script>
<!--  jQuery 1.7+  -->
    <script src="<?php echo get_template_directory_uri(); ?>/jquery-1.9.1.min.js"></script>
     
    <!-- Include js plugin -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/responsiveCarousel.min.js"></script>

    <script type="text/javascript">
      jQuery(document).ready(function($){
        $('.galery').carousel({ visible: 4, itemMinWidth: 225, itemMargin: 15 });
      });
    </script>

    <script>
          $('img').each(function() {
              $(this).addClass('img-responsive');
          });
        </script>

<?php wp_footer(); ?>
</body>
</html>