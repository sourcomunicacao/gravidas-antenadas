<?php // custom functions.php template @ digwp.com
add_filter( 'wpcf7_load_css', '__return_false' );
// add feed links to header
if (function_exists('automatic_feed_links')) {
	automatic_feed_links();
} else {
	return;
}
		
//Gets post cat slug and looks for single-[cat slug].php and applies it
add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
		return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);

function new_subcategory_hierarchy() { 
    $category = get_queried_object();

    $parent_id = $category->category_parent;

    $templates = array();

    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';     
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );

        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";

        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php'; 
    }
    return locate_template( $templates );
}

add_filter( 'category_template', 'new_subcategory_hierarchy' );

// smart jquery inclusion
if (!is_admin()) {
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '1.3.2');
	wp_enqueue_script('jquery');
}


// enable threaded comments
function enable_threaded_comments(){
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1))
			wp_enqueue_script('comment-reply');
		}
}
add_action('get_header', 'enable_threaded_comments');


// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);


// add google analytics to footer
function add_google_analytics() {
	echo '<script src="http://www.google-analytics.com/ga.js" type="text/javascript"></script>';
	echo '<script type="text/javascript">';
	echo 'var pageTracker = _gat._getTracker("UA-XXXXX-X");';
	echo 'pageTracker._trackPageview();';
	echo '</script>';
}
add_action('wp_footer', 'add_google_analytics');


// custom excerpt length
function custom_excerpt_length($length) {
	return 17;
}
add_filter('excerpt_length', 'custom_excerpt_length');


// custom excerpt ellipses for 2.9+
function custom_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'custom_excerpt_more');

/* custom excerpt ellipses for 2.8-
function custom_excerpt_more($excerpt) {
	return str_replace('[...]', '...', $excerpt);
}
add_filter('wp_trim_excerpt', 'custom_excerpt_more'); 
*/


// add a favicon to your 
function blog_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('wpurl').'/favicon.ico" />';
}
add_action('wp_head', 'blog_favicon');


// add a favicon for your admin
function admin_favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('stylesheet_directory').'/images/favicon.png" />';
}
add_action('admin_head', 'admin_favicon');


// custom admin login logo
function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image: url('.get_bloginfo('template_directory').'/images/custom-login-logo.png) !important; }
	</style>';
}
add_action('login_head', 'custom_login_logo');


// disable all widget areas
function disable_all_widgets($sidebars_widgets) {
	//if (is_home())
		$sidebars_widgets = array(false);
	return $sidebars_widgets;
}
add_filter('sidebars_widgets', 'disable_all_widgets');


// kill the admin nag
if (!current_user_can('edit_users')) {
	add_action('init', create_function('$a', "remove_action('init', 'wp_version_check');"), 2);
	add_filter('pre_option_update_core', create_function('$a', "return null;"));
}


// category id in body and post class
function category_id_class($classes) {
	global $post;
	foreach((get_the_category($post->ID)) as $category)
		$classes [] = 'cat-' . $category->cat_ID . '-id';
		return $classes;
}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');


// get the first category id
function get_first_category_ID() {
	$category = get_the_category();
	return $category[0]->cat_ID;
}

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 400, 240, true );
add_image_size( 'inspiracao_thumb', '380', '530', true );
add_image_size( 'papoantenado_thumb', '795', '387', true );

/** Pagination */
function pagination_funtion() {
// Get total number of pages
global $wp_query;
$total = $wp_query->max_num_pages;
// Only paginate if we have more than one page                   
if ( $total > 1 )  {
    // Get the current page
    if ( !$current_page = get_query_var('paged') )
        $current_page = 1;
                           
        $big = 999999999;
        // Structure of "format" depends on whether we’re using pretty permalinks
        $permalink_structure = get_option('permalink_structure');
        $format = empty( $permalink_structure ) ? '&page=%#%' : 'page/%#%/';
        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
            'format' => $format,
            'current' => $current_page,
            'total' => $total,
            'mid_size' => 2,
            'type' => 'list'
        ));
    }
}
/** END Pagination */

/** AUTHORS **/

function get_authors() { global $wpdb; $order = 'user_nicename'; $user_ids = $wpdb->get_col("SELECT ID FROM $wpdb->users ORDER BY $order"); foreach($user_ids as $user_id) : $user = get_userdata($user_id); $all_authors[$user_id] = $user->display_name; endforeach; return $all_authors; }

add_action('wp_head','hook_css');
function hook_css() {
	$output='<style>ul[class^="huge_it"] li[id^="image_id"]{
    overflow: visible!important;
}

ul[class^="huge_it"] li[id^="image_id"] div[class^="huge_it_slideshow_title"]{
    position: relative!important;
    width: 100%!important;
    min-height:80px;
    background: #fff!important;
    color:#303030!important;
    font-size: 26px!important;
    font-family: "FuturaBold", sans-serif!important;
    letter-spacing: 1px!important;
    text-transform: uppercase!important;
    top:0;bottom:0;left:0;right:0;
    padding:20px;
    text-align:left!important;
}
ul[class^="huge_it"] li[id^="image_id"] div[class^="huge_it_slideshow_description"]{
    position: relative!important;
    width: 100%!important;
    min-height:78px;
    background: #fff!important;
    color:#303030!important;
    font-size: 13px!important;
    top:0;bottom:0;left:0;right:0;
    padding:20px;
    padding-top:0;
}
</style>';
	echo $output;
}

add_filter('get_avatar','change_avatar_css');
function change_avatar_css($class) {
    $class = str_replace('class="avatar', 'class="avatar photo img-responsive', $class) ;
    return $class;
}

//Adiciona campos extras a tela de edição/inserção de usuários (by wptotal.com.br)
add_action('show_user_profile', 'add_extra_fields_user_profile');
add_action('edit_user_profile', 'add_extra_fields_user_profile');
 
 
 
function add_extra_fields_user_profile($user) {
?>
<table class="form-table">
    <tr>
        <th><label for="telefone">Instagram</label></th>
        <td><input type="text" name="instagram" id="instagram" value="<?php echo esc_attr(get_the_author_meta('instagram', $user->ID)); ?>" class="regular-text" /></td>
    </tr>
</table>
<?php
}
//Gravando campos extras inseridos no banco de dados (by wptotal.com.br)
add_action('personal_options_update', 'extra_fields_to_user_save');
add_action('edit_user_profile_update', 'extra_fields_to_user_save');
function extra_fields_to_user_save($user_id)
{
if (!current_user_can('edit_user', $user_id))
    return false;
    update_usermeta($user_id, 'instagram', $_POST['instagram']);
}