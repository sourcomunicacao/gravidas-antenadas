<?php get_header(); ?>

<section class="page">
  <div class="container">
    <div class="col-xs-12 col-sm-12">
      <div class="row">
        <div class="col-xs-12 col-sm-8">
          <h2>Publicações da categoria:  </h2>
          <hr>
          <?php if ( have_posts() ) : ?>
          <?php while ( have_posts() ) : the_post();  ?>
          <div class="row blogRow">
            <div class="col-xs-12 col-sm-4"> <a href="<?php the_permalink(); ?>"><img src="http://www.placehold.it/400x250" class="img-responsive"></a> </div>
            <div class="col-xs-12 col-sm-8">
              <h3>
                <?php the_title(); ?>
              </h3>
              <?php the_excerpt(); ?>
              <a href="<?php the_permalink(); ?>" class="btn btn-primary">Continuar lendo</a> </div>
          </div>
          <?php endwhile; ?>
          <div class="col-xs-12 col-sm-12">
            <div class="nav-previous alignleft">
              <?php next_posts_link( 'Mais antigos' ); ?>
            </div>
            <div class="nav-next alignright">
              <?php previous_posts_link( 'Mais novos' ); ?>
            </div>
          </div>
          <?php else : ?>
          <p>
            <?php _e('Desculpe, não encontramos nada.'); ?>
          </p>
          <?php endif; ?>
        </div>
        <?php get_sidebar(); ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
