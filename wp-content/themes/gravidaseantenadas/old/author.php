<?php get_header(); ?>

<div id="content" class="container">
	<?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>

	<div class="row">
		<div class="col-sm-3" style="margin-top: 20px;">
			<?php echo get_wp_user_avatar($author->ID, 250, '', '', array('class' => 'img-responsive' )); ?>
		</div>
		<div class="col-sm-9">
		    <h2><?php echo $curauth->first_name.' '.$curauth->last_name; ?></h2>
		    <p><?php echo get_user_meta($curauth->ID, 'description', true); ?></p>
                  <hr>
                  <h2>Últimos posts: </h2>
                  <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
					<h4>
						<?php the_title(); ?>
						 <span> - <small><?php the_time('d M Y'); ?> em <?php the_category('&'); ?></small></span>
					</h4>
				</a>

			<?php endwhile; else : ?>
				<p><?php _e('Esse autor não possui posts.'); ?></p>
			<?php endif; ?>
      
      <hr>
      
      <?php if ( isset($curauth->facebook) || isset($curauth->twitter) || isset($curauth->googleplus) || isset($curauth->instagram) ): ?>
      	<h2>Me encontre nas redes sociais: </h2>
      <?php endif ?>
      
      <?php if (isset($curauth->facebook) && !empty($curauth->facebook)): ?>
      	<a href="<?php echo $curauth->facebook; ?>" target="_blank"><span class="icon-facebook fa-2x"></span></a>
      <?php endif ?>
      <?php if (isset($curauth->twitter) && !empty($curauth->twitter)): ?>
      	<a href="http://twitter.com/<?php echo $curauth->twitter; ?>" target="_blank"><span class="icon-twitter fa-2x"></span></a>
      <?php endif ?>
      <?php if (isset($curauth->googleplus) && !empty($curauth->googleplus)): ?>
      	<a href="http://plus.google.com/<?php echo $curauth->googleplus; ?>" target="_blank"><span class="icon-googleplus fa-2x"></span></a>
      <?php endif ?>
      <?php if (isset($curauth->instagram) && !empty($curauth->instagram)): ?>
      	<a href="http://instagram.com/<?php echo $curauth->instagram; ?>" target="_blank"><span class="icon-instagram fa-2x"></span></a>
      <?php endif ?>

		</div>

	</div>
</div>

<br>
<br>
<br>

<?php get_footer(); ?>