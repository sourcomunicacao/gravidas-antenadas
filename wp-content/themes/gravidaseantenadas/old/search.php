<?php
/*
Template Name: Search Page
*/
?>
<?php get_header(); ?>

<section class="posts">
  <div class="container">
    <?php 
      // global $query_string;

      // $query_args = explode("&", $query_string);
      // $search_query = array();

      // foreach($query_args as $key => $string) {
      //   $query_split = explode("=", $string);
      //   $search_query[$query_split[0]] = urldecode($query_split[1]);
      // } // foreach

      // $search = new WP_Query($search_query);

    $search = &new WP_Query("s=$s&showposts=-1"); 
    //$key = esc_html($s, 1); 
    //$count = $allsearch->post_count; 
    //_e(''); 
    //_e('<span class="search-terms">'); 
    //echo $key; 
    //_e('</span>'); 
    //_e(' &mdash; '); 
    //echo $count . ' '; 
    //_e('articles');

      if($search->have_posts()):
			while ($search->have_posts()): 
      $search->the_post(); 
    ?>
  		<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-4'); ?>>
    		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <?php the_post_thumbnail('thumb_post', array('class' => 'img-responsive') ); //400x240 ?>
      	</a>
        <div class="post_content">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <h2><?php the_title(); ?></h2>
          </a>
          <div class="post_excerpt"><?php the_excerpt(); ?></div>
        </div>
  		</div>
 		<?php endwhile; 
    else: ?>
    <div class="text-center">
      <p>A pesquisa não retornou nenhum resultado.</p>
    </div>
    <?php
    endif;
    wp_reset_query(); ?>
  </div>
</section>

<?php get_footer(); ?>