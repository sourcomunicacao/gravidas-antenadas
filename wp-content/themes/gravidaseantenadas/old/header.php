<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
<?php if(is_home()){ echo ""; } elseif(is_404()){ echo "404 (Página não encontrada) - "; } elseif(is_search()){ echo "Resultados da busca para "; echo wp_specialchars($s, 1); echo " - ";  } else { echo wp_title(); echo " - "; } echo bloginfo('name'); ?>
</title>
  
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.png" />
  <link rel="icon" href="<?php bloginfo('siteurl'); ?>/img/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="<?php bloginfo('siteurl'); ?>/img/favicon.ico" type="image/x-icon" />
  
<link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" >
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/css/theme.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.css"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php wp_head(); ?>
<script>
 //(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 //(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 //m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 //})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 //ga('create', 'UA-15593202-32', 'auto');
 //ga('send', 'pageview');
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46998716-1', 'auto');
  ga('send', 'pageview');
</script>
</head>
<body id="topo" <?php body_class(); ?>>
  <div class="fluid-container">
    <div class="newsletter-fixed">
    	<div class="container">
      	<div class="col-sm-6 nopadding">
          <p class="text-right">Cadastre-se em nossa Newsletter e fique Antenada!</p>	
        </div>
        <div class="col-sm-6 nopadding">

          <form action="//gravidaseantenadas.us12.list-manage.com/subscribe/post?u=99d7eb3721e8662408f252bc8&amp;id=b8690ca0e0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div class="col-xs-10 col-sm-5 nopadding">
              <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
            </div>
            <div class="col-xs-2 col-sm-1 nopadding">
              <div style="position: absolute; left: -5000px;"><input type="text" name="b_99d7eb3721e8662408f252bc8_b8690ca0e0" tabindex="-1" value=""></div>
              <div class="clear"><input type="submit" value="OK" name="subscribe" id="mc-embedded-subscribe" class="button form-control"></div>
            </div>
            <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
              </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
          </form>
          
        </div>
      </div>
    </div>
  </div>
  
  
<div class="social">
  <ul class="list-unstyled social-list" style="margin-top: -15px;">
    <li><a href="https://www.facebook.com/gravidaseantenadas?ref=hl" target="_blank">
      <!-- <span class="icon-facebook fa-2x"></span> -->
      <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/facebook.png" alt="Facebook">
    </a></li>
    <li><a href="https://twitter.com/patriciacamilo" target="_blank">
      <!-- <span class="icon-twitter fa-2x"></span> -->
      <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/twitter.png" alt="Twitter">
    </a></li>
    <li><a href="https://www.pinterest.com/patricamilo/" target="_blank">
      <!-- <span class="icon-pinterest fa-2x"></span> -->
      <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/pinterest.png" alt="Pinterest">
    </a></li>
    <li><a href="https://instagram.com/gravidaseantenadas/" target="_blank">
      <!-- <span class="icon-instagram fa-2x"></span> -->
      <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/instagram.png" alt="Instagram">
    </a></li>
    <li><a href="https://plus.google.com/u/0/+PatriciaCamilogravidaseantenadas/posts" target="_blank">
      <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/googleplus.png" alt="Instagram">
    </a></li>
    <li><a href="#" target="_blank">
      <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/youtube.png" alt="Youtube">
    </a></li>
    <li><a href="http://gravidaseantenadas.tumblr.com/" target="_blank" target="_blank">
      <!-- <span class="icon-email fa-2x"></span> -->
      <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/email.png" alt="Tumblr">
    </a></li>
  </ul>
</div>

<!-- Newsletter + Cookie method -->
<script>
  $(document).ready(function() {
    if ($.cookie("no_thanks") == null) {
      $('#modal-newsletter').modal("toggle");
          function show_modal(){
            $('#modal-newsletter').modal();
          }
      window.setTimeout(show_modal, 1000);
      }
    $(".nothanks").click(function() {
      document.cookie = "no_thanks=true; expires=Fri, 31 Dec 9999 23:59:59 UTC";

    });
  });
</script>  
  
<div class="modal fade" id="modal-newsletter" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
 		<div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close nothanks" data-dismiss="modal" aria-label="Close" aria-hidden="true"><span aria-hidden="true">&times;</span></button>
        <form>
          <h3>Cadastre-se em nossa Newsletter e não perca nenhuma novidade! :)</h3>
          <hr>
          <label>E-mail* <input type="email" style="width: 100%;" name="email" required></label>
          <br>
          <button type="send" class="newsletter-send">Cadastrar</button>
        </form>
      </div>
    </div>
  </div>
</div>  
  
<!-- Fim Newsletter -->
  
<!--<div class="newsletter hidden-xs">
  <div class="container">
    <div class="col-xs-12 col-sm-6">
      <p>Lorem ipsum dolor sit amet consectetuer amet</p>
    </div>
    <div class="col-xs-12 col-sm-6">
      <form class="form-inline">
        <input type="text" class="form-control" placeholder="Cadastre-se e receba nossas novidades">
        <button type="submit" class="btn btn-primary">Cadastrar</button>
      </form>
    </div>
  </div>
</div>-->

<header>
  <div class="container">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
      <a href="<?php bloginfo('url'); ?>/" id="alogo_header">
      	<img src="<?php bloginfo('template_directory'); ?>/img/logo.png" class="img-responsive clearall" style="margin-top: 20px; margin-bottom: 20px;">
      </a>
    </div>
    <!--<div class="col-xs-12 col-sm-4 text-right">
      <ul class="list-unstyled list-inline">
        <li><a href="#">PT</a></li>
        <li><a href="#">EN</a></li>
      </ul>
    </div>-->
    <div class="clearfix"></div>
    <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>

   <div class="row">
    <div class="col-sm-10 collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="margin-left: 10px;">
      <nav>
        <ul class="list-unstyled list-inline text-center nav-responsive menu-topo">
					<li><a href="<?php bloginfo('url'); ?>/sobre-tudo/">Sobre tudo</a></li>
          <li><a href="<?php bloginfo('url'); ?>/papo-antenado/">Papo antenado</a></li>
					<li><a href="<?php bloginfo('url'); ?>/inspiracao/">Inspiração</a></li>
					<li><a href="<?php bloginfo('url'); ?>/moda/">Moda</a></li>
	        <li><a href="http://www.mamaeachei.com.br/" target="_blank">Shop</a></li>
          <li><a href="<?php bloginfo('url'); ?>/colunistas/">Colunistas</a></li>
          <li><a href="<?php bloginfo('url'); ?>/contato/">Contato</a></li>
        </ul>
      </nav>
    </div>
    <div class="col-sm-4 pull-right" style="margin-right: 15px; padding-left: 20px;">
      <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div class="input-group correction">
      		<input type="text" name="s" id="s" class="form-control" placeholder="Buscar..." value="<?php echo get_search_query(); ?>">
      		<span class="input-group-btn">
        		<button class="btn btn-default" id="searchsubmit" type="submit"><span class="fa fa-search"></span></button>
      		</span>
    		</div>
      </form>
    </div>
	</div>
 </div>

</header>
