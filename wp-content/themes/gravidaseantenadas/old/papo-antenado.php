<?php
/*
Template Name: Papo Antenado
*/
?>

<?php get_header(); ?>

<div class="container">

	<div class="col-sm-9">
		<section class="papo-antenado">
			<div class="col-sm-12">
				<?php $query_papo = new WP_Query(array('posts_per_page' => 6, 'cat' => 3)); while($query_papo->have_posts()) : $query_papo->the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="margin-bottom: 40px">
		  		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		        <?php the_post_thumbnail('papoantenado_thumb', array('class' => 'img-responsive', 'style' => 'margin-bottom: 0;') ); //400x240 ?>
		    	</a>
		    	<div class="post_content">
		        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		        	<h2><?php the_title(); ?></h2>
		        	<?php the_excerpt(); ?>
		        </a>
		      </div>
				</div>
				<?php endwhile; ?>
			</div>
		</section>
		<hr>
		<section class="nav-menu">
			<div class="navigation">
				<div class="pull-left">
					<?php previous_posts_link('&laquo;  Página anterior') ?>
				</div>
				<div class="pull-right">
					<?php next_posts_link('Próxima página &raquo;') ?>
				</div>
				<?php wp_reset_query(); ?>
			</div>
		</section>
	</div>


	

	<div class="col-sm-3">

    <div class="profile" style="margin-bottom: 20px;">
      <a href="<?php echo get_option('home'); ?>/sobre" title="">
        <img src="<?php echo ot_get_option("foto_perfil"); ?>" class="img-responsive" style="margin:0">
        <div class="content_profile">
          <h3><?php echo ot_get_option("nome_perfil"); ?></h3>
          <p><?php echo ot_get_option("desc_perfil"); ?></p>
          <strong>Leia mais &raquo;</strong>
        </div>
      </a>
    </div>
    
    <div class="box-container">
			<div class="col-sm-12">
				<div id="myCarousel" class="carousel slide">
					<div class="carousel-inner">
						
            <div class="item active">
							<div class="row">
								<div class="col-sm-12">
									<a href="http://www.mamaeachei.com.br/preco/ovelha-media/" title="OVELHA MÉDIA" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/01-OVELHA-MEDIA.jpg" alt="OVELHA MÉDIA" class="img-responsive">
                  </a>
								</div>
							</div>
						</div>

						<div class="item">
							<div class="row">
								<div class="col-sm-12">
									<a href="http://www.mamaeachei.com.br/preco/balao-kiko-colorido/" title="BALÃO KIKO COLORIDO" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/02-Balao-Kiko-colorido.jpg" alt="BALÃO KIKO COLORIDO" class="img-responsive">
                  </a>
								</div>
							</div>
						</div>

						<div class="item">
							<div class="row">
								<div class="col-sm-12">
									<a href="http://www.mamaeachei.com.br/preco/prateleira-nuvem-pati-2/" title="PRATELEIRA NUVEM PATI" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/03-Prateleira-Nuvem-Pati.jpg" alt="PRATELEIRA NUVEM PATI" class="img-responsive">
                  </a>
								</div>
							</div>
						</div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/nicho-nico-casinha-madeira/" title="NICHO NICO CASINHA MADEIRA" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/04-Nicho-Nico-casinha-madeira.jpg" alt="NICHO NICO CASINHA MADEIRA" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/porta-livros-nuvem-luca/" title="PORTA LIVROS NUVEM LUCA" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/05-Porta-Livros-Nuvem-Luca.jpg" alt="PORTA LIVROS NUVEM LUCA" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/beau/" title="BEAU" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/06-bonecobranco.jpg" alt="BEAU" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/babybag-zigzag-rosa/" title="BABYBAG ZIGZAG ROSA" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/07-babybag-zigzag-rosa.jpg" alt="BABYBAG ZIGZAG ROSA" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/sapatilha-perky-bandana/" title="SAPATILHA PERKY BANDANA" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/08-Perky-infantil-estampada.jpg" alt="SAPATILHA PERKY BANDANA" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/piticao-gato-pablo/" title="PITICÃO GATO PABLO" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/09-gatotricot.jpg" alt="PITICÃO GATO PABLO" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/toalha-de-banho-princesa/" title="TOALHA DE BANHO PRINCESA" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/10-toalhaprincesas.jpg" alt="TOALHA DE BANHO PRINCESA" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/kit-forget-me-not-nuvem/" title="KIT FORGET ME NOT NUVEM" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/11-kitskiphop.jpg" alt="KIT FORGET ME NOT NUVEM" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/fogao-portatil/" title="FOGÃO PORTÁTIL" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/12-cozinhademadeira.jpg" alt="FOGÃO PORTÁTIL" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/saco-de-dormir-reino/" title="SACO DE DORMIR – REINO" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/13-sacodedormirpessoinha.jpg" alt="SACO DE DORMIR – REINO" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/mala-plush-mescla/" title="MALA PLUSH MESCLA" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/14-bolsa.jpg" alt="MALA PLUSH MESCLA" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/conjunto-de-alimentacao-do-lobo-maluco/" title="CONJUNTO DE ALIMENTAÇÃO DO LOBO MALUCO" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/15-pratos.jpg" alt="CONJUNTO DE ALIMENTAÇÃO DO LOBO MALUCO" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>

            <div class="item">
              <div class="row">
                <div class="col-sm-12">
                  <a href="http://www.mamaeachei.com.br/preco/kit-hair-care-princesas/" title="KIT HAIR CARE PRINCESAS" class="thumbnail" target="_blank">
                    <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/16-kithairprincesas.jpg" alt="KIT HAIR CARE PRINCESAS" class="img-responsive">
                  </a>
                </div>
              </div>
            </div>
						
					</div>
          <a class="left carousel-control anuncio" href="#myCarousel" data-slide="prev" style="top:100px;">&nbsp;</span></a> 
       		<a class="right carousel-control anuncio" href="#myCarousel" data-slide="next"style="top:100px;">&nbsp;</span></a> 
				</div>
			</div>
      
      <?php if (get_field('ad1_papo_antenado')): ?>
          <div class="publicidade">
            <img src="<?php the_field('ad1_papo_antenado'); //600x600 ?>" class="img-responsive" style="margin-bottom: 15px;">
          </div>
          <?php endif; ?>
          
          <?php if (get_field('ad2_papo_antenado')): ?>
          <div class="publicidade">
            <img src="<?php the_field('ad2_papo_antenado'); //600x600 ?>" class="img-responsive" style="margin-bottom: 15px;">
          </div>
          <?php endif; ?>

          <?php if (get_field('ad3_papo_antenado')): ?>
          <div class="publicidade">
            <img src="<?php the_field('ad3_papo_antenado'); //600x600 ?>" class="img-responsive" style="margin-bottom: 15px;">
          </div>
          <?php endif; ?>

          <?php if (get_field('ad4_papo_antenado')): ?>
          <div class="publicidade">
            <img src="<?php the_field('ad4_papo_antenado'); //600x600 ?>" class="img-responsive" style="margin-bottom: 15px;">
          </div>
          <?php endif; ?>

          <?php if (get_field('ad5_papo_antenado')): ?>
          <div class="publicidade">
            <img src="<?php the_field('ad5_papo_antenado'); //600x600 ?>" class="img-responsive" style="margin-bottom: 15px;">
          </div>
          <?php endif; ?>
      
      <div class="col-sm-12">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-page" data-href="https://www.facebook.com/gravidaseantenadas" data-width="320" data-height="320" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/gravidaseantenadas"><a href="https://www.facebook.com/gravidaseantenadas">Gravidas e Antenadas</a></blockquote></div></div>
      </div>

    </div>
	</div>



	

</section>
<!--
<br>
<br>
<section class="relacionados">
	<div class="container">
		<div class="col-md-12">
			<div id="myCarousel2" class="carousel slide">
				<div class="carousel-inner">
					<div class="item active">
						<div class="row">
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
						</div>
					</div>
					<div class="item">
						<div class="row">
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
						</div>
					</div>
					<div class="item">
						<div class="row">
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
						</div>
					</div>
				</div>
					<a class="left  carousel-control vertical-center" href="#myCarousel2" data-slide="prev"> &nbsp;</a> 
          <a class="right carousel-control vertical-center" href="#myCarousel2" data-slide="next">&nbsp;</a>
			</div>
		</div>
	</div>
</section>
-->

</div>
<hr>
<section class="instagram">
  <div class="container">
    <div class="col-xs-12 col-sm-12 text-center">
      <h2 class="insta_title">INSTAGRAM @GRAVIDASEANTENADAS</h2>
    </div>
  </div>
  <div class="row">
    <iframe src="http://snapwidget.com/sc/?u=Z3JhdmlkYXNlYW50ZW5hZGFzfGlufDMyMHwzfDN8fG5vfDV8ZmFkZUlufHx5ZXN8bm8=&ve=161015" title="Instagram Widget" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; height:320px"></iframe>
  </div>
</section>
<?php get_footer(); ?>