<?php
/*
Template Name: Moda
*/
?>

<?php get_header(); ?>

<div class="container">
	<section class="moda">
		<?php $query_moda = new WP_Query(array('posts_per_page' => 15, 'cat' => 5)); while($query_moda->have_posts()) : $query_moda->the_post(); ?>
			<div class="col-sm-4">
				<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php if (get_field('imagem_destacada_2')): ?>
						<?php 
							$image_array = get_field('imagem_destacada_2');
							$image_thumb = wp_get_attachment_image_src($image_array, 'inspiracao_thumb');
						?>
						<img src="<?php echo $image_thumb[0]; ?>" class="img-responsive" style="margin-bottom: 30px;">
					<?php else: ?>
						<?php the_post_thumbnail('inspiracao_thumb', array('class' => 'img-responsive') ); //400x240 ?>	
					<?php endif ?>
				</a>
			</div>
		<?php endwhile; ?>
	</section>
</div>

<hr>

	<section class="nav-menu">
		<div class="navigation">
			<div class="pull-left">
				<?php previous_posts_link('&laquo;  Página anterior') ?>
			</div>
			<div class="pull-right">
				<?php next_posts_link('Próxima página &raquo;') ?>
			</div>
			<?php wp_reset_query(); ?>
		</div>
	</section>
</div>

<br>
<br>
<section class="relacionados">
	<div class="container">
		<div class="col-md-12">
			<div id="myCarousel2" class="carousel slide">
				<div class="carousel-inner">
					<div class="item active">
						<div class="row">
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
						</div>
					</div>
					<div class="item">
						<div class="row">
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
						</div>
					</div>
					<div class="item">
						<div class="row">
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
							<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
						</div>
					</div>
				</div>
				<a class="left carousel-control" href="#myCarousel2" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> 
				<a class="right carousel-control" href="#myCarousel2" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> 
			</div>
		</div>
	</div>
</section>

</div>

<section class="instagram">
	<div class="container">
    <div class="col-xs-12 col-sm-12 text-center">
      <h2 class="insta_title">INSTAGRAM @GRAVIDASEANTENADAS</h2>
    </div>
  </div>
	<div class="row">
    <iframe src="http://snapwidget.com/sc/?u=Z3JhdmlkYXNlYW50ZW5hZGFzfGlufDMyMHwzfDN8fG5vfDV8ZmFkZUlufHx5ZXN8bm8=&ve=161015" title="Instagram Widget" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; height:320px"></iframe>
  </div>
</section>
<?php get_footer(); ?>