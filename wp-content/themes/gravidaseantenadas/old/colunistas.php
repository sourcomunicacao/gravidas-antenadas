<?php
/*
*	Template name: Colunistas
*/
?>

<?php get_header(); ?>

<?php $args = array('meta_query' => array(array('who' => 'authors')),); 

$site_url = get_site_url();
$wp_user_query = new WP_User_Query($args);
$authors = $wp_user_query->get_results(); 

?>

<div class="container">
	<div class="row">

		<?php if (!empty($authors)) : 
			  foreach ($authors as $author) : ?>

				<div class="col-sm-6">
					<?php $author_info = get_userdata($author->ID);
					$link = '<a href="' . get_author_posts_url($author->ID, $author->user_nicename) . '" title="' . esc_attr(sprintf(__("Biografia de: $s"), $author->display_name)) . '">';
					//echo $link;
					?>
				
            <div class="panel panel-default">  
            	<div class="panel-body nopadding">
                <div class="col-sm-4 nopadding"><?php echo get_wp_user_avatar($author->ID, 180, '', '', array('class' => 'img-responsive' )); ?></div>
                <div class="col-sm-8">
                  <h3><?php echo '<a href="'.get_option('home' ) .'/author/'.$author->user_nicename.'/">' . $author_info->first_name.' '.$author_info->last_name . '</a>'; ?></h3>
                  <?php $longbio = get_user_meta($author->ID, 'description', true); ?>
                  <?php $bio = substr($longbio, 0, 150); ?>
                  <?php $bio .= "..."; ?>
                  <p class="font-new"><?php echo $bio; ?></p>
                  <?php echo '<a href="'.get_option('home' ) .'/author/'.$author->user_nicename.'/">'; ?>Ver perfil completo &raquo;<?php echo '</a>'; ?>
                </div>
               </div>
             </div>

				</div>

		<?php endforeach; ?>
		<?php endif; ?>

	</div>
</div>


<?php get_footer(); ?>