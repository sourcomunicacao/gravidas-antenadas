<div class="col-xs-12 col-sm-4 responsive-mobile">
	<div class="profile">
		<a href="<?php echo get_option('home'); ?>/sobre" title="">
			<img src="<?php echo ot_get_option("foto_perfil"); ?>" class="img-responsive">
			<div class="content_profile">
				<h3><?php echo ot_get_option("nome_perfil"); ?></h3>
				<p><?php echo ot_get_option("desc_perfil"); ?></p>
				<strong>Leia mais &raquo;</strong>
			</div>
		</a>
	</div>
</div>
