<?php get_header(); ?>

<section class="content">
  <div class="container">
    
    <!--<?php if(have_posts()) : ?>
    <?php while(have_posts()) : the_post(); ?>
    	<div class="col-sm-8">
        <a href="<?php the_permalink(); ?>">
        	<img src="http://www.placehold.it/1000x800&text=Matéria Destaque" class="img-responsive">
        	<h2><?php the_title(); ?></h2>
          <p><?php the_excerpt(); ?></p>
        </a>
      </div>
    <?php endwhile; ?>
    <?php endif; ?>-->
    
    <div class="col-sm-8 hidden-xs">
    	<?php echo do_shortcode("[huge_it_slider id='1']"); ?>
    </div>
      
    <?php get_sidebar(); ?>
  </div>
</section>

<section class="ads">
  <div class="container">

    <div class="col-xs-12 col-sm-4">
      <a href="<?php echo ot_get_option('ad1_home_link', ''); ?>" title="<?php echo ot_get_option('ad1_home_titulo', ''); ?>">
        <img src="<?php echo ot_get_option('ad1_home_foto', ''); ?>" class="img-responsive" alt="<?php echo ot_get_option('ad1_home_titulo', ''); ?>">
      </a>
    </div>

    <div class="col-xs-12 col-sm-4">
      <a href="<?php echo ot_get_option('ad2_home_link', ''); ?>" title="<?php echo ot_get_option('ad2_home_titulo', ''); ?>">
        <img src="<?php echo ot_get_option('ad2_home_foto', ''); ?>" class="img-responsive" alt="<?php echo ot_get_option('ad2_home_titulo', ''); ?>">
      </a>
    </div>

    <div class="col-xs-12 col-sm-4">
      <a href="<?php echo ot_get_option('ad3_home_link', ''); ?>" title="<?php echo ot_get_option('ad3_home_titulo', ''); ?>">
        <img src="<?php echo ot_get_option('ad3_home_foto', ''); ?>" class="img-responsive" alt="<?php echo ot_get_option('ad3_home_titulo', ''); ?>">
      </a>
    </div>

  </div>
</section>

<?php 
  $shop = '
  <section class="produtos_home">
    <div class="container">
      <div class="col-xs-12">
        <h2 class="text-center">Shop</h2>
      </div>
      <div id="nav-01" class="crsl-nav" style="position: relative;">
        <a href="#" class="previous left  carousel-control" style="margin-left:  0; margin-top: 40px">&nbsp;</a>
        <a href="#" class="next     right carousel-control" style="margin-right: 0; margin-top: 40px">&nbsp;</a>
      </div>
      <div class="galery crsl-items" data-navigation="nav-01">
        <div class="crsl-wrap">

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/ovelha-media/" title="OVELHA MÉDIA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/01-OVELHA-MEDIA.jpg" alt="OVELHA MÉDIA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/balao-kiko-colorido/" title="BALÃO KIKO COLORIDO" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/02-Balao-Kiko-colorido.jpg" alt="BALÃO KIKO COLORIDO" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/prateleira-nuvem-pati-2/" title="PRATELEIRA NUVEM PATI" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/03-Prateleira-Nuvem-Pati.jpg" alt="PRATELEIRA NUVEM PATI" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/nicho-nico-casinha-madeira/" title="NICHO NICO CASINHA MADEIRA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/04-Nicho-Nico-casinha-madeira.jpg" alt="NICHO NICO CASINHA MADEIRA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/porta-livros-nuvem-luca/" title="PORTA LIVROS NUVEM LUCA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/05-Porta-Livros-Nuvem-Luca.jpg" alt="PORTA LIVROS NUVEM LUCA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/beau/" title="BEAU" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/06-bonecobranco.jpg" alt="BEAU" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/babybag-zigzag-rosa/" title="BABYBAG ZIGZAG ROSA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/07-babybag-zigzag-rosa.jpg" alt="BABYBAG ZIGZAG ROSA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/sapatilha-perky-bandana/" title="SAPATILHA PERKY BANDANA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/08-Perky-infantil-estampada.jpg" alt="SAPATILHA PERKY BANDANA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/piticao-gato-pablo/" title="PITICÃO GATO PABLO" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/09-gatotricot.jpg" alt="PITICÃO GATO PABLO" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/toalha-de-banho-princesa/" title="TOALHA DE BANHO PRINCESA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/10-toalhaprincesas.jpg" alt="TOALHA DE BANHO PRINCESA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/kit-forget-me-not-nuvem/" title="KIT FORGET ME NOT NUVEM" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/11-kitskiphop.jpg" alt="KIT FORGET ME NOT NUVEM" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/fogao-portatil/" title="FOGÃO PORTÁTIL" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/12-cozinhademadeira.jpg" alt="FOGÃO PORTÁTIL" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/saco-de-dormir-reino/" title="SACO DE DORMIR – REINO" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/13-sacodedormirpessoinha.jpg" alt="SACO DE DORMIR – REINO" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/mala-plush-mescla/" title="MALA PLUSH MESCLA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/14-bolsa.jpg" alt="MALA PLUSH MESCLA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/conjunto-de-alimentacao-do-lobo-maluco/" title="CONJUNTO DE ALIMENTAÇÃO DO LOBO MALUCO" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/15-pratos.jpg" alt="CONJUNTO DE ALIMENTAÇÃO DO LOBO MALUCO" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/kit-hair-care-princesas/" title="KIT HAIR CARE PRINCESAS" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/16-kithairprincesas.jpg" alt="KIT HAIR CARE PRINCESAS" class="img-responsive">
            </a>
          </div>

        </div>
      </div>
    </div>
  </section>
  '
?>

<?php 
  global $post;
  $posts = get_posts(array(
    'numberposts' => 9,
    'post_type'   => 'post',
    'meta_query'  => array(
      array(
        'key'   => 'ocultar_da_home',
        'value'     => array(0 => array(0 => "Ocultar na Home" ) ),
        //'compare'   => '==',
      ),
    ),
  ));
  if($posts):
?>
<section class="posts">
  <div class="container">

    <?php foreach ($posts as $post): setup_postdata( $post ) ?>
  		<div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-4'); ?>>
    		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <?php the_post_thumbnail(array('class' => 'img-responsive') ); //400x240 ?>
      	</a>
        <div class="post_content">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <h2><?php the_title(); //print_r(get_metadata('post', $post->ID, 'ocultar_da_home', false)); ?></h2>
          </a>
          <div class="post_excerpt"><?php the_excerpt(); ?></div>
        </div>
  		</div>
 		<?php endforeach; ?>

  </div>
</section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>


<section class="produtos_home">
    <div class="container">
      <div class="col-xs-12">
        <h2 class="text-center">Shop</h2>
      </div>
      <div id="nav-01" class="crsl-nav" style="position: relative;">
        <a href="#" class="previous left  carousel-control" style="margin-left:  0; margin-top: 40px">&nbsp;</a>
        <a href="#" class="next     right carousel-control" style="margin-right: 0; margin-top: 40px">&nbsp;</a>
      </div>
      <div class="galery crsl-items" data-navigation="nav-01">
        <div class="crsl-wrap">

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/ovelha-media/" title="OVELHA MÉDIA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/01-OVELHA-MEDIA.jpg" alt="OVELHA MÉDIA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/balao-kiko-colorido/" title="BALÃO KIKO COLORIDO" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/02-Balao-Kiko-colorido.jpg" alt="BALÃO KIKO COLORIDO" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/prateleira-nuvem-pati-2/" title="PRATELEIRA NUVEM PATI" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/03-Prateleira-Nuvem-Pati.jpg" alt="PRATELEIRA NUVEM PATI" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/nicho-nico-casinha-madeira/" title="NICHO NICO CASINHA MADEIRA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/04-Nicho-Nico-casinha-madeira.jpg" alt="NICHO NICO CASINHA MADEIRA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/porta-livros-nuvem-luca/" title="PORTA LIVROS NUVEM LUCA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/05-Porta-Livros-Nuvem-Luca.jpg" alt="PORTA LIVROS NUVEM LUCA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/beau/" title="BEAU" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/06-bonecobranco.jpg" alt="BEAU" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/babybag-zigzag-rosa/" title="BABYBAG ZIGZAG ROSA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/07-babybag-zigzag-rosa.jpg" alt="BABYBAG ZIGZAG ROSA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/sapatilha-perky-bandana/" title="SAPATILHA PERKY BANDANA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/08-Perky-infantil-estampada.jpg" alt="SAPATILHA PERKY BANDANA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/piticao-gato-pablo/" title="PITICÃO GATO PABLO" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/09-gatotricot.jpg" alt="PITICÃO GATO PABLO" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/toalha-de-banho-princesa/" title="TOALHA DE BANHO PRINCESA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/10-toalhaprincesas.jpg" alt="TOALHA DE BANHO PRINCESA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/kit-forget-me-not-nuvem/" title="KIT FORGET ME NOT NUVEM" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/11-kitskiphop.jpg" alt="KIT FORGET ME NOT NUVEM" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/fogao-portatil/" title="FOGÃO PORTÁTIL" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/12-cozinhademadeira.jpg" alt="FOGÃO PORTÁTIL" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/saco-de-dormir-reino/" title="SACO DE DORMIR – REINO" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/13-sacodedormirpessoinha.jpg" alt="SACO DE DORMIR – REINO" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/mala-plush-mescla/" title="MALA PLUSH MESCLA" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/14-bolsa.jpg" alt="MALA PLUSH MESCLA" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/conjunto-de-alimentacao-do-lobo-maluco/" title="CONJUNTO DE ALIMENTAÇÃO DO LOBO MALUCO" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/15-pratos.jpg" alt="CONJUNTO DE ALIMENTAÇÃO DO LOBO MALUCO" class="img-responsive">
            </a>
          </div>

          <div class="crsl-item">
            <a href="http://www.mamaeachei.com.br/preco/kit-hair-care-princesas/" title="KIT HAIR CARE PRINCESAS" class="thumbnail" target="_blank">
              <img src="http://www.gravidaseantenadas.com.br/wp-content/uploads/2015/10/16-kithairprincesas.jpg" alt="KIT HAIR CARE PRINCESAS" class="img-responsive">
            </a>
          </div>

        </div>
      </div>
    </div>
  </section>

<?php 
  global $post;
  $posts = get_posts(array(
    'numberposts' => 9,
    'offset' => 9,
    'post_type'   => 'post',
    'meta_query'  => array(
      array(
        'key'   => 'ocultar_da_home',
        'value'     => array(0 => array(0 => "Ocultar na Home" ) ),
        //'compare'   => '==',
      ),
    ),
  ));
  if($posts):
?>
<section class="posts continued">
  <div class="container">

    <?php foreach ($posts as $post): setup_postdata( $post ) ?>
      <div id="post-<?php the_ID(); ?>" <?php post_class('col-sm-4'); ?>>
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <?php the_post_thumbnail(array('class' => 'img-responsive') ); //400x240 ?>
        </a>
        <div class="post_content">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <h2><?php the_title(); //print_r(get_metadata('post', $post->ID, 'ocultar_da_home', false)); ?></h2>
          </a>
          <div class="post_excerpt"><?php the_excerpt(); ?></div>
        </div>
      </div>
    <?php endforeach; ?>

  </div>
</section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>

<section class="brands">
  <div class="container">
    <div class="col-xs-12 col-sm-12">
      <h2>Parceiros</h2>
    </div>
    <div class="col-md-12">
      <div id="myCarousel2" class="carousel slide">
        <div class="carousel-inner">
          <div class="item active">
            <div class="row">

              <?php if (ot_get_option( 'parceiro_1_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_1_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_1_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_1_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_2_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_2_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_2_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_2_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_3_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_3_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_3_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_3_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_4_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_4_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_4_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_4_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_5_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_5_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_5_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_5_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_6_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_6_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_6_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_6_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

            </div>
          </div>
          <div class="item">
            <div class="row">
              
              <?php if (ot_get_option( 'parceiro_7_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_7_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_7_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_7_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_8_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_8_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_8_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_8_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_9_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_9_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_9_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_9_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_10_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_10_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_10_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_10_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_11_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_11_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_11_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_11_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

              <?php if (ot_get_option( 'parceiro_12_on_off') == 'on'): ?>
                <div class="col-sm-2">
                  <a href="<?php echo ot_get_option( 'parceiro_12_link') ?>" class="thumbnail">
                    <img src="<?php echo ot_get_option( 'parceiro_12_foto') ?>" alt="<?php echo ot_get_option( 'parceiro_12_nome') ?>" class="img-responsive">
                  </a>
                </div>
              <?php endif ?>

            </div>
          </div>
          </div>
        </div>
    		<div class="col-sm-offset-1">
          <a class="left  carousel-control vertical-center" href="#myCarousel2" data-slide="prev"> &nbsp;</a> 
          <a class="right carousel-control vertical-center" href="#myCarousel2" data-slide="next">&nbsp;</a>
        </div>

      </div>
  </div>
</section>

<section class="instagram hidden-xs">
  <div class="container">
    <div class="col-xs-12 col-sm-12 text-center">
      <h2 class="insta_title">INSTAGRAM @GRAVIDASEANTENADAS</h2>
    </div>
  </div>
  <div class="row">
    <iframe src="http://snapwidget.com/sc/?u=Z3JhdmlkYXNlYW50ZW5hZGFzfGlufDMyMHwzfDN8fG5vfDV8ZmFkZUlufHx5ZXN8bm8=&ve=161015" title="Instagram Widget" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; height:320px"></iframe>
  </div>
</section>
<?php get_footer(); ?>