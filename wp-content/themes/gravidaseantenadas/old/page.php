﻿<?php get_header(); ?>

<section class="page">
  <div class="container"> <img src="http://placehold.it/1170x600&text=foto" class="img-responsive">
    <div class="col-xs-12 col-sm-12">
      <?php if ( have_posts() ) : ?>
      <?php while ( have_posts() ) : the_post();  ?>
      <h2>
      <?php the_title(); ?></h2>
      <div class="row">
        <?php the_content(); ?>
      </div>
      <?php endwhile; ?>
      <?php else : ?>
      <p>
        <?php _e('Desculpe, não encontramos nada.'); ?>
      </p>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php get_footer(); ?>
