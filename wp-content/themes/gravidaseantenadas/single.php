<?php get_header(); ?>
  <section class="page">
  	<div class="container">
			<div class="col-xs-12 col-sm-9 single-content">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <h2><?php the_title(); ?><?php edit_post_link('Editar', ' - [', ']'); ?></h2>
        <p>Por <?php echo get_the_author_link(); ?> em <?php the_time("d/m/Y"); ?></p>
        <?php the_content(); ?>
				<div class="tags">
					<?php the_tags("Tags: ", ", ", ""); ?>
				</div>
        <?php endwhile; else :?><p><?php _e('Desculpe, não encontramos nada.')?></p><?php endif; ?>
        <div class="clearfix"></div><hr>
        <div class="col-xs-6 nomargin"><?php previous_post_link('%link', '<button class="btn pull-left">« Post Anterior</button>', TRUE); ?></div>
        <div class="col-xs-6 nomargin"><?php next_post_link('%link', '<button class="btn pull-right">Próximo Post »</button>', TRUE); ?></div>
        <div class="col-xs-12 nomargin">
						<?php comments_template(); ?>
      	</div>
        <div class="col-xs-12 posts-relacionados nopadding">
          <div class="relatedposts">
            <?php
              $orig_post = $post;
              global $post;
              $tags = wp_get_post_tags($post->ID);

              if ($tags) {
                $tag_ids = array();
                foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
                $args=array(
                  'tag__in' => $tag_ids,
                  'post__not_in' => array($post->ID),
                  'posts_per_page'=>3, // Number of related posts to display.
                  'caller_get_posts'=>1
                );

                $my_query = new wp_query( $args );

                if( $my_query->have_posts() ) {
                  ?>
                  <div class="col-xs-12">
                    <h3>Posts Relacionados</h3>
                  </div>
                  <?php
                  while( $my_query->have_posts() ) {
                  $my_query->the_post();
                  ?>

                  <div class="relatedthumb col-xs-12 col-md-4">
                    <a rel="external" href="<?php the_permalink(); ?>">
                      <?php the_post_thumbnail('thumbnail', array('class'=>'img-responsive') ); ?>
                      <?php the_title('<h4 style="margin-top:10px;">', '</h4>'); ?>
                    </a>
                  </div>

                <?php } }
              }
              $post = $orig_post;
              wp_reset_query();
              ?>
            </div>
        </div>
  		</div>
      <div class="col-xs-12 col-sm-3">
        <?php get_sidebar(); ?>
      </div>
  	</div>
  </section>
<?php get_footer(); ?>
