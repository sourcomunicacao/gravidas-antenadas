<?php
/*
*	Template name: Mansory
*/
?>
<?php get_header(); ?>
<style>.post_content p, .post_content h2 { margin-bottom: 0; }</style>
<section class="posts">

  <div class="container">
	  
	  
	  
	  
	  
	  <div class="row masonry-container">
	    <?php 
		    query_posts('posts_per_page=9&paged='.get_query_var('paged'));
				while (have_posts()): the_post(); 
	    ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class('col-md-4 col-sm-6 item'); ?>>
	  		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	        <?php the_post_thumbnail('thumb_post', array('class' => 'img-responsive') ); //400x240 ?>
	    	</a>
	      <div class="post_content" >
	        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	          <h2><?php the_title(); ?></h2>
	        </a>
	        <div class="post_excerpt" style="height: auto; min-height: 1px; margin-bottom: 0"><?php the_excerpt(); //if( function_exists('zilla_likes') ) zilla_likes(); ?></div>
	      </div>
			</div>
	 		<?php endwhile; ?>	
	  </div>
		
		
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js"></script>
 		<script language="javascript" type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>
		<script>
			var $container = $('.masonry-container');
			$container.imagesLoaded( function () {
			  $container.masonry({
			    columnWidth: '.item',
			    itemSelector: '.item'
			  });   
			});
		</script>
		
  </div>
</section>
<?php get_footer(); ?>