<?php
//<section class="relacionados">
//  <div class="container">
//    <div class="col-xs-12 col-sm-12 text-center">
//      <h2>Posts Relacionados</h2>
//      <hr>
//    </div>
//    <div class="col-md-12">
//      <div id="myCarousel" class="carousel slide">
//        <div class="carousel-inner">
//          <div class="item active">
//            <div class="row">
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/500x500" alt="Image" class="img-responsive"></a> </div>
//            </div>
//          </div>
//          <div class="item">
//            <div class="row">
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
//            </div>
//          </div>
//          <div class="item">
//            <div class="row">
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
//              <div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" class="img-responsive"></a> </div>
//            </div>
//          </div>
//        </div>
//       <a class="left  carousel-control vertical-center" href="#myCarousel" data-slide="prev"> &nbsp;</span></a> 
//       <a class="right carousel-control vertical-center" href="#myCarousel" data-slide="next">&nbsp;</span></a> 
//      </div>
//    </div>
//  </div>
//</section>
?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/instafeed.min.js"></script>
<section class="instagram">
	<div class="container">
		<div class="col-xs-12 col-sm-12 text-center">
			<h2 class="insta_title">INSTAGRAM @GRAVIDASEANTENADAS</h2>
		</div>
	</div>
	<div class="row">
		<style type="text/css" media="screen">
			a.link-istagram{
				background: #000;
			}
			.photo-instagram{
				transition: all .5s ease;
			}
			.photo-instagram:hover{
				opacity: .5;
			}
			/*#load-more{
				width: 100%;
		    text-align: center;
		    color: #FFF;
		    font-size: 35px;
		    line-height: 35px;
		    font-family: arial;
		    background-color: #468EE1;
		    border: 0;
		    transition: all .5s ease;
			}
			#load-more:hover{
				background-color: #68AFE2;
			}*/
		</style>
		<div id="instagram_list"></div>
		<!--<button id="load-more">+</button>-->
		<script type="text/javascript">
			//var loadButton = document.getElementById('load-more');
	    var userFeed = new Instafeed({
	        get: 'user',
	        target:"instagram_list",
	        userId: '359164345',
	        limit:6,
	        accessToken: '359164345.5b9e1e6.3d09b152014c4436be5e87a3d8b2ab9a',
	        resolution:"low_resolution",
	        template: '<a class="col-xs-4 col-sm-2 nopadding nomargin link-istagram" href="{{link}}" target="_blank"><img src="{{image}}" class="img-responsive photo-instagram" /></a>',
	        //after: function() {
              //disable button if no more results to load
          //  if (!this.hasNext()) {
          //      loadButton.setAttribute('disabled', 'disabled');
          //      $("#load-more").hide();
          //  }
					//},
	    });
	    // bind the load more button
	    //loadButton.addEventListener('click', function() {
	    //    userFeed.next();
	    //});
	    userFeed.run();
		</script>
	</div>
</section>


<footer>
  <div class="container">
    <div class="footerNav">
      <div class="col-xs-12 col-sm-9">
        <nav>
          <ul class="list-unstyled list-inline nav-responsive menu-footer">
            <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/coroa_footer.png" style="width: auto; height: 84px;"></a></li>
            <li><a href="<?php bloginfo('url'); ?>/inspiracao/">Inspiração</a></li>
            <li><a href="<?php bloginfo('url'); ?>/moda/">Moda</a></li>
            <li><a href="<?php bloginfo('url'); ?>/papo-antenado/">Papo antenado</a></li>
            <li><a href="<?php bloginfo('url'); ?>/diario-da-patricia/">Diário da Patrícia</a></li>
            <li><a href="http://www.mamaeachei.com.br/">Shop</a></li>
            <li><a href="<?php bloginfo('url'); ?>/colunistas/">Colunistas</a></li>
            <li><a href="<?php bloginfo('url'); ?>/contato/">Contato</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-xs-12 col-sm-3 text-right">
        <ul class="list-unstyled social-list social-list-footer">
            <li><a href="https://www.facebook.com/gravidaseantenadas?ref=hl" target="_blank" title="Facebook">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/facebook.png" alt="Facebook" class="img-responsive"></a></li>
            <li><a href="https://twitter.com/patriciacamilo" target="_blank" title="Twitter">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/twitter.png" alt="Twitter" class="img-responsive"></a></li>
            <li><a href="https://www.pinterest.com/patricamilo/" target="_blank" title="Pinterest">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/pinterest.png" alt="Pinterest" class="img-responsive"></a></li>
            <li><a href="https://instagram.com/gravidaseantenadas/" target="_blank" title="Instagram">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/instagram.png" alt="Instagram" class="img-responsive"></a></li>
            <li><a href="https://plus.google.com/u/0/+PatriciaCamilogravidaseantenadas/posts" target="_blank" title="Google+">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/googleplus.png" alt="Google+" class="img-responsive"></a></li>
            <li><a href="#" target="_blank" title="Youtube">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/youtube.png" alt="Youtube" class="img-responsive"></a></li>
            <li><a href="http://gravidaseantenadas.tumblr.com" target="_blank" title="Tumblr">
              <img src="<?php echo bloginfo('template_url'); ?>/img/socialicons/tumblr.png" alt="Tumblr" class="img-responsive"></a></li>
          </ul>
      </div>
    </div>
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 informativo_footer">
      <?php echo ot_get_option('informativo', ''); ?>
    </div>
    <div class="col-xs-12 copyright">
      <p>&copy;2015 Grávidas e Antenadas. Todos os direitos reservados.</p>
    </div>
  </div>
</footer>

<a href="#topo" id="voltar-topo"><span class="icon-seta fa-2x"></span></a>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js"></script>
	<script language="javascript" type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>
<script>
	var $container = $('.masonry-container');
	$container.imagesLoaded( function () {
	  $container.masonry({
	    columnWidth: '.item',
	    itemSelector: '.item'
	  });   
	});
</script>

<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.min.js"></script>

<!--  jQuery 1.7+  
<script src="<?php echo get_template_directory_uri(); ?>/jquery-1.9.1.min.js"></script>-->

<!-- Include js plugin -->
<script src="<?php echo get_template_directory_uri(); ?>/js/responsiveCarousel.min.js"></script>

<script type="text/javascript">
  jQuery(document).ready(function($){
    $('.galery').carousel({ visible: 4, itemMinWidth: 225, itemMargin: 15 });
    $('.galerytwo').carousel({ visible: 7, itemMinWidth: 150, itemMargin: 15 });
  });
</script>

<?php wp_footer(); ?>

<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script>
  <script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us12.list-manage.com","uuid":"99d7eb3721e8662408f252bc8","lid":"b8690ca0e0"}) })</script>

</body>
</html>