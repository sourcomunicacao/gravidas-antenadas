<?php 
	// Get a SimplePie feed object from the specified feed source.
	$rss = fetch_feed( 'http://www.mamaeachei.com.br/category/destaque/feed/' );
	 
	$maxitems = 0;
	 
	if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
	 
	    // Figure out how many total items there are, but limit it to 5. 
	    $maxitems = $rss->get_item_quantity( 16 ); 
	 
	    // Build an array of all the items, starting with element 0 (first element).
	    $rss_items = $rss->get_items( 0, $maxitems );
	 
	endif;
	
	if ($_GET['debug'] && $_GET['debug'] == "1") {
		echo "<pre>";
		print_r($rss);
		die;
	}

	function get_first_image_url($html) {
		if (preg_match('/<img.+?src="(.+?)"/', $html, $matches)) { return $matches[1]; }
	}
	
	function shorten($string, $length) {
		$suffix = '&hellip;';
		$short_desc = trim(str_replace(array("/r", "/n", "/t"), ' ', strip_tags($string)));
		$desc = trim(substr($short_desc, 0, $length));
		$lastchar = substr($desc, -1, 1);
		if ($lastchar == '.' || $lastchar == '!' || $lastchar == '?') $suffix='';
		$desc .= $suffix;
		return $desc;
	}
	
	if ($maxitems > 3): ?>
		
	<section class="produtos_home">
		<div class="container">
			<div class="col-xs-12">
				<h2 class="text-center">Shop</h2>
			</div>
			<div id="nav-01" class="crsl-nav" style="position: relative;">
				<a href="#" class="previous left  carousel-control" style="margin-left:  0; margin-top: 40px">&nbsp;</a>
				<a href="#" class="next     right carousel-control" style="margin-right: 0; margin-top: 40px">&nbsp;</a>
			</div>
			<div class="galery crsl-items" data-navigation="nav-01">
				<div class="crsl-wrap">
					<?php foreach ( $rss_items as $item ) : ?>
					<div class="crsl-item">
						<a href='<?php echo esc_url( $item->get_permalink() ); ?>' title='<?php echo esc_html( $item->get_title() ); ?>' class="thumbnail" target="_blank">
							<?php echo '<img src="' .get_first_image_url($item->get_content()). '" class="img-responsive" alt="">'; ?>
						</a>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
	
	<?php endif ?>