<?php 
	
	$rss = fetch_feed('http://www.mamaeachei.com.br/category/categorias/feed/');
	
	if (!is_wp_error( $rss ) ) : 
		$maxitems = $rss->get_item_quantity(16); 
		$rss_items = $rss->get_items(0, $maxitems); 
	endif;
	
	function get_first_image_url($html) {
		if (preg_match('/<img.+?src="(.+?)"/', $html, $matches)) { return $matches[1]; }
	}
	
	function shorten($string, $length) {
		$suffix = '&hellip;';
		$short_desc = trim(str_replace(array("/r", "/n", "/t"), ' ', strip_tags($string)));
		$desc = trim(substr($short_desc, 0, $length));
		$lastchar = substr($desc, -1, 1);
		if ($lastchar == '.' || $lastchar == '!' || $lastchar == '?') $suffix='';
		$desc .= $suffix;
		return $desc;
	}
	
	if ($maxitems > 3): ?>
	
	<div id="myCarousel" class="carousel slide">
		<div class="carousel-inner">
			
			<?php $count = 1; foreach ( $rss_items as $item ) : ?>
			<div class="item <?php if($count==1) echo "active"; $count++; ?>">
				<a href='<?php echo esc_url( $item->get_permalink() ); ?>' title='<?php echo esc_html( $item->get_title() ); ?>' class="thumbnail" target="_blank">
					<?php echo '<img src="' .get_first_image_url($item->get_content()). '" class="img-responsive" alt="'.esc_html( $item->get_title() ).'">'; ?>
				</a>
			</div>
			<?php endforeach; ?>
			
		</div>
		<a class="left carousel-control anuncio" href="#myCarousel" data-slide="prev" style="top:100px;">&nbsp;</span></a> 
			<a class="right carousel-control anuncio" href="#myCarousel" data-slide="next"style="top:100px;">&nbsp;</span></a> 
	</div>
	<?php endif ?>