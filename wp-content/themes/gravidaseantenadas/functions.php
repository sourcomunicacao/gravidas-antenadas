<?php 

function add_responsive_class($content){
    $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
    $document = new DOMDocument();
    libxml_use_internal_errors(true);
    $document->loadHTML(utf8_decode($content));
    $imgs = $document->getElementsByTagName('img');
    foreach ($imgs as $img) {
        $existing_class = $img->getAttribute('class');
        $img->setAttribute('class',"img-responsive $existing_class");
    }
    $html = $document->saveHTML();
    return $html;
}
add_filter('the_content', 'add_responsive_class');



function catch_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches[1][0];

  if(empty($first_img)) {
    $first_img = "/path/to/default.png";
  }
  return $first_img;
}



function single_social_assets() {
	?>
	<style>
		@media screen and (max-widt:767px){
			.single-social-link { color:#FFF;line-height:32px;padding-right:10px; margin: 2px auto; cursor: pointer;display:inline-block!important; }	
			.single-social-link:hover,.single-social-link:active { color: white; }
			.single-social-link-twitter { background: #00aced; }
			.single-social-link-twitter:hover,.single-social-link-twitter:active { background: #0084b4; }
			.single-social-link-facebook { background: #3B5997; }
			.single-social-link-facebook:hover,.single-social-link-facebook:active { background: #2d4372; }
			.single-social-link-googleplus { background: #D64937; }
			.single-social-link-googleplus:hover,.single-social-link-googleplus:active { background: #b53525; }
			.single-social-link-whatsup { background: #29A619; }
			.single-social-link-whatsup:hover,.single-social-link-whatsup:active { background: #62C04F; }
			.single-social-block { margin: 20px auto; -webkit-font-smoothing: antialiased; font-size: 14px; text-align: center; font-weight:bold;	}
		}
	</style>
	<!-- ========> Twitter script: <======== -->
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	<!-- ========> Facebook HTML5 script: + PopUP <========= -->
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
	<!-- ========> Google+ script: <======== -->
	<script type="text/javascript">
		(function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/platform.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();
	</script>
	<?php
};
add_action('wp_head','single_social_assets');



function single_social_links($content) {
	if( is_singular() ) {

		// Get current page URL
		$crunchifyURL = get_permalink();

		// Get current page title
		$crunchifyTitle = str_replace( ' ', '%20', get_the_title());

		// Get Post Thumbnail for pinterest
		$crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL;
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
		$googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
		$whatsURL = 'whatsapp://send?text='.$crunchifyTitle.' - '.$crunchifyURL;

		// Add sharing button at the end of page/page content
		$content .= '<div class="single-social-block">';
		//$content .= '<h5>Compartilhar </h5> <a class="single-social-link single-social-link-twitter" href="'. $twitterURL .'" target="_blank"><img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI1MTIiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB3aWR0aD0iNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIiB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZGVmcyBpZD0iZGVmczEyIi8+PGcgaWQ9ImcyOTk1Ij48cmVjdCBoZWlnaHQ9IjUxMiIgaWQ9InJlY3QyOTg3IiBzdHlsZT0iZmlsbDojMDBhYmYxO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIiB3aWR0aD0iNTEyIiB4PSIwIiB5PSI1LjY4NDM0MTllLTAxNCIvPjxwYXRoIGQ9Im0gMzU0Ljk0NDE1LDM1NS45ODE1MiBoIC05OC43MDIxIGMgLTEzLjcyMTcsMCAtMjUuMzkwOCwtNC44MDExIC0zNC45NzQyLC0xNC40NDI1IC05LjYyNzQsLTkuNjU0MSAtMTQuMzg5NCwtMjEuMzE4NCAtMTQuMzg5NCwtMzUuMTA5MSB2IC0zNS4xMjMyIGggMTQwLjYzNjggYyAxMi42ODc3LDAgMjMuNjA5NCwtNC41NjQ1MiAzMi42ODM2LC0xMy42MTk5MiA5LjA2OTQsLTkuMTI0MzUgMTMuNjI0NSwtMjAuMDM4MTUgMTMuNjI0NSwtMzIuNzM5ODUgMCwtMTIuNzQwODcgLTQuNTU1MSwtMjMuNjM5IC0xMy42NTU5LC0zMi43Mzk4NCAtOS4wOTkyLC05LjA3NDIxIC0yMC4wNTIzLC0xMy42MjQ2MyAtMzIuNzkzMSwtMTMuNjI0NjMgaCAtMTQwLjQ5NzUgdiAtNzIuNTYzODkgYyAwLC0xMy43MzQzMTcgLTQuODk1MSwtMjUuNDkxMTM3IC0xNC42MjQzLC0zNS4yODc2OTcgLTkuNjgwNiwtOS44MDc1NSAtMjEuMzkyLC0xNC43MzA5MSAtMzQuOTk3OCwtMTQuNzMwOTEgLTEzLjk4ODIsMCAtMjUuODkyMyw0Ljg0NjU5IC0zNS41NzQ0LDE0LjQ0MjU4IC05LjcxNjcsOS42MjczNCAtMTQuNTY5NiwyMS40ODkxNSAtMTQuNTY5NiwzNS42MTUxOTcgdiAyMDAuNDA1MDYgYyAwLDQxLjIyMDIgMTQuNTY5Niw3Ni40MzExIDQzLjcyMjYsMTA1LjY1NjMgMjkuMTc2NiwyOS4yOCA2NC4zNTE1LDQzLjg4MDkgMTA1LjQ2OTksNDMuODgwOSBoIDk4LjY3MjQgYyAxMy43MDI5LDAgMjUuNDYyOSwtNC45MjAzIDM1LjI0MDYsLTE0LjczMjUgOS43ODg5LC05Ljc2ODQgMTQuNjczLC0yMS41NDcyIDE0LjY3MywtMzUuMjY4OCAwLC0xMy43MjAzIC00Ljg4NDEsLTI1LjQ4NTEgLTE0LjY3MywtMzUuMjkyNSAtOS43NzkzLC05Ljc4NzIgLTIxLjU2MjcsLTE0LjcyNDcgLTM1LjI3MjEsLTE0LjcyNDcgeiIgaWQ9IlR3aXR0ZXJfM18iIHN0eWxlPSJmaWxsOiNmZmZmZmYiLz48L2c+PC9zdmc+" alt="" width="32" height="32"> Twitter</a>';
		//$content .= '<a class="single-social-link single-social-link-facebook" href="'.$facebookURL.'" onclick="window.open (\''.$facebookURL.'\',\'facepopup\',\'menubar=1,resizable=1,width=640,height=480\'); return false;"><img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI1MTIiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB3aWR0aD0iNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIiB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZGVmcyBpZD0iZGVmczEyIi8+PGcgaWQ9Imc1OTkxIj48cmVjdCBoZWlnaHQ9IjUxMiIgaWQ9InJlY3QyOTg3IiBzdHlsZT0iZmlsbDojM2I1OTk4O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIiB3aWR0aD0iNTEyIiB4PSIwIiB5PSIwIi8+PHBhdGggZD0iTSAyODYuOTY3ODMsNDU1Ljk5OTcyIFYgMjczLjUzNzUzIGggNjEuMjQ0IGwgOS4xNjk5LC03MS4xMDI2NiBoIC03MC40MTI0NiB2IC00NS4zOTQ5MyBjIDAsLTIwLjU4ODI4IDUuNzIwNjYsLTM0LjYxOTQyIDM1LjIzNDk2LC0zNC42MTk0MiBsIDM3LjY1NTQsLTAuMDExMiBWIDU4LjgwNzkxNSBjIC02LjUwOTcsLTAuODczODEgLTI4Ljg1NzEsLTIuODA3OTQgLTU0Ljg2NzUsLTIuODA3OTQgLTU0LjI4ODAzLDAgLTkxLjQ0OTk1LDMzLjE0NTg1IC05MS40NDk5NSw5My45OTgxMjUgdiA1Mi40MzcwOCBoIC02MS40MDE4MSB2IDcxLjEwMjY2IGggNjEuNDAwMzkgdiAxODIuNDYyMTkgaCA3My40MjcwNyB6IiBpZD0iZl8xXyIgc3R5bGU9ImZpbGw6I2ZmZmZmZiIvPjwvZz48L3N2Zz4=" alt="Facebook" width="32" height="32"> Facebook</a>';
		//$content .= '<a class="single-social-link single-social-link-googleplus" href="'.$googleURL.'" onclick="window.open (\''.$googleURL.'\',\'facepopup\',\'menubar=1,resizable=1,width=640,height=480\'); return false;"><img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI1MTIiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB3aWR0aD0iNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIiB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZGVmcyBpZD0iZGVmczEyIi8+PGcgaWQ9Imc2NjM2Ij48cmVjdCBoZWlnaHQ9IjUxMiIgaWQ9InJlY3QyOTg3IiBzdHlsZT0iZmlsbDojZDA0MjJhO2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIiB3aWR0aD0iNTEyIiB4PSIwIiB5PSIxLjcwNTMwMjZlLTAxMyIvPjxwYXRoIGQ9Im0gMzg3LjEwNjY5LDU1Ljk5OTk5OCBoIC0xMDguMjQ0NiBjIC0yOC4zNjg3OSwwIC02NC4wOTc2Nyw0LjE4NjY2IC05NC4wMzU0NCwyOC44NDQ0NSAtMjIuNjIyMjQsMTkuNDQwMDAyIC0zMy42MjY2Nyw0Ni4xODY2NTIgLTMzLjYyNjY3LDcwLjM0NjY3MiAwLDQwLjkzNzc2IDMxLjUyODksODIuNDI2NjUgODcuMjIyMjMsODIuNDI2NjUgNS4yNTMzMiwwIDExLjAwODg5LC0wLjUzMzMyIDE2LjgzMTEsLTEuMDQ0NDQgLTIuNjMxMTIsNi4yOCAtNS4yODQ0MywxMS41Mjg4OCAtNS4yODQ0MywyMC40NjY2OCAwLDE2LjI3OTk5IDguNDEzMzIsMjYuMjM1NTUgMTUuNzY0MzEsMzUuNjg4ODggLTIzLjYxNzY2LDEuNTY0NDUgLTY3Ljc2ODc2LDQuMTg2NjUgLTEwMC4zNjQzMiwyNC4xMjQ0NSAtMzEuMDAwMDEsMTguNDA4ODggLTQwLjQ3NTU2LDQ1LjE1NTU1IC00MC40NzU1Niw2NC4wNjIyMSBDIDEyNC44ODQzMSw0MTkuNzU5OTkgMTYxLjY3OTk5LDQ1NiAyMzcuODc5OTcsNDU2IGMgOTAuMzgyMTIsMCAxMzguMjEzNDIsLTQ5Ljg2NjY2IDEzOC4yMTM0MiwtOTkuMjQ0NDUgMCwtMzYuMTg2NjUgLTIxLjAyMjMsLTU0LjA2MjIxIC00NC4xNDI0LC03My40ODQ0NSBsIC0xOC45MjQzLC0xNC42ODg4OCBjIC01Ljc1NTcsLTQuNzMzMzMgLTEzLjYzNTUsLTExLjAxNzc5IC0xMy42MzU1LC0yMi41Nzc3OCAwLC0xMS41NDIyMSA3Ljg3OTgsLTE4LjkwNjY2IDE0LjY5NzYsLTI1LjcyODg5IDIyLjA1OCwtMTcuMzE1NTQgNDQuMTQyNCwtMzUuNjg4ODggNDQuMTQyNCwtNzQuNTMzMzMgMCwtMzkuOTA2NjcgLTI1LjI0NDUsLTYwLjkwNjY2MiAtMzcuMzE1NiwtNzAuODc1NTUyIGggMzIuNTk1NiBsIDMzLjU5NTUsLTE4Ljg2NjY3IHogbSAtNDYuMjA0NCwzMjEuMjM5OTkyIGMgMCwzMi41NDY2OCAtMjYuODEzNSw1Ni42OCAtNzcuMjY2OCw1Ni42OCAtNTYuMjEzMywwIC05Mi40ODQzOSwtMjYuNzczMzMgLTkyLjQ4NDM5LC02NC4wMjIyIDAsLTM3LjI5NzggMzMuNjM1NTQsLTQ5Ljg1NzggNDUuMTgyMjEsLTU0LjA2MjI0IDIyLjA4NDQ0LC03LjM1OTk5IDUwLjQ1MzM4LC04LjQwNDQzIDU1LjE3MzM4LC04LjQwNDQzIDUuMjYyMiwwIDcuODg5LDAgMTIuMTExMiwwLjUxNTUzIDM5Ljk0NjYsMjguMzQyMjUgNTcuMjg0NCw0Mi41Mjg5IDU3LjI4NDQsNjkuMjkzMzQgeiBtIC00Mi4wNDksLTE2OS4wMjIyMSBjIC04LjQxMzQsOC4zODIyMSAtMjIuNjEzMiwxNC42ODg4OSAtMzUuNzU1NSwxNC42ODg4OSAtNDUuMTczMzcsMCAtNjUuNjU3ODEsLTU4LjI4MDAxIC02NS42NTc4MSwtOTMuNDM1NTcgMCwtMTMuNjUzMzEgMi42MzExMiwtMjcuODEzMzEgMTEuNTQ2NjcsLTM4Ljg0NDQyMiA4LjQxMzMyLC0xMC40OTc4IDIzLjExNTU2LC0xNy4zMjg5IDM2Ljc4NjY1LC0xNy4zMjg5IDQzLjYwNDQ5LDAgNjYuMjIyMjksNTguNzk1NTQyIDY2LjIyMjI5LDk2LjU3Nzc2MiAwLDkuNDY2NjggLTEuMDcxMiwyNi4yNDg5MSAtMTMuMTQyMywzOC4zNDIyNCB6IiBpZD0iR29vZ2xlIiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtb3BhY2l0eToxIi8+PC9nPjwvc3ZnPg==" alt="Google+" width="32" height"32"> Google+</a>';
		$content .= '<a class="single-social-link single-social-link-whatsup visible-xs" href="'.$whatsURL.'" target="_blank"><img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSI1MTIiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiB3aWR0aD0iNTEyIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6aW5rc2NhcGU9Imh0dHA6Ly93d3cuaW5rc2NhcGUub3JnL25hbWVzcGFjZXMvaW5rc2NhcGUiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIiB4bWxuczpzdmc9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48ZGVmcyBpZD0iZGVmczEyIi8+PGcgaWQ9Imc1MTI0Ij48cmVjdCBoZWlnaHQ9IjUxMiIgaWQ9InJlY3QyOTg3IiBzdHlsZT0iZmlsbDojNjViYzU0O2ZpbGwtb3BhY2l0eToxO2ZpbGwtcnVsZTpub256ZXJvO3N0cm9rZTpub25lIiB3aWR0aD0iNTEyIiB4PSIwIiB5PSIwIi8+PHBhdGggZD0ibSA0NTYsMjUwLjg1MjY2IGMgMCwxMDcuNjA5MDQgLTg3LjkxMjYsMTk0Ljg0NDIgLTE5Ni4zNjM5NywxOTQuODQ0MiAtMzQuNDMwNjYsMCAtNjYuNzc2NzcsLTguODAxNjggLTk0LjkxOTksLTI0LjI0MTYyIEwgNTYuMDAwMDA1LDQ1NiA5MS40Mzc3NDUsMzUxLjQ1NTg0IEMgNzMuNTU5NzE1LDMyMi4wODg3MiA2My4yNjUwMjUsMjg3LjY1NTIzIDYzLjI2NTAyNSwyNTAuODUxMjQgNjMuMjY1MDI1LDE0My4yMzUxNiAxNTEuMTgwNDksNTYgMjU5LjYzNDYzLDU2IDM2OC4wODc0LDU2LjAwMSA0NTYsMTQzLjIzNjU3IDQ1NiwyNTAuODUyNjYgeiBNIDI1OS42MzYwMyw4Ny4wMzE5NiBjIC05MS4wNDA5MiwwIC0xNjUuMDkzOTY1LDczLjQ5MjQ4IC0xNjUuMDkzOTY1LDE2My44MjA3IDAsMzUuODQwNTYgMTEuNjgzNDY1LDY5LjA0MTYyIDMxLjQ0NjA1NSw5Ni4wNDUyOSBsIC0yMC42MjE3Nyw2MC44MzE1MSA2My40NDI4NSwtMjAuMTY0MDMgYyAyNi4wNzEyNiwxNy4xMTMyMyA1Ny4yOTE5NiwyNy4wOTgwNSA5MC44MjU0MywyNy4wOTgwNSA5MS4wMjk2NSwwIDE2NS4wOTM5NiwtNzMuNDg1NDMgMTY1LjA5Mzk2LC0xNjMuODEyMjQgMCwtOTAuMzI2OCAtNzQuMDYyOTIsLTE2My44MTkyOCAtMTY1LjA5MjU2LC0xNjMuODE5MjggeiBtIDk5LjE1NTI2LDIwOC42ODk3MiBjIC0xLjIwOTg5LC0xLjk4ODc5IC00LjQxODUsLTMuMTg2MDIgLTkuMjI0MjQsLTUuNTcwNiAtNC44MTcwNSwtMi4zODc0IC0yOC40ODk2NCwtMTMuOTQ1NTEgLTMyLjg5NCwtMTUuNTM0MjkgLTQuNDE4NDUsLTEuNTkzMDEgLTcuNjMxMjIsLTIuMzkzMDQgLTEwLjgzODM4LDIuMzg0NTggLTMuMjA0MzIsNC43OTAyOCAtMTIuNDI4NTYsMTUuNTM0MjkgLTE1LjI0MjczLDE4LjcyMDMxIC0yLjgwODUzLDMuMTkxNjYgLTUuNjA4NjMsMy41OTAyNiAtMTAuNDI1NjksMS4yMDAwMyAtNC44MDU3OCwtMi4zODczOSAtMjAuMzIxNzcsLTcuNDI4NCAtMzguNzA4MjYsLTIzLjcwMjE1IC0xNC4zMDc0OSwtMTIuNjU4MTUgLTIzLjk2OTc4LC0yOC4yODU0IC0yNi43NzgzMSwtMzMuMDcxNDcgLTIuODA4NTQsLTQuNzc5MDMgLTAuMjk3MiwtNy4zNjIyIDIuMTA5OTMsLTkuNzM5NzUgMi4xNjYyNiwtMi4xNDc5NiA0LjgxNDIzLC01LjU4MTg2IDcuMjI0MTYsLTguMzYzNjQgMi40MDcxMiwtMi43OTQ0NyAzLjIwNzE1LC00Ljc4MTg0IDQuODA4NjEsLTcuOTY5MjYgMS42MTI3MiwtMy4xODg4NCAwLjgwMDAyLC01Ljk3NDg1IC0wLjM5ODYsLTguMzcwNyAtMS4yMDI4NiwtMi4zODMxNyAtMTAuODMyNzQsLTI1Ljg4OTU1IC0xNC44NDQxNSwtMzUuNDQ5IC00LjAxMTM4LC05LjU1OTQ3IC04LjAxMTUsLTcuOTY2NDYgLTEwLjgyNTY4LC03Ljk2NjQ2IC0yLjgwOTk2LDAgLTYuMDE1NjksLTAuNDAwMDIgLTkuMjI5ODcsLTAuNDAwMDIgLTMuMjA5OTcsMCAtOC40MjcwMywxLjE5ODY0IC0xMi44MzU2Miw1Ljk3MzQ0IC00LjQxMDAxLDQuNzgzMjUgLTE2Ljg0MTM4LDE2LjMzMjkxIC0xNi44NDEzOCwzOS44MzM2NSAwLDIzLjUwNDk3IDE3LjI0Mjc5LDQ2LjIxMTMzIDE5LjY1MjczLDQ5LjM5NTk0IDIuNDA0MzEsMy4xNzc1NiAzMy4yODgzOCw1Mi45NzIxIDgyLjIxODExLDcyLjEwMjI4IDQ4Ljk0ODAyLDE5LjExMzI4IDQ4Ljk0ODAyLDEyLjc0NDA3IDU3Ljc3MzY1LDExLjkzNyA4LjgxNDM3LC0wLjc4NzM1IDI4LjQ2OTkyLC0xMS41NDQwMyAzMi40ODgzMiwtMjIuNzAwNzIgNC4wMDg2LC0xMS4xNDk2NCA0LjAwODYsLTIwLjcxODk2IDIuODExNCwtMjIuNzA5MTcgeiIgaWQ9IldoYXRzQXBwXzJfIiBzdHlsZT0iZmlsbDojZmZmZmZmO2ZpbGwtcnVsZTpldmVub2RkIi8+PC9nPjwvc3ZnPg==" alt="WhatsApp" height="32" width="32"> WhatsApp</a>';
		$content .= '</div>';

		return $content;
	}else{
		// if not a post/page then don't include sharing button
		return $content;
	}
};
add_filter( 'the_content', 'single_social_links', 10);






// custom functions.php template @ digwp.com
add_filter( 'wpcf7_load_css', '__return_false' );

// add feed links to header
if (function_exists('automatic_feed_links')) {
	automatic_feed_links();
} else {
	return;
}
		
//Gets post cat slug and looks for single-[cat slug].php and applies it
add_filter('single_template', create_function(
	'$the_template',
	'foreach( (array) get_the_category() as $cat ) {
		if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
		return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
	return $the_template;' )
);

function new_subcategory_hierarchy() { 
    $category = get_queried_object();

    $parent_id = $category->category_parent;

    $templates = array();

    if ( $parent_id == 0 ) {
        // Use default values from get_category_template()
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";
        $templates[] = 'category.php';     
    } else {
        // Create replacement $templates array
        $parent = get_category( $parent_id );

        // Current first
        $templates[] = "category-{$category->slug}.php";
        $templates[] = "category-{$category->term_id}.php";

        // Parent second
        $templates[] = "category-{$parent->slug}.php";
        $templates[] = "category-{$parent->term_id}.php";
        $templates[] = 'category.php'; 
    }
    return locate_template( $templates );
}

add_filter( 'category_template', 'new_subcategory_hierarchy' );

// smart jquery inclusion
if (!is_admin()) {
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '1.3.2');
	wp_enqueue_script('jquery');
}


// enable threaded comments
function enable_threaded_comments(){
	if (!is_admin()) {
		if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1))
			wp_enqueue_script('comment-reply');
		}
}
add_action('get_header', 'enable_threaded_comments');


// remove junk from head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);


// custom excerpt length
function custom_excerpt_length($length) {
	return 17;
}
add_filter('excerpt_length', 'custom_excerpt_length');


// custom excerpt ellipses for 2.9+
function custom_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'custom_excerpt_more');


// kill the admin nag
if (!current_user_can('edit_users')) {
	add_action('init', create_function('$a', "remove_action('init', 'wp_version_check');"), 2);
	add_filter('pre_option_update_core', create_function('$a', "return null;"));
}


// category id in body and post class
function category_id_class($classes) {
	global $post;
	foreach((get_the_category($post->ID)) as $category)
		$classes [] = 'cat-' . $category->cat_ID . '-id';
		return $classes;
}
add_filter('post_class', 'category_id_class');
add_filter('body_class', 'category_id_class');


// get the first category id
function get_first_category_ID() {
	$category = get_the_category();
	return $category[0]->cat_ID;
}

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 400, 240, true );
add_image_size( 'inspiracao_thumb', '380', '530', true );
add_image_size( 'papoantenado_thumb', '825', '402', true );

/** Pagination */
function pagination_funtion() {
// Get total number of pages
global $wp_query;
$total = $wp_query->max_num_pages;
// Only paginate if we have more than one page                   
if ( $total > 1 )  {
    // Get the current page
    if ( !$current_page = get_query_var('paged') )
        $current_page = 1;
                           
        $big = 999999999;
        // Structure of "format" depends on whether we’re using pretty permalinks
        $permalink_structure = get_option('permalink_structure');
        $format = empty( $permalink_structure ) ? '&page=%#%' : 'page/%#%/';
        echo paginate_links(array(
            'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
            'format' => $format,
            'current' => $current_page,
            'total' => $total,
            'mid_size' => 2,
            'type' => 'list'
        ));
    }
}
/** END Pagination */




function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    'base'            => get_pagenum_link(1) . '%_%',
    'format'          => 'page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination col-xs-12 text-center'>";
      echo "<span class='page-numbers page-num'>Página " . $paged . " de " . $numpages . "</span> ";
      echo $paginate_links;
    echo "</nav>";
  }

}







/** AUTHORS **/

function get_authors() { 
	global $wpdb;
	$order = 'user_nicename';
	$user_ids = $wpdb->get_col("SELECT ID FROM $wpdb->users ORDER BY $order"); 
	foreach($user_ids as $user_id) : 
		$user = get_userdata($user_id); 
		$all_authors[$user_id] = $user->display_name; 
	endforeach; 
	return $all_authors;
}

add_action('wp_head','hook_css');
function hook_css() {
	$output='<style>ul[class^="huge_it"] li[id^="image_id"]{
    overflow: visible!important;
}

ul[class^="huge_it"] li[id^="image_id"] div[class^="huge_it_slideshow_title"]{
    position: relative!important;
    width: 100%!important;
    min-height:80px;
    background: #fff!important;
    color:#303030!important;
    font-size: 26px!important;
    font-family: "FuturaBold", sans-serif!important;
    letter-spacing: 1px!important;
    text-transform: uppercase!important;
    top:0;bottom:0;left:0;right:0;
    padding:20px;
    text-align:left!important;
}
ul[class^="huge_it"] li[id^="image_id"] div[class^="huge_it_slideshow_description"]{
    position: relative!important;
    width: 100%!important;
    min-height:78px;
    background: #fff!important;
    color:#303030!important;
    font-size: 13px!important;
    top:0;bottom:0;left:0;right:0;
    padding:20px;
    padding-top:0;
}
</style>';
	echo $output;
}





add_filter('get_wp_user_avatar','change_avatar_css');
function change_avatar_css($class) {
    $class = str_replace('class="avatar', 'class="avatar photo img-responsive ', $class) ;
    return $class;
}




//Adiciona campos extras a tela de edição/inserção de usuários (by wptotal.com.br)
add_action('show_user_profile', 'add_extra_fields_user_profile');
add_action('edit_user_profile', 'add_extra_fields_user_profile');
function add_extra_fields_user_profile($user) {
?>
<table class="form-table">
    <tr>
        <th><label for="telefone">Instagram</label></th>
        <td><input type="text" name="instagram" id="instagram" value="<?php echo esc_attr(get_the_author_meta('instagram', $user->ID)); ?>" class="regular-text" /></td>
    </tr>
</table>
<?php
}
//Gravando campos extras inseridos no banco de dados (by wptotal.com.br)
add_action('personal_options_update', 'extra_fields_to_user_save');
add_action('edit_user_profile_update', 'extra_fields_to_user_save');
function extra_fields_to_user_save($user_id)
{
if (!current_user_can('edit_user', $user_id))
    return false;
    update_usermeta($user_id, 'instagram', $_POST['instagram']);
};