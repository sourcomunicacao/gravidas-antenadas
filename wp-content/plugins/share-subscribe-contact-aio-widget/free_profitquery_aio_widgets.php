<?php
/* 
* +--------------------------------------------------------------------------+
* | Copyright (c) ShemOtechnik Profitquery Team shemotechnik@profitquery.com |
* +--------------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify     |
* | it under the terms of the GNU General Public License as published by     |
* | the Free Software Foundation; either version 2 of the License, or        |
* | (at your option) any later version.                                      |
* |                                                                          |
* | This program is distributed in the hope that it will be useful,          |
* | but WITHOUT ANY WARRANTY; without even the implied warranty of           |
* | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            |
* | GNU General Public License for more details.                             |
* |                                                                          |
* | You should have received a copy of the GNU General Public License        |
* | along with this program; if not, write to the Free Software              |
* | Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA |
* +--------------------------------------------------------------------------+
*/
/**
* Plugin Name: 20 free growth tools in one WordPress plugin
* Plugin URI: http://profitquery.com/aio_widgets.html
* Description: 20 free growth tools in one WordPress plugin for websites by Profitquery. Growth traffic, email, subscription, feedback, phone number, shares, referral's, followers, etc.
* Version: 4.1.11
*
* Author: Profitquery Team <support@profitquery.com>
* Author URI: http://profitquery.com/?utm_campaign=aio_widgets_wp
*/

//update_option('profitquery', array());
$profitquery = get_option('profitquery');
error_reporting(0);



if (!defined('PROFITQUERY_SMART_WIDGETS_PLUGIN_NAME'))
	define('PROFITQUERY_SMART_WIDGETS_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)), '/'));

if (!defined('PROFITQUERY_SMART_WIDGETS_PAGE_NAME'))
	define('PROFITQUERY_SMART_WIDGETS_PAGE_NAME', 'free_profitquery_aio_widgets');

if (!defined('PROFITQUERY_SMART_WIDGETS_ADMIN_CSS_PATH'))
	define('PROFITQUERY_SMART_WIDGETS_ADMIN_CSS_PATH', 'css/');

if (!defined('PROFITQUERY_SMART_WIDGETS_ADMIN_JS_PATH'))
	define('PROFITQUERY_SMART_WIDGETS_ADMIN_JS_PATH', 'js/');

if (!defined('PROFITQUERY_SMART_WIDGETS_ADMIN_IMG_PATH'))
	define('PROFITQUERY_SMART_WIDGETS_ADMIN_IMG_PATH', 'images/');

if (!defined('PROFITQUERY_SMART_WIDGETS_ADMIN_IMG_PREVIEW_PATH'))
	define('PROFITQUERY_SMART_WIDGETS_ADMIN_IMG_PREVIEW_PATH', 'preview/');

$pathParts = pathinfo(__FILE__);
$path = $pathParts['dirname'];

if (!defined('PROFITQUERY_SMART_WIDGETS_FILENAME'))
	define('PROFITQUERY_SMART_WIDGETS_FILENAME', $path.'/free_profitquery_aio_widgets.php');


require_once 'free_profitquery_aio_widgets_class.php';
$ProfitQuerySmartWidgetsClass = new ProfitQuerySmartWidgetsClass();


add_action('init', 'profitquery_smart_widgets_init');


function profitquery_smart_widgets_init(){
	global $profitquery;
	global $ProfitQuerySmartWidgetsClass;	
	if ( !is_admin()){		
		add_action('wp_head', 'profitquery_smart_widgets_insert_code');		
	}		
}


function printr($array){
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

/* Adding action links on plugin list*/
function profitquery_wordpress_admin_link($links, $file) {
    static $this_plugin;

    if (!$this_plugin) {
        $this_plugin = plugin_basename(__FILE__);
    }

    if ($file == $this_plugin) {
        $settings_link = '<a href="options-general.php?page=free_profitquery_aio_widgets">Settings</a>';
        array_unshift($links, $settings_link);
    }

    return $links;
}

function getThankCodeStructure($data){
	$return = array();
	//enable
	if((string)$data[enable] == 'on' || (int)$data[enable] == 1) $return[enable] = 1; else $return[enable] = 0;
	//animation
	if($data[animation]) $return[animation] = 'pq_animated '.$data[animation];	
	
	//closeIconOption
	$return[closeIconOption][animation] = stripslashes($data[close_icon][animation]);
	$return[closeIconOption][button_text] = htmlspecialchars(stripslashes($data[close_icon][button_text]));
	
	
	//typeWindow
	$return[typeWindow] = $data[typeWindow].' '.$return[animation].' '.$data[popup_form].' '.$data[background].' '.$data[second_background].' '.$data[head_font];
	$return[typeWindow] .= ' '.$data[head_size].' '.$data[head_color].' '.$data[text_font].' '.$data[font_size].' '.$data[text_color];    
	$return[typeWindow] .= ' '.$data[border_color].' '.$data[button_font].' '.$data[button_text_color].' '.$data[button_color].' '.$return[button_font_size];
	$return[typeWindow] .= ' '.$data[close_icon][position].' '.$data[close_icon][form].' '.$data[close_icon][size].' '.$data[close_icon][color].' '.$data[close_icon][background].' '.$data[close_icon][fill_type];
		
	
	
	//img
	$return[header_image_src] = stripslashes($data[header_image_src]);	
	$return[background_image_src] = stripslashes($data[background_image_src]);	
	
	
	//txt
	$return[title] = htmlspecialchars(stripslashes($data[title]));
	$return[sub_title] = htmlspecialchars(stripslashes($data[sub_title]));	
	
	if($data[socnet_block_type] == 'follow' || $data[socnet_block_type] == 'share'){		
		$return[typeWindow] .= ' '.$data[icon][design].' '.$data[icon][form].' '.$data[icon][size].' '.$data[icon][space];
		$return[socnetBlockType] = $data[socnet_block_type];
		$return[hoverAnimation] = $data[icon][animation];		
		
		if($data[socnet_block_type] == 'share'){
			foreach((array)$data[socnet_with_pos] as $k => $v){
				if($v){
					$return[socnetShareBlock][$v][exactPageShare] = 1;
				}
			}
		}
		if($data[socnet_block_type] == 'follow'){
			foreach((array)$data[socnetIconsBlock] as $k => $v){
				if($k && $v){
					if($k == 'FB') $v = 'https://facebook.com/'.$v;
					if($k == 'TW') $v = 'https://twitter.com/'.$v;
					if($k == 'GP') $v = 'https://plus.google.com/'.$v;
					if($k == 'PI') $v = 'https://pinterest.com/'.$v;
					if($k == 'VK') $v = 'http://vk.com/'.$v;
					if($k == 'YT') $v = 'https://www.youtube.com/channel/'.$v;
					if($k == 'RSS') $v = $v;				
					if($k == 'IG') $v = 'http://instagram.com/'.$v;				
					if($k == 'OD') $v = 'http://ok.ru/'.$v;
					
					$return[socnetFollowBlock][$k] = stripslashes($v);
				}
			}
		}
	}
	
	if($data[buttonBlock][type] == 'redirect'){
		$return[buttonActionBlock][type] = 'redirect';
		$return[buttonActionBlock][redirect_url] = stripslashes($data[buttonBlock][url]);
		$return[buttonActionBlock][button_text] = htmlspecialchars(stripslashes($data[buttonBlock][button_text]));
	}	
	
	
	if($data[overlay]){
		$return[blackoutOption][enable] = 1;
		$return[blackoutOption][style] = $data[overlay];
	}
	
	return $return;
}


//Prepare output sctructure
function profitquery_prepare_sctructure_product($data, $bookmark = 0){	
	$return = $data;
	
	//enable
	if((string)$data[enable] == 'on' || (int)$data[enable] == 1) $return[enable] = 1; else $return[enable] = 0;
	
	//hoverAnimation
	$return[hoverAnimation] = $return[icon][animation];
	
	//closeIconOption
	$return[closeIconOption][animation] = stripslashes($data[close_icon][animation]);
	$return[closeIconOption][button_text] = htmlspecialchars(stripslashes($data[close_icon][button_text]));
/************************************************GENERATE TYPE WINDOW********************************************************************************/
	//position
	if($data[position] == 'BAR_TOP') $return[position] = 'pq_top';
	if($data[position] == 'BAR_BOTTOM') $return[position] = 'pq_bottom';
	if($data[position] == 'SIDE_LEFT_TOP') $return[position] = 'pq_left pq_top';
	if($data[position] == 'SIDE_LEFT_MIDDLE') $return[position] = 'pq_left pq_middle';
	if($data[position] == 'SIDE_LEFT_BOTTOM') $return[position] = 'pq_left pq_bottom';
	if($data[position] == 'SIDE_RIGHT_TOP') $return[position] = 'pq_right pq_top';
	if($data[position] == 'SIDE_RIGHT_MIDDLE') $return[position] = 'pq_right pq_middle';
	if($data[position] == 'SIDE_RIGHT_BOTTOM') $return[position] = 'pq_right pq_bottom';
	if($data[position] == 'CENTER') $return[position] = '';
	if($data[position] == 'FLOATING_LEFT_TOP') $return[position] = 'pq_fixed pq_left pq_top';
	if($data[position] == 'FLOATING_LEFT_BOTTOM') $return[position] = 'pq_fixed pq_left pq_bottom';
	if($data[position] == 'FLOATING_RIGHT_TOP') $return[position] = 'pq_fixed pq_right pq_top';
	if($data[position] == 'FLOATING_RIGHT_BOTTOM') $return[position] = 'pq_fixed pq_right pq_bottom';
	
	//animation
	if($data[animation]) $return[animation] = 'pq_animated '.$data[animation];	
	
	if((int)$bookmark == 1){
		$return[typeWindow] = $return[typeWindow];
		$return[typeBookmarkWindow] = $return[position].' '.$return[bookmark_background].' '.$return[bookmark_text_color];		
	}else{
		$return[typeWindow] = $return[typeWindow].' '.$return[position];
	}
	
	$return[typeWindow] .=' '.$return[themeClass].' '.$return[icon][design].' '.$return[icon][form].' '.$return[icon][size].' '.$return[icon][space].' '.$return[icon][shadow];
	$return[typeWindow] .=' '.$return[animation].' '.$return[mobile_type].' '.$return[mobile_position];
	
	$return[typeWindow] .=' '.$return[background].' '.$return[second_background].' '.$return[text_font].' '.$return[font_size].' '.$return[text_color].' '.$return[button_font];
	$return[typeWindow] .=' '.$return[button_text_color].' '.$return[button_color].' '.$return[button_font_size];	
	$return[typeWindow] .=' '.$return[border_color].' '.$return[popup_form].' '.$return[head_font];
	$return[typeWindow] .=' '.$return[head_size].' '.$return[head_color];
	$return[typeWindow] .=' '.$return[close_icon][position].' '.$return[close_icon][form].' '.$return[close_icon][size].' '.$return[close_icon][color].' '.$return[close_icon][background].' '.$return[close_icon][fill_type];
	
	$return[typeWindow] = str_replace('  ', ' ',$return[typeWindow]);
	$return[typeWindow] = str_replace('  ', ' ',$return[typeWindow]);
	
	//type Design
	$return[typeDesign] = $return[themeClass].' '.$return[icon][form].' '.$return[icon][size].' '.$return[icon][space].' '.$return[icon][shadow].' '.$return[icon][position];
/*************************************************************************************************************************************************/	

	if($data[displayRules]){
		unset($return[displayRules]);
		foreach((array)$data[displayRules][pageMask] as $k => $v){
			if($v){
				if($data[displayRules][pageMaskType][$k] == 'enable'){
					$return[displayRules][enableOnPage][] = $v;
				}else{
					$return[displayRules][disableOnPage][] = $v;
				}
			}
		}
		$return[displayRules][display_on_main_page] = ($data[displayRules][display_on_main_page] == 'on')?1:0;
		$return[displayRules][work_on_mobile] = ($data[displayRules][work_on_mobile] == 'on')?1:0;		
		$return[displayRules][allowedExtensions] = $data[displayRules][allowedExtensions];
		$return[displayRules][allowedImageAddress] = $data[displayRules][allowedImageAddress];
		
	}	
	$return[thank] = getThankCodeStructure($data[thank]);
	

	if($data[overlay]){
		$return[blackoutOption][enable] = 1;
		$return[blackoutOption][style] = $data[overlay];
	}
	
	if($data[url]){
		$return[buttonBlock][action] = 'redirect';
		$return[buttonBlock][redirect_url] = $data[url];
		$return[buttonBlock][button_text] = $data[button_text];
	}
	
	//galerryOptions
	if((string)$data[galleryOption][enable] == 'on' || (int)$data[galleryOption][enable] == 1){
		unset($return[galleryOption]);
		$return[galleryOption][enable] = 1;
		$return[galleryOption][typeWindow] = $data[galleryOption][head_font].' '.$data[galleryOption][head_size].' '.$data[galleryOption][button_font].' '.$data[galleryOption][button_text_color].' '.$data[galleryOption][button_color].' '.$data[galleryOption][background_color];
		$return[galleryOption][title] = htmlspecialchars(stripslashes($data[galleryOption][title]));
		$return[galleryOption][button_text] = htmlspecialchars(stripslashes($data[galleryOption][button_text]));		
		$return[galleryOption][minWidth] = (int)$data[galleryOption][minWidth];		
	}
	
	//Image Sharer socnet
	if(isset($data[socnet])){
		unset($return[socnet]);
		foreach((array)$data[socnet] as $k => $v){
			if($v){
				$return[socnet][$k] = $data[socnetOption][$k];
				if($data[socnetOption][$k][type] == 'pq'){
					$return[socnet][$k][exactPageShare] = 0;
				} else {
					$return[socnet][$k][exactPageShare] = 1;
				}
			}
		}
		
	}
		
	//socnet_with_pos
	if(isset($data[socnet_with_pos])){
		unset($return[socnet]);
		foreach((array)$data[socnet_with_pos] as $k => $v){
			if($v){
				$return[shareSocnet][$v] = $data[socnetOption][$v];
				if($data[socnetOption][$v][type] == 'pq'){
					$return[shareSocnet][$v][exactPageShare] = 0;
				} else {
					$return[shareSocnet][$v][exactPageShare] = 1;
				}
			}
		}
		
	}
	
	
	//followSocnet
	if(isset($data[socnetIconsBlock])){
		foreach((array)$data[socnetIconsBlock] as $k => $v){
			if($k && $v){
				if($k == 'FB') $v = 'https://facebook.com/'.$v;
				if($k == 'TW') $v = 'https://twitter.com/'.$v;
				if($k == 'GP') $v = 'https://plus.google.com/'.$v;
				if($k == 'PI') $v = 'https://pinterest.com/'.$v;
				if($k == 'VK') $v = 'http://vk.com/'.$v;
				if($k == 'YT') $v = 'https://www.youtube.com/channel/'.$v;
				if($k == 'RSS') $v = $v;				
				if($k == 'IG') $v = 'http://instagram.com/'.$v;				
				if($k == 'OD') $v = 'http://ok.ru/'.$v;
				
				$return[socnetFollowBlock][$k] = stripslashes($v);
			}
		}
	}	
	
	return $return;
}
function profitquery_smart_widgets_insert_code(){
	global $profitquery;		
	$profitquerySmartWidgetsStructure = array();
	//sharingSidebar
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[sharingSidebar], 0);
	$sendMailWindow = profitquery_prepare_sctructure_product($profitquery[sharingSidebar][sendMailWindow], 0);
	$moreShareWindow = profitquery_prepare_sctructure_product($profitquery[sharingSidebar][moreShareWindow], 0);
	$moreShareWindow[typeWindow] = $preparedObject[themeClass].' '.$moreShareWindow[typeWindow];
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	//printr($profitquery[sharingSidebar]);
	//printr($preparedObject);
	
	//printr($sendMailWindow);	
	$profitquerySmartWidgetsStructure['sharingSideBarOptions'] = array(
		'typeWindow'=>'pq_icons '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],
		'socnetIconsBlock'=>$preparedObject[shareSocnet],
		'withCounters'=>$preparedObject[withCounter],
		'mobile_title'=>htmlspecialchars(stripslashes($preparedObject[mobile_title])),		
		'hoverAnimation'=>stripslashes($preparedObject[hoverAnimation]),		
		'eventHandler'=>$preparedObject[eventHandler],
		'galleryOption'=>$preparedObject[galleryOption],
		'displayRules'=>$preparedObject[displayRules],	
		'sendMailWindow'=>$sendMailWindow,	
		'moreShareWindow'=>$moreShareWindow,	
		'thankPopup'=>$preparedObject[thank]
	);	
	//imageSharer
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[imageSharer], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];
	$sendMailWindow = profitquery_prepare_sctructure_product($profitquery[imageSharer][sendMailWindow], 0);
	$profitquerySmartWidgetsStructure['imageSharer'] = array(
		'typeDesign'=>$preparedObject[typeDesign],		
		'enable'=>(int)$preparedObject[enable],
		'hoverAnimation'=>stripslashes($preparedObject[hoverAnimation]),
		'minWidth'=>(int)$preparedObject[minWidth],		
		'minHeight'=>$proOptions[minHeight],
		'activeSocnet'=>$preparedObject[socnet],
		'sendMailWindow'=>$sendMailWindow,
		'displayRules'=>$preparedObject[displayRules],	
		'thankPopup'=>$preparedObject[thank]
	);
	
	//emailListBuilderBar
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[emailListBuilderBar], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$profitquerySmartWidgetsStructure['subscribeBarOptions'] = array(
		'typeWindow'=>'pq_bar '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),
		'mobile_title'=>stripslashes($preparedObject[mobile_title]),
		'closeIconOption'=>$preparedObject[closeIconOption],
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'enter_email_text'=>stripslashes($preparedObject[enter_email_text]),
		'enter_name_text'=>stripslashes($preparedObject[enter_name_text]),
		'button_text'=>stripslashes($preparedObject[button_text]),		
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],
		'subscribeProvider'=>stripslashes($preparedObject[provider]),
		'subscribeProviderOption'=>$preparedObject[providerOption],
		'thankPopup'=>$preparedObject[thank]
	);
	
	//emailListBuilderPopup
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[emailListBuilderPopup], 0);		
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$profitquerySmartWidgetsStructure['subscribePopupOptions'] = array(
		'typeWindow'=>$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'enter_email_text'=>stripslashes($preparedObject[enter_email_text]),
		'enter_name_text'=>stripslashes($preparedObject[enter_name_text]),		
		'button_text'=>stripslashes($preparedObject[button_text]),
		'blackoutOption'=>$preparedObject[blackoutOption],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],
		'subscribeProvider'=>stripslashes($preparedObject[provider]),
		'subscribeProviderOption'=>$preparedObject[providerOption],
		'thankPopup'=>$preparedObject[thank]
	);
	
	//emailListBuilderFloating
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[emailListBuilderFloating], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$profitquerySmartWidgetsStructure['subscribeFloatingOptions'] = array(
		'typeWindow'=>'pq_fixed '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'enter_email_text'=>stripslashes($preparedObject[enter_email_text]),
		'enter_name_text'=>stripslashes($preparedObject[enter_name_text]),		
		'button_text'=>stripslashes($preparedObject[button_text]),		
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],
		'subscribeProvider'=>stripslashes($preparedObject[provider]),
		'subscribeProviderOption'=>$preparedObject[providerOption],
		'thankPopup'=>$preparedObject[thank]
	);
	
	//sharingPopup
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[sharingPopup], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];		
	$profitquerySmartWidgetsStructure['sharingPopup'] = array(
		'typeWindow'=>$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'shareSocnet'=>$preparedObject[shareSocnet],
		'blackoutOption'=>$preparedObject[blackoutOption],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);	
	
	//sharingBar
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[sharingBar], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];		
	$profitquerySmartWidgetsStructure['sharingBar'] = array(
		'typeWindow'=>'pq_bar '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],
		'mobile_title'=>htmlspecialchars(stripslashes($preparedObject[mobile_title])),	
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'shareSocnet'=>$preparedObject[shareSocnet],		
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//sharingFloating
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[sharingFloating], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$profitquerySmartWidgetsStructure['sharingFloating'] = array(
		'typeWindow'=>'pq_fixed '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'shareSocnet'=>$preparedObject[shareSocnet],
		'blackoutOption'=>$preparedObject[blackoutOption],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//promotePopup
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[promotePopup], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$profitquerySmartWidgetsStructure['promotePopup'] = array(
		'typeWindow'=>$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],		
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//promoteBar
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[promoteBar], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];		
	$profitquerySmartWidgetsStructure['promoteBar'] = array(
		'typeWindow'=>'pq_bar '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],		
		'mobile_title'=>htmlspecialchars(stripslashes($preparedObject[mobile_title])),	
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],		
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//promoteFloating
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[promoteFloating], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$profitquerySmartWidgetsStructure['promoteFloating'] = array(
		'typeWindow'=>'pq_fixed '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],		
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	
	//followPopup
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[followPopup], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];		
	$profitquerySmartWidgetsStructure['followPopup'] = array(
		'typeWindow'=>$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],		
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],
		'followSocnet'=>$preparedObject[socnetFollowBlock],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//followBar
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[followBar], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];		
	$profitquerySmartWidgetsStructure['followBar'] = array(
		'typeWindow'=>'pq_bar '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],		
		'mobile_title'=>htmlspecialchars(stripslashes($preparedObject[mobile_title])),	
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],		
		'followSocnet'=>$preparedObject[socnetFollowBlock],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//followFloating
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[followFloating], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];		
	$profitquerySmartWidgetsStructure['followFloating'] = array(
		'typeWindow'=>'pq_fixed '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],		
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],
		'followSocnet'=>$preparedObject[socnetFollowBlock],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//callMePopup (bookmark)
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[callMePopup], 1);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$preparedObject[to_email] = $profitquery[settings][email];		
	$profitquerySmartWidgetsStructure['callMePopup'] = array(
		'typeBookmarkWindow'=>'pq_stick pq_call '.$preparedObject[typeBookmarkWindow],
		'typeWindow'=>$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'to_email'=>stripslashes($preparedObject[to_email]),
		'button_text'=>stripslashes($preparedObject[button_text]),		
		'enter_name_text'=>stripslashes($preparedObject[enter_name_text]),		
		'enter_phone_text'=>stripslashes($preparedObject[enter_phone_text]),		
		'mail_subject'=>stripslashes($preparedObject[mail_subject]),		
		'bookmark_text'=>stripslashes($preparedObject[loader_text]),		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),				
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],		
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);	
	
	//callMeFloating
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[callMeFloating], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$preparedObject[to_email] = $profitquery[settings][email];		
	$profitquerySmartWidgetsStructure['callMeFloating'] = array(		
		'typeWindow'=>'pq_fixed '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'to_email'=>stripslashes($preparedObject[to_email]),
		'button_text'=>stripslashes($preparedObject[button_text]),		
		'enter_name_text'=>stripslashes($preparedObject[enter_name_text]),		
		'enter_phone_text'=>stripslashes($preparedObject[enter_phone_text]),		
		'mail_subject'=>stripslashes($preparedObject[mail_subject]),		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),				
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],		
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//contactUsPopup (bookmark)
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[contactUsPopup], 1);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$preparedObject[to_email] = $profitquery[settings][email];		
	$profitquerySmartWidgetsStructure['contactUsPopup'] = array(
		'typeBookmarkWindow'=>'pq_stick pq_contact '.$preparedObject[typeBookmarkWindow],
		'typeWindow'=>$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'to_email'=>stripslashes($preparedObject[to_email]),
		'button_text'=>stripslashes($preparedObject[button_text]),		
		'enter_name_text'=>stripslashes($preparedObject[enter_name_text]),		
		'enter_email_text'=>stripslashes($preparedObject[enter_email_text]),		
		'enter_message_text'=>stripslashes($preparedObject[enter_message_text]),		
		'mail_subject'=>stripslashes($preparedObject[mail_subject]),		
		'bookmark_text'=>stripslashes($preparedObject[loader_text]),		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),				
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],		
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//contactUsFloating (bookmark)
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[contactUsFloating], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$preparedObject[to_email] = $profitquery[settings][email];		
	
	$profitquerySmartWidgetsStructure['contactUsFloating'] = array(		
		'typeWindow'=>'pq_fixed '.$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'to_email'=>stripslashes($preparedObject[to_email]),
		'button_text'=>stripslashes($preparedObject[button_text]),		
		'enter_name_text'=>stripslashes($preparedObject[enter_name_text]),		
		'enter_email_text'=>stripslashes($preparedObject[enter_email_text]),		
		'enter_message_text'=>stripslashes($preparedObject[enter_message_text]),		
		'mail_subject'=>stripslashes($preparedObject[mail_subject]),		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),				
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],		
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//youtubePopup
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[youtubePopup], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$profitquerySmartWidgetsStructure['youtubePopup'] = array(
		'typeWindow'=>$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],		
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'iframe_src'=>stripslashes($preparedObject[iframe_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	
	//socialPopup
	$preparedObject = profitquery_prepare_sctructure_product($profitquery[socialPopup], 0);	
	$preparedObject[displayRules][main_page] = $profitquery[settings][mainPage];	
	$profitquerySmartWidgetsStructure['socialPopup'] = array(
		'typeWindow'=>$preparedObject[typeWindow],
		'enable'=>(int)$preparedObject[enable],		
		'title'=>stripslashes($preparedObject[title]),		
		'sub_title'=>stripslashes($preparedObject[sub_title]),		
		'closeIconOption'=>$preparedObject[closeIconOption],		
		'header_image_src'=>stripslashes($preparedObject[header_image_src]),
		'social_iframe_src'=>stripslashes($preparedObject[iframe_src]),
		'background_image_src'=>stripslashes($preparedObject[background_image_src]),
		'buttonBlock'=>$preparedObject[buttonBlock],
		'blackoutOption'=>$preparedObject[blackoutOption],
		'lockedMechanism'=>$preparedObject[lockedMechanism],		
		'displayRules'=>$preparedObject[displayRules],
		'eventHandler'=>$preparedObject[eventHandler],		
		'thankPopup'=>$preparedObject[thank]
	);
	$additionalOptionText = '';
	if((string)$profitquery[settings][enableGA] != 'on' && isset($profitquery[settings])){
		$additionalOptionText = 'profitquery.productOptions.disableGA = 1;';
	}
	
	if((int)$profitquery[i_agree] == 1){		
		print "
		<script>
		(function () {			
				var PQInit = function(){
					profitquery.loadFunc.callAfterPQInit(function(){					
						profitquery.loadFunc.callAfterPluginsInit(						
							function(){									
								PQLoadTools();
							}
							, ['//profitquery-a.akamaihd.net/lib/plugins/aio.plugin.profitquery.v3.min.js']							
						);
					});
				};
				var s = document.createElement('script');
				var _isPQLibraryLoaded = false;
				s.type = 'text/javascript';
				s.async = true;			
				s.profitqueryAPIKey = '".stripslashes($profitquery[settings][apiKey])."';
				s.profitqueryPROLoader = '".stripslashes($profitquery[settings][pro_loader_filename])."';
				s.profitqueryLang = 'en';			
				s.src = '//profitquery-a.akamaihd.net/lib/profitquery.v4.min.js';								
				s.onload = function(){
					if ( !_isPQLibraryLoaded )
					{					
					  _isPQLibraryLoaded = true;				  
					  PQInit();
					}
				}
				s.onreadystatechange = function() {								
					if ( !_isPQLibraryLoaded && (this.readyState == 'complete' || this.readyState == 'loaded') )
					{					
					  _isPQLibraryLoaded = true;				    
						
						PQInit();					
					}
				};
				var x = document.getElementsByTagName('script')[0];						
				x.parentNode.insertBefore(s, x);			
			})();
			
			function PQLoadTools(){
				profitquery.loadFunc.callAfterPQInit(function(){
							".$additionalOptionText."
							var smartWidgetsBoxObject = ".json_encode($profitquerySmartWidgetsStructure).";							
							profitquery.widgets.smartWidgetsBox.init(smartWidgetsBoxObject);	
						});
			}
		</script>	
		";		
	}
}


add_filter('plugin_action_links', 'profitquery_wordpress_admin_link', 10, 2);