<?php
/* 
* +--------------------------------------------------------------------------+
* | Copyright (c) ShemOtechnik Profitquery Team shemotechnik@profitquery.com |
* +--------------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify     |
* | it under the terms of the GNU General Public License as published by     |
* | the Free Software Foundation; either version 2 of the License, or        |
* | (at your option) any later version.                                      |
* |                                                                          |
* | This program is distributed in the hope that it will be useful,          |
* | but WITHOUT ANY WARRANTY; without even the implied warranty of           |
* | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            |
* | GNU General Public License for more details.                             |
* |                                                                          |
* | You should have received a copy of the GNU General Public License        |
* | along with this program; if not, write to the Free Software              |
* | Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA |
* +--------------------------------------------------------------------------+
*/
/**
* @category Class
* @package  Wordpress_Plugin
* @author   ShemOtechnik Profitquery Team <support@profitquery.com>
* @license  http://www.php.net/license/3_01.txt  PHP License 3.01
* @version  SVN: 4.1.11
*/
class ProfitQuerySmartWidgetsClass
{
	/** Profitquery Settings **/
    var $_options;
	var $_themes;
	var $_default_themes;
	var $_pro_options;
	var $_plugin_settings;
	var $_dictionary;
	function ProfitQuerySmartWidgetsClass(){
		$this->__construct();
	}
	/**
     * Initializes the plugin.
     *
     * @param null     
     * @return null
     * */
    function __construct()
    {		
		$this->_options = $this->getSettings();				
        add_action('admin_menu', array($this, 'ProfitquerySmartWidgetsMenu'));		
		// Deactivation
        register_deactivation_hook(
            PROFITQUERY_SMART_WIDGETS_FILENAME,
            array($this, 'pluginDeactivation')
        );
		register_activation_hook(
            PROFITQUERY_SMART_WIDGETS_FILENAME,
            array($this, 'pluginActivation')
        );
    }
	
	/*
		IsPLuginPage
		return boolean
	*/
	function isPluginPage(){
		$ret = false;
		if(strstr($_SERVER[REQUEST_URI], 'wp-admin/plugins.php')){
			$ret = true;
		}		
		return $ret;
	}
	
	
	/**
     * Functions to execute on plugin activation
     * 
     * @return null
     */
    public function pluginActivation()
    {		
		$this->_options[aio_widgets_loaded] = 1;		
		update_option('profitquery', $this->_options); 		
    }
	
	
	 /**
     * Functions to execute on plugin deactivation
     * 
     * @return null
     */
    public function pluginDeactivation()
    {
        if (get_option('profitquery')) {			
			$this->_options[aio_widgets_loaded] = 0;
			update_option('profitquery', $this->_options);
        }
    }
	
	/**
     * Adds sub menu page to the WP settings menu
     * 
     * @return null
     */
    function ProfitquerySmartWidgetsMenu()
    {
		add_menu_page( 'Profitquery Settings', 'Profitquery', 'manage_options', PROFITQUERY_SMART_WIDGETS_PAGE_NAME, array($this, 'ProfitquerySmartWidgetsOptions'), plugins_url( 'i/pq_icon_2.png', __FILE__ ), 103.787 );        
		 add_options_page(
            'Profitquery', 'Profitquery',
            'manage_options', PROFITQUERY_SMART_WIDGETS_PAGE_NAME,
            array($this, 'ProfitquerySmartWidgetsOptions')
        );
    }
	
	 /**
     * Get the plugin's settings page url
     * 
     * @return string
     */
    function getSettingsPageUrl()
    {
        return admin_url("options-general.php?page=" . PROFITQUERY_SMART_WIDGETS_PAGE_NAME);
    }
	
		
	
	function setDefaultProductData(){		
		//Other default params
		$thankPopup = Array(
			'enable' => '',
			'typeWindow' => 'pq_medium',
			'popup_form' => 'pq_br_sq',
			'background' => 'pq_bg_backgroundimage_whtsmoke',
			'title' => 'Thank You For Sharing',
			'head_font' => 'pq_h_font_h1_helvetica',
			'head_size' => 'pq_head_size28',
			'head_color' => 'pq_h_color_h1_dimgrey',
			'sub_title' => 'Follow Our News',
			'text_font' => 'pq_text_font_helvetica',
			'font_size' => 'pq_text_size16',
			'text_color' => 'pq_text_color_p_darkgrey',
			'close_icon' => Array
				(
					'form' => 'pq_x_type8',
					'button_text' => 'No, Thanks',
					'color' => 'pq_x_color_pqclose_dimgrey',
					'animation' => 'pq_hvr_rotate'
				),
			'animation' => 'pq_anim_bounceInDown',
			'socnet_block_type' => 'follow',
			'socnetIconsBlock' => Array
				(
					'FB' => 'profitquery',
					'TW' => 'profitquery',
					'PI' => 'profitquery'

				),
			'displayRules' => Array(
				'work_on_mobile' => 'on',
				'display_on_main_page'
			),
			'icon' => Array
				(
					'design' => 'c4',
					'form' => 'pq_square',
					'size' => 'x40',
					'space' => 'pq_step2',
					'animation' => 'pq_hvr_push'
				),
			'buttonBlock' => Array
				(
					'button_font' => 'pq_btn_font_btngroupinput_helvetica'
				)
		);
		
		$sendMailWindow = Array(
			'enable' => '',
			'typeWindow' => 'pq_medium',
			'popup_form' => 'pq_br_sq',
			'background' => 'pq_bg_backgroundimage_whtsmoke',
			'title' => 'Send Email',
			'head_font' => 'pq_h_font_h1_helvetica',
			'head_size' => 'pq_head_size28',
			'head_color' => 'pq_h_color_h1_dimgrey',
			'text_font' => 'pq_text_font_helvetica',
			'font_size' => 'pq_text_size16',
			'text_color' => 'pq_text_color_p_darkgrey',
			'enter_name_text' => 'Name',
			'enter_email_text' => 'Email',
			'enter_subject_text' => 'Email',
			'enter_message_text' => 'Message',
			'button_text' => 'Send',
			'button_font' => 'pq_btn_font_btngroupinput_helvetica',
			'button_text_color' => 'pq_btn_color_btngroupinput_white',
			'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
			'close_icon' => Array
				(
					'form' => 'pq_x_type1',
					'button_text' => '',
					'color' => 'pq_x_color_pqclose_dimgrey',
					'animation' => 'pq_hvr_rotate'
				),
			'displayRules' => Array(
				'work_on_mobile' => 'on',
				'display_on_main_page'
			),
			'animation' => 'pq_anim_bounceInDown',
			'overlay' => 'pq_over_bgcolor_lightgrey'
		);
		if(!$this->_options[imageSharer]){
		$this->_options['imageSharer'] = Array(
				'theme' => 'imagesharer_5',
				'socnet' => Array
					(
						'FB' => 'on',
						'TW' => 'on',
						'PI' => 'on',
						'TR' => 'on'
					),
				'socnetOption' => Array
				(
					'FB' => array('type'=>'pq'),
					'TW' => array('type'=>'pq')
				),
				'icon' => Array
					(
						'form' => 'pq_square',
						'size' => 'x30',
						'space' => 'pq_step2',
						'shadow' => 'sh6',
						'animation' => 'pq_hvr_shrink'
					),
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'minWidth' => '0',
				'minHeight' => '0',
				'enable' => 'on',
				'themeClass' => 'c5'
			);
			$this->_options['imageSharer']['thank'] = $thankPopup;
			$this->_options['imageSharer']['sendMailWindow'] = $sendMailWindow;
		}
		if(!$this->_options[emailListBuilderBar]){    
		$this->_options['emailListBuilderBar'] = Array
			(
				'position' => 'BAR_TOP',
				'theme' => 'emaillistbuilderbar_1',
				'themeClass' => 'pq_t_emaillistbuilderbar_1',				
				'title' => 'Join Our Newsletter',				
				'enter_email_text' => 'Enter your email',
				'enter_name_text' => 'Enter your name',
				'button_text' => 'Subscribe',				
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size20',
				'head_color' => 'pq_h_color_h1_dimgrey',				
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInDown',				
				'mobile_position' => 'pq_mobile_top',
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'provider' => 'mailchimp',
				'mobile_title' => 'Join Our Newsletter'
			);
			$this->_options['emailListBuilderBar']['thank'] = $thankPopup;
		}
		if(!$this->_options[emailListBuilderFloating]){ 
		$this->_options[emailListBuilderFloating] = Array
			(
				'position' => 'FLOATING_RIGHT_BOTTOM',
				'theme' => 'emaillistbuilderfloating_1',
				'themeClass' => 'pq_t_emaillistbuilderfloating_1',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'second_background' => '',
				'background_image_src' => '',
				'title' => 'Join Our Newsletter',
				'sub_title' => 'Signup today for free and be the first to get this exclusive report',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'sub_title' => '',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'enter_email_text' => 'Enter your email',
				'enter_name_text' => 'Enter your name',
				'button_text' => 'Subscribe',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInUp',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'provider' => 'mailchimp',
				'enable' => ''
			);
			$this->_options['emailListBuilderFloating']['thank'] = $thankPopup;
		}
		if(!$this->_options[sharingSidebar]){ 
		$this->_options[sharingSidebar] = Array
			(
				'position' => 'SIDE_LEFT_TOP',
				'theme' => 'sharingsidebar_8',
				'themeClass' => 'c4',
				'socnet_with_pos' => Array
					(
						'0' => 'FB',
						'1' => 'TW',
						'2' => 'PI',
						'3' => 'LI',
						'4' => 'GP',
						'5' => 'Mail'
					),
				'icon' => Array
					(
						'form' => 'pq_square',
						'size' => 'x40',
						'space' => 'pq_step2',
						'animation' => 'pq_hvr_push'
					),
				'animation' => 'pq_anim_bounceInLeft',
				'withCounter' => 'on',
				'galleryOption' => Array
					(
						'enable' => 'on',
						'background_color'=>'pq_bg_bgcolor_lightgrey',
						'button_text_color'=>'pq_btn_color_pqbtn_white',
						'button_color'=>'pq_btn_bg_bgcolor_pqbtn_mediumspringgreen',
						'button_font'=>'Helvetica',
						'button_font_size'=>'pq_btn_size28',
						'head_size','pq_head_size28',						
						'gallery_min_width' => 300,
						'gallery_head_color' => '',						
						'head_font'=>'Helvetica',
						'title'=>'Share Image',
						'button_text'=>'Share'						
					),
				'moreShareWindow' => Array
					(
						'typeWindow' => 'pq_large',
						'popup_form' => 'pq_br_sq',
						'background' => 'pq_bg_backgroundimage_whtsmoke',
						'background_image_src' => '',
						'title' => 'Share Link',
						'head_font' => 'pq_h_font_h1_helvetica',
						'head_size' => 'pq_head_size16',
						'head_color' => 'pq_h_color_h1_dimgrey',
						'text_font' => 'pq_text_font_helvetica',
						'text_color'=>'pq_text_color_p_darkgrey',						
						'close_icon' => Array
							(
								'form' => 'pq_x_type1',
								'button_text' => '',
								'color' => 'pq_x_color_pqclose_dimgrey',
								'animation' => 'pq_hvr_rotate'
							),
						'icon' => Array
							(
								'animation' => 'pq_hvr_push'
							),
						'animation' => 'pq_anim_bounceInDown',
						'overlay' => 'pq_over_bgcolor_lightgrey'
					),
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'mobile_position' => 'pq_mobile_bottom',
				'mobile_title' => 'Share This',
				'eventHandler' => Array
					(
						'type' => 'delay',
						'delay_value' => '0',
						'scrolling_value' => '0'
					),				
				'enable' => 'on'				
			);
			$this->_options['sharingSidebar']['thank'] = $thankPopup;
			$this->_options['sharingSidebar']['sendMailWindow'] = $sendMailWindow;
		}
		
		
		if(!$this->_options[sharingPopup]){ 
		$this->_options[sharingPopup] = Array
			(
				'position' => 'CENTER',
				'theme' => 'sharingpopup_1',
				'themeClass' => 'pq_t_sharingpopup_1',
				'socnet_with_pos' => Array
					(
						'0' => 'FB',
						'1' => 'TW',
						'2' => 'PI',
						'3' => 'LI',
						'4' => 'GP',
						'5' => 'Mail'
					),
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Share Link',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'icon' => Array
					(
						'design' => 'c4',
						'form' => 'pq_square',
						'size' => 'x40',
						'space' => 'pq_step2',
						'animation' => 'pq_hvr_push'
					),
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInDown',
				'overlay' => 'pq_over_bgcolor_lightgrey',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['sharingPopup']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[sharingBar]){
		$this->_options[sharingBar] = Array
			(
				'position' => 'BAR_BOTTOM',
				'theme' => 'sharingbar_1',
				'themeClass' => 'pq_t_sharingbar_1',
				'socnet_with_pos' => Array
					(
						'0' => 'FB',
						'1' => 'TW',
						'2' => 'PI',
						'3' => 'LI',
						'4' => 'GP',
						'5' => 'Mail'
					),
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Share This Page',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size20',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'icon' => Array
					(
						'design' => 'c4',
						'form' => 'pq_square',
						'size' => 'x30',
						'space' => 'pq_step2',
						'animation' => 'pq_hvr_push'
					),
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInUp',
				'mobile_position' => 'pq_mobile_bottom',
				'mobile_title' => 'Share This Page',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['sharingBar']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[sharingFloating]){
		$this->_options[sharingFloating] = Array
			(
				'position' => 'FLOATING_RIGHT_BOTTOM',
				'theme' => 'sharingfloating_1',
				'themeClass' => 'pq_t_sharingfloating_1',
				'socnet_with_pos' => Array
					(
						'0' => 'FB',
						'1' => 'TW',
						'2' => 'PI',
						'3' => 'LI',
						'4' => 'GP',
						'5' => 'Mail'
					),
				'typeWindow' => 'pq_mini',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Share This Page',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'icon' => Array
					(
						'design' => 'c4',
						'form' => 'pq_square',
						'size' => 'x40',
						'space' => 'pq_step2',
						'animation' => 'pq_hvr_push'
					),
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInUp',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['sharingFloating']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[emailListBuilderPopup]){
		$this->_options[emailListBuilderPopup] = Array
			(
				'position' => 'CENTER',
				'theme' => 'emaillistbuilderpopup_1',
				'themeClass' => 'pq_t_emaillistbuilderpopup_1',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Join Our Newsletter',
				'sub_title' => 'Signup today for free and be the first to get this exclusive report',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'sub_title' => 'Signup today  and be the first to get notified on new updates',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'enter_email_text' => 'Enter your email',
				'enter_name_text' => 'Enter your name',
				'button_text' => 'Subscribe',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInDown',
				'overlay' => 'pq_over_bgcolor_lightgrey',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'provider' => 'mailchimp',
				'enable' => ''
			);
			$this->_options['emailListBuilderPopup']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[contactUsPopup]){
		$this->_options[contactUsPopup] = Array
			(
				'position' => 'SIDE_RIGHT_BOTTOM',
				'theme' => 'contactuspopup_1',
				'themeClass' => 'pq_t_contactuspopup_1',
				'loader_text' => 'Contact Us',
				'bookmark_background' => 'pq_bg_bgcolor_black',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'second_background' => '',
				'background_image_src' => '',
				'title' => 'Contact Us',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'enter_name_text' => 'Name',
				'enter_email_text' => 'Email',
				'enter_message_text' => 'Message',
				'button_text' => 'Send ',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInRight',
				'overlay' => 'pq_over_bgcolor_lightgrey',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['contactUsPopup']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[contactUsFloating]){
		$this->_options[contactUsFloating] = Array
			(
				'position' => 'FLOATING_LEFT_BOTTOM',
				'theme' => 'contactusfloating_1',
				'themeClass' => 'pq_t_contactusfloating_1',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Contact Us',
				'head_font' => 'pq_h_font_h1_arial',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'text_font' => 'pq_text_font_helvetic_w',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'enter_name_text' => 'Name',
				'enter_email_text' => 'Email',
				'enter_message_text' => 'Message',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInLeft',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['contactUsFloating']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[promotePopup]){
		$this->_options[promotePopup] = Array
			(
				'position' => 'CENTER',
				'theme' => 'promotepopup_1',
				'themeClass' => 'pq_t_promotepopup_1',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Promote Link',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'sub_title' => 'Enter the url that will be shown on all website vizitors',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'url' => 'http://profitquery.com/',
				'button_text' => 'Click Now',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'color' => 'pq_x_color_pqclose_lightslategrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInDown',
				'overlay' => 'pq_over_bgcolor_lightgrey',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['promotePopup']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[promoteBar]){
		$this->_options[promoteBar] = Array
			(
				'position' => 'BAR_TOP',
				'theme' => 'promotebar_1',
				'themeClass' => 'pq_t_promotebar_1',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'second_background' => '',
				'background_image_src' => '',
				'title' => 'Promote LInk',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size20',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'url' => 'http://profitquery.com/',
				'button_text' => 'Click Now',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInDown',
				'mobile_position' => 'pq_mobile_top',
				'mobile_title' => 'Click Now',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['promoteBar']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[promoteFloating]){
		$this->_options[promoteFloating] = Array
			(
				'position' => 'FLOATING_RIGHT_TOP',
				'theme' => 'promotefloating_1',
				'themeClass' => 'pq_t_promotefloating_1',
				'typeWindow' => 'pq_mini',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Promote Link',
				'head_font' => 'pq_h_font_h1_helvetic_w',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'sub_title' => '',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'url' => 'http://profitquery.com/',
				'button_text' => 'Click Now',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'header_image_src' => '',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInRight',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['promoteFloating']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[callMePopup]){
		$this->_options[callMePopup] = Array
			(
				'position' => 'SIDE_LEFT_BOTTOM',
				'theme' => 'callmepopup_1',
				'themeClass' => 'pq_t_callmepopup_1',
				'loader_text' => 'Call Me',
				'bookmark_background' => 'pq_bg_bgcolor_black',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Call Me',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'enter_name_text' => 'Name',
				'enter_phone_text' => 'Phone',
				'button_text' => 'Send',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInLeft',
				'overlay' => 'pq_over_bgcolor_lightgrey',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['callMePopup']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[callMeFloating]){
		$this->_options[callMeFloating] = Array
			(
				'position' => 'FLOATING_RIGHT_BOTTOM',
				'theme' => 'callmefloating_1',
				'themeClass' => 'pq_t_callmefloating_1',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Call Me',
				'head_font' => 'pq_h_font_h1_helvetic_w',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'button_font' => 'pq_btn_font_btngroupinput_helvetica',
				'button_text_color' => 'pq_btn_color_btngroupinput_white',
				'button_color' => 'pq_btn_bg_bgcolor_btngroupinput_mediumspringgreen',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInRight',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['callMeFloating']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[followPopup]){
		$this->_options[followPopup] = Array
			(
				'position' => 'CENTER',
				'theme' => 'followpopup_1',
				'themeClass' => 'pq_t_followpopup_1',
				'socnetIconsBlock' => Array
					(
						'FB' => 'profitquery',
						'TW' => 'profitquery',
						'PI' => 'profitquery',
					),
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Follow Us',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'sub_title' => '',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'icon' => Array
					(
						'design' => 'c4',
						'form' => 'pq_square',
						'size' => 'x40',
						'space' => 'pq_step2',
						'animation' => 'pq_hvr_push'
					),
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInDown',
				'overlay' => 'pq_over_bgcolor_lightgrey',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['followPopup']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[followBar]){
		$this->_options[followBar] = Array
			(
				'position' => 'BAR_BOTTOM',
				'theme' => 'followbar_1',
				'themeClass' => 'pq_t_followbar_1',
				'socnetIconsBlock' => Array
					(
						'FB' => 'profitquery',
						'TW' => 'profitquery',
						'PI' => 'profitquery',
					),
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Follow Us',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size20',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'icon' => Array
					(
						'design' => 'c4',
						'form' => 'pq_square',
						'size' => 'x30',
						'space' => 'pq_step2',
						'animation' => 'pq_hvr_push'
					),
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInUp',
				'mobile_position' => 'pq_mobile_top',				
				'mobile_title' => 'Follow Us',
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['followBar']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[followFloating]){
		$this->_options[followFloating] = Array
			(
				'position' => 'FLOATING_RIGHT_BOTTOM',
				'theme' => 'followfloating_1',
				'themeClass' => 'pq_t_followfloating_1',
				'socnetIconsBlock' => Array
					(
						'FB' => 'profitquery',
						'TW' => 'profitquery',
						'PI' => 'profitquery'
					),
				'typeWindow' => 'pq_mini',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Follow Us',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'icon' => Array
					(
						'design' => 'c4',
						'form' => 'pq_square',
						'size' => 'x40',
						'space' => 'pq_step2',
						'animation' => 'pq_hvr_push'
					),
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'animation' => 'pq_anim_bounceInRight',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['followFloating']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[socialPopup]){
		$this->_options[socialPopup] = Array
			(
				'position' => 'CENTER',
				'theme' => 'socialpopup_1',
				'themeClass' => 'pq_t_socialpopup_1',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'Facebook',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'sub_title' => '',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'button_text' => '',
						'color' => 'pq_x_color_pqclose_dimgrey',
						'animation' => 'pq_hvr_rotate'
					),
				'overlay' => 'pq_over_bgcolor_lightgrey',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => '',
			);
			$this->_options['socialPopup']['thank'] = $thankPopup;
		}
		
		if(!$this->_options[youtubePopup]){
		$this->_options[youtubePopup] = Array
			(
				'position' => 'CENTER',
				'theme' => 'youtubepopup_1',
				'themeClass' => 'pq_t_youtubepopup_1',
				'typeWindow' => 'pq_medium',
				'popup_form' => 'pq_br_sq',
				'background' => 'pq_bg_backgroundimage_whtsmoke',
				'title' => 'YouTube Video',
				'head_font' => 'pq_h_font_h1_helvetica',
				'head_size' => 'pq_head_size28',
				'head_color' => 'pq_h_color_h1_dimgrey',
				'text_font' => 'pq_text_font_helvetica',
				'font_size' => 'pq_text_size16',
				'text_color' => 'pq_text_color_p_darkgrey',
				'close_icon' => Array
					(
						'form' => 'pq_x_type1',
						'color' => 'pq_x_color_pqclose_dimgrey'
					),
				'animation' => 'pq_anim_bounceInDown',
				'overlay' => 'pq_over_bgcolor_lightgrey',				
				'displayRules' => Array(
					'work_on_mobile' => 'on',
					'display_on_main_page'
				),
				'enable' => ''
			);
			$this->_options['youtubePopup']['thank'] = $thankPopup;
		}
		
		update_option('profitquery', $this->_options);
	}	
		
	
	
	
	
	/**
     *  Get LitePQ Share Image settings array
     * 
     *  @return string
     */
    function getSettings()
    {
        return get_option('profitquery');
    }
	
	/*
	 *	parseSubscribeProviderForm
	 *	
	 *	@return array
	 */
	function parseSubscribeProviderForm($provider, $formCode)
	{		
		if($provider == 'mailchimp'){
			$return = $this->_parseMailchimpForm($formCode);
		}
		
		if($provider == 'acampaign'){
			$return = $this->_parseACampaignForm($formCode);
		}
		if($provider == 'aweber'){
			$return = $this->_parseAweberForm($formCode);
		}
		if($provider == 'newsletter2go'){
			$return = $this->_parseNewsLetter2goForm($formCode);
		}
		if($provider == 'madmini'){
			$return = $this->_parseMadMiniForm($formCode);
		}
		if($provider == 'getresponse'){
			$return = $this->_parseGetResponseForm($formCode);
		}
		if($provider == 'klickmail'){
			$return = $this->_parseGetKlickmailForm($formCode);
		}
		
		return $return;
	}
	
	
	function _parseMailchimpForm($code)
	{
		$txt = trim($code);		
		$array = array();
		$matches = array();
		if($txt){
			$txt = stripslashes($txt);
			$txt = str_replace("\t", ' ', $txt);
			$txt = str_replace("\r", '', $txt);
			$txt = str_replace("\n", '', $txt);
			$txt = str_replace("  ", " ", $txt);
			$txt = str_replace("  ", " ", $txt);			
			preg_match_all('/(\<)(.*)(form)(.*)(action=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);
			$array[formAction] = trim($matches[8][0]);
			if(!strstr($array[formAction], 'list-manage.com')){
				$array[formAction] = '';
				$array[is_error] = 1;
			}			
		}
		return $array;
	}
	
	function _parseNewsLetter2goForm($code)
	{
		$txt = trim($code);		
		$array = array();
		$matches = array();
		if($txt){
			$txt = stripslashes(stripslashes($txt));
			$txt = str_replace("\t", ' ', $txt);
			$txt = str_replace("\r", '', $txt);
			$txt = str_replace("\n", '', $txt);
			$txt = str_replace("  ", " ", $txt);
			$txt = str_replace("  ", " ", $txt);			
			preg_match_all('/(\<)(.*)(form)(.*)(action=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);
			$array[formAction] = trim($matches[8][0]);
			
			if(!strstr($array[formAction], 'newsletter2go.com')){
				$array[formAction] = '';
				$array[is_error] = 1;
			} else {
				preg_match_all('/(\<)(.*)(input)(.*)(hidden)(.*)(name=)(.*)([\"\'])(.*)([\"\'])(.*)(value=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);				
				foreach((array)$matches[10] as $k => $v){
					$hiddenField[$v] = $matches[16][$k];
				}
				if($hiddenField['nl2go--key']){
					$array[hidden] = $hiddenField;
				} else {
					$array[formAction] = '';
					$array[is_error] = 1;
				}
			}		
		}
		return $array;
	}
	
	
	function _parseMadMiniForm($code)
	{
		$txt = trim($code);		
		$array = array();
		$matches = array();
		$hiddenField = array();		
		if($txt){
			$txt = stripslashes($txt);
			$txt = str_replace("\t", ' ', $txt);
			$txt = str_replace("\r", '', $txt);
			$txt = str_replace("\n", '', $txt);
			$txt = str_replace("  ", " ", $txt);
			$txt = str_replace("  ", " ", $txt);			
			preg_match_all('/(\<)(.*)(form)(.*)(action=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);
			$array[formAction] = trim($matches[8][0]);			
			if(!strstr($array[formAction], 'madmimi.com/signups/')){
				$array[formAction] = '';
				$array[is_error] = 1;
			} else {
				preg_match_all('/(\<)(.*)(input)(.*)(hidden)(.*)(name=)(.*)([\"\'])(.*)([\"\'])(.*)(value=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);				
				foreach((array)$matches[10] as $k => $v){
					$hiddenField[$v] = $matches[16][$k];
				}
			}
		}		
		return $array;
	}
	
	function _parseGetKlickmailForm($code)
	{
		$txt = trim($code);		
		$array = array();
		$matches = array();
		$hiddenField = array();		
		if($txt){
			$txt = stripslashes($txt);
			$txt = str_replace("\t", ' ', $txt);
			$txt = str_replace("\r", '', $txt);
			$txt = str_replace("\n", '', $txt);
			$txt = str_replace("  ", " ", $txt);
			$txt = str_replace("  ", " ", $txt);			
			preg_match_all('/(\<)(.*)(form)(.*)(action=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);
			$array[formAction] = trim($matches[8][0]);			
			if(!strstr($array[formAction], 'klickmail.com.br')){
				$array[formAction] = '';
				$array[is_error] = 1;
			} else {
				preg_match_all('/(\<)(.*)(input)(.*)(hidden)(.*)(name=)(.*)([\"\'])(.*)([\"\'])(.*)(value=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);				
				foreach((array)$matches[10] as $k => $v){
					$hiddenField[$v] = $matches[16][$k];
				}				
				if($hiddenField[FormValue_FormID]){
					$array[hidden] = $hiddenField;
				} else {
					$array[formAction] = '';
					$array[is_error] = 1;
				}
			}
		}		
		return $array;
	}
	
	function _parseGetResponseForm($code)
	{
		$txt = trim($code);		
		$array = array();
		$matches = array();
		$hiddenField = array();		
		if($txt){
			$txt = stripslashes($txt);
			$txt = str_replace("\t", ' ', $txt);
			$txt = str_replace("\r", '', $txt);
			$txt = str_replace("\n", '', $txt);
			$txt = str_replace("  ", " ", $txt);
			$txt = str_replace("  ", " ", $txt);			
			preg_match_all('/(\<)(.*)(form)(.*)(action=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);
			$array[formAction] = trim($matches[8][0]);			
			if(!strstr($array[formAction], 'getresponse.com')){
				$array[formAction] = '';
				$array[is_error] = 1;
			} else {
				preg_match_all('/(\<)(.*)(input)(.*)(hidden)(.*)(name=)(.*)([\"\'])(.*)([\"\'])(.*)(value=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);				
				foreach((array)$matches[10] as $k => $v){
					$hiddenField[$v] = $matches[16][$k];
				}				
				if($hiddenField[webform_id]){
					$array[hidden] = $hiddenField;
				} else {
					$array[formAction] = '';
					$array[is_error] = 1;
				}
			}
		}		
		return $array;
	}
	
	function _parseACampaignForm($code)
	{
		$txt = trim($code);		
		$array = array();
		$matches = array();
		$hiddenField = array();		
		if($txt){
			$txt = stripslashes($txt);
			$txt = str_replace("\t", ' ', $txt);
			$txt = str_replace("\r", '', $txt);
			$txt = str_replace("\n", '', $txt);
			$txt = str_replace("  ", " ", $txt);
			$txt = str_replace("  ", " ", $txt);			
			preg_match_all('/(\<)(.*)(form)(.*)(action=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);
			$array[formAction] = trim($matches[8][0]);			
			if(!strstr($array[formAction], 'activehosted.com')){
				$array[formAction] = '';
				$array[is_error] = 1;
			} else {
				preg_match_all('/(\<)(.*)(input)(.*)(hidden)(.*)(name=)(.*)([\"\'])(.*)([\"\'])(.*)(value=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);				
				foreach((array)$matches[10] as $k => $v){
					$hiddenField[$v] = $matches[16][$k];
				}				
				if($hiddenField[act]){
					$array[hidden] = $hiddenField;
				} else {
					$array[formAction] = '';
					$array[is_error] = 1;
				}
			}
		}		
		return $array;
	}
	
	function _parseAweberForm($code)
	{
		$txt = trim($code);		
		$array = array();
		$matches = array();
		$hiddenField = array();
		if($txt){
			$txt = stripslashes($txt);
			$txt = str_replace("\t", ' ', $txt);
			$txt = str_replace("\r", '', $txt);
			$txt = str_replace("\n", '', $txt);
			$txt = str_replace("  ", " ", $txt);
			$txt = str_replace("  ", " ", $txt);			
			preg_match_all('/(\<)(.*)(form)(.*)(action=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);
			$array[formAction] = trim($matches[8][0]);
			if(!strstr($array[formAction], 'aweber.com')){
				$array[formAction] = '';
				$array[is_error] = 1;
			} else {
				preg_match_all('/(\<)(.*)(input)(.*)(hidden)(.*)(name=)(.*)([\"\'])(.*)([\"\'])(.*)(value=)(.*)([\"\'])(.*)([\"\'])(.*)(\>)/Ui', $txt, $matches);
				foreach((array)$matches[10] as $k => $v){
					$hiddenField[$v] = $matches[16][$k];
				}
				if($hiddenField[meta_web_form_id]){
					$array[hidden] = $hiddenField;
				} else {
					$array[formAction] = '';
					$array[is_error] = 1;
				}
			}
		}
		return $array;
	}
		
	
	
	function sharingIconsSelect($object, $scructure){
		$ret = '';
		$sharingCnt = 10;
		
	}
	
	function generateInput($nameTool, $title, $id, $nameField, $val){
		$id = $this->prepareIdName($id);
		$jsFunc = str_replace('[', '',$nameTool);
		$jsFunc = str_replace(']', '',$jsFunc);
		$jsFunc = str_replace('buttonBlock','',$jsFunc);
		$jsFunc = str_replace('moreShareWindow','',$jsFunc);
		$ret = '
			<div id="'.$id.'_div">
			<p>'.$title.'</p>
					<input class="text" type="text" id="'.$id.'" onKeyUp="'.$jsFunc.'Preview();" name="'.$nameField.'" value="'.htmlspecialchars(stripslashes($val)).'">
				<br>
			</div>
		';
		return $ret;
	}
	
	function prepareIdName($id){
		$id = str_replace('[', '_', $id);
		$id = str_replace(']', '_', $id);
		$id = str_replace('__', '_', $id);
		$id = str_replace('__', '_', $id);		
		return $id;
	}
	
	function getFontSelect($tool, $name, $text, $valArray, $id, $prefixForValue){
		$id = $this->prepareIdName($id);
		$ret = '';
		$jsFunc = str_replace('[', '',$tool);
		$jsFunc = str_replace(']', '',$jsFunc);		
		$jsFunc = str_replace('moreShareWindow','',$jsFunc);
		$jsFunc = str_replace('buttonBlock','',$jsFunc);
		
		$ret = '
		<div id="'.$id.'_div">
		<p>'.$text.'</p>		
		<select name="'.$name.'" id="'.$id.'" onchange="'.$jsFunc.'Preview()">		
		
		<optgroup label="Latin">
		<option value="'.$prefixForValue.'aguafina_script" '.sprintf("%s", (($valArray == $prefixForValue.'aguafina_script' ) ? "selected" : "")).'  style="font-family: \'Aguafina Script\', cursive;" >Aguafina Script</option>
		<option value="'.$prefixForValue.'amita" '.sprintf("%s", (($valArray == $prefixForValue.'amita' ) ? "selected" : "")).'  style="font-family: \'Amita\', cursive;" >Amita</option>
		<option value="'.$prefixForValue.'anonymous_pro" '.sprintf("%s", (($valArray == $prefixForValue.'anonymous_pro' ) ? "selected" : "")).'  style="font-family: \'Anonymous Pro\', ;" >Anonymous Pro</option>
		<option value="'.$prefixForValue.'anonymous_pr_w" '.sprintf("%s", (($valArray == $prefixForValue.'anonymous_pr_w' ) ? "selected" : "")).'  style="font-family: \'Anonymous Pro\', font-weight: 700;" >Anonymous Pro Weight</option>
		<option value="'.$prefixForValue.'arial" '.sprintf("%s", (($valArray == $prefixForValue.'arial' ) ? "selected" : "")).'  style="font-family: \'Arial\', Helvetica, sans-serif;" >Arial</option>
		<option value="'.$prefixForValue.'ar_w" '.sprintf("%s", (($valArray == $prefixForValue.'ar_w' ) ? "selected" : "")).'  style="font-family: \'Arial\', Helvetica, sans-serif, font-weight: 700;" >Arial Weight</option>
		<option value="'.$prefixForValue.'arimo" '.sprintf("%s", (($valArray == $prefixForValue.'arimo' ) ? "selected" : "")).'  style="font-family: \'Arimo\', sans-serif;" >Arimo</option>
		<option value="'.$prefixForValue.'armo_w" '.sprintf("%s", (($valArray == $prefixForValue.'armo_w' ) ? "selected" : "")).'  style="font-family: \'Arimo\', sans-serif, font-weight: 700;;" >Arimo Weight</option>
		<option value="'.$prefixForValue.'averia_gruesa_libre" '.sprintf("%s", (($valArray == $prefixForValue.'averia_gruesa_libre' ) ? "selected" : "")).'  style="font-family: \'Averia Gruesa Libre\', cursive;" >Averia Gruesa Libre</option>
		<option value="'.$prefixForValue.'berkshire_swash" '.sprintf("%s", (($valArray == $prefixForValue.'berkshire_swash' ) ? "selected" : "")).'  style="font-family: \'Berkshire Swash\', cursive;" >Berkshire Swash</option>
		<option value="'.$prefixForValue.'bitter" '.sprintf("%s", (($valArray == $prefixForValue.'bitter' ) ? "selected" : "")).'  style="font-family: \'Bitter\', serif;" >Bitter</option>
		<option value="'.$prefixForValue.'bittr_w" '.sprintf("%s", (($valArray == $prefixForValue.'bittr_w' ) ? "selected" : "")).'  style="font-family: \'Bitter\', serif, font-weight: 700;" >Bitter Weight</option>
		<option value="'.$prefixForValue.'black_ops_one" '.sprintf("%s", (($valArray == $prefixForValue.'black_ops_one' ) ? "selected" : "")).'  style="font-family: \'Black Ops One\', cursive;" >Black Ops One</option>
		<option value="'.$prefixForValue.'butterfly_kids" '.sprintf("%s", (($valArray == $prefixForValue.'butterfly_kids' ) ? "selected" : "")).'  style="font-family: \'Butterfly Kids\', cursive;" >Butterfly Kids</option>
		<option value="'.$prefixForValue.'cantata_one" '.sprintf("%s", (($valArray == $prefixForValue.'cantata_one' ) ? "selected" : "")).'  style="font-family: \'Cantata One\', serif;" >Cantata One</option>
		<option value="'.$prefixForValue.'chango" '.sprintf("%s", (($valArray == $prefixForValue.'chango' ) ? "selected" : "")).'  style="font-family: \'Chango\', cursive;" >Chango</option>
		<option value="'.$prefixForValue.'chela_one" '.sprintf("%s", (($valArray == $prefixForValue.'chela_one' ) ? "selected" : "")).'  style="font-family: \'Chela One\', cursive;" >Chela One</option>
		<option value="'.$prefixForValue.'chicle" '.sprintf("%s", (($valArray == $prefixForValue.'chicle' ) ? "selected" : "")).'  style="font-family: \'Chicle\', cursive;" >Chicle</option>
		<option value="'.$prefixForValue.'clicker_script" '.sprintf("%s", (($valArray == $prefixForValue.'clicker_script' ) ? "selected" : "")).'  style="font-family: \'Clicker Script\', cursive;" >Clicker Script</option>
		<option value="'.$prefixForValue.'codystar" '.sprintf("%s", (($valArray == $prefixForValue.'codystar' ) ? "selected" : "")).'  style="font-family: \'Codystar\', cursive;" >Codystar</option>
		<option value="'.$prefixForValue.'combo" '.sprintf("%s", (($valArray == $prefixForValue.'combo' ) ? "selected" : "")).'  style="font-family: \'Combo\', cursive;" >Combo</option>
		<option value="'.$prefixForValue.'comfortaa" '.sprintf("%s", (($valArray == $prefixForValue.'comfortaa' ) ? "selected" : "")).'  style="font-family: \'Comfortaa\', cursive;" >Comfortaa</option>
		<option value="'.$prefixForValue.'comfort_w" '.sprintf("%s", (($valArray == $prefixForValue.'comfort_w' ) ? "selected" : "")).'  style="font-family: \'Comfortaa\', cursive, font-weight: 700;" >Comfortaa Weight</option>
		<option value="'.$prefixForValue.'courier_new" '.sprintf("%s", (($valArray == $prefixForValue.'courier_new' ) ? "selected" : "")).'  style="font-family: \'courier_new\', \'times new roman\', sans-serif;" >Courier New</option>
		<option value="'.$prefixForValue.'croissant_one" '.sprintf("%s", (($valArray == $prefixForValue.'croissant_one' ) ? "selected" : "")).'  style="font-family: \'Croissant One\', cursive;" >Croissant One</option>
		<option value="'.$prefixForValue.'dynalight" '.sprintf("%s", (($valArray == $prefixForValue.'dynalight' ) ? "selected" : "")).'  style="font-family: \'Dynalight\', cursive;" >Dynalight</option>
		<option value="'.$prefixForValue.'eagle_lake" '.sprintf("%s", (($valArray == $prefixForValue.'eagle_lake' ) ? "selected" : "")).'  style="font-family: \'Eagle Lake\', cursive;" >Eagle Lake</option>
		<option value="'.$prefixForValue.'elsie" '.sprintf("%s", (($valArray == $prefixForValue.'elsie' ) ? "selected" : "")).'  style="font-family: \'Elsie\', cursive;" >Elsie</option>
		<option value="'.$prefixForValue.'els_w" '.sprintf("%s", (($valArray == $prefixForValue.'els_w' ) ? "selected" : "")).'  style="font-family: \'Elsie\', cursive, font-weight: 900;" >Elsie Weight</option>
		<option value="'.$prefixForValue.'emilys_candy" '.sprintf("%s", (($valArray == $prefixForValue.'emilys_candy' ) ? "selected" : "")).'  style="font-family: \'Emilys Candy\', cursive;" >Emilys Candy</option>
		<option value="'.$prefixForValue.'esteban" '.sprintf("%s", (($valArray == $prefixForValue.'esteban' ) ? "selected" : "")).'  style="font-family: \'Esteban\', serif;" >Esteban</option>
		<option value="'.$prefixForValue.'euphoria_script" '.sprintf("%s", (($valArray == $prefixForValue.'euphoria_script' ) ? "selected" : "")).'  style="font-family: \'Euphoria Script\', cursive;" >Euphoria Script</option>
		<option value="'.$prefixForValue.'exo" '.sprintf("%s", (($valArray == $prefixForValue.'exo' ) ? "selected" : "")).'  style="font-family: \'Exo\', sans-serif;" >Exo</option>
		<option value="'.$prefixForValue.'ex_w" '.sprintf("%s", (($valArray == $prefixForValue.'ex_w' ) ? "selected" : "")).'  style="font-family: \'Exo\', font-weight: 700;" >Exo Weight</option>
		<option value="'.$prefixForValue.'georgia" '.sprintf("%s", (($valArray == $prefixForValue.'georgia' ) ? "selected" : "")).'  style="font-family: \'georgia\', \'times new roman\', Arial, sans-serif;" >Georgia</option>
		<option value="'.$prefixForValue.'great_vibes" '.sprintf("%s", (($valArray == $prefixForValue.'great_vibes' ) ? "selected" : "")).'  style="font-family: \'Great Vibes\', cursive;" >Great Vibes</option>
		<option value="'.$prefixForValue.'gruppo" '.sprintf("%s", (($valArray == $prefixForValue.'gruppo' ) ? "selected" : "")).'  style="font-family: \'Gruppo\', cursive;" >Gruppo</option>		
		<option value="'.$prefixForValue.'happy_monkey" '.sprintf("%s", (($valArray == $prefixForValue.'happy_monkey' ) ? "selected" : "")).'  style="font-family: \'Happy Monkey\', cursive;" >Happy Monkey</option>		
		<option value="'.$prefixForValue.'helvetica" '.sprintf("%s", (($valArray == $prefixForValue.'helvetica' ) ? "selected" : "")).'  style="font-family: \'helvetica\', arial, \'open sans\', sans-serif;" >Helvetica</option>
		<option value="'.$prefixForValue.'helvetic_w" '.sprintf("%s", (($valArray == $prefixForValue.'helvetic_w' ) ? "selected" : "")).'  style="font-family: \'helvetica\', arial, \'open sans\', sans-serif, font-weight: 700;" >Helvetica Weight</option>
		<option value="'.$prefixForValue.'julius_sans_one" '.sprintf("%s", (($valArray == $prefixForValue.'julius_sans_one' ) ? "selected" : "")).'  style="font-family: \'Julius Sans One\', sans-serif;" >Julius Sans One</option>
		<option value="'.$prefixForValue.'kavoon" '.sprintf("%s", (($valArray == $prefixForValue.'kavoon' ) ? "selected" : "")).'  style="font-family: \'Kavoon\', cursive;" >Kavoon</option>
		<option value="'.$prefixForValue.'lancelot" '.sprintf("%s", (($valArray == $prefixForValue.'lancelot' ) ? "selected" : "")).'  style="font-family: \'Lancelot\', cursive;" >Lancelot</option>
		<option value="'.$prefixForValue.'lato" '.sprintf("%s", (($valArray == $prefixForValue.'lato' ) ? "selected" : "")).'  style="font-family: \'Lato\', sans-serif;" >Lato</option>
		<option value="'.$prefixForValue.'lat_w" '.sprintf("%s", (($valArray == $prefixForValue.'lat_w' ) ? "selected" : "")).'  style="font-family: \'Lato\', sans-serif, font-weight: 700;" >Lato Weight</option>		
		<option value="'.$prefixForValue.'life_savers" '.sprintf("%s", (($valArray == $prefixForValue.'life_savers' ) ? "selected" : "")).'  style="font-family: \'Life Savers\', cursive;" >Life Savers</option>
		<option value="'.$prefixForValue.'life_svrs_w" '.sprintf("%s", (($valArray == $prefixForValue.'life_svrs_w' ) ? "selected" : "")).'  style="font-family: \'Life Savers\', cursive, font-weight: 700;" >Life Savers Weight</option>
		<option value="'.$prefixForValue.'lilita_one" '.sprintf("%s", (($valArray == $prefixForValue.'lilita_one' ) ? "selected" : "")).'  style="font-family: \'Lilita One\', cursive;" >Lilita One</option>
		<option value="'.$prefixForValue.'lily_script_one" '.sprintf("%s", (($valArray == $prefixForValue.'lily_script_one' ) ? "selected" : "")).'  style="font-family: \'Lily Script One\', cursive;" >Lilita One Weight</option>
		<option value="'.$prefixForValue.'limelight" '.sprintf("%s", (($valArray == $prefixForValue.'limelight' ) ? "selected" : "")).'  style="font-family: \'Limelight\', cursive;" >Limelight</option>
		<option value="'.$prefixForValue.'lobster" '.sprintf("%s", (($valArray == $prefixForValue.'lobster' ) ? "selected" : "")).'  style="font-family: \'Lobster Two\', cursive;" >Lobster Two</option>
		<option value="'.$prefixForValue.'lobstr_w" '.sprintf("%s", (($valArray == $prefixForValue.'lobstr_w' ) ? "selected" : "")).'  style="font-family: \'Lobster Two\', cursive, font-weight: 700;" >Lobster Two Weight</option>
		<option value="'.$prefixForValue.'mclaren" '.sprintf("%s", (($valArray == $prefixForValue.'mclaren' ) ? "selected" : "")).'  style="font-family: \'McLaren\', cursive;" >McLaren</option>
		<option value="'.$prefixForValue.'metamorphous" '.sprintf("%s", (($valArray == $prefixForValue.'metamorphous' ) ? "selected" : "")).'  style="font-family: \'Metamorphous\', cursive;" >Metamorphous</option>
		<option value="'.$prefixForValue.'milonga" '.sprintf("%s", (($valArray == $prefixForValue.'milonga' ) ? "selected" : "")).'  style="font-family: \'Milonga\', cursive;" >Milonga</option>
		<option value="'.$prefixForValue.'modak" '.sprintf("%s", (($valArray == $prefixForValue.'modak' ) ? "selected" : "")).'  style="font-family: \'Modak\', cursive;" >Modak</option>
		<option value="'.$prefixForValue.'molle" '.sprintf("%s", (($valArray == $prefixForValue.'molle' ) ? "selected" : "")).'  style="font-family: \'Molle\', cursive;" >Molle</option>
		<option value="'.$prefixForValue.'mystery_quest" '.sprintf("%s", (($valArray == $prefixForValue.'mystery_quest' ) ? "selected" : "")).'  style="font-family: \'Mystery Quest\', cursive;" >Mystery Quest</option>
		<option value="'.$prefixForValue.'neuton" '.sprintf("%s", (($valArray == $prefixForValue.'neuton' ) ? "selected" : "")).'  style="font-family: \'Neuton\', serif;" >Neuton</option>
		<option value="'.$prefixForValue.'neutn_w" '.sprintf("%s", (($valArray == $prefixForValue.'neutn_w' ) ? "selected" : "")).'  style="font-family: \'Neuton\', serif, font-weight: 700;" >Neuton Weight</option>
		<option value="'.$prefixForValue.'nosifer" '.sprintf("%s", (($valArray == $prefixForValue.'nosifer' ) ? "selected" : "")).'  style="font-family: \'Nosifer\', cursive;" >Nosifer</option>
		<option value="'.$prefixForValue.'oleo_script_swash_caps" '.sprintf("%s", (($valArray == $prefixForValue.'oleo_script_swash_caps' ) ? "selected" : "")).'  style="font-family: \'Oleo Script Swash Caps\', cursive;" >Oleo Script Swash Caps</option>
		<option value="'.$prefixForValue.'oleo_script_swash_cps_w" '.sprintf("%s", (($valArray == $prefixForValue.'oleo_script_swash_cps_w' ) ? "selected" : "")).'  style="font-family: \'Oleo Script Swash Caps\', cursive, font-weight: 700;" >Oleo Script Swash Caps Weight</option>
		<option value="'.$prefixForValue.'open_sans" '.sprintf("%s", (($valArray == $prefixForValue.'open_sans' ) ? "selected" : "")).'  style="font-family: \'Open Sans\', sans-serif;" >Open Sans</option>
		<option value="'.$prefixForValue.'open_sns_w" '.sprintf("%s", (($valArray == $prefixForValue.'open_sns_w' ) ? "selected" : "")).'  style="font-family: \'Open Sans\', sans-serif, font-weight: 700;" >Open Sans Weight</option>
		<option value="'.$prefixForValue.'oswald" '.sprintf("%s", (($valArray == $prefixForValue.'oswald' ) ? "selected" : "")).'  style="font-family: \'Oswald\', sans-serif;" >Oswald</option>
		<option value="'.$prefixForValue.'oswld_w" '.sprintf("%s", (($valArray == $prefixForValue.'oswld_w' ) ? "selected" : "")).'  style="font-family: \'Oswald\', sans-serif, font-weight: 700;" >Oswald Weight</option>
		<option value="'.$prefixForValue.'overlock" '.sprintf("%s", (($valArray == $prefixForValue.'overlock' ) ? "selected" : "")).'  style="font-family: \'Overlock\', cursive;" >Overlock</option>
		<option value="'.$prefixForValue.'overlck_w" '.sprintf("%s", (($valArray == $prefixForValue.'overlck_w' ) ? "selected" : "")).'  style="font-family: \'Overlock\', cursive, font-weight: 900;" >Overlock Weight</option>
		<option value="'.$prefixForValue.'passion_one" '.sprintf("%s", (($valArray == $prefixForValue.'passion_one' ) ? "selected" : "")).'  style="font-family: \'Passion One\', cursive;" >Passion One</option>
		<option value="'.$prefixForValue.'passion_on_w" '.sprintf("%s", (($valArray == $prefixForValue.'passion_on_w' ) ? "selected" : "")).'  style="font-family: \'Passion One\', cursive, font-weight: 700;" >Passion One Weight</option>
		<option value="'.$prefixForValue.'pathway_gothic_one" '.sprintf("%s", (($valArray == $prefixForValue.'pathway_gothic_one' ) ? "selected" : "")).'  style="font-family: \'Pathway Gothic One\', sans-serif;" >Pathway Gothic One</option>
		<option value="'.$prefixForValue.'petit_formal_script" '.sprintf("%s", (($valArray == $prefixForValue.'petit_formal_script' ) ? "selected" : "")).'  style="font-family: \'Petit Formal Script\', cursive;" >Petit Formal Script</option>
		<option value="'.$prefixForValue.'playball" '.sprintf("%s", (($valArray == $prefixForValue.'playball' ) ? "selected" : "")).'  style="font-family: \'Playball\', cursive;" >Playball</option>
		<option value="'.$prefixForValue.'playfair_display" '.sprintf("%s", (($valArray == $prefixForValue.'playfair_display' ) ? "selected" : "")).'  style="font-family: \'Playfair Display\', serif;" >Playfair Display</option>
		<option value="'.$prefixForValue.'playfair_disply_w" '.sprintf("%s", (($valArray == $prefixForValue.'playfair_disply_w' ) ? "selected" : "")).'  style="font-family: \'Playfair Display\', serif, font-weight: 900;" >Playfair Display Weight</option>
		<option value="'.$prefixForValue.'poiret_one" '.sprintf("%s", (($valArray == $prefixForValue.'poiret_one' ) ? "selected" : "")).'  style="font-family: \'Poiret One\', cursive;" >Poiret One</option>
		<option value="'.$prefixForValue.'pragati_narrow" '.sprintf("%s", (($valArray == $prefixForValue.'pragati_narrow' ) ? "selected" : "")).'  style="font-family: \'Pragati Narrow\', sans-serif;" >Pragati Narrow</option>
		<option value="'.$prefixForValue.'pragati_narrw_w" '.sprintf("%s", (($valArray == $prefixForValue.'pragati_narrw_w' ) ? "selected" : "")).'  style="font-family: \'Pragati Narrow\', sans-serif, font-weight: 700;" >Pragati Narrow Weight</option>
		<option value="'.$prefixForValue.'princess_sofia" '.sprintf("%s", (($valArray == $prefixForValue.'princess_sofia' ) ? "selected" : "")).'  style="font-family: \'Princess Sofia\', cursive;" >Princess Sofia</option>
		<option value="'.$prefixForValue.'prosto_one" '.sprintf("%s", (($valArray == $prefixForValue.'prosto_one' ) ? "selected" : "")).'  style="font-family: \'Prosto One\', cursive;" >Prosto One</option>
		<option value="'.$prefixForValue.'pt_sans" '.sprintf("%s", (($valArray == $prefixForValue.'pt_sans' ) ? "selected" : "")).'  style="font-family: \'PT Sans\', sans-serif;" >PT Sans</option>
		<option value="'.$prefixForValue.'pt_sns_w" '.sprintf("%s", (($valArray == $prefixForValue.'pt_sns_w' ) ? "selected" : "")).'  style="font-family: \'PT Sans\', sans-serif, font-weight: 700;" >PT Sans Weight</option>
		<option value="'.$prefixForValue.'pt_sns_nrrow" '.sprintf("%s", (($valArray == $prefixForValue.'pt_sns_nrrow' ) ? "selected" : "")).'  style="font-family: \'PT Sans Narrow\', sans-serif;" >PT Sans Narrow</option>
		<option value="'.$prefixForValue.'pt_sns_nrrw_w" '.sprintf("%s", (($valArray == $prefixForValue.'pt_sns_nrrw_w' ) ? "selected" : "")).'  style="font-family: \'PT Sans Narrow\', font-weight: 700;" >PT Sans Narrow Weight</option>
		<option value="'.$prefixForValue.'purple_purse" '.sprintf("%s", (($valArray == $prefixForValue.'purple_purse' ) ? "selected" : "")).'  style="font-family: \'Purple Purse\', cursive;" >Purple Purse</option>
		<option value="'.$prefixForValue.'quattrocento" '.sprintf("%s", (($valArray == $prefixForValue.'quattrocento' ) ? "selected" : "")).'  style="font-family: \'Quattrocento Sans\', sans-serif;" >Quattrocento Sans</option>
		<option value="'.$prefixForValue.'quattrocent_w" '.sprintf("%s", (($valArray == $prefixForValue.'quattrocent_w' ) ? "selected" : "")).'  style="font-family: \'Quattrocento Sans\', sans-serif, font-weight: 700;" >Quattrocento Sans Weight</option>
		<option value="'.$prefixForValue.'quintessential" '.sprintf("%s", (($valArray == $prefixForValue.'quintessential' ) ? "selected" : "")).'  style="font-family: \'Quintessential\', cursive;" >Quintessential</option>
		<option value="'.$prefixForValue.'racing_sans_one" '.sprintf("%s", (($valArray == $prefixForValue.'racing_sans_one' ) ? "selected" : "")).'  style="font-family: \'Racing Sans One\', cursive;" >Racing Sans One</option>
		<option value="'.$prefixForValue.'righteous" '.sprintf("%s", (($valArray == $prefixForValue.'righteous' ) ? "selected" : "")).'  style="font-family: \'Righteous\', cursive;" >Righteous</option>
		<option value="'.$prefixForValue.'roboto" '.sprintf("%s", (($valArray == $prefixForValue.'roboto' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif;" >Roboto</option>
		<option value="'.$prefixForValue.'robot_w" '.sprintf("%s", (($valArray == $prefixForValue.'robot_w' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif, font-weight: 700;" >Roboto Weight</option>
		<option value="'.$prefixForValue.'robt_ww" '.sprintf("%s", (($valArray == $prefixForValue.'robt_ww' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif, font-weight: 900;" >Roboto Weight+</option>
		<option value="'.$prefixForValue.'rubik_mono_one" '.sprintf("%s", (($valArray == $prefixForValue.'rubik_mono_one' ) ? "selected" : "")).'  style="font-family: \'Rubik Mono One\', sans-serif;" >Rubik Mono One</option>
		<option value="'.$prefixForValue.'sacramento" '.sprintf("%s", (($valArray == $prefixForValue.'sacramento' ) ? "selected" : "")).'  style="font-family: \'Sacramento\', cursive;" >Sacramento</option>
		<option value="'.$prefixForValue.'shadows_into_light_two" '.sprintf("%s", (($valArray == $prefixForValue.'shadows_into_light_two' ) ? "selected" : "")).'  style="font-family: \'Shadows Into Light Two\', cursive;" >Shadows Into Light Two</option>
		<option value="'.$prefixForValue.'signika" '.sprintf("%s", (($valArray == $prefixForValue.'signika' ) ? "selected" : "")).'  style="font-family: \'Signika\', sans-serif;" >Signika</option>
		<option value="'.$prefixForValue.'signik_w" '.sprintf("%s", (($valArray == $prefixForValue.'signik_w' ) ? "selected" : "")).'  style="font-family: \'Signika\', sans-serif, font-weight: 700;" >Signika Weight</option>
		<option value="'.$prefixForValue.'simonetta" '.sprintf("%s", (($valArray == $prefixForValue.'simonetta' ) ? "selected" : "")).'  style="font-family: \'Simonetta\', cursive;" >Simonetta</option>
		<option value="'.$prefixForValue.'simonett_w" '.sprintf("%s", (($valArray == $prefixForValue.'simonett_w' ) ? "selected" : "")).'  style="font-family: \'Simonetta\', cursive, font-weight: 900;" >Simonetta Weight</option>
		<option value="'.$prefixForValue.'sniglet" '.sprintf("%s", (($valArray == $prefixForValue.'sniglet' ) ? "selected" : "")).'  style="font-family: \'Sniglet\', cursive;" >Sniglet</option>
		<option value="'.$prefixForValue.'sniglt_w" '.sprintf("%s", (($valArray == $prefixForValue.'sniglt_w' ) ? "selected" : "")).'  style="font-family: \'Sniglet\', cursive, font-weight: 800;" >Sniglet Weight</option>
		<option value="'.$prefixForValue.'source_sans_pro" '.sprintf("%s", (($valArray == $prefixForValue.'source_sans_pro' ) ? "selected" : "")).'  style="font-family: \'Source Sans Pro\', sans-serif;" >Source Sans Pro</option>
		<option value="'.$prefixForValue.'source_sans_pr_w" '.sprintf("%s", (($valArray == $prefixForValue.'source_sans_pr_w' ) ? "selected" : "")).'  style="font-family: \'Source Sans Pro\', sans-serif, font-weight: 700;" >Source Sans Pro Weight</option>
		<option value="'.$prefixForValue.'stint_ultra_expanded" '.sprintf("%s", (($valArray == $prefixForValue.'stint_ultra_expanded' ) ? "selected" : "")).'  style="font-family: \'Stint Ultra Expanded\', cursive;" >Stint Ultra Expanded</option>
		<option value="'.$prefixForValue.'teko" '.sprintf("%s", (($valArray == $prefixForValue.'teko' ) ? "selected" : "")).'  style="font-family: \'Teko\', sans-serif;" >Teko</option>
		<option value="'.$prefixForValue.'tek_w" '.sprintf("%s", (($valArray == $prefixForValue.'tek_w' ) ? "selected" : "")).'  style="font-family: \'Teko\', sans-serif, font-weight: 700;" >Teko Weight</option>
		<option value="'.$prefixForValue.'text_me_one" '.sprintf("%s", (($valArray == $prefixForValue.'text_me_one' ) ? "selected" : "")).'  style="font-family: \'Text Me One\', sans-serif;" >Text Me One</option>
		<option value="'.$prefixForValue.'times_new_roman" '.sprintf("%s", (($valArray == $prefixForValue.'times_new_roman' ) ? "selected" : "")).'  style="font-family: \'times new roman\', Arial, sans-serif;" >Times new roman</option>
		<option value="'.$prefixForValue.'titan_one" '.sprintf("%s", (($valArray == $prefixForValue.'titan_one' ) ? "selected" : "")).'  style="font-family: \'Titan One\', cursive;" >Titan One</option>
		<option value="'.$prefixForValue.'trocchi" '.sprintf("%s", (($valArray == $prefixForValue.'trocchi' ) ? "selected" : "")).'  style="font-family: \'Trocchi\', serif;" >Trocchi</option>
		<option value="'.$prefixForValue.'ubuntu" '.sprintf("%s", (($valArray == $prefixForValue.'ubuntu' ) ? "selected" : "")).'  style="font-family: \'Ubuntu\', sans-serif;" >Ubuntu</option>
		<option value="'.$prefixForValue.'ubunt_w" '.sprintf("%s", (($valArray == $prefixForValue.'ubunt_w' ) ? "selected" : "")).'  style="font-family: \'Ubuntu\', sans-serif, font-weight: 700;" >Ubuntu Weight</option>
		<option value="'.$prefixForValue.'unica_one" '.sprintf("%s", (($valArray == $prefixForValue.'unica_one' ) ? "selected" : "")).'  style="font-family: \'Unica One\', cursive;" >Unica One</option>
		<option value="'.$prefixForValue.'varela" '.sprintf("%s", (($valArray == $prefixForValue.'varela' ) ? "selected" : "")).'  style="font-family: \'Varela\', sans-serif;" >Varela</option>
		<option value="'.$prefixForValue.'verdana" '.sprintf("%s", (($valArray == $prefixForValue.'verdana' ) ? "selected" : "")).'  style="font-family: \'Verdana\', Arial, sans-serif;" >Verdana</option>
		<option value="'.$prefixForValue.'viga" '.sprintf("%s", (($valArray == $prefixForValue.'viga' ) ? "selected" : "")).'  style="font-family: \'Viga\', sans-serif;" >Viga</option>
		<option value="'.$prefixForValue.'warnes" '.sprintf("%s", (($valArray == $prefixForValue.'warnes' ) ? "selected" : "")).'  style="font-family: \'Warnes\', cursive;" >Warnes</option>
		</optgroup>
		
		<optgroup label="Cyrillic">
		<option value="'.$prefixForValue.'andika" '.sprintf("%s", (($valArray == $prefixForValue.'andika' ) ? "selected" : "")).'  style="font-family: \'Andika\', sans-serif;" >Andika</option>
		<option value="'.$prefixForValue.'arial" '.sprintf("%s", (($valArray == $prefixForValue.'arial' ) ? "selected" : "")).'  style="font-family: \'Arial\', Helvetica, sans-serif;" >Arial</option>
		<option value="'.$prefixForValue.'ar_w" '.sprintf("%s", (($valArray == $prefixForValue.'ar_w' ) ? "selected" : "")).'  style="font-family: \'Arial\', Helvetica, sans-serif, font-weight: 700;" >Arial Weight</option>
		<option value="'.$prefixForValue.'comfortaa" '.sprintf("%s", (($valArray == $prefixForValue.'comfortaa' ) ? "selected" : "")).'  style="font-family: \'Comfortaa\', cursive;" >Comfortaa</option>
		<option value="'.$prefixForValue.'comfort_w" '.sprintf("%s", (($valArray == $prefixForValue.'comfort_w' ) ? "selected" : "")).'  style="font-family: \'Comfortaa\', cursive, font-weight: 700;" >Comfortaa Weight</option>
		<option value="'.$prefixForValue.'courier_new" '.sprintf("%s", (($valArray == $prefixForValue.'courier_new' ) ? "selected" : "")).'  style="font-family: \'courier_new\', \'times new roman\', sans-serif;" >Courier New</option>
		<option value="'.$prefixForValue.'cousine" '.sprintf("%s", (($valArray == $prefixForValue.'cousine' ) ? "selected" : "")).'  style="font-family: \'Cousine\', ;" >Cousine</option>
		<option value="'.$prefixForValue.'cousin_w" '.sprintf("%s", (($valArray == $prefixForValue.'cousin_w' ) ? "selected" : "")).'  style="font-family: \'Cousine\', font-weight: 700;" >Cousine Weight</option>
		<option value="'.$prefixForValue.'didact_gothic" '.sprintf("%s", (($valArray == $prefixForValue.'didact_gothic' ) ? "selected" : "")).'  style="font-family: \'Didact Gothic\', sans-serif;" >Didact Gothic</option>
		<option value="'.$prefixForValue.'didact_gothc_w" '.sprintf("%s", (($valArray == $prefixForValue.'didact_gothc_w' ) ? "selected" : "")).'  style="font-family: \'Didact Gothic\', sans-serif, font-weight: 700;" >Didact Gothic Weight</option>
		<option value="'.$prefixForValue.'eb_garamond" '.sprintf("%s", (($valArray == $prefixForValue.'eb_garamond' ) ? "selected" : "")).'  style="font-family: \'EB Garamond\', serif;" >EB Garamond</option>
		<option value="'.$prefixForValue.'eb_garamnd_w" '.sprintf("%s", (($valArray == $prefixForValue.'eb_garamnd_w' ) ? "selected" : "")).'  style="font-family: \'EB Garamond\', serif, font-weight: 700;" >EB Garamond Weight</option>
		<option value="'.$prefixForValue.'exx2" '.sprintf("%s", (($valArray == $prefixForValue.'exx2' ) ? "selected" : "")).'  style="font-family: \'Exo 2\', sans-serif;" >Exo 2</option>
		<option value="'.$prefixForValue.'exxx2_w" '.sprintf("%s", (($valArray == $prefixForValue.'exxx2_w' ) ? "selected" : "")).'  style="font-family: \'Exo 2\', sans-serif, font-weight: 700;" >Exo 2 Weight</option>
		<option value="'.$prefixForValue.'fira_sans" '.sprintf("%s", (($valArray == $prefixForValue.'fira_sans' ) ? "selected" : "")).'  style="font-family: \'Fira Sans\', sans-serif;" >Fira Sans</option>
		<option value="'.$prefixForValue.'fira_sns_w" '.sprintf("%s", (($valArray == $prefixForValue.'fira_sns_w' ) ? "selected" : "")).'  style="font-family: \'Fira Sans\', sans-serif, font-weight: 700;" >Fira Sans Weight</option>
		<option value="'.$prefixForValue.'georgia" '.sprintf("%s", (($valArray == $prefixForValue.'georgia' ) ? "selected" : "")).'  style="font-family: \'georgia\', \'times new roman\', Arial, sans-serif;" >Georgia</option>		
		<option value="'.$prefixForValue.'helvetica" '.sprintf("%s", (($valArray == $prefixForValue.'helvetica' ) ? "selected" : "")).'  style="font-family: \'helvetica\', arial, \'open sans\', sans-serif;" >Helvetica</option>
		<option value="'.$prefixForValue.'helvetic_w" '.sprintf("%s", (($valArray == $prefixForValue.'helvetic_w' ) ? "selected" : "")).'  style="font-family: \'helvetica\', arial, \'open sans\', sans-serif, font-weight: 700;" >Helvetica Weight</option>
		<option value="'.$prefixForValue.'jura" '.sprintf("%s", (($valArray == $prefixForValue.'jura' ) ? "selected" : "")).'  style="font-family: \'Jura\', sans-serif;" >Jura</option>
		<option value="'.$prefixForValue.'jur_w" '.sprintf("%s", (($valArray == $prefixForValue.'jur_w' ) ? "selected" : "")).'  style="font-family: \'Jura\', sans-serif, font-weight: 500;" >Jura Weight</option>
		<option value="'.$prefixForValue.'marmelad" '.sprintf("%s", (($valArray == $prefixForValue.'marmelad' ) ? "selected" : "")).'  style="font-family: \'Marmelad\', sans-serif;" >Marmelad</option>
		<option value="'.$prefixForValue.'marmeld_w" '.sprintf("%s", (($valArray == $prefixForValue.'marmeld_w' ) ? "selected" : "")).'  style="font-family: \'Marmelad\', sans-serif, font-weight: 700;" >Marmelad Weight</option>
		<option value="'.$prefixForValue.'neucha" '.sprintf("%s", (($valArray == $prefixForValue.'neucha' ) ? "selected" : "")).'  style="font-family: \'Neucha\', cursive;" >Neucha</option>
		<option value="'.$prefixForValue.'noto_serif" '.sprintf("%s", (($valArray == $prefixForValue.'noto_serif' ) ? "selected" : "")).'  style="font-family: \'Noto Serif\', serif;" >Noto Serif</option>
		<option value="'.$prefixForValue.'noto_serf_w" '.sprintf("%s", (($valArray == $prefixForValue.'noto_serf_w' ) ? "selected" : "")).'  style="font-family: \'Noto Serif\', serif, font-weight: 700;" >Noto Serif Weight</option>
		<option value="'.$prefixForValue.'open_sans" '.sprintf("%s", (($valArray == $prefixForValue.'open_sans' ) ? "selected" : "")).'  style="font-family: \'Open Sans\', sans-serif;" >Open Sans</option>
		<option value="'.$prefixForValue.'open_sns_w" '.sprintf("%s", (($valArray == $prefixForValue.'open_sns_w' ) ? "selected" : "")).'  style="font-family: \'Open Sans\', sans-serif, font-weight: 700;" >Open Sans Weight</option>
		<option value="'.$prefixForValue.'oranienbaum" '.sprintf("%s", (($valArray == $prefixForValue.'oranienbaum' ) ? "selected" : "")).'  style="font-family: \'Oranienbaum\', serif;" >Oranienbaum</option>
		<option value="'.$prefixForValue.'oranienbam_w" '.sprintf("%s", (($valArray == $prefixForValue.'oranienbam_w' ) ? "selected" : "")).'  style="font-family: \'Oranienbaum\', serif;" >Oranienbaum Weight</option>
		<option value="'.$prefixForValue.'philosopher" '.sprintf("%s", (($valArray == $prefixForValue.'philosopher' ) ? "selected" : "")).'  style="font-family: \'Philosopher\', sans-serif;" >Philosopher</option>
		<option value="'.$prefixForValue.'philosophr_w" '.sprintf("%s", (($valArray == $prefixForValue.'philosophr_w' ) ? "selected" : "")).'  style="font-family: \'Philosopher\', font-weight: 700;" >Philosopher Weight</option>
		<option value="'.$prefixForValue.'play" '.sprintf("%s", (($valArray == $prefixForValue.'play' ) ? "selected" : "")).'  style="font-family: \'Play\', sans-serif;" >Play</option>
		<option value="'.$prefixForValue.'ply_w" '.sprintf("%s", (($valArray == $prefixForValue.'ply_w' ) ? "selected" : "")).'  style="font-family: \'Play\', sans-serif, font-weight: 700;" >Play Weight</option>
		<option value="'.$prefixForValue.'pt_mono" '.sprintf("%s", (($valArray == $prefixForValue.'pt_mono' ) ? "selected" : "")).'  style="font-family: \'PT Mono\', ;" >PT Mono</option>
		<option value="'.$prefixForValue.'pt_sans" '.sprintf("%s", (($valArray == $prefixForValue.'pt_sans' ) ? "selected" : "")).'  style="font-family: \'PT Sans\', sans-serif;" >PT Sans</option>
		<option value="'.$prefixForValue.'pt_sns_nrrow" '.sprintf("%s", (($valArray == $prefixForValue.'pt_sns_nrrow' ) ? "selected" : "")).'  style="font-family: \'PT Sans Narrow\', sans-serif;" >PT Sans Narrow</option>
		<option value="'.$prefixForValue.'pt_sns_nrrw_w" '.sprintf("%s", (($valArray == $prefixForValue.'pt_sns_nrrw_w' ) ? "selected" : "")).'  style="font-family: \'PT Sans Narrow\', font-weight: 700;" >PT Sans Narrow Weight</option>
		<option value="'.$prefixForValue.'pt_sns_w" '.sprintf("%s", (($valArray == $prefixForValue.'pt_sns_w' ) ? "selected" : "")).'  style="font-family: \'PT Sans\', sans-serif, font-weight: 700;" >PT Sans Weight</option>
		<option value="'.$prefixForValue.'pt_serif_caption" '.sprintf("%s", (($valArray == $prefixForValue.'pt_serif_caption' ) ? "selected" : "")).'  style="font-family: \'PT Serif Caption\', serif;" >PT Serif Caption</option>
		<option value="'.$prefixForValue.'roboto" '.sprintf("%s", (($valArray == $prefixForValue.'roboto' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif;" >Roboto</option>
		<option value="'.$prefixForValue.'robot_w" '.sprintf("%s", (($valArray == $prefixForValue.'robot_w' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif, font-weight: 700;" >Roboto Weight</option>
		<option value="'.$prefixForValue.'robt_ww" '.sprintf("%s", (($valArray == $prefixForValue.'robt_ww' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif, font-weight: 900;" >Roboto Weight+</option>
		<option value="'.$prefixForValue.'rubik_one" '.sprintf("%s", (($valArray == $prefixForValue.'rubik_one' ) ? "selected" : "")).'  style="font-family: \'Rubik One\', sans-serif;" >Rubik One</option>
		<option value="'.$prefixForValue.'ruslan_display" '.sprintf("%s", (($valArray == $prefixForValue.'ruslan_display' ) ? "selected" : "")).'  style="font-family: \'Ruslan Display\', cursive;" >Ruslan Display</option>
		<option value="'.$prefixForValue.'russo_one" '.sprintf("%s", (($valArray == $prefixForValue.'russo_one' ) ? "selected" : "")).'  style="font-family: \'Russo One\', sans-serif;" >Russo One</option>
		<option value="'.$prefixForValue.'times_new_roman" '.sprintf("%s", (($valArray == $prefixForValue.'times_new_roman' ) ? "selected" : "")).'  style="font-family: \'times new roman\', Arial, sans-serif;" >Times new roman</option>
		<option value="'.$prefixForValue.'tinos" '.sprintf("%s", (($valArray == $prefixForValue.'tinos' ) ? "selected" : "")).'  style="font-family: \'Tinos\', serif;" >Tinos</option>
		<option value="'.$prefixForValue.'tins_w" '.sprintf("%s", (($valArray == $prefixForValue.'tins_w' ) ? "selected" : "")).'  style="font-family: \'Tinos\', serif, font-weight: 700;" >Tinos Weight</option>
		<option value="'.$prefixForValue.'ubuntu" '.sprintf("%s", (($valArray == $prefixForValue.'ubuntu' ) ? "selected" : "")).'  style="font-family: \'Ubuntu\', sans-serif;" >Ubuntu</option>
		<option value="'.$prefixForValue.'ubunt_mono" '.sprintf("%s", (($valArray == $prefixForValue.'ubunt_mono' ) ? "selected" : "")).'  style="font-family: \'Ubuntu Mono\', ;" >Ubuntu Mono</option>
		<option value="'.$prefixForValue.'ubunt_mon_w" '.sprintf("%s", (($valArray == $prefixForValue.'ubunt_mon_w' ) ? "selected" : "")).'  style="font-family: \'Ubuntu Mono\', font-weight: 700;" >Ubuntu Mono Weight</option>
		<option value="'.$prefixForValue.'ubunt_w" '.sprintf("%s", (($valArray == $prefixForValue.'ubunt_w' ) ? "selected" : "")).'  style="font-family: \'Ubuntu\', sans-serif, font-weight: 700;" >Ubuntu Weight</option>		
		<option value="'.$prefixForValue.'verdana" '.sprintf("%s", (($valArray == $prefixForValue.'verdana' ) ? "selected" : "")).'  style="font-family: \'Verdana\', Arial, sans-serif;" >Verdana</option>
		<option value="'.$prefixForValue.'yeseva_one" '.sprintf("%s", (($valArray == $prefixForValue.'yeseva_one' ) ? "selected" : "")).'  style="font-family: \'Yeseva One\', cursive;" >Yeseva One</option>
		<option value="'.$prefixForValue.'yeseva_on_w" '.sprintf("%s", (($valArray == $prefixForValue.'yeseva_on_w' ) ? "selected" : "")).'  style="font-family: \'Yeseva One\', cursive, font-weight: 700;" >Yeseva One Weight</option>
		</optgroup>
		
		<optgroup label="Arabic">
		<option value="'.$prefixForValue.'amiri" '.sprintf("%s", (($valArray == $prefixForValue.'amiri' ) ? "selected" : "")).'  style="font-family: \'Amiri\', serif;" >Amiri</option>
		<option value="'.$prefixForValue.'amir_w" '.sprintf("%s", (($valArray == $prefixForValue.'amir_w' ) ? "selected" : "")).'  style="font-family: \'Amiri\', serif, font-weight: 700;" >Amiri Weight</option>
		<option value="'.$prefixForValue.'lateef" '.sprintf("%s", (($valArray == $prefixForValue.'lateef' ) ? "selected" : "")).'  style="font-family: \'Lateef\', cursive;" >Lateef</option>		
		<option value="'.$prefixForValue.'scheherazade" '.sprintf("%s", (($valArray == $prefixForValue.'scheherazade' ) ? "selected" : "")).'  style="font-family: \'Scheherazade\', serif;" >Scheherazade</option>
		<option value="'.$prefixForValue.'scheherazad_w" '.sprintf("%s", (($valArray == $prefixForValue.'scheherazad_w' ) ? "selected" : "")).'  style="font-family: \'Scheherazade\', serif, font-weight: 700;" >Scheherazade Weight</option>
		</optgroup>
		
		<optgroup label="Devanagari">
		<option value="'.$prefixForValue.'amiri" '.sprintf("%s", (($valArray == $prefixForValue.'amiri' ) ? "selected" : "")).'  style="font-family: \'Amiri\', serif;" >Amiri</option>
		<option value="'.$prefixForValue.'amir_w" '.sprintf("%s", (($valArray == $prefixForValue.'amir_w' ) ? "selected" : "")).'  style="font-family: \'Amiri\', serif, font-weight: 700;" >Amiri Weight</option>
		<option value="'.$prefixForValue.'arya" '.sprintf("%s", (($valArray == $prefixForValue.'arya' ) ? "selected" : "")).'  style="font-family: \'Arya\', sans-serif;" >Arya</option>
		<option value="'.$prefixForValue.'ary_w" '.sprintf("%s", (($valArray == $prefixForValue.'ary_w' ) ? "selected" : "")).'  style="font-family: \'Arya\', sans-serif, font-weight: 700;" >Arya Weight</option>
		<option value="'.$prefixForValue.'biryani" '.sprintf("%s", (($valArray == $prefixForValue.'biryani' ) ? "selected" : "")).'  style="font-family: \'Biryani\', sans-serif;" >Biryani</option>
		<option value="'.$prefixForValue.'biryan_w" '.sprintf("%s", (($valArray == $prefixForValue.'biryan_w' ) ? "selected" : "")).'  style="font-family: \'Biryani\', sans-serif, font-weight: 700;" >Biryani Weight</option>
		<option value="'.$prefixForValue.'cambay" '.sprintf("%s", (($valArray == $prefixForValue.'cambay' ) ? "selected" : "")).'  style="font-family: \'Cambay\', sans-serif;" >Cambay</option>
		<option value="'.$prefixForValue.'camby_w" '.sprintf("%s", (($valArray == $prefixForValue.'camby_w' ) ? "selected" : "")).'  style="font-family: \'Cambay\', sans-serif, font-weight: 700;" >Cambay Weight</option>
		<option value="'.$prefixForValue.'dekko" '.sprintf("%s", (($valArray == $prefixForValue.'dekko' ) ? "selected" : "")).'  style="font-family: \'Dekko\', cursive;" >Dekko</option>
		<option value="'.$prefixForValue.'ekmukta" '.sprintf("%s", (($valArray == $prefixForValue.'ekmukta' ) ? "selected" : "")).'  style="font-family: \'Ek Mukta\', sans-serif;" >Ek Mukta</option>
		<option value="'.$prefixForValue.'ekmukt_w" '.sprintf("%s", (($valArray == $prefixForValue.'ekmukt_w' ) ? "selected" : "")).'  style="font-family: \'Ek Mukta\', sans-serif, font-weight: 700;" >Ek Mukta Weight</option>
		<option value="'.$prefixForValue.'glegoo" '.sprintf("%s", (($valArray == $prefixForValue.'glegoo' ) ? "selected" : "")).'  style="font-family: \'Glegoo\', serif;" >Glegoo</option>
		<option value="'.$prefixForValue.'gleg_w" '.sprintf("%s", (($valArray == $prefixForValue.'gleg_w' ) ? "selected" : "")).'  style="font-family: \'Glegoo\', serif, font-weight: 700;" >Glegoo Weight</option>
		<option value="'.$prefixForValue.'halant" '.sprintf("%s", (($valArray == $prefixForValue.'halant' ) ? "selected" : "")).'  style="font-family: \'Halant\', serif;" >Halant</option>
		<option value="'.$prefixForValue.'halnt_w" '.sprintf("%s", (($valArray == $prefixForValue.'halnt_w' ) ? "selected" : "")).'  style="font-family: \'Halant\', serif, font-weight: 700;" >Halant Weight</option>
		<option value="'.$prefixForValue.'hind" '.sprintf("%s", (($valArray == $prefixForValue.'hind' ) ? "selected" : "")).'  style="font-family: \'Hind\', sans-serif;" >Hind</option>
		<option value="'.$prefixForValue.'hnd_w" '.sprintf("%s", (($valArray == $prefixForValue.'hnd_w' ) ? "selected" : "")).'  style="font-family: \'Hind\', sans-serif, font-weight: 700;" >Hind Weight</option>
		<option value="'.$prefixForValue.'inknut_antiqua" '.sprintf("%s", (($valArray == $prefixForValue.'inknut_antiqua' ) ? "selected" : "")).'  style="font-family: \'Inknut Antiqua\', serif;" >Inknut Antiqua</option>
		<option value="'.$prefixForValue.'inknut_antiq_w" '.sprintf("%s", (($valArray == $prefixForValue.'inknut_antiq_w' ) ? "selected" : "")).'  style="font-family: \'Inknut Antiqua\', serif, font-weight: 700;" >Inknut Antiqua Weight</option>
		<option value="'.$prefixForValue.'karma" '.sprintf("%s", (($valArray == $prefixForValue.'karma' ) ? "selected" : "")).'  style="font-family: \'Karma\', serif;" >Karma</option>
		<option value="'.$prefixForValue.'karm_w" '.sprintf("%s", (($valArray == $prefixForValue.'karm_w' ) ? "selected" : "")).'  style="font-family: \'Karma\', serif, font-weight: 700;" >Karma Weight</option>
		<option value="'.$prefixForValue.'khand" '.sprintf("%s", (($valArray == $prefixForValue.'khand' ) ? "selected" : "")).'  style="font-family: \'Khand\', sans-serif;" >Khand</option>
		<option value="'.$prefixForValue.'khnd_w" '.sprintf("%s", (($valArray == $prefixForValue.'khnd_w' ) ? "selected" : "")).'  style="font-family: \'Khand\', sans-serif, font-weight: 700;" >Khand Weight</option>
		<option value="'.$prefixForValue.'khula" '.sprintf("%s", (($valArray == $prefixForValue.'khula' ) ? "selected" : "")).'  style="font-family: \'Khula\', sans-serif;" >Khula</option>
		<option value="'.$prefixForValue.'khul_w" '.sprintf("%s", (($valArray == $prefixForValue.'khul_w' ) ? "selected" : "")).'  style="font-family: \'Khula\', sans-serif, font-weight: 700;" >Khula Weight</option>
		<option value="'.$prefixForValue.'kurale" '.sprintf("%s", (($valArray == $prefixForValue.'kurale' ) ? "selected" : "")).'  style="font-family: \'Kurale\', serif;" >Kurale</option>
		<option value="'.$prefixForValue.'laila" '.sprintf("%s", (($valArray == $prefixForValue.'laila' ) ? "selected" : "")).'  style="font-family: \'Laila\', serif;" >Laila</option>
		<option value="'.$prefixForValue.'lail_w" '.sprintf("%s", (($valArray == $prefixForValue.'lail_w' ) ? "selected" : "")).'  style="font-family: \'Laila\', serif, font-weight: 700;" >Laila Weight</option>		
		<option value="'.$prefixForValue.'lateef" '.sprintf("%s", (($valArray == $prefixForValue.'lateef' ) ? "selected" : "")).'  style="font-family: \'Lateef\', cursive;" >Lateef</option>
		<option value="'.$prefixForValue.'martel" '.sprintf("%s", (($valArray == $prefixForValue.'martel' ) ? "selected" : "")).'  style="font-family: \'Martel\', serif;" >Martel</option>
		<option value="'.$prefixForValue.'martl_w" '.sprintf("%s", (($valArray == $prefixForValue.'martl_w' ) ? "selected" : "")).'  style="font-family: \'Martel\', serif, font-weight: 700;" >Martel Weight</option>				
		<option value="'.$prefixForValue.'noto_sans" '.sprintf("%s", (($valArray == $prefixForValue.'noto_sans' ) ? "selected" : "")).'  style="font-family: \'Noto Sans\', sans-serif;" >Noto Sans</option>
		<option value="'.$prefixForValue.'noto_sns_w" '.sprintf("%s", (($valArray == $prefixForValue.'noto_sns_w' ) ? "selected" : "")).'  style="font-family: \'Noto Sans\', sans-serif, font-weight: 700;" >Noto Sans Weight</option>
		<option value="'.$prefixForValue.'palanquin_dark" '.sprintf("%s", (($valArray == $prefixForValue.'palanquin_dark' ) ? "selected" : "")).'  style="font-family: \'Palanquin Dark\', sans-serif;" >Palanquin Dark</option>
		<option value="'.$prefixForValue.'palanquin_drk_w" '.sprintf("%s", (($valArray == $prefixForValue.'palanquin_drk_w' ) ? "selected" : "")).'  style="font-family: \'Palanquin Dark\', sans-serif, font-weight: 700;" >Palanquin Dark Weight</option>
		<option value="'.$prefixForValue.'rajdhani" '.sprintf("%s", (($valArray == $prefixForValue.'rajdhani' ) ? "selected" : "")).'  style="font-family: \'Rajdhani\', sans-serif;" >Rajdhani</option>
		<option value="'.$prefixForValue.'rajdhan_w" '.sprintf("%s", (($valArray == $prefixForValue.'rajdhan_w' ) ? "selected" : "")).'  style="font-family: \'Rajdhani\', sans-serif, font-weight: 700;" >Rajdhani Weight</option>
		<option value="'.$prefixForValue.'rozha_one" '.sprintf("%s", (($valArray == $prefixForValue.'rozha_one' ) ? "selected" : "")).'  style="font-family: \'Rozha One\', serif;" >Rozha One</option>
		<option value="'.$prefixForValue.'sahitya" '.sprintf("%s", (($valArray == $prefixForValue.'sahitya' ) ? "selected" : "")).'  style="font-family: \'Sahitya\', serif;" >Sahitya</option>
		<option value="'.$prefixForValue.'sahit_w" '.sprintf("%s", (($valArray == $prefixForValue.'sahit_w' ) ? "selected" : "")).'  style="font-family: \'Sahitya\', serif, font-weight: 700;" >Sahitya Weight</option>
		<option value="'.$prefixForValue.'sarpanch" '.sprintf("%s", (($valArray == $prefixForValue.'sarpanch' ) ? "selected" : "")).'  style="font-family: \'Sarpanch\', sans-serif;" >Sarpanch</option>
		<option value="'.$prefixForValue.'sarpnch_w" '.sprintf("%s", (($valArray == $prefixForValue.'sarpnch_w' ) ? "selected" : "")).'  style="font-family: \'Sarpanch\', sans-serif, font-weight: 700;" >Sarpanch Weight</option>		
		<option value="'.$prefixForValue.'scheherazade" '.sprintf("%s", (($valArray == $prefixForValue.'scheherazade' ) ? "selected" : "")).'  style="font-family: \'Scheherazade\', serif;" >Scheherazade</option>
		<option value="'.$prefixForValue.'scheherazad_w" '.sprintf("%s", (($valArray == $prefixForValue.'scheherazad_w' ) ? "selected" : "")).'  style="font-family: \'Scheherazade\', serif, font-weight: 700;" >Scheherazade Weight</option>
		<option value="'.$prefixForValue.'tillana" '.sprintf("%s", (($valArray == $prefixForValue.'tillana' ) ? "selected" : "")).'  style="font-family: \'Tillana\', cursive;" >Tillana</option>
		<option value="'.$prefixForValue.'tillan_w" '.sprintf("%s", (($valArray == $prefixForValue.'tillan_w' ) ? "selected" : "")).'  style="font-family: \'Tillana\', cursive, font-weight: 700;" >Tillana Weight</option>
		<option value="'.$prefixForValue.'vesper_libre" '.sprintf("%s", (($valArray == $prefixForValue.'vesper_libre' ) ? "selected" : "")).'  style="font-family: \'Vesper Libre\', serif;" >Vesper Libre</option>
		<option value="'.$prefixForValue.'vesper_libr_w" '.sprintf("%s", (($valArray == $prefixForValue.'vesper_libr_w' ) ? "selected" : "")).'  style="font-family: \'Vesper Libre\', serif, font-weight: 700;" >Vesper Libre Weight</option>
		<option value="'.$prefixForValue.'yantramanav" '.sprintf("%s", (($valArray == $prefixForValue.'yantramanav' ) ? "selected" : "")).'  style="font-family: \'Yantramanav\', sans-serif;" >Yantramanav</option>
		<option value="'.$prefixForValue.'yantramanv_w" '.sprintf("%s", (($valArray == $prefixForValue.'yantramanv_w' ) ? "selected" : "")).'  style="font-family: \'Yantramanav\', sans-serif, font-weight: 700;" >Yantramanav Weight</option>
		</optgroup>
		
		<optgroup label="Greek">
		<option value="'.$prefixForValue.'advent_pro" '.sprintf("%s", (($valArray == $prefixForValue.'advent_pro' ) ? "selected" : "")).'  style="font-family: \'Advent Pro\', sans-serif;" >Advent Pro</option>
		<option value="'.$prefixForValue.'advent_pr_w" '.sprintf("%s", (($valArray == $prefixForValue.'advent_pr_w' ) ? "selected" : "")).'  style="font-family: \'Advent Pro\', sans-serif, font-weight: 700;" >Advent Pro Weight</option>
		<option value="'.$prefixForValue.'arimo" '.sprintf("%s", (($valArray == $prefixForValue.'arimo' ) ? "selected" : "")).'  style="font-family: \'Arimo\', sans-serif;" >Arimo</option>
		<option value="'.$prefixForValue.'armo_w" '.sprintf("%s", (($valArray == $prefixForValue.'armo_w' ) ? "selected" : "")).'  style="font-family: \'Arimo\', sans-serif, font-weight: 700;;" >Arimo Weight</option>
		<option value="'.$prefixForValue.'cardo" '.sprintf("%s", (($valArray == $prefixForValue.'cardo' ) ? "selected" : "")).'  style="font-family: \'Cardo\', serif;" >Cardo</option>
		<option value="'.$prefixForValue.'card_w" '.sprintf("%s", (($valArray == $prefixForValue.'card_w' ) ? "selected" : "")).'  style="font-family: \'Cardo\', font-weight: 700;" >Cardo Weight</option>
		<option value="'.$prefixForValue.'caudex" '.sprintf("%s", (($valArray == $prefixForValue.'caudex' ) ? "selected" : "")).'  style="font-family: \'Caudex\', serif;" >Caudex</option>
		<option value="'.$prefixForValue.'caudx_w" '.sprintf("%s", (($valArray == $prefixForValue.'caudx_w' ) ? "selected" : "")).'  style="font-family: \'Caudex\', serif, font-weight: 700;" >Caudex Weight</option>
		<option value="'.$prefixForValue.'comfortaa" '.sprintf("%s", (($valArray == $prefixForValue.'comfortaa' ) ? "selected" : "")).'  style="font-family: \'Comfortaa\', cursive;" >Comfortaa</option>
		<option value="'.$prefixForValue.'comfort_w" '.sprintf("%s", (($valArray == $prefixForValue.'comfort_w' ) ? "selected" : "")).'  style="font-family: \'Comfortaa\', cursive, font-weight: 700;" >Comfortaa Weight</option>
		<option value="'.$prefixForValue.'didact_gothic" '.sprintf("%s", (($valArray == $prefixForValue.'didact_gothic' ) ? "selected" : "")).'  style="font-family: \'Didact Gothic\', sans-serif;" >Didact Gothic</option>
		<option value="'.$prefixForValue.'didact_gothc_w" '.sprintf("%s", (($valArray == $prefixForValue.'didact_gothc_w' ) ? "selected" : "")).'  style="font-family: \'Didact Gothic\', sans-serif, font-weight: 700;" >Didact Gothic Weight</option>
		<option value="'.$prefixForValue.'fira_mono" '.sprintf("%s", (($valArray == $prefixForValue.'fira_mono' ) ? "selected" : "")).'  style="font-family: \'Fira Mono\', ;" >Fira Mono</option>
		<option value="'.$prefixForValue.'fira_mon_w" '.sprintf("%s", (($valArray == $prefixForValue.'fira_mon_w' ) ? "selected" : "")).'  style="font-family: \'Fira Mono\', font-weight: 700;" >Fira Mono Weight</option>
		<option value="'.$prefixForValue.'fira_sans" '.sprintf("%s", (($valArray == $prefixForValue.'fira_sans' ) ? "selected" : "")).'  style="font-family: \'Fira Sans\', sans-serif;" >Fira Sans</option>
		<option value="'.$prefixForValue.'fira_sns_w" '.sprintf("%s", (($valArray == $prefixForValue.'fira_sns_w' ) ? "selected" : "")).'  style="font-family: \'Fira Sans\', sans-serif, font-weight: 700;" >Fira Sans Weight</option>
		<option value="'.$prefixForValue.'nova_mono" '.sprintf("%s", (($valArray == $prefixForValue.'nova_mono' ) ? "selected" : "")).'  style="font-family: \'Nova Mono\', ;" >Nova Mono</option>
		<option value="'.$prefixForValue.'open_sans" '.sprintf("%s", (($valArray == $prefixForValue.'open_sans' ) ? "selected" : "")).'  style="font-family: \'Open Sans\', sans-serif;" >Open Sans</option>
		<option value="'.$prefixForValue.'open_sans_condensed" '.sprintf("%s", (($valArray == $prefixForValue.'open_sans_condensed' ) ? "selected" : "")).'  style="font-family: \'Open Sans Condensed\', sans-serif;" >Open Sans Condensed</option>
		<option value="'.$prefixForValue.'open_sans_condensd_w" '.sprintf("%s", (($valArray == $prefixForValue.'open_sans_condensd_w' ) ? "selected" : "")).'  style="font-family: \'Open Sans Condensed\', sans-serif, font-weight: 700;" >Open Sans Condensed Weight</option>
		<option value="'.$prefixForValue.'open_sns_w" '.sprintf("%s", (($valArray == $prefixForValue.'open_sns_w' ) ? "selected" : "")).'  style="font-family: \'Open Sans\', sans-serif, font-weight: 700;" >Open Sans Weight</option>
		<option value="'.$prefixForValue.'play" '.sprintf("%s", (($valArray == $prefixForValue.'play' ) ? "selected" : "")).'  style="font-family: \'Play\', sans-serif;" >Play</option>
		<option value="'.$prefixForValue.'ply_w" '.sprintf("%s", (($valArray == $prefixForValue.'ply_w' ) ? "selected" : "")).'  style="font-family: \'Play\', sans-serif, font-weight: 700;" >Play Weight</option>
		<option value="'.$prefixForValue.'roboto" '.sprintf("%s", (($valArray == $prefixForValue.'roboto' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif;" >Roboto</option>
		<option value="'.$prefixForValue.'roboto_condensed" '.sprintf("%s", (($valArray == $prefixForValue.'roboto_condensed' ) ? "selected" : "")).'  style="font-family: \'Roboto Condensed\', sans-serif;" >Roboto Condensed</option>
		<option value="'.$prefixForValue.'roboto_condensd_w" '.sprintf("%s", (($valArray == $prefixForValue.'roboto_condensd_w' ) ? "selected" : "")).'  style="font-family: \'Roboto Condensed\', sans-serif, font-weight: 700;" >Roboto Condensed Weight</option>
		<option value="'.$prefixForValue.'robot_w" '.sprintf("%s", (($valArray == $prefixForValue.'robot_w' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif, font-weight: 700;" >Roboto Weight</option>
		<option value="'.$prefixForValue.'robt_ww" '.sprintf("%s", (($valArray == $prefixForValue.'robt_ww' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif, font-weight: 900;" >Roboto Weight+</option>
		<option value="'.$prefixForValue.'tinos" '.sprintf("%s", (($valArray == $prefixForValue.'tinos' ) ? "selected" : "")).'  style="font-family: \'Tinos\', serif;" >Tinos</option>
		<option value="'.$prefixForValue.'tins_w" '.sprintf("%s", (($valArray == $prefixForValue.'tins_w' ) ? "selected" : "")).'  style="font-family: \'Tinos\', serif, font-weight: 700;" >Tinos Weight</option>		
		<option value="'.$prefixForValue.'ubuntu" '.sprintf("%s", (($valArray == $prefixForValue.'ubuntu' ) ? "selected" : "")).'  style="font-family: \'Ubuntu\', sans-serif;" >Ubuntu</option>
		<option value="'.$prefixForValue.'ubunt_mono" '.sprintf("%s", (($valArray == $prefixForValue.'ubunt_mono' ) ? "selected" : "")).'  style="font-family: \'Ubuntu Mono\', ;" >Ubuntu Mono</option>
		<option value="'.$prefixForValue.'ubunt_mon_w" '.sprintf("%s", (($valArray == $prefixForValue.'ubunt_mon_w' ) ? "selected" : "")).'  style="font-family: \'Ubuntu Mono\', font-weight: 700;" >Ubuntu Mono Weight</option>
		<option value="'.$prefixForValue.'ubunt_w" '.sprintf("%s", (($valArray == $prefixForValue.'ubunt_w' ) ? "selected" : "")).'  style="font-family: \'Ubuntu\', sans-serif, font-weight: 700;" >Ubuntu Weight</option>		
		</optgroup>
		
		<optgroup label="Hebrew">
		<option value="'.$prefixForValue.'alef" '.sprintf("%s", (($valArray == $prefixForValue.'alef' ) ? "selected" : "")).'  style="font-family: \'Alef\', sans-serif;" >Alef</option>
		<option value="'.$prefixForValue.'alf_w" '.sprintf("%s", (($valArray == $prefixForValue.'alf_w' ) ? "selected" : "")).'  style="font-family: \'Alef\', sans-serif, font-weight: 700;" >Alef Weight</option>
		<option value="'.$prefixForValue.'arimo" '.sprintf("%s", (($valArray == $prefixForValue.'arimo' ) ? "selected" : "")).'  style="font-family: \'Arimo\', sans-serif;" >Arimo</option>
		<option value="'.$prefixForValue.'armo_w" '.sprintf("%s", (($valArray == $prefixForValue.'armo_w' ) ? "selected" : "")).'  style="font-family: \'Arimo\', sans-serif, font-weight: 700;;" >Arimo Weight</option>
		<option value="'.$prefixForValue.'cousine" '.sprintf("%s", (($valArray == $prefixForValue.'cousine' ) ? "selected" : "")).'  style="font-family: \'Cousine\', ;" >Cousine</option>
		<option value="'.$prefixForValue.'cousin_w" '.sprintf("%s", (($valArray == $prefixForValue.'cousin_w' ) ? "selected" : "")).'  style="font-family: \'Cousine\', font-weight: 700;" >Cousine Weight</option>
		<option value="'.$prefixForValue.'tinos" '.sprintf("%s", (($valArray == $prefixForValue.'tinos' ) ? "selected" : "")).'  style="font-family: \'Tinos\', serif;" >Tinos</option>
		<option value="'.$prefixForValue.'tins_w" '.sprintf("%s", (($valArray == $prefixForValue.'tins_w' ) ? "selected" : "")).'  style="font-family: \'Tinos\', serif, font-weight: 700;" >Tinos Weight</option>
		</optgroup>
		
		<optgroup label="Khmer">
		<option value="'.$prefixForValue.'angkor" '.sprintf("%s", (($valArray == $prefixForValue.'angkor' ) ? "selected" : "")).'  style="font-family: \'Angkor\', cursive;" >Angkor</option>
		<option value="'.$prefixForValue.'battambang" '.sprintf("%s", (($valArray == $prefixForValue.'battambang' ) ? "selected" : "")).'  style="font-family: \'Battambang\', cursive;" >Battambang</option>
		<option value="'.$prefixForValue.'battambng_w" '.sprintf("%s", (($valArray == $prefixForValue.'battambng_w' ) ? "selected" : "")).'  style="font-family: \'Battambang\', cursive, font-weight: 700;" >Battambang Weight</option>
		<option value="'.$prefixForValue.'bokor" '.sprintf("%s", (($valArray == $prefixForValue.'bokor' ) ? "selected" : "")).'  style="font-family: \'Bokor\', cursive;" >Bokor</option>
		<option value="'.$prefixForValue.'dangrek" '.sprintf("%s", (($valArray == $prefixForValue.'dangrek' ) ? "selected" : "")).'  style="font-family: \'Dangrek\', cursive;" >Dangrek</option>
		<option value="'.$prefixForValue.'freehand" '.sprintf("%s", (($valArray == $prefixForValue.'freehand' ) ? "selected" : "")).'  style="font-family: \'Freehand\', cursive;" >Freehand</option>
		<option value="'.$prefixForValue.'nokora" '.sprintf("%s", (($valArray == $prefixForValue.'nokora' ) ? "selected" : "")).'  style="font-family: \'Nokora\', serif;" >Nokora</option>
		<option value="'.$prefixForValue.'nokor_w" '.sprintf("%s", (($valArray == $prefixForValue.'nokor_w' ) ? "selected" : "")).'  style="font-family: \'Nokora\', serif, font-weight: 700;" >Nokora Weight</option>
		<option value="'.$prefixForValue.'siemreap" '.sprintf("%s", (($valArray == $prefixForValue.'siemreap' ) ? "selected" : "")).'  style="font-family: \'Siemreap\', cursive;" >Siemreap</option>
		</optgroup>
		
		<optgroup label="Tamil">
		<option value="'.$prefixForValue.'catamaran" '.sprintf("%s", (($valArray == $prefixForValue.'catamaran' ) ? "selected" : "")).'  style="font-family: \'Catamaran\', sans-serif;" >Catamaran</option>
		<option value="'.$prefixForValue.'catamarn_w" '.sprintf("%s", (($valArray == $prefixForValue.'catamarn_w' ) ? "selected" : "")).'  style="font-family: \'Catamaran\', sans-serif, font-weight: 700;" >Catamaran Weight</option>
		</optgroup>
		
		<optgroup label="Telugu">		
		<option value="'.$prefixForValue.'mallanna" '.sprintf("%s", (($valArray == $prefixForValue.'mallanna' ) ? "selected" : "")).'  style="font-family: \'Mallanna\', sans-serif;" >Mallanna</option>
		<option value="'.$prefixForValue.'ramabhadra" '.sprintf("%s", (($valArray == $prefixForValue.'ramabhadra' ) ? "selected" : "")).'  style="font-family: \'Ramabhadra\', sans-serif;" >Ramabhadra</option>
		<option value="'.$prefixForValue.'ramaraja" '.sprintf("%s", (($valArray == $prefixForValue.'ramaraja' ) ? "selected" : "")).'  style="font-family: \'Ramaraja\', serif;" >Ramaraja</option>
		<option value="'.$prefixForValue.'timmana" '.sprintf("%s", (($valArray == $prefixForValue.'timmana' ) ? "selected" : "")).'  style="font-family: \'Timmana\', sans-serif;" >Timmana</option>
		</optgroup>
		
		<optgroup label="Thai">
		<option value="'.$prefixForValue.'chonburi" '.sprintf("%s", (($valArray == $prefixForValue.'chonburi' ) ? "selected" : "")).'  style="font-family: \'Chonburi\', cursive;" >Chonburi</option>
		<option value="'.$prefixForValue.'itim" '.sprintf("%s", (($valArray == $prefixForValue.'itim' ) ? "selected" : "")).'  style="font-family: \'Itim\', cursive;" >Itim</option>		
		</optgroup>
		
		<optgroup label="Vietnamese">
		<option value="'.$prefixForValue.'arimo" '.sprintf("%s", (($valArray == $prefixForValue.'arimo' ) ? "selected" : "")).'  style="font-family: \'Arimo\', sans-serif;" >Arimo</option>
		<option value="'.$prefixForValue.'armo_w" '.sprintf("%s", (($valArray == $prefixForValue.'armo_w' ) ? "selected" : "")).'  style="font-family: \'Arimo\', sans-serif, font-weight: 700;;" >Arimo Weight</option>
		<option value="'.$prefixForValue.'chonburi" '.sprintf("%s", (($valArray == $prefixForValue.'chonburi' ) ? "selected" : "")).'  style="font-family: \'Chonburi\', cursive;" >Chonburi</option>
		<option value="'.$prefixForValue.'eb_garamond" '.sprintf("%s", (($valArray == $prefixForValue.'eb_garamond' ) ? "selected" : "")).'  style="font-family: \'EB Garamond\', serif;" >EB Garamond</option>
		<option value="'.$prefixForValue.'eb_garamnd_w" '.sprintf("%s", (($valArray == $prefixForValue.'eb_garamnd_w' ) ? "selected" : "")).'  style="font-family: \'EB Garamond\', serif, font-weight: 700;" >EB Garamond Weight</option>
		<option value="'.$prefixForValue.'itim" '.sprintf("%s", (($valArray == $prefixForValue.'itim' ) ? "selected" : "")).'  style="font-family: \'Itim\', cursive;" >Itim</option>
		<option value="'.$prefixForValue.'lobster" '.sprintf("%s", (($valArray == $prefixForValue.'lobster' ) ? "selected" : "")).'  style="font-family: \'Lobster Two\', cursive;" >Lobster Two</option>
		<option value="'.$prefixForValue.'lobstr_w" '.sprintf("%s", (($valArray == $prefixForValue.'lobstr_w' ) ? "selected" : "")).'  style="font-family: \'Lobster Two\', cursive, font-weight: 700;" >Lobster Two Weight</option>		
		<option value="'.$prefixForValue.'open_sans_condensed" '.sprintf("%s", (($valArray == $prefixForValue.'open_sans_condensed' ) ? "selected" : "")).'  style="font-family: \'Open Sans Condensed\', sans-serif;" >Open Sans Condensed</option>
		<option value="'.$prefixForValue.'open_sans_condensd_w" '.sprintf("%s", (($valArray == $prefixForValue.'open_sans_condensd_w' ) ? "selected" : "")).'  style="font-family: \'Open Sans Condensed\', sans-serif, font-weight: 700;" >Open Sans Condensed Weight</option>
		<option value="'.$prefixForValue.'roboto" '.sprintf("%s", (($valArray == $prefixForValue.'roboto' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif;" >Roboto</option>
		<option value="'.$prefixForValue.'robot_w" '.sprintf("%s", (($valArray == $prefixForValue.'robot_w' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif, font-weight: 700;" >Roboto Weight</option>
		<option value="'.$prefixForValue.'robt_ww" '.sprintf("%s", (($valArray == $prefixForValue.'robt_ww' ) ? "selected" : "")).'  style="font-family: \'Roboto\', sans-serif, font-weight: 900;" >Roboto Weight+</option>
		<option value="'.$prefixForValue.'source_sans_pro" '.sprintf("%s", (($valArray == $prefixForValue.'source_sans_pro' ) ? "selected" : "")).'  style="font-family: \'Source Sans Pro\', sans-serif;" >Source Sans Pro</option>
		<option value="'.$prefixForValue.'source_sans_pr_w" '.sprintf("%s", (($valArray == $prefixForValue.'source_sans_pr_w' ) ? "selected" : "")).'  style="font-family: \'Source Sans Pro\', sans-serif, font-weight: 700;" >Source Sans Pro Weight</option>
		<option value="'.$prefixForValue.'tinos" '.sprintf("%s", (($valArray == $prefixForValue.'tinos' ) ? "selected" : "")).'  style="font-family: \'Tinos\', serif;" >Tinos</option>
		<option value="'.$prefixForValue.'tins_w" '.sprintf("%s", (($valArray == $prefixForValue.'tins_w' ) ? "selected" : "")).'  style="font-family: \'Tinos\', serif, font-weight: 700;" >Tinos Weight</option>		
		</optgroup>		
		</select>
		</div>
		';
		return $ret;
	}
	
	function generateSelect($nameTool, $nameSelect, $title, $optionArray, $valueArray, $classArray, $id, $disableDefault){
		$id = $this->prepareIdName($id);
		$ret = '';
		$jsFunc = str_replace('[', '',$nameTool);
		$jsFunc = str_replace(']', '',$jsFunc);		
		$jsFunc = str_replace('moreShareWindow','',$jsFunc);
		$jsFunc = str_replace('buttonBlock','',$jsFunc);
		if($optionArray){
			$ret = '
				<div id="'.$id.'_div">
				<p>'.$title.'</p>				
				<select name="'.$nameSelect.'" id="'.$id.'" onchange="'.$jsFunc.'Preview()">';
				if((int)$disableDefault == 0){
					$ret.='<option value="" selected></option>';
				}				
				foreach((array)$optionArray as $k => $v){
					$ret .= '<option value="'.$k.'" class="'.$classArray[$k].'" '.sprintf("%s", (($valueArray == $k ) ? "selected" : "")).' >'.$v.'</option>';
				}
			$ret .= '
				</select>				
				</div>
			';
		}
		return $ret;
	}
	
	function getLockBlock($name, $arrayValue){
		$ret = '		
		<h2>'.$this->_dictionary[lockBlock][title].'</h2>
		<p>'.$this->_dictionary[lockBlock][description].'</p>
		<div class="pq_clear"></div>
		<label class="pq_after_action"><p>'.$this->_dictionary[lockBlock][after_proceed].'</p><input type="text" name="'.$name.'[lockedMechanism][afterProceed]" value="'.(float)$arrayValue[lockedMechanism][afterProceed].'"></label>
		<label class="pq_after_action"><p>'.$this->_dictionary[lockBlock][after_close].'</p><input type="text" name="'.$name.'[lockedMechanism][afterClose]" value="'.(float)$arrayValue[lockedMechanism][afterClose].'"></label>
		';
		return $ret;
	}
	
	function getProviderBlock($name, $arrayValue){
		$id = $this->prepareIdName($name);
		$jsFunc = str_replace('[', '',$name);
		$jsFunc = str_replace(']', '',$jsFunc);			
		$ret = '		
		<h2>'.$this->_dictionary[selectProviderBlock][title].'</h2>
		<p>'.$this->_dictionary[selectProviderBlock][description].'</p>
		<div class="pq_clear"></div>
		<label class="pq_rule pq_provider" onclick="selectSubscribeProvider(\''.$name.'\', \'mailchimp\');checkNameFieldsByProvider(\''.$name.'\');">
			<input type="radio" id="'.$id.'_provider_mailchimp" name="'.$name.'[provider]" '.sprintf("%s", (($arrayValue[provider] == "mailchimp") ? "checked" : "")).' value="mailchimp">
			<div>
				<img src="'.plugins_url("i/provider_mailchimp.png", __FILE__).'" />
				<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
				<p>MailChimp</p>
			</div>
		</label>
		<label class="pq_rule pq_provider" onclick="selectSubscribeProvider(\''.$name.'\', \'getresponse\');checkNameFieldsByProvider(\''.$name.'\');">
			<input type="radio" id="'.$id.'_provider_getresponse" name="'.$name.'[provider]" '.sprintf("%s", (($arrayValue[provider] == "getresponse") ? "checked" : "")).' value="getresponse">
			<div>
				<img src="'.plugins_url("i/provider_getresponse.png", __FILE__).'" />
				<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
				<p>GetResponse</p>
			</div>
		</label>
		<label class="pq_rule pq_provider" onclick="selectSubscribeProvider(\''.$name.'\', \'aweber\');checkNameFieldsByProvider(\''.$name.'\');">
			<input type="radio" id="'.$id.'_provider_aweber" name="'.$name.'[provider]" '.sprintf("%s", (($arrayValue[provider] == "aweber") ? "checked" : "")).' value="aweber">
			<div>
				<img src="'.plugins_url("i/provider_aweber.png", __FILE__).'" />
				<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
				<p>Aweber</p>
			</div>
		</label>
		<!--label class="pq_rule pq_provider" onclick="selectSubscribeProvider(\''.$name.'\', \'newsletter2go\');checkNameFieldsByProvider(\''.$name.'\');">
			<input type="radio" id="'.$id.'_provider_newsletter2go" name="'.$name.'[provider]" '.sprintf("%s", (($arrayValue[provider] == "newsletter2go") ? "checked" : "")).' value="newsletter2go">
			<div>
				<img src="'.plugins_url("i/provider_newsletter2go.png", __FILE__).'" />
				<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
				<p>Newsletter2Go</p>
			</div>
		</label--!>
		
		<label class="pq_rule pq_provider" onclick="selectSubscribeProvider(\''.$name.'\', \'madmini\');checkNameFieldsByProvider(\''.$name.'\');">
			<input type="radio" id="'.$id.'_provider_madmini" name="'.$name.'[provider]" '.sprintf("%s", (($arrayValue[provider] == "madmini") ? "checked" : "")).' value="madmini">
			<div>
				<img src="'.plugins_url("i/provider_madmimi.png", __FILE__).'" />
				<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
				<p>Mad Mini</p>
			</div>
		</label>
		<label class="pq_rule pq_provider" onclick="selectSubscribeProvider(\''.$name.'\', \'acampaign\');checkNameFieldsByProvider(\''.$name.'\');">
			<input type="radio" id="'.$id.'_provider_acampaign" name="'.$name.'[provider]" '.sprintf("%s", (($arrayValue[provider] == "acampaign") ? "checked" : "")).' value="acampaign">
			<div>
				<img src="'.plugins_url("i/provider_activecampaign.png", __FILE__).'" />
				<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
				<p>ActiveCompaign</p>
			</div>
		</label>
		<label class="pq_rule pq_provider" onclick="selectSubscribeProvider(\''.$name.'\', \'klickmail\');checkNameFieldsByProvider(\''.$name.'\');">
			<input type="radio" id="'.$id.'_provider_klickmail" name="'.$name.'[provider]" '.sprintf("%s", (($arrayValue[provider] == "klickmail") ? "checked" : "")).' value="klickmail">
			<div>
				<img src="'.plugins_url("i/provider_klickmail.png", __FILE__).'" />
				<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
				<p>Klick Mail</p>
			</div>
		</label>
		<label class="pq_rule pq_provider" >
			<div>
				<a href="http://profitquery.com" target="_another_signin_provider">
				<img src="'.plugins_url("i/provider_any.png", __FILE__).'" />
				<p>Another Provider</p>
				</a>
			</div>
		</label>
		<label class="pq_rule pq_provider" >
			<div>
				<a href="http://profitquery.com" target="_provider_csv">
				<img src="'.plugins_url("i/provider_csv.png", __FILE__).'" />
				<p>Your CSV File</p>
				</a>
			</div>
		</label>
		<div class="pq_clear"></div>
		<div style="max-width: 100%;margin: 30px auto 0; font-size: 16px;" '.sprintf("%s", (((int)$arrayValue[providerOption][is_error] == 1) ? "class='pq_error'" : "")).'>
			<h2 style="margin: 0px;" id="'.$name.'_provier_title"></h2>
			<p>'.$this->_dictionary[selectProviderBlock][paste_code].'
			<a id="'.$name.'_provier_help_url" target="_provider_help_url">'.$this->_dictionary[selectProviderBlock][how_get_code].'</a></p>
		<textarea name="'.$name.'[providerForm]" rows="10">'.stripslashes($arrayValue[providerForm]).'</textarea>
		<div style="display:'.sprintf("%s", (((int)$arrayValue[providerOption][is_error] == 1) ? "block" : "none")).';" id="'.$name.'_providerErrorBlock" class="pq_attr"><p>'.$this->_dictionary[selectProviderBlock][error_provider].'</p><a class="pq_close" onclick="document.getElementById(\''.$name.'_providerErrorBlock\').style.display=\'none\';"></a></div>
		</div>
		<script>
			selectSubscribeProvider(\''.$name.'\', \''.$arrayValue[provider].'\');
			checkNameFieldsByProvider(\''.$name.'\');
		</script>
		';		
		return $ret;
	}
	
	function setProMailForm($name, $withSubject, $value){
		$mailto = 'checked';
		$mail = '';
		if($value[sendMail] == 'on'){
			$mailto = '';
			$mail = 'checked';
		}
		$ret='		
		<div class="pq_clear"></div>				
		<label class="pq_rule pq_mailservice pq_active" style="margin-right: 2%!important;" onclick="changeStatusToRadio(\''.$name.'_sendMail_\', \'off\', [\'on\',\'off\'])">
			<input type="radio" '.$mailto.' id="'.$name.'_sendMail_off" name="'.$name.'[sendMail]" value="">
			<div>
				<img src="'.plugins_url('i/send_mailto.png', __FILE__).'" onclick="changeStatusToRadio(\''.$name.'_sendMail_\', \'off\', [\'on\',\'off\'])" />
				<img src="'.plugins_url('i/select.png', __FILE__).'" class="pq_active">
				<p>'.$this->_dictionary[globalSettings][default_mailto_text].'</p>
			</div>
		</label>
		<label class="pq_rule pq_mailservice pq_pro">
			<input type="radio" '.$mail.' id="'.$name.'_sendMail_on" name="'.$name.'[sendMail]" value="on">
			<div>
				<img src="'.plugins_url('i/send_profitquery.png', __FILE__).'" onclick="changeStatusToRadio(\''.$name.'_sendMail_\', \'on\', [\'on\',\'off\'])" />
				<img src="'.plugins_url('i/select.png', __FILE__).'" class="pq_active">	
				<p>'.$this->_dictionary[globalSettings][pq_mail_text].'</p>
			</div>
		</label>				
		<div class="pq_clear"></div>
		<div class="pq_mailservice_inputs">
			
		';
		if($withSubject){
			$ret.='
				<label>
					<p>'.$this->_dictionary[globalSettings][mail_subject].'</p>
					<input type="text" name="'.$name.'[mail_subject]" value="'.stripslashes($value[mail_subject]).'">
				</label>
			';		
		}
		$ret.='</div><p>'.sprintf($this->_dictionary[globalSettings][change_current_email], $this->_options[settings][email]).'</p>';
		return $ret;
	}
	function getGadgetRules($name, $arrayValue){		
		$ret = '
			<label class="pq_page_settings">
				<p>'.$this->_dictionary[gadgetRules][on_mobile].'</p>			
				<input class="pq_switch" type="checkbox" id="'.$name.'_work_on_mobile" name="'.$name.'[displayRules][work_on_mobile]" '.sprintf("%s", ($arrayValue[displayRules][work_on_mobile] == 'on' || !isset($arrayValue[displayRules][work_on_mobile])) ? "checked" : "").' />
				<label for="'.$name.'_work_on_mobile" class="pq_switch_label"></label>
			</label>			
		';
		return $ret;
	}
	
	function getPageOptions($name, $arrayValue){
		$ret = '			
			<div class="pq_desable_url">
				<h2>'.$this->_dictionary[pageSettings][url_mask_title].'</h2>
				<div class="pq_clear"></div>
				<select name="'.$name.'[displayRules][pageMaskType][0]">
					<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][0] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][0])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
					<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][0] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
				</select>
				<input type="text" name="'.$name.'[displayRules][pageMask][0]" value="'.stripslashes($arrayValue[displayRules][pageMask][0]).'">
				
				<select name="'.$name.'[displayRules][pageMaskType][1]">
					<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][1] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][1])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
					<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][1] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
				</select>
				<input type="text" name="'.$name.'[displayRules][pageMask][1]" value="'.stripslashes($arrayValue[displayRules][pageMask][1]).'">
				
				<select name="'.$name.'[displayRules][pageMaskType][2]">
					<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][2] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][2])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
					<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][2] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
				</select>
				<input type="text" name="'.$name.'[displayRules][pageMask][2]" value="'.stripslashes($arrayValue[displayRules][pageMask][2]).'">
				
				<select name="'.$name.'[displayRules][pageMaskType][3]">
					<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][3] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][3])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
					<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][3] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
				</select>
				<input type="text" name="'.$name.'[displayRules][pageMask][3]" value="'.stripslashes($arrayValue[displayRules][pageMask][3]).'">
				
				<select name="'.$name.'[displayRules][pageMaskType][4]">
					<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][4] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][4])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
					<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][4] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
				</select>
				<input type="text" name="'.$name.'[displayRules][pageMask][4]" value="'.stripslashes($arrayValue[displayRules][pageMask][4]).'">
				
				<input type="button" id="'.$name.'_displayRules_pageMaskType_Button_More" value="'.$this->_dictionary[pageSettings][more].'" onclick="document.getElementById(\''.$name.'_displayRules_pageMaskType_More\').style.display=\'block\';document.getElementById(\''.$name.'_displayRules_pageMaskType_Button_More\').style.display=\'none\';">
				
				<div id="'.$name.'_displayRules_pageMaskType_More" style="display:none">
					<select name="'.$name.'[displayRules][pageMaskType][5]">
						<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][5] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][5])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
						<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][5] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
					</select>
					<input type="text" name="'.$name.'[displayRules][pageMask][5]" value="'.stripslashes($arrayValue[displayRules][pageMask][5]).'">
					
					<select name="'.$name.'[displayRules][pageMaskType][6]">
						<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][6] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][6])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
						<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][6] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
					</select>
					<input type="text" name="'.$name.'[displayRules][pageMask][6]" value="'.stripslashes($arrayValue[displayRules][pageMask][6]).'">
					
					<select name="'.$name.'[displayRules][pageMaskType][7]">
						<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][7] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][7])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
						<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][7] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
					</select>
					<input type="text" name="'.$name.'[displayRules][pageMask][7]" value="'.stripslashes($arrayValue[displayRules][pageMask][7]).'">
					
					<select name="'.$name.'[displayRules][pageMaskType][8]">
						<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][8] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][8])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
						<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][8] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
					</select>
					<input type="text" name="'.$name.'[displayRules][pageMask][8]" value="'.stripslashes($arrayValue[displayRules][pageMask][8]).'">
					
					<select name="'.$name.'[displayRules][pageMaskType][9]">
						<option value="enable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][9] == 'enable' || !isset($arrayValue[displayRules][pageMaskType][9])) ? "selected" : "").'>'.$this->_dictionary[pageSettings][show].'</option>
						<option value="disable" '.sprintf("%s", ($arrayValue[displayRules][pageMaskType][9] == 'disable') ? "selected" : "").'>'.$this->_dictionary[pageSettings][dont_show].'</option>
					</select>
					<input type="text" name="'.$name.'[displayRules][pageMask][9]" value="'.stripslashes($arrayValue[displayRules][pageMask][9]).'">
				</div>
			</div>
			
			
			<div class="pq_clear"></div>			
			
			<label class="pq_page_settings">
			<p>'.$this->_dictionary[pageSettings][display_main_page].' </p>
			<p>&nbsp;'.stripslashes($this->_options[settings][mainPage]).'</p>			
			<input class="pq_switch" type="checkbox" id="'.$name.'_displayRules_mainPageWork" name="'.$name.'[displayRules][display_on_main_page]" '.sprintf("%s", ($arrayValue[displayRules][display_on_main_page] == 'on' || !isset($arrayValue[displayRules][display_on_main_page])) ? "checked" : "").' />
			<label for="'.$name.'_displayRules_mainPageWork" class="pq_switch_label"></label>
			</label>
			<p>'.$this->_dictionary[pageSettings][mp_setting_info].'</p>
		';
		return $ret;
	}
	
	function getEventHandlerBlock($name, $structure, $arrayValue){
		$ret = '';		
		$ret = '
		<h1>'.$this->_dictionary[eventHandlerBlock][title].'</h1>
		<p>'.$this->_dictionary[eventHandlerBlock][description].'</p>
		';
		foreach((array)$structure as $k => $v){
			$keyStructure[$v] = 1;
		}
		
		$array = array('delay', 'exit', 'scrolling');
		
		if(!$arrayValue[eventHandler][type]) $arrayValue[eventHandler][type] = 'delay';
		
		foreach((array)$array as $k => $v){
			if($v == $arrayValue[eventHandler][type]) $checked = 'checked'; else $checked = '';
			
			if($v == 'delay'){
				
				$ret .= '
					<label class="pq_rule" onclick="setEHBlockActive(\''.$name.'\', \'delay\')">
						<input type="radio" '.sprintf("%s", ((int)$keyStructure[$v] == 1) ? "" : "disabled").' id="'.$name.'_eventHandler_delay" name="'.$name.'[eventHandler][type]" '.$checked.'  value="'.$v.'">
						<div>
							<img src="'.plugins_url("i/display_time.png", __FILE__).'" />
							<p>'.$this->_dictionary[eventHandlerBlock][delay_text].'</p>
							<input type="text" name="'.$name.'[eventHandler][delay_value]" '.sprintf("%s", ((int)$keyStructure[$v] == 1) ? "" : "disabled").'  value="'.(int)$arrayValue[eventHandler][delay_value].'"><span>'.$this->_dictionary[eventHandlerBlock][delay_unit].'</span>
							<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
						</div>
					</label>
				';			
			}
			//disabled need
			if($v == 'scrolling'){
				$ret .= '
					<label class="pq_rule"  onclick="setEHBlockActive(\''.$name.'\', \'scrolling\')">
						<input type="radio" '.sprintf("%s", ((int)$keyStructure[$v] == 1) ? "" : "disabled").' id="'.$name.'_eventHandler_scrolling" name="'.$name.'[eventHandler][type]" '.$checked.'  value="'.$v.'">
						<div>
							<img src="'.plugins_url("i/display_scroll.png", __FILE__).'" />
							<p>'.$this->_dictionary[eventHandlerBlock][scrolling_text].'</p>
							<input type="text" '.sprintf("%s", ((int)$keyStructure[$v] == 1) ? "" : "disabled").' name="'.$name.'[eventHandler][scrolling_value]" value="'.(int)$arrayValue[eventHandler][scrolling_value].'"><span>%</span>
							<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
						</div>
					</label>
				';			
			}
			if($v == 'exit'){
				$ret .= '
					<label class="pq_rule" onclick="setEHBlockActive(\''.$name.'\', \'exit\')">
						<input type="radio" '.sprintf("%s", ((int)$keyStructure[$v] == 1) ? "" : "disabled").' id="'.$name.'_eventHandler_exit" name="'.$name.'[eventHandler][type]" '.$checked.'  value="'.$v.'">
						<div>
							<img src="'.plugins_url("i/display_exit.png", __FILE__).'" />
							<p>'.$this->_dictionary[eventHandlerBlock][exit_text].'</p>							
							<img src="'.plugins_url("i/select.png", __FILE__).'" class="pq_active" />
						</div>
					</label>
				';			
			}
		}		
		return $ret;						
	}
	
	function getImageSharingIcons($name, $valArray){
		$ret = '
		<h3>'.$this->_dictionary[imageSharerBlock][title].'</h3>
		<label class="sm">							
			<input type="checkbox" id="'.$name.'_provider_FB" onclick="'.$name.'Preview()"  name="'.$name.'[socnet][FB]" '.sprintf("%s", ($valArray[socnet][FB] == 'on') ? "checked" : "").' />
			<div class="fb pq_checkbox"></div>
			<select name="'.$name.'[socnetOption][FB][type]" class="sm">
				<option value="app" '.sprintf("%s", ($valArray[socnetOption][FB][type] == "app") ? "selected" : "").'>'.$this->_dictionary[imageSharerBlock][fb_app].'</option>
				<option value="" '.sprintf("%s", ($valArray[socnetOption][FB][type] == "") ? "selected" : "").'>'.sprintf($this->_dictionary[imageSharerBlock][default_share], "Facebook").'</option>
				<option value="pq" '.sprintf("%s", ($valArray[socnetOption][FB][type] == "pq") ? "selected" : "").'>'.$this->_dictionary[imageSharerBlock][pq_share].'</option>				
			</select>
		</label>
		
		<label class="sm">
			<a class="pq_question" href="http://profitquery.com/fb_app.html" target="_blank">?</a>								
			<input type="text" name="'.$name.'[socnetOption][FB][app_id]" value="'.sprintf("%s", ($valArray[socnetOption][FB][app_id] != "") ? stripslashes($valArray[socnetOption][FB][app_id]) : "").'" placeholder="FaceBook APP ID"/>
		</label>
		<label class="sm">
			<input type="checkbox" id="'.$name.'_provider_TW" onclick="'.$name.'Preview()" name="'.$name.'[socnet][TW]" '.sprintf("%s", ($valArray[socnet][TW] == 'on') ? "checked" : "").' />
			<div class="tw pq_checkbox"></div>									
			<select name="'.$name.'[socnetOption][TW][type]" class="sm">
				<option value="" '.sprintf("%s", ($valArray[socnetOption][TW][type] == "") ? "selected" : "").'>'.sprintf($this->_dictionary[imageSharerBlock][default_share], "Twitter").'</option>
				<option value="pq" '.sprintf("%s", ($valArray[socnetOption][TW][type] == "pq") ? "selected" : "").'>'.$this->_dictionary[imageSharerBlock][pq_share].'</option>
			</select>
		</label>
		<label class="sm">
			<input type="checkbox" id="'.$name.'_provider_GP" onclick="'.$name.'Preview()" name="'.$name.'[socnet][GP]" '.sprintf("%s", ($valArray[socnet][GP] == 'on') ? "checked" : "").'/>
			<div class="gp pq_checkbox" ></div>
			<select class="sm" name="'.$name.'[socnetOption][GP][type]" >
				<option value="" '.sprintf("%s", ($valArray[socnetOption][GP][type] == "") ? "selected" : "").'>'.sprintf($this->_dictionary[imageSharerBlock][default_share], "Google Plus").'</option>
				<option value="pq" '.sprintf("%s", ($valArray[socnetOption][GP][type] == "pq") ? "selected" : "").'>'.$this->_dictionary[imageSharerBlock][pq_share].'</option>
				
			</select>
		</label>
		<label class="sm">	
			<input type="checkbox" id="'.$name.'_provider_PI" onclick="'.$name.'Preview()" name="'.$name.'[socnet][PI]" '.sprintf("%s", ($valArray[socnet][PI] == 'on') ? "checked" : "").' />
			<div class="pi pq_checkbox"></div>
			<select class="sm" name="'.$name.'[socnetOption][PI][type]">
				<option value="" '.sprintf("%s", ($valArray[socnetOption][PI][type] == "") ? "selected" : "").'>'.sprintf($this->_dictionary[imageSharerBlock][default_share], "Pinterest").'</option>
				<option value="pq" '.sprintf("%s", ($valArray[socnetOption][PI][type] == "pq") ? "selected" : "").'>'.$this->_dictionary[imageSharerBlock][pq_share].'</option>
				
			</select>
		</label>
		<label class="sm">	
			<input type="checkbox" id="'.$name.'_provider_TR" onclick="'.$name.'Preview()" name="'.$name.'[socnet][TR]" '.sprintf("%s", ($valArray[socnet][TR] == 'on') ? "checked" : "").' />
			<div class="tr pq_checkbox"></div>
			<select class="sm" name="'.$name.'[socnetOption][TR][type]" >
				<option value="" '.sprintf("%s", ($valArray[socnetOption][TR][type] == "") ? "selected" : "").'>'.sprintf($this->_dictionary[imageSharerBlock][default_share], "Tumblr").'</option>
				<option value="pq" '.sprintf("%s", ($valArray[socnetOption][TR][type] == "pq") ? "selected" : "").'>'.$this->_dictionary[imageSharerBlock][pq_share].'</option>
			</select>
		</label>
		<label class="sm">
			<input type="checkbox" id="'.$name.'_provider_VK" onclick="'.$name.'Preview()" name="'.$name.'[socnet][VK]" '.sprintf("%s", ($valArray[socnet][VK] == 'on') ? "checked" : "").' />
			<div class="vk pq_checkbox"></div>
			<select class="sm" name="'.$name.'[socnetOption][VK][type]" >
				<option value="" '.sprintf("%s", ($valArray[socnetOption][VK][type] == "") ? "selected" : "").'>'.sprintf($this->_dictionary[imageSharerBlock][default_share], "Vkontakte").'</option>
				<option value="pq" '.sprintf("%s", ($valArray[socnetOption][VK][type] == "pq") ? "selected" : "").'>'.$this->_dictionary[imageSharerBlock][pq_share].'</option>
			</select>
		</label>
		<label class="sm">	
			<input type="checkbox" id="'.$name.'_provider_OD" onclick="'.$name.'Preview()" name="'.$name.'[socnet][OD]" '.sprintf("%s", ($valArray[socnet][OD] == 'on') ? "checked" : "").' />
			<div class="od pq_checkbox"></div>									
			<select class="sm" name="'.$name.'[socnetOption][OD][type]" >
				<option value="" '.sprintf("%s", ($valArray[socnetOption][OD][type] == "") ? "selected" : "").'>'.sprintf($this->_dictionary[imageSharerBlock][default_share], "Odnoklassniki").'</option>
				<option value="pq" '.sprintf("%s", ($valArray[socnetOption][OD][type] == "pq") ? "selected" : "").'>'.$this->_dictionary[imageSharerBlock][pq_share].'</option>
			</select>
		</label>
		<label class="sm">
			<input type="checkbox" id="'.$name.'_provider_WU" onclick="'.$name.'Preview()" name="'.$name.'[socnet][WU]" '.sprintf("%s", ($valArray[socnet][WU] == 'on') ? "checked" : "").' />
			<div class="wu pq_checkbox"></div>
			<select class="sm" disabled="disabled"></select>									
		</label>
		<label class="sm">
			<input type="checkbox" id="'.$name.'_provider_Mail" onclick="'.$name.'Preview()" name="'.$name.'[socnet][Mail]" '.sprintf("%s", ($valArray[socnet][Mail] == 'on') ? "checked" : "").' />
			<div class="em pq_checkbox"></div>
			<select class="sm" disabled="disabled"></select>										
		</label>';
		
		return $ret;
	}
	
	function getSharingIcons($name, $valArray, $additionalArrays, $disableMail = 0){
		$ret = '';
		$sharingCnt = 10;
		$jsFunc = str_replace('[', '',$name);
		$jsFunc = str_replace(']', '',$jsFunc);
		$id = $this->prepareIdName($name).'_sharing_icon';
		$id = str_replace('__', '_', $id);
		for($i=0; $i<$sharingCnt; $i++)
		{						
			$ret .= '
			<select onchange="'.$jsFunc.'Preview()" id="'.$id.'_'.$i.'" name="'.$name.'[socnet_with_pos]['.$i.']"  class="'.sprintf("%s", ((int)$additionalArrays[socnet_with_pos_error][error_socnet][$valArray[$i]] == 1) ? "pq_error" : "").'">
				<option value="" selected></option>
				<option value="FB" '.sprintf("%s", (($valArray[$i] == "FB") ? "selected" : "")).' >Facebook</option>
				<option value="TW" '.sprintf("%s", (($valArray[$i] == "TW") ? "selected" : "")).'>Twitter</option>
				<option value="GP" '.sprintf("%s", (($valArray[$i] == "GP") ? "selected" : "")).'>Google plus</option>
				<option value="RD" '.sprintf("%s", (($valArray[$i] == "RD") ? "selected" : "")).'>Reddit</option>
				<option value="PI" '.sprintf("%s", (($valArray[$i] == "PI") ? "selected" : "")).'>Pinterest</option>
				<option value="VK" '.sprintf("%s", (($valArray[$i] == "VK") ? "selected" : "")).'>Vkontakte</option>
				<option value="OD" '.sprintf("%s", (($valArray[$i] == "OD") ? "selected" : "")).'>Odnoklassniki</option>
				<option value="LJ" '.sprintf("%s", (($valArray[$i] == "LJ") ? "selected" : "")).'>Live Journal</option>
				<option value="TR" '.sprintf("%s", (($valArray[$i] == "TR") ? "selected" : "")).'>Tumblr</option>
				<option value="LI" '.sprintf("%s", (($valArray[$i] == "LI") ? "selected" : "")).'>LinkedIn</option>
				<option value="SU" '.sprintf("%s", (($valArray[$i] == "SU") ? "selected" : "")).'>StumbleUpon</option>
				<option value="DG" '.sprintf("%s", (($valArray[$i] == "DG") ? "selected" : "")).'>Digg</option>
				<option value="DL" '.sprintf("%s", (($valArray[$i] == "DL") ? "selected" : "")).'>Delicious</option>
				<option value="WU" '.sprintf("%s", (($valArray[$i] == "WU") ? "selected" : "")).'>WhatsApp</option>
				<option value="BR" '.sprintf("%s", (($valArray[$i] == "BR") ? "selected" : "")).'>Blogger</option>
				<option value="RR" '.sprintf("%s", (($valArray[$i] == "RR") ? "selected" : "")).'>Renren</option>
				<option value="WB" '.sprintf("%s", (($valArray[$i] == "WB") ? "selected" : "")).'>Weibo</option>
				<option value="MW" '.sprintf("%s", (($valArray[$i] == "MW") ? "selected" : "")).'>My World</option>
				<option value="EN" '.sprintf("%s", (($valArray[$i] == "EN") ? "selected" : "")).'>Evernote</option>
				<option value="PO" '.sprintf("%s", (($valArray[$i] == "PO") ? "selected" : "")).'>Pocket</option>
				<option value="AK" '.sprintf("%s", (($valArray[$i] == "AK") ? "selected" : "")).'>Kindle</option>
				<option value="FL" '.sprintf("%s", (($valArray[$i] == "FL") ? "selected" : "")).'>Flipboard</option>
				<option value="Print" '.sprintf("%s", (($valArray[$i] == "Print") ? "selected" : "")).'>Print</option>';
			if((int)$disableMail == 0){
				$ret .='<option value="Mail" '.sprintf("%s", (($valArray[$i] == "Mail") ? "selected" : "")).'>Email</option>';
			}
			$ret .='
			</select>
			<br>
			';
		}
		return $ret;
	}
	

	function getFollowIcons($name, $valueArray)
	{
		$jsFunc = str_replace('[', '',$name);
		$jsFunc = str_replace(']', '',$jsFunc);
		$id = $this->prepareIdName($name).'_follow_icon';
		$id = str_replace('__', '_', $id);
		$ret = '';
		$ret = '
			<p>facebook.com</p>
				<input type="text" onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_FB" name="'.$name.'[socnetIconsBlock][FB]" value="'.stripslashes($valueArray[socnetIconsBlock][FB]).'">
			<br>						
			<p>twitter.com</p>
				<input type="text" onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_TW"  name="'.$name.'[socnetIconsBlock][TW]" value="'.stripslashes($valueArray[socnetIconsBlock][TW]).'">
			<br>
			<p>plus.google.com</p>								
				<input type="text" onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_GP" name="'.$name.'[socnetIconsBlock][GP]" value="'.stripslashes($valueArray[socnetIconsBlock][GP]).'">
			<br>
			<p>pinterest.com</p>
				<input type="text" onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_PI" name="'.$name.'[socnetIconsBlock][PI]" value="'.stripslashes($valueArray[socnetIconsBlock][PI]).'">
			<br>
			<p>www.youtube.com/channel/</p>
				<input type="text" onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_YT" name="'.$name.'[socnetIconsBlock][YT]" value="'.stripslashes($valueArray[socnetIconsBlock][YT]).'">
			<br>
			<p>vk.com</p>
				<input type="text" onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_VK" name="'.$name.'[socnetIconsBlock][VK]" value="'.stripslashes($valueArray[socnetIconsBlock][VK]).'">
			<br>
			<p>odnoklassniki</p>
				<input type="text" onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_OD" name="'.$name.'[socnetIconsBlock][OD]" value="'.stripslashes($valueArray[socnetIconsBlock][OD]).'">
			<br>
			<p>instagram</p>
				<input type="text" onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_IG" name="'.$name.'[socnetIconsBlock][IG]" value="'.stripslashes($valueArray[socnetIconsBlock][IG]).'">
			<br>
			<p>RSS feed</p>
				<input type="text onkeyup="'.$jsFunc.'Preview()" id="'.$id.'_RSS" name="'.$name.'[socnetIconsBlock][RSS]" value="'.stripslashes($valueArray[socnetIconsBlock][RSS]).'">
		';
		return $ret;
	}
	
	
	
	function getFormCodeForTool($scructureMap, $name, $valArray)
	{
		
		$allFormElements = array();
		
		//printr($additionalArrays);
		//printr($valArray);
				
		//SIZE WINDOW
		$allFormElements['size_window'] = $this->generateSelect($name, $name.'[typeWindow]', $this->_dictionary[designOptions][_typeWindow], array('pq_large'=>'Size L','pq_medium'=>'Size M', 'pq_mini'=>'Size S'), $valArray[typeWindow], array('pq_large'=>'123','pq_medium'=>'222', 'pq_mini'=>'333'), $name.'_typeWindow', 0);
		//BACKGROUND COLOR
		$allFormElements['background_color'] = $this->generateSelect($name, $name.'[background]', $this->_dictionary[designOptions][_background], array("pq_bg_backgroundimage_indianred"=>"Indianred","pq_bg_backgroundimage_crimson"=>"Crimson","pq_bg_backgroundimage_lightpink"=>"Lightpink","pq_bg_backgroundimage_pink"=>"Pink","pq_bg_backgroundimage_palevioletred"=>"Palevioletred","pq_bg_backgroundimage_hotpink"=>"Hotpink","pq_bg_backgroundimage_mediumvioletred"=>"Mediumvioletred","pq_bg_backgroundimage_orchid"=>"Orchid","pq_bg_backgroundimage_plum"=>"Plum","pq_bg_backgroundimage_violet"=>"Violet","pq_bg_backgroundimage_magenta"=>"Magenta","pq_bg_backgroundimage_purple"=>"Purple","pq_bg_backgroundimage_mediumorchid"=>"Mediumorchid","pq_bg_backgroundimage_darkviolet"=>"Darkviolet","pq_bg_backgroundimage_darkorchid"=>"Darkorchid","pq_bg_backgroundimage_indigo"=>"Indigo","pq_bg_backgroundimage_blviolet"=>"Blueviolet","pq_bg_backgroundimage_mediumpurple"=>"Mediumpurple","pq_bg_backgroundimage_darkslateblue"=>"Darkslateblue","pq_bg_backgroundimage_mediumslateblue"=>"Mediumslateblue","pq_bg_backgroundimage_slateblue"=>"Slateblue","pq_bg_backgroundimage_blue"=>"Blue","pq_bg_backgroundimage_navy"=>"Navy","pq_bg_backgroundimage_midnightblue"=>"Midnightblue","pq_bg_backgroundimage_royalblue"=>"Royalblue","pq_bg_backgroundimage_cornflowerblue"=>"Cornflowerblue","pq_bg_backgroundimage_steelblue"=>"Steelblue","pq_bg_backgroundimage_lightskyblue"=>"Lightskyblue","pq_bg_backgroundimage_skyblue"=>"Skyblue","pq_bg_backgroundimage_deepskyblue"=>"Deepskyblue","pq_bg_backgroundimage_lightblue"=>"Lightblue","pq_bg_backgroundimage_powderblue"=>"Powderblue","pq_bg_backgroundimage_darkturquoise"=>"Darkturquoise","pq_bg_backgroundimage_cadetblue"=>"Cadetblue","pq_bg_backgroundimage_cyan"=>"Cyan","pq_bg_backgroundimage_teal"=>"Teal","pq_bg_backgroundimage_mediumturquoise"=>"Mediumturquoise","pq_bg_backgroundimage_lightseagreen"=>"Lightseagreen","pq_bg_backgroundimage_paleturquoise"=>"Paleturquoise","pq_bg_backgroundimage_mediumspringgreen"=>"Mediumspringgreen","pq_bg_backgroundimage_springgreen"=>"Springgreen","pq_bg_backgroundimage_darkseagreen"=>"Darkseagreen","pq_bg_backgroundimage_palegreen"=>"Palegreen","pq_bg_backgroundimage_lmgreen"=>"Limegreen","pq_bg_backgroundimage_forestgreen"=>"Forestgreen","pq_bg_backgroundimage_darkgreen"=>"Darkgreen","pq_bg_backgroundimage_lawngreen"=>"Lawngreen","pq_bg_backgroundimage_grnyellow"=>"Greenyellow","pq_bg_backgroundimage_darkolivegreen"=>"Darkolivegreen","pq_bg_backgroundimage_olvdrab"=>"Olivedrab","pq_bg_backgroundimage_yellow"=>"Yellow","pq_bg_backgroundimage_olive"=>"Olive","pq_bg_backgroundimage_darkkhaki"=>"Darkkhaki","pq_bg_backgroundimage_khaki"=>"Khaki","pq_bg_backgroundimage_gold"=>"Gold","pq_bg_backgroundimage_gldenrod"=>"Goldenrod","pq_bg_backgroundimage_orange"=>"Orange","pq_bg_backgroundimage_wheat"=>"Wheat","pq_bg_backgroundimage_navajowhite"=>"Navajowhite","pq_bg_backgroundimage_burlywood"=>"Burlywood","pq_bg_backgroundimage_darkorange"=>"Darkorange","pq_bg_backgroundimage_sienna"=>"Sienna","pq_bg_backgroundimage_orngred"=>"Orangered","pq_bg_backgroundimage_tomato"=>"Tomato","pq_bg_backgroundimage_salmon"=>"Salmon","pq_bg_backgroundimage_brown"=>"Brown","pq_bg_backgroundimage_red"=>"Red","pq_bg_backgroundimage_black"=>"Black","pq_bg_backgroundimage_darkgrey"=>"Darkgrey","pq_bg_backgroundimage_dimgrey"=>"Dimgrey","pq_bg_backgroundimage_lightgrey"=>"Lightgrey","pq_bg_backgroundimage_slategrey"=>"Slategrey","pq_bg_backgroundimage_lightslategrey"=>"Lightslategrey","pq_bg_backgroundimage_silver"=>"Silver","pq_bg_backgroundimage_whtsmoke"=>"Whitesmoke","pq_bg_backgroundimage_white"=>"White"), $valArray[background], array("pq_bg_backgroundimage_indianred"=>"pq_indianred","pq_bg_backgroundimage_crimson"=>"pq_crimson","pq_bg_backgroundimage_lightpink"=>"pq_lightpink","pq_bg_backgroundimage_pink"=>"pq_pink","pq_bg_backgroundimage_palevioletred"=>"pq_palevioletred","pq_bg_backgroundimage_hotpink"=>"pq_hotpink","pq_bg_backgroundimage_mediumvioletred"=>"pq_mediumvioletred","pq_bg_backgroundimage_orchid"=>"pq_orchid","pq_bg_backgroundimage_plum"=>"pq_plum","pq_bg_backgroundimage_violet"=>"pq_violet","pq_bg_backgroundimage_magenta"=>"pq_magenta","pq_bg_backgroundimage_purple"=>"pq_purple","pq_bg_backgroundimage_mediumorchid"=>"pq_mediumorchid","pq_bg_backgroundimage_darkviolet"=>"pq_darkviolet","pq_bg_backgroundimage_darkorchid"=>"pq_darkorchid","pq_bg_backgroundimage_indigo"=>"pq_indigo","pq_bg_backgroundimage_blviolet"=>"pq_blviolet","pq_bg_backgroundimage_mediumpurple"=>"pq_mediumpurple","pq_bg_backgroundimage_darkslateblue"=>"pq_darkslateblue","pq_bg_backgroundimage_mediumslateblue"=>"pq_mediumslateblue","pq_bg_backgroundimage_slateblue"=>"pq_slateblue","pq_bg_backgroundimage_blue"=>"pq_blue","pq_bg_backgroundimage_navy"=>"pq_navy","pq_bg_backgroundimage_midnightblue"=>"pq_midnightblue","pq_bg_backgroundimage_royalblue"=>"pq_royalblue","pq_bg_backgroundimage_cornflowerblue"=>"pq_cornflowerblue","pq_bg_backgroundimage_steelblue"=>"pq_steelblue","pq_bg_backgroundimage_lightskyblue"=>"pq_lightskyblue","pq_bg_backgroundimage_skyblue"=>"pq_skyblue","pq_bg_backgroundimage_deepskyblue"=>"pq_deepskyblue","pq_bg_backgroundimage_lightblue"=>"pq_lightblue","pq_bg_backgroundimage_powderblue"=>"pq_powderblue","pq_bg_backgroundimage_darkturquoise"=>"pq_darkturquoise","pq_bg_backgroundimage_cadetblue"=>"pq_cadetblue","pq_bg_backgroundimage_cyan"=>"pq_cyan","pq_bg_backgroundimage_teal"=>"pq_teal","pq_bg_backgroundimage_mediumturquoise"=>"pq_mediumturquoise","pq_bg_backgroundimage_lightseagreen"=>"pq_lightseagreen","pq_bg_backgroundimage_paleturquoise"=>"pq_paleturquoise","pq_bg_backgroundimage_mediumspringgreen"=>"pq_mediumspringgreen","pq_bg_backgroundimage_springgreen"=>"pq_springgreen","pq_bg_backgroundimage_darkseagreen"=>"pq_darkseagreen","pq_bg_backgroundimage_palegreen"=>"pq_palegreen","pq_bg_backgroundimage_lmgreen"=>"pq_lmgreen","pq_bg_backgroundimage_forestgreen"=>"pq_forestgreen","pq_bg_backgroundimage_darkgreen"=>"pq_darkgreen","pq_bg_backgroundimage_lawngreen"=>"pq_lawngreen","pq_bg_backgroundimage_grnyellow"=>"pq_grnyellow","pq_bg_backgroundimage_darkolivegreen"=>"pq_darkolivegreen","pq_bg_backgroundimage_olvdrab"=>"pq_olvdrab","pq_bg_backgroundimage_yellow"=>"pq_yellow","pq_bg_backgroundimage_olive"=>"pq_olive","pq_bg_backgroundimage_darkkhaki"=>"pq_darkkhaki","pq_bg_backgroundimage_khaki"=>"pq_khaki","pq_bg_backgroundimage_gold"=>"pq_gold","pq_bg_backgroundimage_gldenrod"=>"pq_gldenrod","pq_bg_backgroundimage_orange"=>"pq_orange","pq_bg_backgroundimage_wheat"=>"pq_wheat","pq_bg_backgroundimage_navajowhite"=>"pq_navajowhite","pq_bg_backgroundimage_burlywood"=>"pq_burlywood","pq_bg_backgroundimage_darkorange"=>"pq_darkorange","pq_bg_backgroundimage_sienna"=>"pq_sienna","pq_bg_backgroundimage_orngred"=>"pq_orngred","pq_bg_backgroundimage_tomato"=>"pq_tomato","pq_bg_backgroundimage_salmon"=>"pq_salmon","pq_bg_backgroundimage_brown"=>"pq_brown","pq_bg_backgroundimage_red"=>"pq_red","pq_bg_backgroundimage_black"=>"pq_black","pq_bg_backgroundimage_darkgrey"=>"pq_darkgrey","pq_bg_backgroundimage_dimgrey"=>"pq_dimgrey","pq_bg_backgroundimage_lightgrey"=>"pq_lightgrey","pq_bg_backgroundimage_slategrey"=>"pq_slategrey","pq_bg_backgroundimage_lightslategrey"=>"pq_lightslategrey","pq_bg_backgroundimage_silver"=>"pq_silver","pq_bg_backgroundimage_whtsmoke"=>"pq_whtsmoke","pq_bg_backgroundimage_white"=>"pq_white"), $name.'_background', 0);
		//second background
		$allFormElements['second_background'] = $this->generateSelect($name, $name.'[second_background]', $this->_dictionary[designOptions][_second_background], array("pq_bg2_bgcolor_indianred"=>"Indianred","pq_bg2_bgcolor_crimson"=>"Crimson","pq_bg2_bgcolor_lightpink"=>"Lightpink","pq_bg2_bgcolor_pink"=>"Pink","pq_bg2_bgcolor_palevioletred"=>"Palevioletred","pq_bg2_bgcolor_hotpink"=>"Hotpink","pq_bg2_bgcolor_mediumvioletred"=>"Mediumvioletred","pq_bg2_bgcolor_orchid"=>"Orchid","pq_bg2_bgcolor_plum"=>"Plum","pq_bg2_bgcolor_violet"=>"Violet","pq_bg2_bgcolor_magenta"=>"Magenta","pq_bg2_bgcolor_purple"=>"Purple","pq_bg2_bgcolor_mediumorchid"=>"Mediumorchid","pq_bg2_bgcolor_darkviolet"=>"Darkviolet","pq_bg2_bgcolor_darkorchid"=>"Darkorchid","pq_bg2_bgcolor_indigo"=>"Indigo","pq_bg2_bgcolor_blviolet"=>"Blueviolet","pq_bg2_bgcolor_mediumpurple"=>"Mediumpurple","pq_bg2_bgcolor_darkslateblue"=>"Darkslateblue","pq_bg2_bgcolor_mediumslateblue"=>"Mediumslateblue","pq_bg2_bgcolor_slateblue"=>"Slateblue","pq_bg2_bgcolor_blue"=>"Blue","pq_bg2_bgcolor_navy"=>"Navy","pq_bg2_bgcolor_midnightblue"=>"Midnightblue","pq_bg2_bgcolor_royalblue"=>"Royalblue","pq_bg2_bgcolor_cornflowerblue"=>"Cornflowerblue","pq_bg2_bgcolor_steelblue"=>"Steelblue","pq_bg2_bgcolor_lightskyblue"=>"Lightskyblue","pq_bg2_bgcolor_skyblue"=>"Skyblue","pq_bg2_bgcolor_deepskyblue"=>"Deepskyblue","pq_bg2_bgcolor_lightblue"=>"Lightblue","pq_bg2_bgcolor_powderblue"=>"Powderblue","pq_bg2_bgcolor_darkturquoise"=>"Darkturquoise","pq_bg2_bgcolor_cadetblue"=>"Cadetblue","pq_bg2_bgcolor_cyan"=>"Cyan","pq_bg2_bgcolor_teal"=>"Teal","pq_bg2_bgcolor_mediumturquoise"=>"Mediumturquoise","pq_bg2_bgcolor_lightseagreen"=>"Lightseagreen","pq_bg2_bgcolor_paleturquoise"=>"Paleturquoise","pq_bg2_bgcolor_mediumspringgreen"=>"Mediumspringgreen","pq_bg2_bgcolor_springgreen"=>"Springgreen","pq_bg2_bgcolor_darkseagreen"=>"Darkseagreen","pq_bg2_bgcolor_palegreen"=>"Palegreen","pq_bg2_bgcolor_lmgreen"=>"Limegreen","pq_bg2_bgcolor_forestgreen"=>"Forestgreen","pq_bg2_bgcolor_darkgreen"=>"Darkgreen","pq_bg2_bgcolor_lawngreen"=>"Lawngreen","pq_bg2_bgcolor_grnyellow"=>"Greenyellow","pq_bg2_bgcolor_darkolivegreen"=>"Darkolivegreen","pq_bg2_bgcolor_olvdrab"=>"Olivedrab","pq_bg2_bgcolor_yellow"=>"Yellow","pq_bg2_bgcolor_olive"=>"Olive","pq_bg2_bgcolor_darkkhaki"=>"Darkkhaki","pq_bg2_bgcolor_khaki"=>"Khaki","pq_bg2_bgcolor_gold"=>"Gold","pq_bg2_bgcolor_gldenrod"=>"Goldenrod","pq_bg2_bgcolor_orange"=>"Orange","pq_bg2_bgcolor_wheat"=>"Wheat","pq_bg2_bgcolor_navajowhite"=>"Navajowhite","pq_bg2_bgcolor_burlywood"=>"Burlywood","pq_bg2_bgcolor_darkorange"=>"Darkorange","pq_bg2_bgcolor_sienna"=>"Sienna","pq_bg2_bgcolor_orngred"=>"Orangered","pq_bg2_bgcolor_tomato"=>"Tomato","pq_bg2_bgcolor_salmon"=>"Salmon","pq_bg2_bgcolor_brown"=>"Brown","pq_bg2_bgcolor_red"=>"Red","pq_bg2_bgcolor_black"=>"Black","pq_bg2_bgcolor_darkgrey"=>"Darkgrey","pq_bg2_bgcolor_dimgrey"=>"Dimgrey","pq_bg2_bgcolor_lightgrey"=>"Lightgrey","pq_bg2_bgcolor_slategrey"=>"Slategrey","pq_bg2_bgcolor_lightslategrey"=>"Lightslategrey","pq_bg2_bgcolor_silver"=>"Silver","pq_bg2_bgcolor_whtsmoke"=>"Whitesmoke","pq_bg2_bgcolor_white"=>"White"), $valArray[second_background], array("pq_bg2_bgcolor_indianred"=>"pq_indianred","pq_bg2_bgcolor_crimson"=>"pq_crimson","pq_bg2_bgcolor_lightpink"=>"pq_lightpink","pq_bg2_bgcolor_pink"=>"pq_pink","pq_bg2_bgcolor_palevioletred"=>"pq_palevioletred","pq_bg2_bgcolor_hotpink"=>"pq_hotpink","pq_bg2_bgcolor_mediumvioletred"=>"pq_mediumvioletred","pq_bg2_bgcolor_orchid"=>"pq_orchid","pq_bg2_bgcolor_plum"=>"pq_plum","pq_bg2_bgcolor_violet"=>"pq_violet","pq_bg2_bgcolor_magenta"=>"pq_magenta","pq_bg2_bgcolor_purple"=>"pq_purple","pq_bg2_bgcolor_mediumorchid"=>"pq_mediumorchid","pq_bg2_bgcolor_darkviolet"=>"pq_darkviolet","pq_bg2_bgcolor_darkorchid"=>"pq_darkorchid","pq_bg2_bgcolor_indigo"=>"pq_indigo","pq_bg2_bgcolor_blviolet"=>"pq_blviolet","pq_bg2_bgcolor_mediumpurple"=>"pq_mediumpurple","pq_bg2_bgcolor_darkslateblue"=>"pq_darkslateblue","pq_bg2_bgcolor_mediumslateblue"=>"pq_mediumslateblue","pq_bg2_bgcolor_slateblue"=>"pq_slateblue","pq_bg2_bgcolor_blue"=>"pq_blue","pq_bg2_bgcolor_navy"=>"pq_navy","pq_bg2_bgcolor_midnightblue"=>"pq_midnightblue","pq_bg2_bgcolor_royalblue"=>"pq_royalblue","pq_bg2_bgcolor_cornflowerblue"=>"pq_cornflowerblue","pq_bg2_bgcolor_steelblue"=>"pq_steelblue","pq_bg2_bgcolor_lightskyblue"=>"pq_lightskyblue","pq_bg2_bgcolor_skyblue"=>"pq_skyblue","pq_bg2_bgcolor_deepskyblue"=>"pq_deepskyblue","pq_bg2_bgcolor_lightblue"=>"pq_lightblue","pq_bg2_bgcolor_powderblue"=>"pq_powderblue","pq_bg2_bgcolor_darkturquoise"=>"pq_darkturquoise","pq_bg2_bgcolor_cadetblue"=>"pq_cadetblue","pq_bg2_bgcolor_cyan"=>"pq_cyan","pq_bg2_bgcolor_teal"=>"pq_teal","pq_bg2_bgcolor_mediumturquoise"=>"pq_mediumturquoise","pq_bg2_bgcolor_lightseagreen"=>"pq_lightseagreen","pq_bg2_bgcolor_paleturquoise"=>"pq_paleturquoise","pq_bg2_bgcolor_mediumspringgreen"=>"pq_mediumspringgreen","pq_bg2_bgcolor_springgreen"=>"pq_springgreen","pq_bg2_bgcolor_darkseagreen"=>"pq_darkseagreen","pq_bg2_bgcolor_palegreen"=>"pq_palegreen","pq_bg2_bgcolor_lmgreen"=>"pq_lmgreen","pq_bg2_bgcolor_forestgreen"=>"pq_forestgreen","pq_bg2_bgcolor_darkgreen"=>"pq_darkgreen","pq_bg2_bgcolor_lawngreen"=>"pq_lawngreen","pq_bg2_bgcolor_grnyellow"=>"pq_grnyellow","pq_bg2_bgcolor_darkolivegreen"=>"pq_darkolivegreen","pq_bg2_bgcolor_olvdrab"=>"pq_olvdrab","pq_bg2_bgcolor_yellow"=>"pq_yellow","pq_bg2_bgcolor_olive"=>"pq_olive","pq_bg2_bgcolor_darkkhaki"=>"pq_darkkhaki","pq_bg2_bgcolor_khaki"=>"pq_khaki","pq_bg2_bgcolor_gold"=>"pq_gold","pq_bg2_bgcolor_gldenrod"=>"pq_gldenrod","pq_bg2_bgcolor_orange"=>"pq_orange","pq_bg2_bgcolor_wheat"=>"pq_wheat","pq_bg2_bgcolor_navajowhite"=>"pq_navajowhite","pq_bg2_bgcolor_burlywood"=>"pq_burlywood","pq_bg2_bgcolor_darkorange"=>"pq_darkorange","pq_bg2_bgcolor_sienna"=>"pq_sienna","pq_bg2_bgcolor_orngred"=>"pq_orngred","pq_bg2_bgcolor_tomato"=>"pq_tomato","pq_bg2_bgcolor_salmon"=>"pq_salmon","pq_bg2_bgcolor_brown"=>"pq_brown","pq_bg2_bgcolor_red"=>"pq_red","pq_bg2_bgcolor_black"=>"pq_black","pq_bg2_bgcolor_darkgrey"=>"pq_darkgrey","pq_bg2_bgcolor_dimgrey"=>"pq_dimgrey","pq_bg2_bgcolor_lightgrey"=>"pq_lightgrey","pq_bg2_bgcolor_slategrey"=>"pq_slategrey","pq_bg2_bgcolor_lightslategrey"=>"pq_lightslategrey","pq_bg2_bgcolor_silver"=>"pq_silver","pq_bg2_bgcolor_whtsmoke"=>"pq_whtsmoke","pq_bg2_bgcolor_white"=>"pq_white"), $name.'_second_background', 0);
		//OVERLAY COLOR
		$allFormElements['overlay_color'] = $this->generateSelect($name, $name.'[overlay]', $this->_dictionary[designOptions][_overlay], array("pq_over_bgcolor_indianred"=>"Indianred","pq_over_bgcolor_crimson"=>"Crimson","pq_over_bgcolor_lightpink"=>"Lightpink","pq_over_bgcolor_pink"=>"Pink","pq_over_bgcolor_palevioletred"=>"Palevioletred","pq_over_bgcolor_hotpink"=>"Hotpink","pq_over_bgcolor_mediumvioletred"=>"Mediumvioletred","pq_over_bgcolor_orchid"=>"Orchid","pq_over_bgcolor_plum"=>"Plum","pq_over_bgcolor_violet"=>"Violet","pq_over_bgcolor_magenta"=>"Magenta","pq_over_bgcolor_purple"=>"Purple","pq_over_bgcolor_mediumorchid"=>"Mediumorchid","pq_over_bgcolor_darkviolet"=>"Darkviolet","pq_over_bgcolor_darkorchid"=>"Darkorchid","pq_over_bgcolor_indigo"=>"Indigo","pq_over_bgcolor_blviolet"=>"Blueviolet","pq_over_bgcolor_mediumpurple"=>"Mediumpurple","pq_over_bgcolor_darkslateblue"=>"Darkslateblue","pq_over_bgcolor_mediumslateblue"=>"Mediumslateblue","pq_over_bgcolor_slateblue"=>"Slateblue","pq_over_bgcolor_blue"=>"Blue","pq_over_bgcolor_navy"=>"Navy","pq_over_bgcolor_midnightblue"=>"Midnightblue","pq_over_bgcolor_royalblue"=>"Royalblue","pq_over_bgcolor_cornflowerblue"=>"Cornflowerblue","pq_over_bgcolor_steelblue"=>"Steelblue","pq_over_bgcolor_lightskyblue"=>"Lightskyblue","pq_over_bgcolor_skyblue"=>"Skyblue","pq_over_bgcolor_deepskyblue"=>"Deepskyblue","pq_over_bgcolor_lightblue"=>"Lightblue","pq_over_bgcolor_powderblue"=>"Powderblue","pq_over_bgcolor_darkturquoise"=>"Darkturquoise","pq_over_bgcolor_cadetblue"=>"Cadetblue","pq_over_bgcolor_cyan"=>"Cyan","pq_over_bgcolor_teal"=>"Teal","pq_over_bgcolor_mediumturquoise"=>"Mediumturquoise","pq_over_bgcolor_lightseagreen"=>"Lightseagreen","pq_over_bgcolor_paleturquoise"=>"Paleturquoise","pq_over_bgcolor_mediumspringgreen"=>"Mediumspringgreen","pq_over_bgcolor_springgreen"=>"Springgreen","pq_over_bgcolor_darkseagreen"=>"Darkseagreen","pq_over_bgcolor_palegreen"=>"Palegreen","pq_over_bgcolor_lmgreen"=>"Limegreen","pq_over_bgcolor_forestgreen"=>"Forestgreen","pq_over_bgcolor_darkgreen"=>"Darkgreen","pq_over_bgcolor_lawngreen"=>"Lawngreen","pq_over_bgcolor_grnyellow"=>"Greenyellow","pq_over_bgcolor_darkolivegreen"=>"Darkolivegreen","pq_over_bgcolor_olvdrab"=>"Olivedrab","pq_over_bgcolor_yellow"=>"Yellow","pq_over_bgcolor_olive"=>"Olive","pq_over_bgcolor_darkkhaki"=>"Darkkhaki","pq_over_bgcolor_khaki"=>"Khaki","pq_over_bgcolor_gold"=>"Gold","pq_over_bgcolor_gldenrod"=>"Goldenrod","pq_over_bgcolor_orange"=>"Orange","pq_over_bgcolor_wheat"=>"Wheat","pq_over_bgcolor_navajowhite"=>"Navajowhite","pq_over_bgcolor_burlywood"=>"Burlywood","pq_over_bgcolor_darkorange"=>"Darkorange","pq_over_bgcolor_sienna"=>"Sienna","pq_over_bgcolor_orngred"=>"Orangered","pq_over_bgcolor_tomato"=>"Tomato","pq_over_bgcolor_salmon"=>"Salmon","pq_over_bgcolor_brown"=>"Brown","pq_over_bgcolor_red"=>"Red","pq_over_bgcolor_black"=>"Black","pq_over_bgcolor_darkgrey"=>"Darkgrey","pq_over_bgcolor_dimgrey"=>"Dimgrey","pq_over_bgcolor_lightgrey"=>"Lightgrey","pq_over_bgcolor_slategrey"=>"Slategrey","pq_over_bgcolor_lightslategrey"=>"Lightslategrey","pq_over_bgcolor_silver"=>"Silver","pq_over_bgcolor_whtsmoke"=>"Whitesmoke","pq_over_bgcolor_white"=>"White"), $valArray[overlay], array("pq_over_bgcolor_indianred"=>"pq_indianred","pq_over_bgcolor_crimson"=>"pq_crimson","pq_over_bgcolor_lightpink"=>"pq_lightpink","pq_over_bgcolor_pink"=>"pq_pink","pq_over_bgcolor_palevioletred"=>"pq_palevioletred","pq_over_bgcolor_hotpink"=>"pq_hotpink","pq_over_bgcolor_mediumvioletred"=>"pq_mediumvioletred","pq_over_bgcolor_orchid"=>"pq_orchid","pq_over_bgcolor_plum"=>"pq_plum","pq_over_bgcolor_violet"=>"pq_violet","pq_over_bgcolor_magenta"=>"pq_magenta","pq_over_bgcolor_purple"=>"pq_purple","pq_over_bgcolor_mediumorchid"=>"pq_mediumorchid","pq_over_bgcolor_darkviolet"=>"pq_darkviolet","pq_over_bgcolor_darkorchid"=>"pq_darkorchid","pq_over_bgcolor_indigo"=>"pq_indigo","pq_over_bgcolor_blviolet"=>"pq_blviolet","pq_over_bgcolor_mediumpurple"=>"pq_mediumpurple","pq_over_bgcolor_darkslateblue"=>"pq_darkslateblue","pq_over_bgcolor_mediumslateblue"=>"pq_mediumslateblue","pq_over_bgcolor_slateblue"=>"pq_slateblue","pq_over_bgcolor_blue"=>"pq_blue","pq_over_bgcolor_navy"=>"pq_navy","pq_over_bgcolor_midnightblue"=>"pq_midnightblue","pq_over_bgcolor_royalblue"=>"pq_royalblue","pq_over_bgcolor_cornflowerblue"=>"pq_cornflowerblue","pq_over_bgcolor_steelblue"=>"pq_steelblue","pq_over_bgcolor_lightskyblue"=>"pq_lightskyblue","pq_over_bgcolor_skyblue"=>"pq_skyblue","pq_over_bgcolor_deepskyblue"=>"pq_deepskyblue","pq_over_bgcolor_lightblue"=>"pq_lightblue","pq_over_bgcolor_powderblue"=>"pq_powderblue","pq_over_bgcolor_darkturquoise"=>"pq_darkturquoise","pq_over_bgcolor_cadetblue"=>"pq_cadetblue","pq_over_bgcolor_cyan"=>"pq_cyan","pq_over_bgcolor_teal"=>"pq_teal","pq_over_bgcolor_mediumturquoise"=>"pq_mediumturquoise","pq_over_bgcolor_lightseagreen"=>"pq_lightseagreen","pq_over_bgcolor_paleturquoise"=>"pq_paleturquoise","pq_over_bgcolor_mediumspringgreen"=>"pq_mediumspringgreen","pq_over_bgcolor_springgreen"=>"pq_springgreen","pq_over_bgcolor_darkseagreen"=>"pq_darkseagreen","pq_over_bgcolor_palegreen"=>"pq_palegreen","pq_over_bgcolor_lmgreen"=>"pq_lmgreen","pq_over_bgcolor_forestgreen"=>"pq_forestgreen","pq_over_bgcolor_darkgreen"=>"pq_darkgreen","pq_over_bgcolor_lawngreen"=>"pq_lawngreen","pq_over_bgcolor_grnyellow"=>"pq_grnyellow","pq_over_bgcolor_darkolivegreen"=>"pq_darkolivegreen","pq_over_bgcolor_olvdrab"=>"pq_olvdrab","pq_over_bgcolor_yellow"=>"pq_yellow","pq_over_bgcolor_olive"=>"pq_olive","pq_over_bgcolor_darkkhaki"=>"pq_darkkhaki","pq_over_bgcolor_khaki"=>"pq_khaki","pq_over_bgcolor_gold"=>"pq_gold","pq_over_bgcolor_gldenrod"=>"pq_gldenrod","pq_over_bgcolor_orange"=>"pq_orange","pq_over_bgcolor_wheat"=>"pq_wheat","pq_over_bgcolor_navajowhite"=>"pq_navajowhite","pq_over_bgcolor_burlywood"=>"pq_burlywood","pq_over_bgcolor_darkorange"=>"pq_darkorange","pq_over_bgcolor_sienna"=>"pq_sienna","pq_over_bgcolor_orngred"=>"pq_orngred","pq_over_bgcolor_tomato"=>"pq_tomato","pq_over_bgcolor_salmon"=>"pq_salmon","pq_over_bgcolor_brown"=>"pq_brown","pq_over_bgcolor_red"=>"pq_red","pq_over_bgcolor_black"=>"pq_black","pq_over_bgcolor_darkgrey"=>"pq_darkgrey","pq_over_bgcolor_dimgrey"=>"pq_dimgrey","pq_over_bgcolor_lightgrey"=>"pq_lightgrey","pq_over_bgcolor_slategrey"=>"pq_slategrey","pq_over_bgcolor_lightslategrey"=>"pq_lightslategrey","pq_over_bgcolor_silver"=>"pq_silver","pq_over_bgcolor_whtsmoke"=>"pq_whtsmoke","pq_over_bgcolor_white"=>"pq_white"), $name.'_overlay', 0);
		//ANIMATION
		$allFormElements['animation'] = $this->generateSelect($name, $name.'[animation]', $this->_dictionary[designOptions][_animation], array("pq_anim_swing" => "Swing", "pq_anim_bounceInDown"=>"Bounce In Down", "pq_anim_tada" => "Tada","pq_anim_bounceInLeft" => "Bounce In Left","pq_anim_bounceInRight" => "Bounce In Right","pq_anim_bounceInUp" => "Bounce In Up","pq_anim_flipInY" => "Flip In Y","pq_anim_flipInX" => "Flip In X","pq_anim_zoomIn" => "Zoom In"), $valArray[animation], array(), $name.'_animation', 0);
		//BUTTON TEXT_COLOR
		$allFormElements['button_text_color'] = $this->generateSelect($name, $name.'[button_text_color]', $this->_dictionary[designOptions][_button_text_color], array("pq_btn_color_btngroupinput_indianred"=>"Indianred","pq_btn_color_btngroupinput_crimson"=>"Crimson","pq_btn_color_btngroupinput_lightpink"=>"Lightpink","pq_btn_color_btngroupinput_pink"=>"Pink","pq_btn_color_btngroupinput_palevioletred"=>"Palevioletred","pq_btn_color_btngroupinput_hotpink"=>"Hotpink","pq_btn_color_btngroupinput_mediumvioletred"=>"Mediumvioletred","pq_btn_color_btngroupinput_orchid"=>"Orchid","pq_btn_color_btngroupinput_plum"=>"Plum","pq_btn_color_btngroupinput_violet"=>"Violet","pq_btn_color_btngroupinput_magenta"=>"Magenta","pq_btn_color_btngroupinput_purple"=>"Purple","pq_btn_color_btngroupinput_mediumorchid"=>"Mediumorchid","pq_btn_color_btngroupinput_darkviolet"=>"Darkviolet","pq_btn_color_btngroupinput_darkorchid"=>"Darkorchid","pq_btn_color_btngroupinput_indigo"=>"Indigo","pq_btn_color_btngroupinput_blviolet"=>"Blueviolet","pq_btn_color_btngroupinput_mediumpurple"=>"Mediumpurple","pq_btn_color_btngroupinput_darkslateblue"=>"Darkslateblue","pq_btn_color_btngroupinput_mediumslateblue"=>"Mediumslateblue","pq_btn_color_btngroupinput_slateblue"=>"Slateblue","pq_btn_color_btngroupinput_blue"=>"Blue","pq_btn_color_btngroupinput_navy"=>"Navy","pq_btn_color_btngroupinput_midnightblue"=>"Midnightblue","pq_btn_color_btngroupinput_royalblue"=>"Royalblue","pq_btn_color_btngroupinput_cornflowerblue"=>"Cornflowerblue","pq_btn_color_btngroupinput_steelblue"=>"Steelblue","pq_btn_color_btngroupinput_lightskyblue"=>"Lightskyblue","pq_btn_color_btngroupinput_skyblue"=>"Skyblue","pq_btn_color_btngroupinput_deepskyblue"=>"Deepskyblue","pq_btn_color_btngroupinput_lightblue"=>"Lightblue","pq_btn_color_btngroupinput_powderblue"=>"Powderblue","pq_btn_color_btngroupinput_darkturquoise"=>"Darkturquoise","pq_btn_color_btngroupinput_cadetblue"=>"Cadetblue","pq_btn_color_btngroupinput_cyan"=>"Cyan","pq_btn_color_btngroupinput_teal"=>"Teal","pq_btn_color_btngroupinput_mediumturquoise"=>"Mediumturquoise","pq_btn_color_btngroupinput_lightseagreen"=>"Lightseagreen","pq_btn_color_btngroupinput_paleturquoise"=>"Paleturquoise","pq_btn_color_btngroupinput_mediumspringgreen"=>"Mediumspringgreen","pq_btn_color_btngroupinput_springgreen"=>"Springgreen","pq_btn_color_btngroupinput_darkseagreen"=>"Darkseagreen","pq_btn_color_btngroupinput_palegreen"=>"Palegreen","pq_btn_color_btngroupinput_lmgreen"=>"Limegreen","pq_btn_color_btngroupinput_forestgreen"=>"Forestgreen","pq_btn_color_btngroupinput_darkgreen"=>"Darkgreen","pq_btn_color_btngroupinput_lawngreen"=>"Lawngreen","pq_btn_color_btngroupinput_grnyellow"=>"Greenyellow","pq_btn_color_btngroupinput_darkolivegreen"=>"Darkolivegreen","pq_btn_color_btngroupinput_olvdrab"=>"Olivedrab","pq_btn_color_btngroupinput_yellow"=>"Yellow","pq_btn_color_btngroupinput_olive"=>"Olive","pq_btn_color_btngroupinput_darkkhaki"=>"Darkkhaki","pq_btn_color_btngroupinput_khaki"=>"Khaki","pq_btn_color_btngroupinput_gold"=>"Gold","pq_btn_color_btngroupinput_gldenrod"=>"Goldenrod","pq_btn_color_btngroupinput_orange"=>"Orange","pq_btn_color_btngroupinput_wheat"=>"Wheat","pq_btn_color_btngroupinput_navajowhite"=>"Navajowhite","pq_btn_color_btngroupinput_burlywood"=>"Burlywood","pq_btn_color_btngroupinput_darkorange"=>"Darkorange","pq_btn_color_btngroupinput_sienna"=>"Sienna","pq_btn_color_btngroupinput_orngred"=>"Orangered","pq_btn_color_btngroupinput_tomato"=>"Tomato","pq_btn_color_btngroupinput_salmon"=>"Salmon","pq_btn_color_btngroupinput_brown"=>"Brown","pq_btn_color_btngroupinput_red"=>"Red","pq_btn_color_btngroupinput_black"=>"Black","pq_btn_color_btngroupinput_darkgrey"=>"Darkgrey","pq_btn_color_btngroupinput_dimgrey"=>"Dimgrey","pq_btn_color_btngroupinput_lightgrey"=>"Lightgrey","pq_btn_color_btngroupinput_slategrey"=>"Slategrey","pq_btn_color_btngroupinput_lightslategrey"=>"Lightslategrey","pq_btn_color_btngroupinput_silver"=>"Silver","pq_btn_color_btngroupinput_whtsmoke"=>"Whitesmoke","pq_btn_color_btngroupinput_white"=>"White"), $valArray[button_text_color], array("pq_btn_color_btngroupinput_indianred"=>"pq_indianred","pq_btn_color_btngroupinput_crimson"=>"pq_crimson","pq_btn_color_btngroupinput_lightpink"=>"pq_lightpink","pq_btn_color_btngroupinput_pink"=>"pq_pink","pq_btn_color_btngroupinput_palevioletred"=>"pq_palevioletred","pq_btn_color_btngroupinput_hotpink"=>"pq_hotpink","pq_btn_color_btngroupinput_mediumvioletred"=>"pq_mediumvioletred","pq_btn_color_btngroupinput_orchid"=>"pq_orchid","pq_btn_color_btngroupinput_plum"=>"pq_plum","pq_btn_color_btngroupinput_violet"=>"pq_violet","pq_btn_color_btngroupinput_magenta"=>"pq_magenta","pq_btn_color_btngroupinput_purple"=>"pq_purple","pq_btn_color_btngroupinput_mediumorchid"=>"pq_mediumorchid","pq_btn_color_btngroupinput_darkviolet"=>"pq_darkviolet","pq_btn_color_btngroupinput_darkorchid"=>"pq_darkorchid","pq_btn_color_btngroupinput_indigo"=>"pq_indigo","pq_btn_color_btngroupinput_blviolet"=>"pq_blviolet","pq_btn_color_btngroupinput_mediumpurple"=>"pq_mediumpurple","pq_btn_color_btngroupinput_darkslateblue"=>"pq_darkslateblue","pq_btn_color_btngroupinput_mediumslateblue"=>"pq_mediumslateblue","pq_btn_color_btngroupinput_slateblue"=>"pq_slateblue","pq_btn_color_btngroupinput_blue"=>"pq_blue","pq_btn_color_btngroupinput_navy"=>"pq_navy","pq_btn_color_btngroupinput_midnightblue"=>"pq_midnightblue","pq_btn_color_btngroupinput_royalblue"=>"pq_royalblue","pq_btn_color_btngroupinput_cornflowerblue"=>"pq_cornflowerblue","pq_btn_color_btngroupinput_steelblue"=>"pq_steelblue","pq_btn_color_btngroupinput_lightskyblue"=>"pq_lightskyblue","pq_btn_color_btngroupinput_skyblue"=>"pq_skyblue","pq_btn_color_btngroupinput_deepskyblue"=>"pq_deepskyblue","pq_btn_color_btngroupinput_lightblue"=>"pq_lightblue","pq_btn_color_btngroupinput_powderblue"=>"pq_powderblue","pq_btn_color_btngroupinput_darkturquoise"=>"pq_darkturquoise","pq_btn_color_btngroupinput_cadetblue"=>"pq_cadetblue","pq_btn_color_btngroupinput_cyan"=>"pq_cyan","pq_btn_color_btngroupinput_teal"=>"pq_teal","pq_btn_color_btngroupinput_mediumturquoise"=>"pq_mediumturquoise","pq_btn_color_btngroupinput_lightseagreen"=>"pq_lightseagreen","pq_btn_color_btngroupinput_paleturquoise"=>"pq_paleturquoise","pq_btn_color_btngroupinput_mediumspringgreen"=>"pq_mediumspringgreen","pq_btn_color_btngroupinput_springgreen"=>"pq_springgreen","pq_btn_color_btngroupinput_darkseagreen"=>"pq_darkseagreen","pq_btn_color_btngroupinput_palegreen"=>"pq_palegreen","pq_btn_color_btngroupinput_lmgreen"=>"pq_lmgreen","pq_btn_color_btngroupinput_forestgreen"=>"pq_forestgreen","pq_btn_color_btngroupinput_darkgreen"=>"pq_darkgreen","pq_btn_color_btngroupinput_lawngreen"=>"pq_lawngreen","pq_btn_color_btngroupinput_grnyellow"=>"pq_grnyellow","pq_btn_color_btngroupinput_darkolivegreen"=>"pq_darkolivegreen","pq_btn_color_btngroupinput_olvdrab"=>"pq_olvdrab","pq_btn_color_btngroupinput_yellow"=>"pq_yellow","pq_btn_color_btngroupinput_olive"=>"pq_olive","pq_btn_color_btngroupinput_darkkhaki"=>"pq_darkkhaki","pq_btn_color_btngroupinput_khaki"=>"pq_khaki","pq_btn_color_btngroupinput_gold"=>"pq_gold","pq_btn_color_btngroupinput_gldenrod"=>"pq_gldenrod","pq_btn_color_btngroupinput_orange"=>"pq_orange","pq_btn_color_btngroupinput_wheat"=>"pq_wheat","pq_btn_color_btngroupinput_navajowhite"=>"pq_navajowhite","pq_btn_color_btngroupinput_burlywood"=>"pq_burlywood","pq_btn_color_btngroupinput_darkorange"=>"pq_darkorange","pq_btn_color_btngroupinput_sienna"=>"pq_sienna","pq_btn_color_btngroupinput_orngred"=>"pq_orngred","pq_btn_color_btngroupinput_tomato"=>"pq_tomato","pq_btn_color_btngroupinput_salmon"=>"pq_salmon","pq_btn_color_btngroupinput_brown"=>"pq_brown","pq_btn_color_btngroupinput_red"=>"pq_red","pq_btn_color_btngroupinput_black"=>"pq_black","pq_btn_color_btngroupinput_darkgrey"=>"pq_darkgrey","pq_btn_color_btngroupinput_dimgrey"=>"pq_dimgrey","pq_btn_color_btngroupinput_lightgrey"=>"pq_lightgrey","pq_btn_color_btngroupinput_slategrey"=>"pq_slategrey","pq_btn_color_btngroupinput_lightslategrey"=>"pq_lightslategrey","pq_btn_color_btngroupinput_silver"=>"pq_silver","pq_btn_color_btngroupinput_whtsmoke"=>"pq_whtsmoke","pq_btn_color_btngroupinput_white"=>"pq_white"), $name.'_button_text_color', 0);
		//BUTTON COLOR
		$allFormElements['button_color'] = $this->generateSelect($name, $name.'[button_color]', $this->_dictionary[designOptions][_button_color], array("pq_btn_bgcolor_btngroupinput_indianred"=>"Indianred","pq_btn_bgcolor_btngroupinput_crimson"=>"Crimson","pq_btn_bgcolor_btngroupinput_lightpink"=>"Lightpink","pq_btn_bgcolor_btngroupinput_pink"=>"Pink","pq_btn_bgcolor_btngroupinput_palevioletred"=>"Palevioletred","pq_btn_bgcolor_btngroupinput_hotpink"=>"Hotpink","pq_btn_bgcolor_btngroupinput_mediumvioletred"=>"Mediumvioletred","pq_btn_bgcolor_btngroupinput_orchid"=>"Orchid","pq_btn_bgcolor_btngroupinput_plum"=>"Plum","pq_btn_bgcolor_btngroupinput_violet"=>"Violet","pq_btn_bgcolor_btngroupinput_magenta"=>"Magenta","pq_btn_bgcolor_btngroupinput_purple"=>"Purple","pq_btn_bgcolor_btngroupinput_mediumorchid"=>"Mediumorchid","pq_btn_bgcolor_btngroupinput_darkviolet"=>"Darkviolet","pq_btn_bgcolor_btngroupinput_darkorchid"=>"Darkorchid","pq_btn_bgcolor_btngroupinput_indigo"=>"Indigo","pq_btn_bgcolor_btngroupinput_blviolet"=>"Blueviolet","pq_btn_bgcolor_btngroupinput_mediumpurple"=>"Mediumpurple","pq_btn_bgcolor_btngroupinput_darkslateblue"=>"Darkslateblue","pq_btn_bgcolor_btngroupinput_mediumslateblue"=>"Mediumslateblue","pq_btn_bgcolor_btngroupinput_slateblue"=>"Slateblue","pq_btn_bgcolor_btngroupinput_blue"=>"Blue","pq_btn_bgcolor_btngroupinput_navy"=>"Navy","pq_btn_bgcolor_btngroupinput_midnightblue"=>"Midnightblue","pq_btn_bgcolor_btngroupinput_royalblue"=>"Royalblue","pq_btn_bgcolor_btngroupinput_cornflowerblue"=>"Cornflowerblue","pq_btn_bgcolor_btngroupinput_steelblue"=>"Steelblue","pq_btn_bgcolor_btngroupinput_lightskyblue"=>"Lightskyblue","pq_btn_bgcolor_btngroupinput_skyblue"=>"Skyblue","pq_btn_bgcolor_btngroupinput_deepskyblue"=>"Deepskyblue","pq_btn_bgcolor_btngroupinput_lightblue"=>"Lightblue","pq_btn_bgcolor_btngroupinput_powderblue"=>"Powderblue","pq_btn_bgcolor_btngroupinput_darkturquoise"=>"Darkturquoise","pq_btn_bgcolor_btngroupinput_cadetblue"=>"Cadetblue","pq_btn_bgcolor_btngroupinput_cyan"=>"Cyan","pq_btn_bgcolor_btngroupinput_teal"=>"Teal","pq_btn_bgcolor_btngroupinput_mediumturquoise"=>"Mediumturquoise","pq_btn_bgcolor_btngroupinput_lightseagreen"=>"Lightseagreen","pq_btn_bgcolor_btngroupinput_paleturquoise"=>"Paleturquoise","pq_btn_bgcolor_btngroupinput_mediumspringgreen"=>"Mediumspringgreen","pq_btn_bgcolor_btngroupinput_springgreen"=>"Springgreen","pq_btn_bgcolor_btngroupinput_darkseagreen"=>"Darkseagreen","pq_btn_bgcolor_btngroupinput_palegreen"=>"Palegreen","pq_btn_bgcolor_btngroupinput_lmgreen"=>"Limegreen","pq_btn_bgcolor_btngroupinput_forestgreen"=>"Forestgreen","pq_btn_bgcolor_btngroupinput_darkgreen"=>"Darkgreen","pq_btn_bgcolor_btngroupinput_lawngreen"=>"Lawngreen","pq_btn_bgcolor_btngroupinput_grnyellow"=>"Greenyellow","pq_btn_bgcolor_btngroupinput_darkolivegreen"=>"Darkolivegreen","pq_btn_bgcolor_btngroupinput_olvdrab"=>"Olivedrab","pq_btn_bgcolor_btngroupinput_yellow"=>"Yellow","pq_btn_bgcolor_btngroupinput_olive"=>"Olive","pq_btn_bgcolor_btngroupinput_darkkhaki"=>"Darkkhaki","pq_btn_bgcolor_btngroupinput_khaki"=>"Khaki","pq_btn_bgcolor_btngroupinput_gold"=>"Gold","pq_btn_bgcolor_btngroupinput_gldenrod"=>"Goldenrod","pq_btn_bgcolor_btngroupinput_orange"=>"Orange","pq_btn_bgcolor_btngroupinput_wheat"=>"Wheat","pq_btn_bgcolor_btngroupinput_navajowhite"=>"Navajowhite","pq_btn_bgcolor_btngroupinput_burlywood"=>"Burlywood","pq_btn_bgcolor_btngroupinput_darkorange"=>"Darkorange","pq_btn_bgcolor_btngroupinput_sienna"=>"Sienna","pq_btn_bgcolor_btngroupinput_orngred"=>"Orangered","pq_btn_bgcolor_btngroupinput_tomato"=>"Tomato","pq_btn_bgcolor_btngroupinput_salmon"=>"Salmon","pq_btn_bgcolor_btngroupinput_brown"=>"Brown","pq_btn_bgcolor_btngroupinput_red"=>"Red","pq_btn_bgcolor_btngroupinput_black"=>"Black","pq_btn_bgcolor_btngroupinput_darkgrey"=>"Darkgrey","pq_btn_bgcolor_btngroupinput_dimgrey"=>"Dimgrey","pq_btn_bgcolor_btngroupinput_lightgrey"=>"Lightgrey","pq_btn_bgcolor_btngroupinput_slategrey"=>"Slategrey","pq_btn_bgcolor_btngroupinput_lightslategrey"=>"Lightslategrey","pq_btn_bgcolor_btngroupinput_silver"=>"Silver","pq_btn_bgcolor_btngroupinput_whtsmoke"=>"Whitesmoke","pq_btn_bgcolor_btngroupinput_white"=>"White"), $valArray[button_color], array("pq_btn_bgcolor_btngroupinput_indianred"=>"pq_indianred","pq_btn_bgcolor_btngroupinput_crimson"=>"pq_crimson","pq_btn_bgcolor_btngroupinput_lightpink"=>"pq_lightpink","pq_btn_bgcolor_btngroupinput_pink"=>"pq_pink","pq_btn_bgcolor_btngroupinput_palevioletred"=>"pq_palevioletred","pq_btn_bgcolor_btngroupinput_hotpink"=>"pq_hotpink","pq_btn_bgcolor_btngroupinput_mediumvioletred"=>"pq_mediumvioletred","pq_btn_bgcolor_btngroupinput_orchid"=>"pq_orchid","pq_btn_bgcolor_btngroupinput_plum"=>"pq_plum","pq_btn_bgcolor_btngroupinput_violet"=>"pq_violet","pq_btn_bgcolor_btngroupinput_magenta"=>"pq_magenta","pq_btn_bgcolor_btngroupinput_purple"=>"pq_purple","pq_btn_bgcolor_btngroupinput_mediumorchid"=>"pq_mediumorchid","pq_btn_bgcolor_btngroupinput_darkviolet"=>"pq_darkviolet","pq_btn_bgcolor_btngroupinput_darkorchid"=>"pq_darkorchid","pq_btn_bgcolor_btngroupinput_indigo"=>"pq_indigo","pq_btn_bgcolor_btngroupinput_blviolet"=>"pq_blviolet","pq_btn_bgcolor_btngroupinput_mediumpurple"=>"pq_mediumpurple","pq_btn_bgcolor_btngroupinput_darkslateblue"=>"pq_darkslateblue","pq_btn_bgcolor_btngroupinput_mediumslateblue"=>"pq_mediumslateblue","pq_btn_bgcolor_btngroupinput_slateblue"=>"pq_slateblue","pq_btn_bgcolor_btngroupinput_blue"=>"pq_blue","pq_btn_bgcolor_btngroupinput_navy"=>"pq_navy","pq_btn_bgcolor_btngroupinput_midnightblue"=>"pq_midnightblue","pq_btn_bgcolor_btngroupinput_royalblue"=>"pq_royalblue","pq_btn_bgcolor_btngroupinput_cornflowerblue"=>"pq_cornflowerblue","pq_btn_bgcolor_btngroupinput_steelblue"=>"pq_steelblue","pq_btn_bgcolor_btngroupinput_lightskyblue"=>"pq_lightskyblue","pq_btn_bgcolor_btngroupinput_skyblue"=>"pq_skyblue","pq_btn_bgcolor_btngroupinput_deepskyblue"=>"pq_deepskyblue","pq_btn_bgcolor_btngroupinput_lightblue"=>"pq_lightblue","pq_btn_bgcolor_btngroupinput_powderblue"=>"pq_powderblue","pq_btn_bgcolor_btngroupinput_darkturquoise"=>"pq_darkturquoise","pq_btn_bgcolor_btngroupinput_cadetblue"=>"pq_cadetblue","pq_btn_bgcolor_btngroupinput_cyan"=>"pq_cyan","pq_btn_bgcolor_btngroupinput_teal"=>"pq_teal","pq_btn_bgcolor_btngroupinput_mediumturquoise"=>"pq_mediumturquoise","pq_btn_bgcolor_btngroupinput_lightseagreen"=>"pq_lightseagreen","pq_btn_bgcolor_btngroupinput_paleturquoise"=>"pq_paleturquoise","pq_btn_bgcolor_btngroupinput_mediumspringgreen"=>"pq_mediumspringgreen","pq_btn_bgcolor_btngroupinput_springgreen"=>"pq_springgreen","pq_btn_bgcolor_btngroupinput_darkseagreen"=>"pq_darkseagreen","pq_btn_bgcolor_btngroupinput_palegreen"=>"pq_palegreen","pq_btn_bgcolor_btngroupinput_lmgreen"=>"pq_lmgreen","pq_btn_bgcolor_btngroupinput_forestgreen"=>"pq_forestgreen","pq_btn_bgcolor_btngroupinput_darkgreen"=>"pq_darkgreen","pq_btn_bgcolor_btngroupinput_lawngreen"=>"pq_lawngreen","pq_btn_bgcolor_btngroupinput_grnyellow"=>"pq_grnyellow","pq_btn_bgcolor_btngroupinput_darkolivegreen"=>"pq_darkolivegreen","pq_btn_bgcolor_btngroupinput_olvdrab"=>"pq_olvdrab","pq_btn_bgcolor_btngroupinput_yellow"=>"pq_yellow","pq_btn_bgcolor_btngroupinput_olive"=>"pq_olive","pq_btn_bgcolor_btngroupinput_darkkhaki"=>"pq_darkkhaki","pq_btn_bgcolor_btngroupinput_khaki"=>"pq_khaki","pq_btn_bgcolor_btngroupinput_gold"=>"pq_gold","pq_btn_bgcolor_btngroupinput_gldenrod"=>"pq_gldenrod","pq_btn_bgcolor_btngroupinput_orange"=>"pq_orange","pq_btn_bgcolor_btngroupinput_wheat"=>"pq_wheat","pq_btn_bgcolor_btngroupinput_navajowhite"=>"pq_navajowhite","pq_btn_bgcolor_btngroupinput_burlywood"=>"pq_burlywood","pq_btn_bgcolor_btngroupinput_darkorange"=>"pq_darkorange","pq_btn_bgcolor_btngroupinput_sienna"=>"pq_sienna","pq_btn_bgcolor_btngroupinput_orngred"=>"pq_orngred","pq_btn_bgcolor_btngroupinput_tomato"=>"pq_tomato","pq_btn_bgcolor_btngroupinput_salmon"=>"pq_salmon","pq_btn_bgcolor_btngroupinput_brown"=>"pq_brown","pq_btn_bgcolor_btngroupinput_red"=>"pq_red","pq_btn_bgcolor_btngroupinput_black"=>"pq_black","pq_btn_bgcolor_btngroupinput_darkgrey"=>"pq_darkgrey","pq_btn_bgcolor_btngroupinput_dimgrey"=>"pq_dimgrey","pq_btn_bgcolor_btngroupinput_lightgrey"=>"pq_lightgrey","pq_btn_bgcolor_btngroupinput_slategrey"=>"pq_slategrey","pq_btn_bgcolor_btngroupinput_lightslategrey"=>"pq_lightslategrey","pq_btn_bgcolor_btngroupinput_silver"=>"pq_silver","pq_btn_bgcolor_btngroupinput_whtsmoke"=>"pq_whtsmoke","pq_btn_bgcolor_btngroupinput_white"=>"pq_white"), $name.'_button_color', 0);		
		//DESIGN ICON
		$allFormElements['design_icons'] = $this->generateSelect($name, $name.'[icon][design]', $this->_dictionary[designOptions][_icon_design], array("c1"=>"Type 1","c2"=>"Type 2","c3"=>"Type 3","c4"=>"Type 4","c5"=>"Type 5","c6"=>"Type 6","c7"=>"Type 7","c8"=>"Type 8","c9"=>"Type 9","c10"=>"Type 10","c11"=>"Type 11"), $valArray[icon][design], array(), $name.'_icon_design', 0);
		//FORM ICON
		$allFormElements['form_icons'] = $this->generateSelect($name, $name.'[icon][form]', $this->_dictionary[designOptions][_icon_form], array('pq_square'=>'Square', 'pq_rounded'=>'Rounded', 'pq_circle'=>'Circle'), $valArray[icon][form], array(), $name.'_icon_form', 0);
		//SIZE ICON
		$allFormElements['size_icons'] = $this->generateSelect($name, $name.'[icon][size]', $this->_dictionary[designOptions][_icon_size], array('x20'=>'Size S', 'x30'=>'Size M', 'x40'=>'Size M+', 'x50'=>'Size L', 'x70'=>'Size XL'), $valArray[icon][size], array(), $name.'_icon_size', 0);
		//SPACE ICON
		$allFormElements['space_icons'] = $this->generateSelect($name, $name.'[icon][space]', $this->_dictionary[designOptions][_icon_space], array('pq_step1'=>'1px','pq_step2'=>'2px','pq_step3'=>'3px','pq_step4'=>'4px','pq_step5'=>'5px','pq_step6'=>'6px','pq_step7'=>'7px','pq_step8'=>'8px','pq_step9'=>'9px','pq_step10'=>'10px'), $valArray[icon][space], array(), $name.'_icon_space', 0);
		//SHADOW ICON
		$allFormElements['shadow_icons'] = $this->generateSelect($name, $name.'[icon][shadow]', $this->_dictionary[designOptions][_icon_shadow], array('sh1'=>'Shadow №1','sh2'=>'Shadow №2','sh3'=>'Shadow №3','sh4'=>'Shadow №4','sh5'=>'Shadow №5','sh6'=>'Shadow №6'), $valArray[icon][shadow], array(), $name.'_icon_shadow', 0);
		//POSITION ICON
		$allFormElements['position_icons'] = $this->generateSelect($name, $name.'[icon][position]', $this->_dictionary[designOptions][_icon_position], array('pq_inline'=>'Inline'), $valArray[icon][position], array(), $name.'_icon_position', 0);
		//HOVER ICON
		$allFormElements['animation_icons'] = $this->generateSelect($name, $name.'[icon][animation]', $this->_dictionary[designOptions][_icon_animation], array("pq_hvr_grow" => "Grow","pq_hvr_shrink" => "Shrink","pq_hvr_pulse" => "Pulse","pq_hvr_push" => "Push","pq_hvr_float" => "Float","pq_hvr_sink" => "Sink","pq_hvr_hang" => "Hang","pq_hvr_buzz" => "Buzz","pq_hvr_border_fade" => "Bdr Fade","pq_hvr_hollow" => "Hollow","pq_hvr_glow" => "Glow","pq_hvr_grow_shadow" => "Grow Shadow"), $valArray[icon][animation], array(), $name.'_icon_animation', 0);		
		//MOBILE TYPE
		$allFormElements['mobile_view_type'] = $this->generateSelect($name, $name.'[mobile_type]', $this->_dictionary[designOptions][_mobile_type], array("pq_default" => "Default", "pq_mosaic" => "Mosaic", "pq_coin" => "Coin"), $valArray[mobile_type], array(), $name.'_mobile_type', 1);
		//MOBILE POSITION
		$allFormElements['mobile_position'] = $this->generateSelect($name, $name.'[mobile_position]', $this->_dictionary[designOptions][_mobile_position], array("pq_mobile_top" => "Top", "pq_mobile_bottom" => "Bottom"), $valArray[mobile_position], array(), $name.'_mobile_position', 1);		
		//Min Width
		$allFormElements['min_width'] = $this->generateSelect($name, $name.'[minWidth]', $this->_dictionary[designOptions][_minWidth], array(100 => "100px and more", 200 => "200px and more", 300 => "300px and more", 400 => "400px and more", 500 => "500px and more", 600 => "600px and more",), $valArray[minWidth], array(), $name.'_minWidth', 0);
		//Min Height
		$allFormElements['min_height'] = $this->generateSelect($name, $name.'[minHeight]', $this->_dictionary[designOptions][_minHeight], array(0 => "0px and more", 100 => "100px and more", 200 => "200px and more", 300 => "300px and more", 400 => "400px and more", 500 => "500px and more", 600 => "600px and more",), $valArray[minHeight], array(), $name.'_minHeight', 0);				
		//HEAD FONT SIZE
		$allFormElements['head_font_size'] = $this->generateSelect($name, $name.'[head_size]', $this->_dictionary[designOptions][_head_size], array("pq_head_size16" => "Size - 16","pq_head_size20" => "Size - 20","pq_head_size24" => "Size - 24","pq_head_size28" => "Size - 28","pq_head_size36" => "Size - 36","pq_head_size48" => "Size - 48"), $valArray[head_size], array(), $name.'_head_size', 0);
		//TEXT FONT SIZE
		$allFormElements['text_font_size'] = $this->generateSelect($name, $name.'[text_size]', $this->_dictionary[designOptions][_text_size], array("pq_text_size12" => "Size - 12","pq_text_size16" => "Size - 16","pq_text_size20" => "Size - 20","pq_text_size24" => "Size - 24","pq_text_size28" => "Size - 28","pq_text_size36" => "Size - 36"), $valArray[text_size], array(), $name.'_text_size', 0);
		//HEAD COLOR
		$allFormElements['head_color'] = $this->generateSelect($name, $name.'[head_color]', $this->_dictionary[designOptions][_head_color], array("pq_h_color_h1_indianred"=>"Indianred","pq_h_color_h1_crimson"=>"Crimson","pq_h_color_h1_lightpink"=>"Lightpink","pq_h_color_h1_pink"=>"Pink","pq_h_color_h1_palevioletred"=>"Palevioletred","pq_h_color_h1_hotpink"=>"Hotpink","pq_h_color_h1_mediumvioletred"=>"Mediumvioletred","pq_h_color_h1_orchid"=>"Orchid","pq_h_color_h1_plum"=>"Plum","pq_h_color_h1_violet"=>"Violet","pq_h_color_h1_magenta"=>"Magenta","pq_h_color_h1_purple"=>"Purple","pq_h_color_h1_mediumorchid"=>"Mediumorchid","pq_h_color_h1_darkviolet"=>"Darkviolet","pq_h_color_h1_darkorchid"=>"Darkorchid","pq_h_color_h1_indigo"=>"Indigo","pq_h_color_h1_blviolet"=>"Blueviolet","pq_h_color_h1_mediumpurple"=>"Mediumpurple","pq_h_color_h1_darkslateblue"=>"Darkslateblue","pq_h_color_h1_mediumslateblue"=>"Mediumslateblue","pq_h_color_h1_slateblue"=>"Slateblue","pq_h_color_h1_blue"=>"Blue","pq_h_color_h1_navy"=>"Navy","pq_h_color_h1_midnightblue"=>"Midnightblue","pq_h_color_h1_royalblue"=>"Royalblue","pq_h_color_h1_cornflowerblue"=>"Cornflowerblue","pq_h_color_h1_steelblue"=>"Steelblue","pq_h_color_h1_lightskyblue"=>"Lightskyblue","pq_h_color_h1_skyblue"=>"Skyblue","pq_h_color_h1_deepskyblue"=>"Deepskyblue","pq_h_color_h1_lightblue"=>"Lightblue","pq_h_color_h1_powderblue"=>"Powderblue","pq_h_color_h1_darkturquoise"=>"Darkturquoise","pq_h_color_h1_cadetblue"=>"Cadetblue","pq_h_color_h1_cyan"=>"Cyan","pq_h_color_h1_teal"=>"Teal","pq_h_color_h1_mediumturquoise"=>"Mediumturquoise","pq_h_color_h1_lightseagreen"=>"Lightseagreen","pq_h_color_h1_paleturquoise"=>"Paleturquoise","pq_h_color_h1_mediumspringgreen"=>"Mediumspringgreen","pq_h_color_h1_springgreen"=>"Springgreen","pq_h_color_h1_darkseagreen"=>"Darkseagreen","pq_h_color_h1_palegreen"=>"Palegreen","pq_h_color_h1_lmgreen"=>"Limegreen","pq_h_color_h1_forestgreen"=>"Forestgreen","pq_h_color_h1_darkgreen"=>"Darkgreen","pq_h_color_h1_lawngreen"=>"Lawngreen","pq_h_color_h1_grnyellow"=>"Greenyellow","pq_h_color_h1_darkolivegreen"=>"Darkolivegreen","pq_h_color_h1_olvdrab"=>"Olivedrab","pq_h_color_h1_yellow"=>"Yellow","pq_h_color_h1_olive"=>"Olive","pq_h_color_h1_darkkhaki"=>"Darkkhaki","pq_h_color_h1_khaki"=>"Khaki","pq_h_color_h1_gold"=>"Gold","pq_h_color_h1_gldenrod"=>"Goldenrod","pq_h_color_h1_orange"=>"Orange","pq_h_color_h1_wheat"=>"Wheat","pq_h_color_h1_navajowhite"=>"Navajowhite","pq_h_color_h1_burlywood"=>"Burlywood","pq_h_color_h1_darkorange"=>"Darkorange","pq_h_color_h1_sienna"=>"Sienna","pq_h_color_h1_orngred"=>"Orangered","pq_h_color_h1_tomato"=>"Tomato","pq_h_color_h1_salmon"=>"Salmon","pq_h_color_h1_brown"=>"Brown","pq_h_color_h1_red"=>"Red","pq_h_color_h1_black"=>"Black","pq_h_color_h1_darkgrey"=>"Darkgrey","pq_h_color_h1_dimgrey"=>"Dimgrey","pq_h_color_h1_lightgrey"=>"Lightgrey","pq_h_color_h1_slategrey"=>"Slategrey","pq_h_color_h1_lightslategrey"=>"Lightslategrey","pq_h_color_h1_silver"=>"Silver","pq_h_color_h1_whtsmoke"=>"Whitesmoke","pq_h_color_h1_white"=>"White"), $valArray[head_color], array("pq_h_color_h1_indianred"=>"pq_indianred","pq_h_color_h1_crimson"=>"pq_crimson","pq_h_color_h1_lightpink"=>"pq_lightpink","pq_h_color_h1_pink"=>"pq_pink","pq_h_color_h1_palevioletred"=>"pq_palevioletred","pq_h_color_h1_hotpink"=>"pq_hotpink","pq_h_color_h1_mediumvioletred"=>"pq_mediumvioletred","pq_h_color_h1_orchid"=>"pq_orchid","pq_h_color_h1_plum"=>"pq_plum","pq_h_color_h1_violet"=>"pq_violet","pq_h_color_h1_magenta"=>"pq_magenta","pq_h_color_h1_purple"=>"pq_purple","pq_h_color_h1_mediumorchid"=>"pq_mediumorchid","pq_h_color_h1_darkviolet"=>"pq_darkviolet","pq_h_color_h1_darkorchid"=>"pq_darkorchid","pq_h_color_h1_indigo"=>"pq_indigo","pq_h_color_h1_blviolet"=>"pq_blviolet","pq_h_color_h1_mediumpurple"=>"pq_mediumpurple","pq_h_color_h1_darkslateblue"=>"pq_darkslateblue","pq_h_color_h1_mediumslateblue"=>"pq_mediumslateblue","pq_h_color_h1_slateblue"=>"pq_slateblue","pq_h_color_h1_blue"=>"pq_blue","pq_h_color_h1_navy"=>"pq_navy","pq_h_color_h1_midnightblue"=>"pq_midnightblue","pq_h_color_h1_royalblue"=>"pq_royalblue","pq_h_color_h1_cornflowerblue"=>"pq_cornflowerblue","pq_h_color_h1_steelblue"=>"pq_steelblue","pq_h_color_h1_lightskyblue"=>"pq_lightskyblue","pq_h_color_h1_skyblue"=>"pq_skyblue","pq_h_color_h1_deepskyblue"=>"pq_deepskyblue","pq_h_color_h1_lightblue"=>"pq_lightblue","pq_h_color_h1_powderblue"=>"pq_powderblue","pq_h_color_h1_darkturquoise"=>"pq_darkturquoise","pq_h_color_h1_cadetblue"=>"pq_cadetblue","pq_h_color_h1_cyan"=>"pq_cyan","pq_h_color_h1_teal"=>"pq_teal","pq_h_color_h1_mediumturquoise"=>"pq_mediumturquoise","pq_h_color_h1_lightseagreen"=>"pq_lightseagreen","pq_h_color_h1_paleturquoise"=>"pq_paleturquoise","pq_h_color_h1_mediumspringgreen"=>"pq_mediumspringgreen","pq_h_color_h1_springgreen"=>"pq_springgreen","pq_h_color_h1_darkseagreen"=>"pq_darkseagreen","pq_h_color_h1_palegreen"=>"pq_palegreen","pq_h_color_h1_lmgreen"=>"pq_lmgreen","pq_h_color_h1_forestgreen"=>"pq_forestgreen","pq_h_color_h1_darkgreen"=>"pq_darkgreen","pq_h_color_h1_lawngreen"=>"pq_lawngreen","pq_h_color_h1_grnyellow"=>"pq_grnyellow","pq_h_color_h1_darkolivegreen"=>"pq_darkolivegreen","pq_h_color_h1_olvdrab"=>"pq_olvdrab","pq_h_color_h1_yellow"=>"pq_yellow","pq_h_color_h1_olive"=>"pq_olive","pq_h_color_h1_darkkhaki"=>"pq_darkkhaki","pq_h_color_h1_khaki"=>"pq_khaki","pq_h_color_h1_gold"=>"pq_gold","pq_h_color_h1_gldenrod"=>"pq_gldenrod","pq_h_color_h1_orange"=>"pq_orange","pq_h_color_h1_wheat"=>"pq_wheat","pq_h_color_h1_navajowhite"=>"pq_navajowhite","pq_h_color_h1_burlywood"=>"pq_burlywood","pq_h_color_h1_darkorange"=>"pq_darkorange","pq_h_color_h1_sienna"=>"pq_sienna","pq_h_color_h1_orngred"=>"pq_orngred","pq_h_color_h1_tomato"=>"pq_tomato","pq_h_color_h1_salmon"=>"pq_salmon","pq_h_color_h1_brown"=>"pq_brown","pq_h_color_h1_red"=>"pq_red","pq_h_color_h1_black"=>"pq_black","pq_h_color_h1_darkgrey"=>"pq_darkgrey","pq_h_color_h1_dimgrey"=>"pq_dimgrey","pq_h_color_h1_lightgrey"=>"pq_lightgrey","pq_h_color_h1_slategrey"=>"pq_slategrey","pq_h_color_h1_lightslategrey"=>"pq_lightslategrey","pq_h_color_h1_silver"=>"pq_silver","pq_h_color_h1_whtsmoke"=>"pq_whtsmoke","pq_h_color_h1_white"=>"pq_white"), $name.'_head_color', 0);
		//Text COLOR
		$allFormElements['text_color'] = $this->generateSelect($name, $name.'[text_color]', $this->_dictionary[designOptions][_text_color], array("pq_text_color_p_indianred"=>"Indianred","pq_text_color_p_crimson"=>"Crimson","pq_text_color_p_lightpink"=>"Lightpink","pq_text_color_p_pink"=>"Pink","pq_text_color_p_palevioletred"=>"Palevioletred","pq_text_color_p_hotpink"=>"Hotpink","pq_text_color_p_mediumvioletred"=>"Mediumvioletred","pq_text_color_p_orchid"=>"Orchid","pq_text_color_p_plum"=>"Plum","pq_text_color_p_violet"=>"Violet","pq_text_color_p_magenta"=>"Magenta","pq_text_color_p_purple"=>"Purple","pq_text_color_p_mediumorchid"=>"Mediumorchid","pq_text_color_p_darkviolet"=>"Darkviolet","pq_text_color_p_darkorchid"=>"Darkorchid","pq_text_color_p_indigo"=>"Indigo","pq_text_color_p_blviolet"=>"Blueviolet","pq_text_color_p_mediumpurple"=>"Mediumpurple","pq_text_color_p_darkslateblue"=>"Darkslateblue","pq_text_color_p_mediumslateblue"=>"Mediumslateblue","pq_text_color_p_slateblue"=>"Slateblue","pq_text_color_p_blue"=>"Blue","pq_text_color_p_navy"=>"Navy","pq_text_color_p_midnightblue"=>"Midnightblue","pq_text_color_p_royalblue"=>"Royalblue","pq_text_color_p_cornflowerblue"=>"Cornflowerblue","pq_text_color_p_steelblue"=>"Steelblue","pq_text_color_p_lightskyblue"=>"Lightskyblue","pq_text_color_p_skyblue"=>"Skyblue","pq_text_color_p_deepskyblue"=>"Deepskyblue","pq_text_color_p_lightblue"=>"Lightblue","pq_text_color_p_powderblue"=>"Powderblue","pq_text_color_p_darkturquoise"=>"Darkturquoise","pq_text_color_p_cadetblue"=>"Cadetblue","pq_text_color_p_cyan"=>"Cyan","pq_text_color_p_teal"=>"Teal","pq_text_color_p_mediumturquoise"=>"Mediumturquoise","pq_text_color_p_lightseagreen"=>"Lightseagreen","pq_text_color_p_paleturquoise"=>"Paleturquoise","pq_text_color_p_mediumspringgreen"=>"Mediumspringgreen","pq_text_color_p_springgreen"=>"Springgreen","pq_text_color_p_darkseagreen"=>"Darkseagreen","pq_text_color_p_palegreen"=>"Palegreen","pq_text_color_p_lmgreen"=>"Limegreen","pq_text_color_p_forestgreen"=>"Forestgreen","pq_text_color_p_darkgreen"=>"Darkgreen","pq_text_color_p_lawngreen"=>"Lawngreen","pq_text_color_p_grnyellow"=>"Greenyellow","pq_text_color_p_darkolivegreen"=>"Darkolivegreen","pq_text_color_p_olvdrab"=>"Olivedrab","pq_text_color_p_yellow"=>"Yellow","pq_text_color_p_olive"=>"Olive","pq_text_color_p_darkkhaki"=>"Darkkhaki","pq_text_color_p_khaki"=>"Khaki","pq_text_color_p_gold"=>"Gold","pq_text_color_p_gldenrod"=>"Goldenrod","pq_text_color_p_orange"=>"Orange","pq_text_color_p_wheat"=>"Wheat","pq_text_color_p_navajowhite"=>"Navajowhite","pq_text_color_p_burlywood"=>"Burlywood","pq_text_color_p_darkorange"=>"Darkorange","pq_text_color_p_sienna"=>"Sienna","pq_text_color_p_orngred"=>"Orangered","pq_text_color_p_tomato"=>"Tomato","pq_text_color_p_salmon"=>"Salmon","pq_text_color_p_brown"=>"Brown","pq_text_color_p_red"=>"Red","pq_text_color_p_black"=>"Black","pq_text_color_p_darkgrey"=>"Darkgrey","pq_text_color_p_dimgrey"=>"Dimgrey","pq_text_color_p_lightgrey"=>"Lightgrey","pq_text_color_p_slategrey"=>"Slategrey","pq_text_color_p_lightslategrey"=>"Lightslategrey","pq_text_color_p_silver"=>"Silver","pq_text_color_p_whtsmoke"=>"Whitesmoke","pq_text_color_p_white"=>"White"), $valArray[text_color], array("pq_text_color_p_indianred"=>"pq_indianred","pq_text_color_p_crimson"=>"pq_crimson","pq_text_color_p_lightpink"=>"pq_lightpink","pq_text_color_p_pink"=>"pq_pink","pq_text_color_p_palevioletred"=>"pq_palevioletred","pq_text_color_p_hotpink"=>"pq_hotpink","pq_text_color_p_mediumvioletred"=>"pq_mediumvioletred","pq_text_color_p_orchid"=>"pq_orchid","pq_text_color_p_plum"=>"pq_plum","pq_text_color_p_violet"=>"pq_violet","pq_text_color_p_magenta"=>"pq_magenta","pq_text_color_p_purple"=>"pq_purple","pq_text_color_p_mediumorchid"=>"pq_mediumorchid","pq_text_color_p_darkviolet"=>"pq_darkviolet","pq_text_color_p_darkorchid"=>"pq_darkorchid","pq_text_color_p_indigo"=>"pq_indigo","pq_text_color_p_blviolet"=>"pq_blviolet","pq_text_color_p_mediumpurple"=>"pq_mediumpurple","pq_text_color_p_darkslateblue"=>"pq_darkslateblue","pq_text_color_p_mediumslateblue"=>"pq_mediumslateblue","pq_text_color_p_slateblue"=>"pq_slateblue","pq_text_color_p_blue"=>"pq_blue","pq_text_color_p_navy"=>"pq_navy","pq_text_color_p_midnightblue"=>"pq_midnightblue","pq_text_color_p_royalblue"=>"pq_royalblue","pq_text_color_p_cornflowerblue"=>"pq_cornflowerblue","pq_text_color_p_steelblue"=>"pq_steelblue","pq_text_color_p_lightskyblue"=>"pq_lightskyblue","pq_text_color_p_skyblue"=>"pq_skyblue","pq_text_color_p_deepskyblue"=>"pq_deepskyblue","pq_text_color_p_lightblue"=>"pq_lightblue","pq_text_color_p_powderblue"=>"pq_powderblue","pq_text_color_p_darkturquoise"=>"pq_darkturquoise","pq_text_color_p_cadetblue"=>"pq_cadetblue","pq_text_color_p_cyan"=>"pq_cyan","pq_text_color_p_teal"=>"pq_teal","pq_text_color_p_mediumturquoise"=>"pq_mediumturquoise","pq_text_color_p_lightseagreen"=>"pq_lightseagreen","pq_text_color_p_paleturquoise"=>"pq_paleturquoise","pq_text_color_p_mediumspringgreen"=>"pq_mediumspringgreen","pq_text_color_p_springgreen"=>"pq_springgreen","pq_text_color_p_darkseagreen"=>"pq_darkseagreen","pq_text_color_p_palegreen"=>"pq_palegreen","pq_text_color_p_lmgreen"=>"pq_lmgreen","pq_text_color_p_forestgreen"=>"pq_forestgreen","pq_text_color_p_darkgreen"=>"pq_darkgreen","pq_text_color_p_lawngreen"=>"pq_lawngreen","pq_text_color_p_grnyellow"=>"pq_grnyellow","pq_text_color_p_darkolivegreen"=>"pq_darkolivegreen","pq_text_color_p_olvdrab"=>"pq_olvdrab","pq_text_color_p_yellow"=>"pq_yellow","pq_text_color_p_olive"=>"pq_olive","pq_text_color_p_darkkhaki"=>"pq_darkkhaki","pq_text_color_p_khaki"=>"pq_khaki","pq_text_color_p_gold"=>"pq_gold","pq_text_color_p_gldenrod"=>"pq_gldenrod","pq_text_color_p_orange"=>"pq_orange","pq_text_color_p_wheat"=>"pq_wheat","pq_text_color_p_navajowhite"=>"pq_navajowhite","pq_text_color_p_burlywood"=>"pq_burlywood","pq_text_color_p_darkorange"=>"pq_darkorange","pq_text_color_p_sienna"=>"pq_sienna","pq_text_color_p_orngred"=>"pq_orngred","pq_text_color_p_tomato"=>"pq_tomato","pq_text_color_p_salmon"=>"pq_salmon","pq_text_color_p_brown"=>"pq_brown","pq_text_color_p_red"=>"pq_red","pq_text_color_p_black"=>"pq_black","pq_text_color_p_darkgrey"=>"pq_darkgrey","pq_text_color_p_dimgrey"=>"pq_dimgrey","pq_text_color_p_lightgrey"=>"pq_lightgrey","pq_text_color_p_slategrey"=>"pq_slategrey","pq_text_color_p_lightslategrey"=>"pq_lightslategrey","pq_text_color_p_silver"=>"pq_silver","pq_text_color_p_whtsmoke"=>"pq_whtsmoke","pq_text_color_p_white"=>"pq_white"), $name.'_text_color', 0);		
		//Border Color
		$allFormElements['border_color'] = $this->generateSelect($name, $name.'[border_color]', $this->_dictionary[designOptions][_border_color], array("pq_pro_bd_bordercolor_indianred"=>"Indianred","pq_pro_bd_bordercolor_crimson"=>"Crimson","pq_pro_bd_bordercolor_lightpink"=>"Lightpink","pq_pro_bd_bordercolor_pink"=>"Pink","pq_pro_bd_bordercolor_palevioletred"=>"Palevioletred","pq_pro_bd_bordercolor_hotpink"=>"Hotpink","pq_pro_bd_bordercolor_mediumvioletred"=>"Mediumvioletred","pq_pro_bd_bordercolor_orchid"=>"Orchid","pq_pro_bd_bordercolor_plum"=>"Plum","pq_pro_bd_bordercolor_violet"=>"Violet","pq_pro_bd_bordercolor_magenta"=>"Magenta","pq_pro_bd_bordercolor_purple"=>"Purple","pq_pro_bd_bordercolor_mediumorchid"=>"Mediumorchid","pq_pro_bd_bordercolor_darkviolet"=>"Darkviolet","pq_pro_bd_bordercolor_darkorchid"=>"Darkorchid","pq_pro_bd_bordercolor_indigo"=>"Indigo","pq_pro_bd_bordercolor_blviolet"=>"Blueviolet","pq_pro_bd_bordercolor_mediumpurple"=>"Mediumpurple","pq_pro_bd_bordercolor_darkslateblue"=>"Darkslateblue","pq_pro_bd_bordercolor_mediumslateblue"=>"Mediumslateblue","pq_pro_bd_bordercolor_slateblue"=>"Slateblue","pq_pro_bd_bordercolor_blue"=>"Blue","pq_pro_bd_bordercolor_navy"=>"Navy","pq_pro_bd_bordercolor_midnightblue"=>"Midnightblue","pq_pro_bd_bordercolor_royalblue"=>"Royalblue","pq_pro_bd_bordercolor_cornflowerblue"=>"Cornflowerblue","pq_pro_bd_bordercolor_steelblue"=>"Steelblue","pq_pro_bd_bordercolor_lightskyblue"=>"Lightskyblue","pq_pro_bd_bordercolor_skyblue"=>"Skyblue","pq_pro_bd_bordercolor_deepskyblue"=>"Deepskyblue","pq_pro_bd_bordercolor_lightblue"=>"Lightblue","pq_pro_bd_bordercolor_powderblue"=>"Powderblue","pq_pro_bd_bordercolor_darkturquoise"=>"Darkturquoise","pq_pro_bd_bordercolor_cadetblue"=>"Cadetblue","pq_pro_bd_bordercolor_cyan"=>"Cyan","pq_pro_bd_bordercolor_teal"=>"Teal","pq_pro_bd_bordercolor_mediumturquoise"=>"Mediumturquoise","pq_pro_bd_bordercolor_lightseagreen"=>"Lightseagreen","pq_pro_bd_bordercolor_paleturquoise"=>"Paleturquoise","pq_pro_bd_bordercolor_mediumspringgreen"=>"Mediumspringgreen","pq_pro_bd_bordercolor_springgreen"=>"Springgreen","pq_pro_bd_bordercolor_darkseagreen"=>"Darkseagreen","pq_pro_bd_bordercolor_palegreen"=>"Palegreen","pq_pro_bd_bordercolor_lmgreen"=>"Limegreen","pq_pro_bd_bordercolor_forestgreen"=>"Forestgreen","pq_pro_bd_bordercolor_darkgreen"=>"Darkgreen","pq_pro_bd_bordercolor_lawngreen"=>"Lawngreen","pq_pro_bd_bordercolor_grnyellow"=>"Greenyellow","pq_pro_bd_bordercolor_darkolivegreen"=>"Darkolivegreen","pq_pro_bd_bordercolor_olvdrab"=>"Olivedrab","pq_pro_bd_bordercolor_yellow"=>"Yellow","pq_pro_bd_bordercolor_olive"=>"Olive","pq_pro_bd_bordercolor_darkkhaki"=>"Darkkhaki","pq_pro_bd_bordercolor_khaki"=>"Khaki","pq_pro_bd_bordercolor_gold"=>"Gold","pq_pro_bd_bordercolor_gldenrod"=>"Goldenrod","pq_pro_bd_bordercolor_orange"=>"Orange","pq_pro_bd_bordercolor_wheat"=>"Wheat","pq_pro_bd_bordercolor_navajowhite"=>"Navajowhite","pq_pro_bd_bordercolor_burlywood"=>"Burlywood","pq_pro_bd_bordercolor_darkorange"=>"Darkorange","pq_pro_bd_bordercolor_sienna"=>"Sienna","pq_pro_bd_bordercolor_orngred"=>"Orangered","pq_pro_bd_bordercolor_tomato"=>"Tomato","pq_pro_bd_bordercolor_salmon"=>"Salmon","pq_pro_bd_bordercolor_brown"=>"Brown","pq_pro_bd_bordercolor_red"=>"Red","pq_pro_bd_bordercolor_black"=>"Black","pq_pro_bd_bordercolor_darkgrey"=>"Darkgrey","pq_pro_bd_bordercolor_dimgrey"=>"Dimgrey","pq_pro_bd_bordercolor_lightgrey"=>"Lightgrey","pq_pro_bd_bordercolor_slategrey"=>"Slategrey","pq_pro_bd_bordercolor_lightslategrey"=>"Lightslategrey","pq_pro_bd_bordercolor_silver"=>"Silver","pq_pro_bd_bordercolor_whtsmoke"=>"Whitesmoke","pq_pro_bd_bordercolor_white"=>"White"), $valArray[border_color], array("pq_pro_bd_bordercolor_indianred"=>"pq_indianred","pq_pro_bd_bordercolor_crimson"=>"pq_crimson","pq_pro_bd_bordercolor_lightpink"=>"pq_lightpink","pq_pro_bd_bordercolor_pink"=>"pq_pink","pq_pro_bd_bordercolor_palevioletred"=>"pq_palevioletred","pq_pro_bd_bordercolor_hotpink"=>"pq_hotpink","pq_pro_bd_bordercolor_mediumvioletred"=>"pq_mediumvioletred","pq_pro_bd_bordercolor_orchid"=>"pq_orchid","pq_pro_bd_bordercolor_plum"=>"pq_plum","pq_pro_bd_bordercolor_violet"=>"pq_violet","pq_pro_bd_bordercolor_magenta"=>"pq_magenta","pq_pro_bd_bordercolor_purple"=>"pq_purple","pq_pro_bd_bordercolor_mediumorchid"=>"pq_mediumorchid","pq_pro_bd_bordercolor_darkviolet"=>"pq_darkviolet","pq_pro_bd_bordercolor_darkorchid"=>"pq_darkorchid","pq_pro_bd_bordercolor_indigo"=>"pq_indigo","pq_pro_bd_bordercolor_blviolet"=>"pq_blviolet","pq_pro_bd_bordercolor_mediumpurple"=>"pq_mediumpurple","pq_pro_bd_bordercolor_darkslateblue"=>"pq_darkslateblue","pq_pro_bd_bordercolor_mediumslateblue"=>"pq_mediumslateblue","pq_pro_bd_bordercolor_slateblue"=>"pq_slateblue","pq_pro_bd_bordercolor_blue"=>"pq_blue","pq_pro_bd_bordercolor_navy"=>"pq_navy","pq_pro_bd_bordercolor_midnightblue"=>"pq_midnightblue","pq_pro_bd_bordercolor_royalblue"=>"pq_royalblue","pq_pro_bd_bordercolor_cornflowerblue"=>"pq_cornflowerblue","pq_pro_bd_bordercolor_steelblue"=>"pq_steelblue","pq_pro_bd_bordercolor_lightskyblue"=>"pq_lightskyblue","pq_pro_bd_bordercolor_skyblue"=>"pq_skyblue","pq_pro_bd_bordercolor_deepskyblue"=>"pq_deepskyblue","pq_pro_bd_bordercolor_lightblue"=>"pq_lightblue","pq_pro_bd_bordercolor_powderblue"=>"pq_powderblue","pq_pro_bd_bordercolor_darkturquoise"=>"pq_darkturquoise","pq_pro_bd_bordercolor_cadetblue"=>"pq_cadetblue","pq_pro_bd_bordercolor_cyan"=>"pq_cyan","pq_pro_bd_bordercolor_teal"=>"pq_teal","pq_pro_bd_bordercolor_mediumturquoise"=>"pq_mediumturquoise","pq_pro_bd_bordercolor_lightseagreen"=>"pq_lightseagreen","pq_pro_bd_bordercolor_paleturquoise"=>"pq_paleturquoise","pq_pro_bd_bordercolor_mediumspringgreen"=>"pq_mediumspringgreen","pq_pro_bd_bordercolor_springgreen"=>"pq_springgreen","pq_pro_bd_bordercolor_darkseagreen"=>"pq_darkseagreen","pq_pro_bd_bordercolor_palegreen"=>"pq_palegreen","pq_pro_bd_bordercolor_lmgreen"=>"pq_lmgreen","pq_pro_bd_bordercolor_forestgreen"=>"pq_forestgreen","pq_pro_bd_bordercolor_darkgreen"=>"pq_darkgreen","pq_pro_bd_bordercolor_lawngreen"=>"pq_lawngreen","pq_pro_bd_bordercolor_grnyellow"=>"pq_grnyellow","pq_pro_bd_bordercolor_darkolivegreen"=>"pq_darkolivegreen","pq_pro_bd_bordercolor_olvdrab"=>"pq_olvdrab","pq_pro_bd_bordercolor_yellow"=>"pq_yellow","pq_pro_bd_bordercolor_olive"=>"pq_olive","pq_pro_bd_bordercolor_darkkhaki"=>"pq_darkkhaki","pq_pro_bd_bordercolor_khaki"=>"pq_khaki","pq_pro_bd_bordercolor_gold"=>"pq_gold","pq_pro_bd_bordercolor_gldenrod"=>"pq_gldenrod","pq_pro_bd_bordercolor_orange"=>"pq_orange","pq_pro_bd_bordercolor_wheat"=>"pq_wheat","pq_pro_bd_bordercolor_navajowhite"=>"pq_navajowhite","pq_pro_bd_bordercolor_burlywood"=>"pq_burlywood","pq_pro_bd_bordercolor_darkorange"=>"pq_darkorange","pq_pro_bd_bordercolor_sienna"=>"pq_sienna","pq_pro_bd_bordercolor_orngred"=>"pq_orngred","pq_pro_bd_bordercolor_tomato"=>"pq_tomato","pq_pro_bd_bordercolor_salmon"=>"pq_salmon","pq_pro_bd_bordercolor_brown"=>"pq_brown","pq_pro_bd_bordercolor_red"=>"pq_red","pq_pro_bd_bordercolor_black"=>"pq_black","pq_pro_bd_bordercolor_darkgrey"=>"pq_darkgrey","pq_pro_bd_bordercolor_dimgrey"=>"pq_dimgrey","pq_pro_bd_bordercolor_lightgrey"=>"pq_lightgrey","pq_pro_bd_bordercolor_slategrey"=>"pq_slategrey","pq_pro_bd_bordercolor_lightslategrey"=>"pq_lightslategrey","pq_pro_bd_bordercolor_silver"=>"pq_silver","pq_pro_bd_bordercolor_whtsmoke"=>"pq_whtsmoke","pq_pro_bd_bordercolor_white"=>"pq_white"), $name.'_border_color', 0);
		//Popup Form
		$allFormElements['popup_form'] = $this->generateSelect($name, $name.'[popup_form]', $this->_dictionary[designOptions][_popup_form], array("pq_br_sq" => "Square",	"pq_br_cr" => "Rounded"), $valArray[popup_form], array(), $name.'_popup_form', 0);		
		//bookmark_background
		$allFormElements['bookmark_background'] = $this->generateSelect($name, $name.'[bookmark_background]', $this->_dictionary[designOptions][_bookmark_background], array("pq_bg_bgcolor_aliceblue"=>"aliceblue","pq_bg_bgcolor_antiquewhite"=>"antiquewhite","pq_bg_bgcolor_aqua"=>"aqua","pq_bg_bgcolor_aqmarine"=>"aquamarine","pq_bg_bgcolor_azure"=>"azure","pq_bg_bgcolor_beige"=>"beige","pq_bg_bgcolor_bisque"=>"bisque","pq_bg_bgcolor_black"=>"black","pq_bg_bgcolor_blanchedalmond"=>"blanchedalmond","pq_bg_bgcolor_blue"=>"blue","pq_bg_bgcolor_blviolet"=>"blviolet","pq_bg_bgcolor_brown"=>"brown","pq_bg_bgcolor_burlywood"=>"burlywood","pq_bg_bgcolor_cadetblue"=>"cadetblue","pq_bg_bgcolor_chartreuse"=>"chartreuse","pq_bg_bgcolor_chocolate"=>"chocolate","pq_bg_bgcolor_coral"=>"coral","pq_bg_bgcolor_cornflowerblue"=>"cornflowerblue","pq_bg_bgcolor_cornsilk"=>"cornsilk","pq_bg_bgcolor_crimson"=>"crimson","pq_bg_bgcolor_cyan"=>"cyan","pq_bg_bgcolor_darkblue"=>"darkblue","pq_bg_bgcolor_darkcyan"=>"darkcyan","pq_bg_bgcolor_darkgldenrod"=>"darkgldenrod","pq_bg_bgcolor_darkgreen"=>"darkgreen","pq_bg_bgcolor_darkgrey"=>"darkgrey","pq_bg_bgcolor_darkkhaki"=>"darkkhaki","pq_bg_bgcolor_darkmagenta"=>"darkmagenta","pq_bg_bgcolor_darkolivegreen"=>"darkolivegreen","pq_bg_bgcolor_darkorange"=>"darkorange","pq_bg_bgcolor_darkorchid"=>"darkorchid","pq_bg_bgcolor_darkred"=>"darkred","pq_bg_bgcolor_darksalmon"=>"darksalmon","pq_bg_bgcolor_darkseagreen"=>"darkseagreen","pq_bg_bgcolor_darkslateblue"=>"darkslateblue","pq_bg_bgcolor_darkslategrey"=>"darkslategrey","pq_bg_bgcolor_darkturquoise"=>"darkturquoise","pq_bg_bgcolor_darkviolet"=>"darkviolet","pq_bg_bgcolor_deeppink"=>"deeppink","pq_bg_bgcolor_deepskyblue"=>"deepskyblue","pq_bg_bgcolor_dimgrey"=>"dimgrey","pq_bg_bgcolor_dodgerblue"=>"dodgerblue","pq_bg_bgcolor_firebrick"=>"firebrick","pq_bg_bgcolor_floralwhite"=>"floralwhite","pq_bg_bgcolor_forestgreen"=>"forestgreen","pq_bg_bgcolor_fuchsia"=>"fuchsia","pq_bg_bgcolor_gainsboro"=>"gainsboro","pq_bg_bgcolor_ghostwhite"=>"ghostwhite","pq_bg_bgcolor_gold"=>"gold","pq_bg_bgcolor_gldenrod"=>"gldenrod","pq_bg_bgcolor_green"=>"green","pq_bg_bgcolor_grnyellow"=>"grnyellow","pq_bg_bgcolor_grey"=>"grey","pq_bg_bgcolor_honeydew"=>"honeydew","pq_bg_bgcolor_hotpink"=>"hotpink","pq_bg_bgcolor_indianred"=>"indianred","pq_bg_bgcolor_indigo"=>"indigo","pq_bg_bgcolor_ivory"=>"ivory","pq_bg_bgcolor_khaki"=>"khaki","pq_bg_bgcolor_lavender"=>"lavender","pq_bg_bgcolor_lvndrblush"=>"lvndrblush","pq_bg_bgcolor_lawngreen"=>"lawngreen","pq_bg_bgcolor_lemonchiffon"=>"lemonchiffon","pq_bg_bgcolor_lightblue"=>"lightblue","pq_bg_bgcolor_lightcoral"=>"lightcoral","pq_bg_bgcolor_lightcyan"=>"lightcyan","pq_bg_bgcolor_lightgldenrodyellow"=>"lightgldenrodyellow","pq_bg_bgcolor_lightgreen"=>"lightgreen","pq_bg_bgcolor_lightgrey"=>"lightgrey","pq_bg_bgcolor_lightpink"=>"lightpink","pq_bg_bgcolor_lightsalmon"=>"lightsalmon","pq_bg_bgcolor_lightseagreen"=>"lightseagreen","pq_bg_bgcolor_lightskyblue"=>"lightskyblue","pq_bg_bgcolor_lightslategrey"=>"lightslategrey","pq_bg_bgcolor_lightsteelblue"=>"lightsteelblue","pq_bg_bgcolor_lightyellow"=>"lightyellow","pq_bg_bgcolor_lime"=>"lime","pq_bg_bgcolor_lmgreen"=>"lmgreen","pq_bg_bgcolor_linen"=>"linen","pq_bg_bgcolor_magenta"=>"magenta","pq_bg_bgcolor_maroon"=>"maroon","pq_bg_bgcolor_mediumaquamarine"=>"mediumaquamarine","pq_bg_bgcolor_mediumblue"=>"mediumblue","pq_bg_bgcolor_mediumorchid"=>"mediumorchid","pq_bg_bgcolor_mediumpurple"=>"mediumpurple","pq_bg_bgcolor_mediumseagreen"=>"mediumseagreen","pq_bg_bgcolor_mediumslateblue"=>"mediumslateblue","pq_bg_bgcolor_mediumspringgreen"=>"mediumspringgreen","pq_bg_bgcolor_mediumturquoise"=>"mediumturquoise","pq_bg_bgcolor_mediumvioletred"=>"mediumvioletred","pq_bg_bgcolor_midnightblue"=>"midnightblue","pq_bg_bgcolor_mintcream"=>"mintcream","pq_bg_bgcolor_mistyrose"=>"mistyrose","pq_bg_bgcolor_moccasin"=>"moccasin","pq_bg_bgcolor_navajowhite"=>"navajowhite","pq_bg_bgcolor_navy"=>"navy","pq_bg_bgcolor_oldlace"=>"oldlace","pq_bg_bgcolor_olive"=>"olive","pq_bg_bgcolor_olvdrab"=>"olvdrab","pq_bg_bgcolor_orange"=>"orange","pq_bg_bgcolor_orngred"=>"orngred","pq_bg_bgcolor_orchid"=>"orchid","pq_bg_bgcolor_palegldenrod"=>"palegldenrod","pq_bg_bgcolor_palegreen"=>"palegreen","pq_bg_bgcolor_paleturquoise"=>"paleturquoise","pq_bg_bgcolor_palevioletred"=>"palevioletred","pq_bg_bgcolor_papayawhip"=>"papayawhip","pq_bg_bgcolor_peachpuff"=>"peachpuff","pq_bg_bgcolor_peru"=>"peru","pq_bg_bgcolor_pink"=>"pink","pq_bg_bgcolor_plum"=>"plum","pq_bg_bgcolor_powderblue"=>"powderblue","pq_bg_bgcolor_purple"=>"purple","pq_bg_bgcolor_rebeccapurple"=>"rebeccapurple","pq_bg_bgcolor_red"=>"red","pq_bg_bgcolor_rosybrown"=>"rosybrown","pq_bg_bgcolor_royalblue"=>"royalblue","pq_bg_bgcolor_saddlebrown"=>"saddlebrown","pq_bg_bgcolor_salmon"=>"salmon","pq_bg_bgcolor_sandybrown"=>"sandybrown","pq_bg_bgcolor_seagreen"=>"seagreen","pq_bg_bgcolor_seashell"=>"seashell","pq_bg_bgcolor_sienna"=>"sienna","pq_bg_bgcolor_silver"=>"silver","pq_bg_bgcolor_skyblue"=>"skyblue","pq_bg_bgcolor_slateblue"=>"slateblue","pq_bg_bgcolor_slategrey"=>"slategrey","pq_bg_bgcolor_snow"=>"snow","pq_bg_bgcolor_springgreen"=>"springgreen","pq_bg_bgcolor_steelblue"=>"steelblue","pq_bg_bgcolor_tan"=>"tan","pq_bg_bgcolor_teal"=>"teal","pq_bg_bgcolor_thistle"=>"thistle","pq_bg_bgcolor_tomato"=>"tomato","pq_bg_bgcolor_turquoise"=>"turquoise","pq_bg_bgcolor_violet"=>"violet","pq_bg_bgcolor_wheat"=>"wheat","pq_bg_bgcolor_white"=>"white","pq_bg_bgcolor_whtsmoke"=>"whtsmoke","pq_bg_bgcolor_yellow"=>"yellow","pq_bg_bgcolor_yllwgreen"=>"yllwgreen"), $valArray[bookmark_background], array("pq_bg_bgcolor_aliceblue"=>"pq_aliceblue","pq_bg_bgcolor_antiquewhite"=>"pq_antiquewhite","pq_bg_bgcolor_aqua"=>"pq_aqua","pq_bg_bgcolor_aqmarine"=>"pq_aqmarine","pq_bg_bgcolor_azure"=>"pq_azure","pq_bg_bgcolor_beige"=>"pq_beige","pq_bg_bgcolor_bisque"=>"pq_bisque","pq_bg_bgcolor_black"=>"pq_black","pq_bg_bgcolor_blanchedalmond"=>"pq_blanchedalmond","pq_bg_bgcolor_blue"=>"pq_blue","pq_bg_bgcolor_blviolet"=>"pq_blviolet","pq_bg_bgcolor_brown"=>"pq_brown","pq_bg_bgcolor_burlywood"=>"pq_burlywood","pq_bg_bgcolor_cadetblue"=>"pq_cadetblue","pq_bg_bgcolor_chartreuse"=>"pq_chartreuse","pq_bg_bgcolor_chocolate"=>"pq_chocolate","pq_bg_bgcolor_coral"=>"pq_coral","pq_bg_bgcolor_cornflowerblue"=>"pq_cornflowerblue","pq_bg_bgcolor_cornsilk"=>"pq_cornsilk","pq_bg_bgcolor_crimson"=>"pq_crimson","pq_bg_bgcolor_cyan"=>"pq_cyan","pq_bg_bgcolor_darkblue"=>"pq_darkblue","pq_bg_bgcolor_darkcyan"=>"pq_darkcyan","pq_bg_bgcolor_darkgldenrod"=>"pq_darkgldenrod","pq_bg_bgcolor_darkgreen"=>"pq_darkgreen","pq_bg_bgcolor_darkgrey"=>"pq_darkgrey","pq_bg_bgcolor_darkkhaki"=>"pq_darkkhaki","pq_bg_bgcolor_darkmagenta"=>"pq_darkmagenta","pq_bg_bgcolor_darkolivegreen"=>"pq_darkolivegreen","pq_bg_bgcolor_darkorange"=>"pq_darkorange","pq_bg_bgcolor_darkorchid"=>"pq_darkorchid","pq_bg_bgcolor_darkred"=>"pq_darkred","pq_bg_bgcolor_darksalmon"=>"pq_darksalmon","pq_bg_bgcolor_darkseagreen"=>"pq_darkseagreen","pq_bg_bgcolor_darkslateblue"=>"pq_darkslateblue","pq_bg_bgcolor_darkslategrey"=>"pq_darkslategrey","pq_bg_bgcolor_darkturquoise"=>"pq_darkturquoise","pq_bg_bgcolor_darkviolet"=>"pq_darkviolet","pq_bg_bgcolor_deeppink"=>"pq_deeppink","pq_bg_bgcolor_deepskyblue"=>"pq_deepskyblue","pq_bg_bgcolor_dimgrey"=>"pq_dimgrey","pq_bg_bgcolor_dodgerblue"=>"pq_dodgerblue","pq_bg_bgcolor_firebrick"=>"pq_firebrick","pq_bg_bgcolor_floralwhite"=>"pq_floralwhite","pq_bg_bgcolor_forestgreen"=>"pq_forestgreen","pq_bg_bgcolor_fuchsia"=>"pq_fuchsia","pq_bg_bgcolor_gainsboro"=>"pq_gainsboro","pq_bg_bgcolor_ghostwhite"=>"pq_ghostwhite","pq_bg_bgcolor_gold"=>"pq_gold","pq_bg_bgcolor_gldenrod"=>"pq_gldenrod","pq_bg_bgcolor_green"=>"pq_green","pq_bg_bgcolor_grnyellow"=>"pq_grnyellow","pq_bg_bgcolor_grey"=>"pq_grey","pq_bg_bgcolor_honeydew"=>"pq_honeydew","pq_bg_bgcolor_hotpink"=>"pq_hotpink","pq_bg_bgcolor_indianred"=>"pq_indianred","pq_bg_bgcolor_indigo"=>"pq_indigo","pq_bg_bgcolor_ivory"=>"pq_ivory","pq_bg_bgcolor_khaki"=>"pq_khaki","pq_bg_bgcolor_lavender"=>"pq_lavender","pq_bg_bgcolor_lvndrblush"=>"pq_lvndrblush","pq_bg_bgcolor_lawngreen"=>"pq_lawngreen","pq_bg_bgcolor_lemonchiffon"=>"pq_lemonchiffon","pq_bg_bgcolor_lightblue"=>"pq_lightblue","pq_bg_bgcolor_lightcoral"=>"pq_lightcoral","pq_bg_bgcolor_lightcyan"=>"pq_lightcyan","pq_bg_bgcolor_lightgldenrodyellow"=>"pq_lightgldenrodyellow","pq_bg_bgcolor_lightgreen"=>"pq_lightgreen","pq_bg_bgcolor_lightgrey"=>"pq_lightgrey","pq_bg_bgcolor_lightpink"=>"pq_lightpink","pq_bg_bgcolor_lightsalmon"=>"pq_lightsalmon","pq_bg_bgcolor_lightseagreen"=>"pq_lightseagreen","pq_bg_bgcolor_lightskyblue"=>"pq_lightskyblue","pq_bg_bgcolor_lightslategrey"=>"pq_lightslategrey","pq_bg_bgcolor_lightsteelblue"=>"pq_lightsteelblue","pq_bg_bgcolor_lightyellow"=>"pq_lightyellow","pq_bg_bgcolor_lime"=>"pq_lime","pq_bg_bgcolor_lmgreen"=>"pq_lmgreen","pq_bg_bgcolor_linen"=>"pq_linen","pq_bg_bgcolor_magenta"=>"pq_magenta","pq_bg_bgcolor_maroon"=>"pq_maroon","pq_bg_bgcolor_mediumaquamarine"=>"pq_mediumaquamarine","pq_bg_bgcolor_mediumblue"=>"pq_mediumblue","pq_bg_bgcolor_mediumorchid"=>"pq_mediumorchid","pq_bg_bgcolor_mediumpurple"=>"pq_mediumpurple","pq_bg_bgcolor_mediumseagreen"=>"pq_mediumseagreen","pq_bg_bgcolor_mediumslateblue"=>"pq_mediumslateblue","pq_bg_bgcolor_mediumspringgreen"=>"pq_mediumspringgreen","pq_bg_bgcolor_mediumturquoise"=>"pq_mediumturquoise","pq_bg_bgcolor_mediumvioletred"=>"pq_mediumvioletred","pq_bg_bgcolor_midnightblue"=>"pq_midnightblue","pq_bg_bgcolor_mintcream"=>"pq_mintcream","pq_bg_bgcolor_mistyrose"=>"pq_mistyrose","pq_bg_bgcolor_moccasin"=>"pq_moccasin","pq_bg_bgcolor_navajowhite"=>"pq_navajowhite","pq_bg_bgcolor_navy"=>"pq_navy","pq_bg_bgcolor_oldlace"=>"pq_oldlace","pq_bg_bgcolor_olive"=>"pq_olive","pq_bg_bgcolor_olvdrab"=>"pq_olvdrab","pq_bg_bgcolor_orange"=>"pq_orange","pq_bg_bgcolor_orngred"=>"pq_orngred","pq_bg_bgcolor_orchid"=>"pq_orchid","pq_bg_bgcolor_palegldenrod"=>"pq_palegldenrod","pq_bg_bgcolor_palegreen"=>"pq_palegreen","pq_bg_bgcolor_paleturquoise"=>"pq_paleturquoise","pq_bg_bgcolor_palevioletred"=>"pq_palevioletred","pq_bg_bgcolor_papayawhip"=>"pq_papayawhip","pq_bg_bgcolor_peachpuff"=>"pq_peachpuff","pq_bg_bgcolor_peru"=>"pq_peru","pq_bg_bgcolor_pink"=>"pq_pink","pq_bg_bgcolor_plum"=>"pq_plum","pq_bg_bgcolor_powderblue"=>"pq_powderblue","pq_bg_bgcolor_purple"=>"pq_purple","pq_bg_bgcolor_rebeccapurple"=>"pq_rebeccapurple","pq_bg_bgcolor_red"=>"pq_red","pq_bg_bgcolor_rosybrown"=>"pq_rosybrown","pq_bg_bgcolor_royalblue"=>"pq_royalblue","pq_bg_bgcolor_saddlebrown"=>"pq_saddlebrown","pq_bg_bgcolor_salmon"=>"pq_salmon","pq_bg_bgcolor_sandybrown"=>"pq_sandybrown","pq_bg_bgcolor_seagreen"=>"pq_seagreen","pq_bg_bgcolor_seashell"=>"pq_seashell","pq_bg_bgcolor_sienna"=>"pq_sienna","pq_bg_bgcolor_silver"=>"pq_silver","pq_bg_bgcolor_skyblue"=>"pq_skyblue","pq_bg_bgcolor_slateblue"=>"pq_slateblue","pq_bg_bgcolor_slategrey"=>"pq_slategrey","pq_bg_bgcolor_snow"=>"pq_snow","pq_bg_bgcolor_springgreen"=>"pq_springgreen","pq_bg_bgcolor_steelblue"=>"pq_steelblue","pq_bg_bgcolor_tan"=>"pq_tan","pq_bg_bgcolor_teal"=>"pq_teal","pq_bg_bgcolor_thistle"=>"pq_thistle","pq_bg_bgcolor_tomato"=>"pq_tomato","pq_bg_bgcolor_turquoise"=>"pq_turquoise","pq_bg_bgcolor_violet"=>"pq_violet","pq_bg_bgcolor_wheat"=>"pq_wheat","pq_bg_bgcolor_white"=>"pq_white","pq_bg_bgcolor_whtsmoke"=>"pq_whtsmoke","pq_bg_bgcolor_yellow"=>"pq_yellow","pq_bg_bgcolor_yllwgreen"=>"pq_yllwgreen"), $name.'_bookmark_background', 0);		
		//bookmark_text_color
		$allFormElements['bookmark_text_color'] = $this->generateSelect($name, $name.'[bookmark_text_color]', $this->_dictionary[designOptions][_bookmark_text_color], array("pq_text_color_p_aliceblue"=>"aliceblue","pq_text_color_p_antiquewhite"=>"antiquewhite","pq_text_color_p_aqua"=>"aqua","pq_text_color_p_aqmarine"=>"aquamarine","pq_text_color_p_azure"=>"azure","pq_text_color_p_beige"=>"beige","pq_text_color_p_bisque"=>"bisque","pq_text_color_p_black"=>"black","pq_text_color_p_blanchedalmond"=>"blanchedalmond","pq_text_color_p_blue"=>"blue","pq_text_color_p_blviolet"=>"blviolet","pq_text_color_p_brown"=>"brown","pq_text_color_p_burlywood"=>"burlywood","pq_text_color_p_cadetblue"=>"cadetblue","pq_text_color_p_chartreuse"=>"chartreuse","pq_text_color_p_chocolate"=>"chocolate","pq_text_color_p_coral"=>"coral","pq_text_color_p_cornflowerblue"=>"cornflowerblue","pq_text_color_p_cornsilk"=>"cornsilk","pq_text_color_p_crimson"=>"crimson","pq_text_color_p_cyan"=>"cyan","pq_text_color_p_darkblue"=>"darkblue","pq_text_color_p_darkcyan"=>"darkcyan","pq_text_color_p_darkgldenrod"=>"darkgldenrod","pq_text_color_p_darkgreen"=>"darkgreen","pq_text_color_p_darkgrey"=>"darkgrey","pq_text_color_p_darkkhaki"=>"darkkhaki","pq_text_color_p_darkmagenta"=>"darkmagenta","pq_text_color_p_darkolivegreen"=>"darkolivegreen","pq_text_color_p_darkorange"=>"darkorange","pq_text_color_p_darkorchid"=>"darkorchid","pq_text_color_p_darkred"=>"darkred","pq_text_color_p_darksalmon"=>"darksalmon","pq_text_color_p_darkseagreen"=>"darkseagreen","pq_text_color_p_darkslateblue"=>"darkslateblue","pq_text_color_p_darkslategrey"=>"darkslategrey","pq_text_color_p_darkturquoise"=>"darkturquoise","pq_text_color_p_darkviolet"=>"darkviolet","pq_text_color_p_deeppink"=>"deeppink","pq_text_color_p_deepskyblue"=>"deepskyblue","pq_text_color_p_dimgrey"=>"dimgrey","pq_text_color_p_dodgerblue"=>"dodgerblue","pq_text_color_p_firebrick"=>"firebrick","pq_text_color_p_floralwhite"=>"floralwhite","pq_text_color_p_forestgreen"=>"forestgreen","pq_text_color_p_fuchsia"=>"fuchsia","pq_text_color_p_gainsboro"=>"gainsboro","pq_text_color_p_ghostwhite"=>"ghostwhite","pq_text_color_p_gold"=>"gold","pq_text_color_p_gldenrod"=>"gldenrod","pq_text_color_p_green"=>"green","pq_text_color_p_grnyellow"=>"grnyellow","pq_text_color_p_grey"=>"grey","pq_text_color_p_honeydew"=>"honeydew","pq_text_color_p_hotpink"=>"hotpink","pq_text_color_p_indianred"=>"indianred","pq_text_color_p_indigo"=>"indigo","pq_text_color_p_ivory"=>"ivory","pq_text_color_p_khaki"=>"khaki","pq_text_color_p_lavender"=>"lavender","pq_text_color_p_lvndrblush"=>"lvndrblush","pq_text_color_p_lawngreen"=>"lawngreen","pq_text_color_p_lemonchiffon"=>"lemonchiffon","pq_text_color_p_lightblue"=>"lightblue","pq_text_color_p_lightcoral"=>"lightcoral","pq_text_color_p_lightcyan"=>"lightcyan","pq_text_color_p_lightgldenrodyellow"=>"lightgldenrodyellow","pq_text_color_p_lightgreen"=>"lightgreen","pq_text_color_p_lightgrey"=>"lightgrey","pq_text_color_p_lightpink"=>"lightpink","pq_text_color_p_lightsalmon"=>"lightsalmon","pq_text_color_p_lightseagreen"=>"lightseagreen","pq_text_color_p_lightskyblue"=>"lightskyblue","pq_text_color_p_lightslategrey"=>"lightslategrey","pq_text_color_p_lightsteelblue"=>"lightsteelblue","pq_text_color_p_lightyellow"=>"lightyellow","pq_text_color_p_lime"=>"lime","pq_text_color_p_lmgreen"=>"lmgreen","pq_text_color_p_linen"=>"linen","pq_text_color_p_magenta"=>"magenta","pq_text_color_p_maroon"=>"maroon","pq_text_color_p_mediumaquamarine"=>"mediumaquamarine","pq_text_color_p_mediumblue"=>"mediumblue","pq_text_color_p_mediumorchid"=>"mediumorchid","pq_text_color_p_mediumpurple"=>"mediumpurple","pq_text_color_p_mediumseagreen"=>"mediumseagreen","pq_text_color_p_mediumslateblue"=>"mediumslateblue","pq_text_color_p_mediumspringgreen"=>"mediumspringgreen","pq_text_color_p_mediumturquoise"=>"mediumturquoise","pq_text_color_p_mediumvioletred"=>"mediumvioletred","pq_text_color_p_midnightblue"=>"midnightblue","pq_text_color_p_mintcream"=>"mintcream","pq_text_color_p_mistyrose"=>"mistyrose","pq_text_color_p_moccasin"=>"moccasin","pq_text_color_p_navajowhite"=>"navajowhite","pq_text_color_p_navy"=>"navy","pq_text_color_p_oldlace"=>"oldlace","pq_text_color_p_olive"=>"olive","pq_text_color_p_olvdrab"=>"olvdrab","pq_text_color_p_orange"=>"orange","pq_text_color_p_orngred"=>"orngred","pq_text_color_p_orchid"=>"orchid","pq_text_color_p_palegldenrod"=>"palegldenrod","pq_text_color_p_palegreen"=>"palegreen","pq_text_color_p_paleturquoise"=>"paleturquoise","pq_text_color_p_palevioletred"=>"palevioletred","pq_text_color_p_papayawhip"=>"papayawhip","pq_text_color_p_peachpuff"=>"peachpuff","pq_text_color_p_peru"=>"peru","pq_text_color_p_pink"=>"pink","pq_text_color_p_plum"=>"plum","pq_text_color_p_powderblue"=>"powderblue","pq_text_color_p_purple"=>"purple","pq_text_color_p_rebeccapurple"=>"rebeccapurple","pq_text_color_p_red"=>"red","pq_text_color_p_rosybrown"=>"rosybrown","pq_text_color_p_royalblue"=>"royalblue","pq_text_color_p_saddlebrown"=>"saddlebrown","pq_text_color_p_salmon"=>"salmon","pq_text_color_p_sandybrown"=>"sandybrown","pq_text_color_p_seagreen"=>"seagreen","pq_text_color_p_seashell"=>"seashell","pq_text_color_p_sienna"=>"sienna","pq_text_color_p_silver"=>"silver","pq_text_color_p_skyblue"=>"skyblue","pq_text_color_p_slateblue"=>"slateblue","pq_text_color_p_slategrey"=>"slategrey","pq_text_color_p_snow"=>"snow","pq_text_color_p_springgreen"=>"springgreen","pq_text_color_p_steelblue"=>"steelblue","pq_text_color_p_tan"=>"tan","pq_text_color_p_teal"=>"teal","pq_text_color_p_thistle"=>"thistle","pq_text_color_p_tomato"=>"tomato","pq_text_color_p_turquoise"=>"turquoise","pq_text_color_p_violet"=>"violet","pq_text_color_p_wheat"=>"wheat","pq_text_color_p_white"=>"white","pq_text_color_p_whtsmoke"=>"whtsmoke","pq_text_color_p_yellow"=>"yellow","pq_text_color_p_yllwgreen"=>"yllwgreen"), $valArray[bookmark_text_color], array("pq_text_color_p_aliceblue"=>"pq_aliceblue","pq_text_color_p_antiquewhite"=>"pq_antiquewhite","pq_text_color_p_aqua"=>"pq_aqua","pq_text_color_p_aqmarine"=>"pq_aqmarine","pq_text_color_p_azure"=>"pq_azure","pq_text_color_p_beige"=>"pq_beige","pq_text_color_p_bisque"=>"pq_bisque","pq_text_color_p_black"=>"pq_black","pq_text_color_p_blanchedalmond"=>"pq_blanchedalmond","pq_text_color_p_blue"=>"pq_blue","pq_text_color_p_blviolet"=>"pq_blviolet","pq_text_color_p_brown"=>"pq_brown","pq_text_color_p_burlywood"=>"pq_burlywood","pq_text_color_p_cadetblue"=>"pq_cadetblue","pq_text_color_p_chartreuse"=>"pq_chartreuse","pq_text_color_p_chocolate"=>"pq_chocolate","pq_text_color_p_coral"=>"pq_coral","pq_text_color_p_cornflowerblue"=>"pq_cornflowerblue","pq_text_color_p_cornsilk"=>"pq_cornsilk","pq_text_color_p_crimson"=>"pq_crimson","pq_text_color_p_cyan"=>"pq_cyan","pq_text_color_p_darkblue"=>"pq_darkblue","pq_text_color_p_darkcyan"=>"pq_darkcyan","pq_text_color_p_darkgldenrod"=>"pq_darkgldenrod","pq_text_color_p_darkgreen"=>"pq_darkgreen","pq_text_color_p_darkgrey"=>"pq_darkgrey","pq_text_color_p_darkkhaki"=>"pq_darkkhaki","pq_text_color_p_darkmagenta"=>"pq_darkmagenta","pq_text_color_p_darkolivegreen"=>"pq_darkolivegreen","pq_text_color_p_darkorange"=>"pq_darkorange","pq_text_color_p_darkorchid"=>"pq_darkorchid","pq_text_color_p_darkred"=>"pq_darkred","pq_text_color_p_darksalmon"=>"pq_darksalmon","pq_text_color_p_darkseagreen"=>"pq_darkseagreen","pq_text_color_p_darkslateblue"=>"pq_darkslateblue","pq_text_color_p_darkslategrey"=>"pq_darkslategrey","pq_text_color_p_darkturquoise"=>"pq_darkturquoise","pq_text_color_p_darkviolet"=>"pq_darkviolet","pq_text_color_p_deeppink"=>"pq_deeppink","pq_text_color_p_deepskyblue"=>"pq_deepskyblue","pq_text_color_p_dimgrey"=>"pq_dimgrey","pq_text_color_p_dodgerblue"=>"pq_dodgerblue","pq_text_color_p_firebrick"=>"pq_firebrick","pq_text_color_p_floralwhite"=>"pq_floralwhite","pq_text_color_p_forestgreen"=>"pq_forestgreen","pq_text_color_p_fuchsia"=>"pq_fuchsia","pq_text_color_p_gainsboro"=>"pq_gainsboro","pq_text_color_p_ghostwhite"=>"pq_ghostwhite","pq_text_color_p_gold"=>"pq_gold","pq_text_color_p_gldenrod"=>"pq_gldenrod","pq_text_color_p_green"=>"pq_green","pq_text_color_p_grnyellow"=>"pq_grnyellow","pq_text_color_p_grey"=>"pq_grey","pq_text_color_p_honeydew"=>"pq_honeydew","pq_text_color_p_hotpink"=>"pq_hotpink","pq_text_color_p_indianred"=>"pq_indianred","pq_text_color_p_indigo"=>"pq_indigo","pq_text_color_p_ivory"=>"pq_ivory","pq_text_color_p_khaki"=>"pq_khaki","pq_text_color_p_lavender"=>"pq_lavender","pq_text_color_p_lvndrblush"=>"pq_lvndrblush","pq_text_color_p_lawngreen"=>"pq_lawngreen","pq_text_color_p_lemonchiffon"=>"pq_lemonchiffon","pq_text_color_p_lightblue"=>"pq_lightblue","pq_text_color_p_lightcoral"=>"pq_lightcoral","pq_text_color_p_lightcyan"=>"pq_lightcyan","pq_text_color_p_lightgldenrodyellow"=>"pq_lightgldenrodyellow","pq_text_color_p_lightgreen"=>"pq_lightgreen","pq_text_color_p_lightgrey"=>"pq_lightgrey","pq_text_color_p_lightpink"=>"pq_lightpink","pq_text_color_p_lightsalmon"=>"pq_lightsalmon","pq_text_color_p_lightseagreen"=>"pq_lightseagreen","pq_text_color_p_lightskyblue"=>"pq_lightskyblue","pq_text_color_p_lightslategrey"=>"pq_lightslategrey","pq_text_color_p_lightsteelblue"=>"pq_lightsteelblue","pq_text_color_p_lightyellow"=>"pq_lightyellow","pq_text_color_p_lime"=>"pq_lime","pq_text_color_p_lmgreen"=>"pq_lmgreen","pq_text_color_p_linen"=>"pq_linen","pq_text_color_p_magenta"=>"pq_magenta","pq_text_color_p_maroon"=>"pq_maroon","pq_text_color_p_mediumaquamarine"=>"pq_mediumaquamarine","pq_text_color_p_mediumblue"=>"pq_mediumblue","pq_text_color_p_mediumorchid"=>"pq_mediumorchid","pq_text_color_p_mediumpurple"=>"pq_mediumpurple","pq_text_color_p_mediumseagreen"=>"pq_mediumseagreen","pq_text_color_p_mediumslateblue"=>"pq_mediumslateblue","pq_text_color_p_mediumspringgreen"=>"pq_mediumspringgreen","pq_text_color_p_mediumturquoise"=>"pq_mediumturquoise","pq_text_color_p_mediumvioletred"=>"pq_mediumvioletred","pq_text_color_p_midnightblue"=>"pq_midnightblue","pq_text_color_p_mintcream"=>"pq_mintcream","pq_text_color_p_mistyrose"=>"pq_mistyrose","pq_text_color_p_moccasin"=>"pq_moccasin","pq_text_color_p_navajowhite"=>"pq_navajowhite","pq_text_color_p_navy"=>"pq_navy","pq_text_color_p_oldlace"=>"pq_oldlace","pq_text_color_p_olive"=>"pq_olive","pq_text_color_p_olvdrab"=>"pq_olvdrab","pq_text_color_p_orange"=>"pq_orange","pq_text_color_p_orngred"=>"pq_orngred","pq_text_color_p_orchid"=>"pq_orchid","pq_text_color_p_palegldenrod"=>"pq_palegldenrod","pq_text_color_p_palegreen"=>"pq_palegreen","pq_text_color_p_paleturquoise"=>"pq_paleturquoise","pq_text_color_p_palevioletred"=>"pq_palevioletred","pq_text_color_p_papayawhip"=>"pq_papayawhip","pq_text_color_p_peachpuff"=>"pq_peachpuff","pq_text_color_p_peru"=>"pq_peru","pq_text_color_p_pink"=>"pq_pink","pq_text_color_p_plum"=>"pq_plum","pq_text_color_p_powderblue"=>"pq_powderblue","pq_text_color_p_purple"=>"pq_purple","pq_text_color_p_rebeccapurple"=>"pq_rebeccapurple","pq_text_color_p_red"=>"pq_red","pq_text_color_p_rosybrown"=>"pq_rosybrown","pq_text_color_p_royalblue"=>"pq_royalblue","pq_text_color_p_saddlebrown"=>"pq_saddlebrown","pq_text_color_p_salmon"=>"pq_salmon","pq_text_color_p_sandybrown"=>"pq_sandybrown","pq_text_color_p_seagreen"=>"pq_seagreen","pq_text_color_p_seashell"=>"pq_seashell","pq_text_color_p_sienna"=>"pq_sienna","pq_text_color_p_silver"=>"pq_silver","pq_text_color_p_skyblue"=>"pq_skyblue","pq_text_color_p_slateblue"=>"pq_slateblue","pq_text_color_p_slategrey"=>"pq_slategrey","pq_text_color_p_snow"=>"pq_snow","pq_text_color_p_springgreen"=>"pq_springgreen","pq_text_color_p_steelblue"=>"pq_steelblue","pq_text_color_p_tan"=>"pq_tan","pq_text_color_p_teal"=>"pq_teal","pq_text_color_p_thistle"=>"pq_thistle","pq_text_color_p_tomato"=>"pq_tomato","pq_text_color_p_turquoise"=>"pq_turquoise","pq_text_color_p_violet"=>"pq_violet","pq_text_color_p_wheat"=>"pq_wheat","pq_text_color_p_white"=>"pq_white","pq_text_color_p_whtsmoke"=>"pq_whtsmoke","pq_text_color_p_yellow"=>"pq_yellow","pq_text_color_p_yllwgreen"=>"pq_yllwgreen"), $name.'_bookmark_text_color', 0);		
		//font_size
		$allFormElements['font_size'] = $this->generateSelect($name, $name.'[font_size]', $this->_dictionary[designOptions][_font_size], array("pq_text_size16"=>"Size 16","pq_text_size20"=>"Size 20","pq_text_size24"=>"Size 24","pq_text_size28"=>"Size 28","pq_text_size36"=>"Size 36","pq_text_size48"=>"Size 48"), $valArray[font_size], array(), $name.'_font_size', 0);		
		//border_type
		$allFormElements['border_type'] = $this->generateSelect($name, $name.'[border_type]', $this->_dictionary[designOptions][_border_type], array("pq_pro_bs_dotted"=>"Border 1","pq_pro_bs_dashed"=>"Border 2","pq_pro_bs_double"=>"Border 3","pq_pro_bs_post"=>"Border 4"), $valArray[border_type], array(), $name.'_border_type', 0);
		//border_depth
		$allFormElements['border_depth'] = $this->generateSelect($name, $name.'[border_depth]', $this->_dictionary[designOptions][_border_depth], array("pq_pro_bd"=>"Type 1","pq_pro_bd2"=>"Type 2","pq_pro_bd3"=>"Type 3","pq_pro_bd4"=>"Type 4","pq_pro_bd5"=>"Type 5","pq_pro_bd6"=>"Type 6","pq_pro_bd7"=>"Type 7","pq_pro_bd8"=>"Type 8","pq_pro_bd9"=>"Type 9","pq_pro_bd10"=>"Type 10"), $valArray[border_depth], array(), $name.'_border_depth', 0);
		//button_font_size
		$allFormElements['button_font_size'] = $this->generateSelect($name, $name.'[button_font_size]', $this->_dictionary[designOptions][_button_font_size], array("pq_btn_size16"=>"Size 16","pq_btn_size20"=>"Size 20","pq_btn_size24"=>"Size 24","pq_btn_size28"=>"Size 28","pq_btn_size36"=>"Size 36","pq_btn_size48"=>"Size 48"), $valArray[button_font_size], array(), $name.'_button_font_size', 0);		
		
		//Head Font
		$allFormElements['head_font'] = $this->getFontSelect($name, $name.'[head_font]', $this->_dictionary[designOptions][_head_font], $valArray[head_font], $name.'_head_font', 'pq_h_font_h1_');
		//Font
		$allFormElements['text_font'] = $this->getFontSelect($name, $name.'[text_font]', $this->_dictionary[designOptions][_text_font], $valArray[text_font], $name.'_text_font', 'pq_text_font_');		
		//button_font
		$allFormElements['button_font'] = $this->getFontSelect($name, $name.'[button_font]', $this->_dictionary[designOptions][_button_font],$valArray[button_font], $name.'_button_font', 'pq_btn_font_btngroupinput_');				
		
		
		//HOVER CLOSE ICON
		$allFormElements['close_icon_animation'] = $this->generateSelect($name, $name.'[close_icon][animation]', $this->_dictionary[designOptions][_animation_close_icon], array("pq_hvr_rotate"=>"Rotate","pq_hvr_grow" => "Grow","pq_hvr_shrink" => "Shrink","pq_hvr_pulse" => "Pulse","pq_hvr_float" => "Float","pq_hvr_sink" => "Sink","pq_hvr_hang" => "Hang","pq_hvr_buzz" => "Buzz","pq_hvr_border_fade" => "Bdr Fade","pq_hvr_hollow" => "Hollow","pq_hvr_glow" => "Glow","pq_hvr_grow_shadow" => "Grow Shadow"), $valArray[close_icon][animation], array(), $name.'_animation_close_icon', 0);		
		//close_icon_type
		$allFormElements['close_icon_type'] = $this->generateSelect($name, $name.'[close_icon][form]', $this->_dictionary[designOptions][_close_icon_type], array("pq_x_type1"=>"Close Icon 1","pq_x_type2"=>"Close Icon 2","pq_x_type3"=>"Close Icon 3","pq_x_type4"=>"Close Icon 4","pq_x_type5"=>"Close Icon 5","pq_x_type9"=>"Close Icon 6","pq_x_type6"=>"Close Text 1","pq_x_type7"=>"Close Text 2","pq_x_type8"=>"Close Text 3"), $valArray[close_icon][form], array(), $name.'_close_icon_type', 0);		
		//close_icon_color
		$allFormElements['close_icon_color'] = $this->generateSelect($name, $name.'[close_icon][color]', $this->_dictionary[designOptions][_close_icon_color], array("pq_x_color_pqclose_indianred"=>"Indianred","pq_x_color_pqclose_crimson"=>"Crimson","pq_x_color_pqclose_lightpink"=>"Lightpink","pq_x_color_pqclose_pink"=>"Pink","pq_x_color_pqclose_palevioletred"=>"Palevioletred","pq_x_color_pqclose_hotpink"=>"Hotpink","pq_x_color_pqclose_mediumvioletred"=>"Mediumvioletred","pq_x_color_pqclose_orchid"=>"Orchid","pq_x_color_pqclose_plum"=>"Plum","pq_x_color_pqclose_violet"=>"Violet","pq_x_color_pqclose_magenta"=>"Magenta","pq_x_color_pqclose_purple"=>"Purple","pq_x_color_pqclose_mediumorchid"=>"Mediumorchid","pq_x_color_pqclose_darkviolet"=>"Darkviolet","pq_x_color_pqclose_darkorchid"=>"Darkorchid","pq_x_color_pqclose_indigo"=>"Indigo","pq_x_color_pqclose_blviolet"=>"Blueviolet","pq_x_color_pqclose_mediumpurple"=>"Mediumpurple","pq_x_color_pqclose_darkslateblue"=>"Darkslateblue","pq_x_color_pqclose_mediumslateblue"=>"Mediumslateblue","pq_x_color_pqclose_slateblue"=>"Slateblue","pq_x_color_pqclose_blue"=>"Blue","pq_x_color_pqclose_navy"=>"Navy","pq_x_color_pqclose_midnightblue"=>"Midnightblue","pq_x_color_pqclose_royalblue"=>"Royalblue","pq_x_color_pqclose_cornflowerblue"=>"Cornflowerblue","pq_x_color_pqclose_steelblue"=>"Steelblue","pq_x_color_pqclose_lightskyblue"=>"Lightskyblue","pq_x_color_pqclose_skyblue"=>"Skyblue","pq_x_color_pqclose_deepskyblue"=>"Deepskyblue","pq_x_color_pqclose_lightblue"=>"Lightblue","pq_x_color_pqclose_powderblue"=>"Powderblue","pq_x_color_pqclose_darkturquoise"=>"Darkturquoise","pq_x_color_pqclose_cadetblue"=>"Cadetblue","pq_x_color_pqclose_cyan"=>"Cyan","pq_x_color_pqclose_teal"=>"Teal","pq_x_color_pqclose_mediumturquoise"=>"Mediumturquoise","pq_x_color_pqclose_lightseagreen"=>"Lightseagreen","pq_x_color_pqclose_paleturquoise"=>"Paleturquoise","pq_x_color_pqclose_mediumspringgreen"=>"Mediumspringgreen","pq_x_color_pqclose_springgreen"=>"Springgreen","pq_x_color_pqclose_darkseagreen"=>"Darkseagreen","pq_x_color_pqclose_palegreen"=>"Palegreen","pq_x_color_pqclose_lmgreen"=>"Limegreen","pq_x_color_pqclose_forestgreen"=>"Forestgreen","pq_x_color_pqclose_darkgreen"=>"Darkgreen","pq_x_color_pqclose_lawngreen"=>"Lawngreen","pq_x_color_pqclose_grnyellow"=>"Greenyellow","pq_x_color_pqclose_darkolivegreen"=>"Darkolivegreen","pq_x_color_pqclose_olvdrab"=>"Olivedrab","pq_x_color_pqclose_yellow"=>"Yellow","pq_x_color_pqclose_olive"=>"Olive","pq_x_color_pqclose_darkkhaki"=>"Darkkhaki","pq_x_color_pqclose_khaki"=>"Khaki","pq_x_color_pqclose_gold"=>"Gold","pq_x_color_pqclose_gldenrod"=>"Goldenrod","pq_x_color_pqclose_orange"=>"Orange","pq_x_color_pqclose_wheat"=>"Wheat","pq_x_color_pqclose_navajowhite"=>"Navajowhite","pq_x_color_pqclose_burlywood"=>"Burlywood","pq_x_color_pqclose_darkorange"=>"Darkorange","pq_x_color_pqclose_sienna"=>"Sienna","pq_x_color_pqclose_orngred"=>"Orangered","pq_x_color_pqclose_tomato"=>"Tomato","pq_x_color_pqclose_salmon"=>"Salmon","pq_x_color_pqclose_brown"=>"Brown","pq_x_color_pqclose_red"=>"Red","pq_x_color_pqclose_black"=>"Black","pq_x_color_pqclose_darkgrey"=>"Darkgrey","pq_x_color_pqclose_dimgrey"=>"Dimgrey","pq_x_color_pqclose_lightgrey"=>"Lightgrey","pq_x_color_pqclose_slategrey"=>"Slategrey","pq_x_color_pqclose_lightslategrey"=>"Lightslategrey","pq_x_color_pqclose_silver"=>"Silver","pq_x_color_pqclose_whtsmoke"=>"Whitesmoke","pq_x_color_pqclose_white"=>"White"), $valArray[close_icon][color], array("pq_x_color_pqclose_indianred"=>"pq_indianred","pq_x_color_pqclose_crimson"=>"pq_crimson","pq_x_color_pqclose_lightpink"=>"pq_lightpink","pq_x_color_pqclose_pink"=>"pq_pink","pq_x_color_pqclose_palevioletred"=>"pq_palevioletred","pq_x_color_pqclose_hotpink"=>"pq_hotpink","pq_x_color_pqclose_mediumvioletred"=>"pq_mediumvioletred","pq_x_color_pqclose_orchid"=>"pq_orchid","pq_x_color_pqclose_plum"=>"pq_plum","pq_x_color_pqclose_violet"=>"pq_violet","pq_x_color_pqclose_magenta"=>"pq_magenta","pq_x_color_pqclose_purple"=>"pq_purple","pq_x_color_pqclose_mediumorchid"=>"pq_mediumorchid","pq_x_color_pqclose_darkviolet"=>"pq_darkviolet","pq_x_color_pqclose_darkorchid"=>"pq_darkorchid","pq_x_color_pqclose_indigo"=>"pq_indigo","pq_x_color_pqclose_blviolet"=>"pq_blviolet","pq_x_color_pqclose_mediumpurple"=>"pq_mediumpurple","pq_x_color_pqclose_darkslateblue"=>"pq_darkslateblue","pq_x_color_pqclose_mediumslateblue"=>"pq_mediumslateblue","pq_x_color_pqclose_slateblue"=>"pq_slateblue","pq_x_color_pqclose_blue"=>"pq_blue","pq_x_color_pqclose_navy"=>"pq_navy","pq_x_color_pqclose_midnightblue"=>"pq_midnightblue","pq_x_color_pqclose_royalblue"=>"pq_royalblue","pq_x_color_pqclose_cornflowerblue"=>"pq_cornflowerblue","pq_x_color_pqclose_steelblue"=>"pq_steelblue","pq_x_color_pqclose_lightskyblue"=>"pq_lightskyblue","pq_x_color_pqclose_skyblue"=>"pq_skyblue","pq_x_color_pqclose_deepskyblue"=>"pq_deepskyblue","pq_x_color_pqclose_lightblue"=>"pq_lightblue","pq_x_color_pqclose_powderblue"=>"pq_powderblue","pq_x_color_pqclose_darkturquoise"=>"pq_darkturquoise","pq_x_color_pqclose_cadetblue"=>"pq_cadetblue","pq_x_color_pqclose_cyan"=>"pq_cyan","pq_x_color_pqclose_teal"=>"pq_teal","pq_x_color_pqclose_mediumturquoise"=>"pq_mediumturquoise","pq_x_color_pqclose_lightseagreen"=>"pq_lightseagreen","pq_x_color_pqclose_paleturquoise"=>"pq_paleturquoise","pq_x_color_pqclose_mediumspringgreen"=>"pq_mediumspringgreen","pq_x_color_pqclose_springgreen"=>"pq_springgreen","pq_x_color_pqclose_darkseagreen"=>"pq_darkseagreen","pq_x_color_pqclose_palegreen"=>"pq_palegreen","pq_x_color_pqclose_lmgreen"=>"pq_lmgreen","pq_x_color_pqclose_forestgreen"=>"pq_forestgreen","pq_x_color_pqclose_darkgreen"=>"pq_darkgreen","pq_x_color_pqclose_lawngreen"=>"pq_lawngreen","pq_x_color_pqclose_grnyellow"=>"pq_grnyellow","pq_x_color_pqclose_darkolivegreen"=>"pq_darkolivegreen","pq_x_color_pqclose_olvdrab"=>"pq_olvdrab","pq_x_color_pqclose_yellow"=>"pq_yellow","pq_x_color_pqclose_olive"=>"pq_olive","pq_x_color_pqclose_darkkhaki"=>"pq_darkkhaki","pq_x_color_pqclose_khaki"=>"pq_khaki","pq_x_color_pqclose_gold"=>"pq_gold","pq_x_color_pqclose_gldenrod"=>"pq_gldenrod","pq_x_color_pqclose_orange"=>"pq_orange","pq_x_color_pqclose_wheat"=>"pq_wheat","pq_x_color_pqclose_navajowhite"=>"pq_navajowhite","pq_x_color_pqclose_burlywood"=>"pq_burlywood","pq_x_color_pqclose_darkorange"=>"pq_darkorange","pq_x_color_pqclose_sienna"=>"pq_sienna","pq_x_color_pqclose_orngred"=>"pq_orngred","pq_x_color_pqclose_tomato"=>"pq_tomato","pq_x_color_pqclose_salmon"=>"pq_salmon","pq_x_color_pqclose_brown"=>"pq_brown","pq_x_color_pqclose_red"=>"pq_red","pq_x_color_pqclose_black"=>"pq_black","pq_x_color_pqclose_darkgrey"=>"pq_darkgrey","pq_x_color_pqclose_dimgrey"=>"pq_dimgrey","pq_x_color_pqclose_lightgrey"=>"pq_lightgrey","pq_x_color_pqclose_slategrey"=>"pq_slategrey","pq_x_color_pqclose_lightslategrey"=>"pq_lightslategrey","pq_x_color_pqclose_silver"=>"pq_silver","pq_x_color_pqclose_whtsmoke"=>"pq_whtsmoke","pq_x_color_pqclose_white"=>"pq_white"), $name.'_close_icon_color', 0);
		
		
		//GALLERY BACKGROUND COLOR
		$allFormElements['gallery_background_color'] = $this->generateSelect($name, $name.'[galleryOption][background_color]', $this->_dictionary[designOptions][_galleryOption_background_color], array("pq_bg_bgcolor_indianred"=>"Indianred","pq_bg_bgcolor_crimson"=>"Crimson","pq_bg_bgcolor_lightpink"=>"Lightpink","pq_bg_bgcolor_pink"=>"Pink","pq_bg_bgcolor_palevioletred"=>"Palevioletred","pq_bg_bgcolor_hotpink"=>"Hotpink","pq_bg_bgcolor_mediumvioletred"=>"Mediumvioletred","pq_bg_bgcolor_orchid"=>"Orchid","pq_bg_bgcolor_plum"=>"Plum","pq_bg_bgcolor_violet"=>"Violet","pq_bg_bgcolor_magenta"=>"Magenta","pq_bg_bgcolor_purple"=>"Purple","pq_bg_bgcolor_mediumorchid"=>"Mediumorchid","pq_bg_bgcolor_darkviolet"=>"Darkviolet","pq_bg_bgcolor_darkorchid"=>"Darkorchid","pq_bg_bgcolor_indigo"=>"Indigo","pq_bg_bgcolor_blviolet"=>"Blueviolet","pq_bg_bgcolor_mediumpurple"=>"Mediumpurple","pq_bg_bgcolor_darkslateblue"=>"Darkslateblue","pq_bg_bgcolor_mediumslateblue"=>"Mediumslateblue","pq_bg_bgcolor_slateblue"=>"Slateblue","pq_bg_bgcolor_blue"=>"Blue","pq_bg_bgcolor_navy"=>"Navy","pq_bg_bgcolor_midnightblue"=>"Midnightblue","pq_bg_bgcolor_royalblue"=>"Royalblue","pq_bg_bgcolor_cornflowerblue"=>"Cornflowerblue","pq_bg_bgcolor_steelblue"=>"Steelblue","pq_bg_bgcolor_lightskyblue"=>"Lightskyblue","pq_bg_bgcolor_skyblue"=>"Skyblue","pq_bg_bgcolor_deepskyblue"=>"Deepskyblue","pq_bg_bgcolor_lightblue"=>"Lightblue","pq_bg_bgcolor_powderblue"=>"Powderblue","pq_bg_bgcolor_darkturquoise"=>"Darkturquoise","pq_bg_bgcolor_cadetblue"=>"Cadetblue","pq_bg_bgcolor_cyan"=>"Cyan","pq_bg_bgcolor_teal"=>"Teal","pq_bg_bgcolor_mediumturquoise"=>"Mediumturquoise","pq_bg_bgcolor_lightseagreen"=>"Lightseagreen","pq_bg_bgcolor_paleturquoise"=>"Paleturquoise","pq_bg_bgcolor_mediumspringgreen"=>"Mediumspringgreen","pq_bg_bgcolor_springgreen"=>"Springgreen","pq_bg_bgcolor_darkseagreen"=>"Darkseagreen","pq_bg_bgcolor_palegreen"=>"Palegreen","pq_bg_bgcolor_lmgreen"=>"Limegreen","pq_bg_bgcolor_forestgreen"=>"Forestgreen","pq_bg_bgcolor_darkgreen"=>"Darkgreen","pq_bg_bgcolor_lawngreen"=>"Lawngreen","pq_bg_bgcolor_grnyellow"=>"Greenyellow","pq_bg_bgcolor_darkolivegreen"=>"Darkolivegreen","pq_bg_bgcolor_olvdrab"=>"Olivedrab","pq_bg_bgcolor_yellow"=>"Yellow","pq_bg_bgcolor_olive"=>"Olive","pq_bg_bgcolor_darkkhaki"=>"Darkkhaki","pq_bg_bgcolor_khaki"=>"Khaki","pq_bg_bgcolor_gold"=>"Gold","pq_bg_bgcolor_gldenrod"=>"Goldenrod","pq_bg_bgcolor_orange"=>"Orange","pq_bg_bgcolor_wheat"=>"Wheat","pq_bg_bgcolor_navajowhite"=>"Navajowhite","pq_bg_bgcolor_burlywood"=>"Burlywood","pq_bg_bgcolor_darkorange"=>"Darkorange","pq_bg_bgcolor_sienna"=>"Sienna","pq_bg_bgcolor_orngred"=>"Orangered","pq_bg_bgcolor_tomato"=>"Tomato","pq_bg_bgcolor_salmon"=>"Salmon","pq_bg_bgcolor_brown"=>"Brown","pq_bg_bgcolor_red"=>"Red","pq_bg_bgcolor_black"=>"Black","pq_bg_bgcolor_darkgrey"=>"Darkgrey","pq_bg_bgcolor_dimgrey"=>"Dimgrey","pq_bg_bgcolor_lightgrey"=>"Lightgrey","pq_bg_bgcolor_slategrey"=>"Slategrey","pq_bg_bgcolor_lightslategrey"=>"Lightslategrey","pq_bg_bgcolor_silver"=>"Silver","pq_bg_bgcolor_whtsmoke"=>"Whitesmoke","pq_bg_bgcolor_white"=>"White"), $valArray[galleryOption][background_color], array("pq_bg_bgcolor_indianred"=>"pq_indianred","pq_bg_bgcolor_crimson"=>"pq_crimson","pq_bg_bgcolor_lightpink"=>"pq_lightpink","pq_bg_bgcolor_pink"=>"pq_pink","pq_bg_bgcolor_palevioletred"=>"pq_palevioletred","pq_bg_bgcolor_hotpink"=>"pq_hotpink","pq_bg_bgcolor_mediumvioletred"=>"pq_mediumvioletred","pq_bg_bgcolor_orchid"=>"pq_orchid","pq_bg_bgcolor_plum"=>"pq_plum","pq_bg_bgcolor_violet"=>"pq_violet","pq_bg_bgcolor_magenta"=>"pq_magenta","pq_bg_bgcolor_purple"=>"pq_purple","pq_bg_bgcolor_mediumorchid"=>"pq_mediumorchid","pq_bg_bgcolor_darkviolet"=>"pq_darkviolet","pq_bg_bgcolor_darkorchid"=>"pq_darkorchid","pq_bg_bgcolor_indigo"=>"pq_indigo","pq_bg_bgcolor_blviolet"=>"pq_blviolet","pq_bg_bgcolor_mediumpurple"=>"pq_mediumpurple","pq_bg_bgcolor_darkslateblue"=>"pq_darkslateblue","pq_bg_bgcolor_mediumslateblue"=>"pq_mediumslateblue","pq_bg_bgcolor_slateblue"=>"pq_slateblue","pq_bg_bgcolor_blue"=>"pq_blue","pq_bg_bgcolor_navy"=>"pq_navy","pq_bg_bgcolor_midnightblue"=>"pq_midnightblue","pq_bg_bgcolor_royalblue"=>"pq_royalblue","pq_bg_bgcolor_cornflowerblue"=>"pq_cornflowerblue","pq_bg_bgcolor_steelblue"=>"pq_steelblue","pq_bg_bgcolor_lightskyblue"=>"pq_lightskyblue","pq_bg_bgcolor_skyblue"=>"pq_skyblue","pq_bg_bgcolor_deepskyblue"=>"pq_deepskyblue","pq_bg_bgcolor_lightblue"=>"pq_lightblue","pq_bg_bgcolor_powderblue"=>"pq_powderblue","pq_bg_bgcolor_darkturquoise"=>"pq_darkturquoise","pq_bg_bgcolor_cadetblue"=>"pq_cadetblue","pq_bg_bgcolor_cyan"=>"pq_cyan","pq_bg_bgcolor_teal"=>"pq_teal","pq_bg_bgcolor_mediumturquoise"=>"pq_mediumturquoise","pq_bg_bgcolor_lightseagreen"=>"pq_lightseagreen","pq_bg_bgcolor_paleturquoise"=>"pq_paleturquoise","pq_bg_bgcolor_mediumspringgreen"=>"pq_mediumspringgreen","pq_bg_bgcolor_springgreen"=>"pq_springgreen","pq_bg_bgcolor_darkseagreen"=>"pq_darkseagreen","pq_bg_bgcolor_palegreen"=>"pq_palegreen","pq_bg_bgcolor_lmgreen"=>"pq_lmgreen","pq_bg_bgcolor_forestgreen"=>"pq_forestgreen","pq_bg_bgcolor_darkgreen"=>"pq_darkgreen","pq_bg_bgcolor_lawngreen"=>"pq_lawngreen","pq_bg_bgcolor_grnyellow"=>"pq_grnyellow","pq_bg_bgcolor_darkolivegreen"=>"pq_darkolivegreen","pq_bg_bgcolor_olvdrab"=>"pq_olvdrab","pq_bg_bgcolor_yellow"=>"pq_yellow","pq_bg_bgcolor_olive"=>"pq_olive","pq_bg_bgcolor_darkkhaki"=>"pq_darkkhaki","pq_bg_bgcolor_khaki"=>"pq_khaki","pq_bg_bgcolor_gold"=>"pq_gold","pq_bg_bgcolor_gldenrod"=>"pq_gldenrod","pq_bg_bgcolor_orange"=>"pq_orange","pq_bg_bgcolor_wheat"=>"pq_wheat","pq_bg_bgcolor_navajowhite"=>"pq_navajowhite","pq_bg_bgcolor_burlywood"=>"pq_burlywood","pq_bg_bgcolor_darkorange"=>"pq_darkorange","pq_bg_bgcolor_sienna"=>"pq_sienna","pq_bg_bgcolor_orngred"=>"pq_orngred","pq_bg_bgcolor_tomato"=>"pq_tomato","pq_bg_bgcolor_salmon"=>"pq_salmon","pq_bg_bgcolor_brown"=>"pq_brown","pq_bg_bgcolor_red"=>"pq_red","pq_bg_bgcolor_black"=>"pq_black","pq_bg_bgcolor_darkgrey"=>"pq_darkgrey","pq_bg_bgcolor_dimgrey"=>"pq_dimgrey","pq_bg_bgcolor_lightgrey"=>"pq_lightgrey","pq_bg_bgcolor_slategrey"=>"pq_slategrey","pq_bg_bgcolor_lightslategrey"=>"pq_lightslategrey","pq_bg_bgcolor_silver"=>"pq_silver","pq_bg_bgcolor_whtsmoke"=>"pq_whtsmoke","pq_bg_bgcolor_white"=>"pq_white"), $name.'_galleryOption_background_color', 0);
		//GALLERY BUTTON TEXT COLOR
		$allFormElements['gallery_button_text_color'] = $this->generateSelect($name, $name.'[galleryOption][button_text_color]', $this->_dictionary[designOptions][_galleryOption_button_text_color], array("pq_btn_color_pqbtn_indianred"=>"Indianred","pq_btn_color_pqbtn_crimson"=>"Crimson","pq_btn_color_pqbtn_lightpink"=>"Lightpink","pq_btn_color_pqbtn_pink"=>"Pink","pq_btn_color_pqbtn_palevioletred"=>"Palevioletred","pq_btn_color_pqbtn_hotpink"=>"Hotpink","pq_btn_color_pqbtn_mediumvioletred"=>"Mediumvioletred","pq_btn_color_pqbtn_orchid"=>"Orchid","pq_btn_color_pqbtn_plum"=>"Plum","pq_btn_color_pqbtn_violet"=>"Violet","pq_btn_color_pqbtn_magenta"=>"Magenta","pq_btn_color_pqbtn_purple"=>"Purple","pq_btn_color_pqbtn_mediumorchid"=>"Mediumorchid","pq_btn_color_pqbtn_darkviolet"=>"Darkviolet","pq_btn_color_pqbtn_darkorchid"=>"Darkorchid","pq_btn_color_pqbtn_indigo"=>"Indigo","pq_btn_color_pqbtn_blviolet"=>"Blueviolet","pq_btn_color_pqbtn_mediumpurple"=>"Mediumpurple","pq_btn_color_pqbtn_darkslateblue"=>"Darkslateblue","pq_btn_color_pqbtn_mediumslateblue"=>"Mediumslateblue","pq_btn_color_pqbtn_slateblue"=>"Slateblue","pq_btn_color_pqbtn_blue"=>"Blue","pq_btn_color_pqbtn_navy"=>"Navy","pq_btn_color_pqbtn_midnightblue"=>"Midnightblue","pq_btn_color_pqbtn_royalblue"=>"Royalblue","pq_btn_color_pqbtn_cornflowerblue"=>"Cornflowerblue","pq_btn_color_pqbtn_steelblue"=>"Steelblue","pq_btn_color_pqbtn_lightskyblue"=>"Lightskyblue","pq_btn_color_pqbtn_skyblue"=>"Skyblue","pq_btn_color_pqbtn_deepskyblue"=>"Deepskyblue","pq_btn_color_pqbtn_lightblue"=>"Lightblue","pq_btn_color_pqbtn_powderblue"=>"Powderblue","pq_btn_color_pqbtn_darkturquoise"=>"Darkturquoise","pq_btn_color_pqbtn_cadetblue"=>"Cadetblue","pq_btn_color_pqbtn_cyan"=>"Cyan","pq_btn_color_pqbtn_teal"=>"Teal","pq_btn_color_pqbtn_mediumturquoise"=>"Mediumturquoise","pq_btn_color_pqbtn_lightseagreen"=>"Lightseagreen","pq_btn_color_pqbtn_paleturquoise"=>"Paleturquoise","pq_btn_color_pqbtn_mediumspringgreen"=>"Mediumspringgreen","pq_btn_color_pqbtn_springgreen"=>"Springgreen","pq_btn_color_pqbtn_darkseagreen"=>"Darkseagreen","pq_btn_color_pqbtn_palegreen"=>"Palegreen","pq_btn_color_pqbtn_lmgreen"=>"Limegreen","pq_btn_color_pqbtn_forestgreen"=>"Forestgreen","pq_btn_color_pqbtn_darkgreen"=>"Darkgreen","pq_btn_color_pqbtn_lawngreen"=>"Lawngreen","pq_btn_color_pqbtn_grnyellow"=>"Greenyellow","pq_btn_color_pqbtn_darkolivegreen"=>"Darkolivegreen","pq_btn_color_pqbtn_olvdrab"=>"Olivedrab","pq_btn_color_pqbtn_yellow"=>"Yellow","pq_btn_color_pqbtn_olive"=>"Olive","pq_btn_color_pqbtn_darkkhaki"=>"Darkkhaki","pq_btn_color_pqbtn_khaki"=>"Khaki","pq_btn_color_pqbtn_gold"=>"Gold","pq_btn_color_pqbtn_gldenrod"=>"Goldenrod","pq_btn_color_pqbtn_orange"=>"Orange","pq_btn_color_pqbtn_wheat"=>"Wheat","pq_btn_color_pqbtn_navajowhite"=>"Navajowhite","pq_btn_color_pqbtn_burlywood"=>"Burlywood","pq_btn_color_pqbtn_darkorange"=>"Darkorange","pq_btn_color_pqbtn_sienna"=>"Sienna","pq_btn_color_pqbtn_orngred"=>"Orangered","pq_btn_color_pqbtn_tomato"=>"Tomato","pq_btn_color_pqbtn_salmon"=>"Salmon","pq_btn_color_pqbtn_brown"=>"Brown","pq_btn_color_pqbtn_red"=>"Red","pq_btn_color_pqbtn_black"=>"Black","pq_btn_color_pqbtn_darkgrey"=>"Darkgrey","pq_btn_color_pqbtn_dimgrey"=>"Dimgrey","pq_btn_color_pqbtn_lightgrey"=>"Lightgrey","pq_btn_color_pqbtn_slategrey"=>"Slategrey","pq_btn_color_pqbtn_lightslategrey"=>"Lightslategrey","pq_btn_color_pqbtn_silver"=>"Silver","pq_btn_color_pqbtn_whtsmoke"=>"Whitesmoke","pq_btn_color_pqbtn_white"=>"White"), $valArray[galleryOption][button_text_color], array("pq_btn_color_pqbtn_indianred"=>"pq_indianred","pq_btn_color_pqbtn_crimson"=>"pq_crimson","pq_btn_color_pqbtn_lightpink"=>"pq_lightpink","pq_btn_color_pqbtn_pink"=>"pq_pink","pq_btn_color_pqbtn_palevioletred"=>"pq_palevioletred","pq_btn_color_pqbtn_hotpink"=>"pq_hotpink","pq_btn_color_pqbtn_mediumvioletred"=>"pq_mediumvioletred","pq_btn_color_pqbtn_orchid"=>"pq_orchid","pq_btn_color_pqbtn_plum"=>"pq_plum","pq_btn_color_pqbtn_violet"=>"pq_violet","pq_btn_color_pqbtn_magenta"=>"pq_magenta","pq_btn_color_pqbtn_purple"=>"pq_purple","pq_btn_color_pqbtn_mediumorchid"=>"pq_mediumorchid","pq_btn_color_pqbtn_darkviolet"=>"pq_darkviolet","pq_btn_color_pqbtn_darkorchid"=>"pq_darkorchid","pq_btn_color_pqbtn_indigo"=>"pq_indigo","pq_btn_color_pqbtn_blviolet"=>"pq_blviolet","pq_btn_color_pqbtn_mediumpurple"=>"pq_mediumpurple","pq_btn_color_pqbtn_darkslateblue"=>"pq_darkslateblue","pq_btn_color_pqbtn_mediumslateblue"=>"pq_mediumslateblue","pq_btn_color_pqbtn_slateblue"=>"pq_slateblue","pq_btn_color_pqbtn_blue"=>"pq_blue","pq_btn_color_pqbtn_navy"=>"pq_navy","pq_btn_color_pqbtn_midnightblue"=>"pq_midnightblue","pq_btn_color_pqbtn_royalblue"=>"pq_royalblue","pq_btn_color_pqbtn_cornflowerblue"=>"pq_cornflowerblue","pq_btn_color_pqbtn_steelblue"=>"pq_steelblue","pq_btn_color_pqbtn_lightskyblue"=>"pq_lightskyblue","pq_btn_color_pqbtn_skyblue"=>"pq_skyblue","pq_btn_color_pqbtn_deepskyblue"=>"pq_deepskyblue","pq_btn_color_pqbtn_lightblue"=>"pq_lightblue","pq_btn_color_pqbtn_powderblue"=>"pq_powderblue","pq_btn_color_pqbtn_darkturquoise"=>"pq_darkturquoise","pq_btn_color_pqbtn_cadetblue"=>"pq_cadetblue","pq_btn_color_pqbtn_cyan"=>"pq_cyan","pq_btn_color_pqbtn_teal"=>"pq_teal","pq_btn_color_pqbtn_mediumturquoise"=>"pq_mediumturquoise","pq_btn_color_pqbtn_lightseagreen"=>"pq_lightseagreen","pq_btn_color_pqbtn_paleturquoise"=>"pq_paleturquoise","pq_btn_color_pqbtn_mediumspringgreen"=>"pq_mediumspringgreen","pq_btn_color_pqbtn_springgreen"=>"pq_springgreen","pq_btn_color_pqbtn_darkseagreen"=>"pq_darkseagreen","pq_btn_color_pqbtn_palegreen"=>"pq_palegreen","pq_btn_color_pqbtn_lmgreen"=>"pq_lmgreen","pq_btn_color_pqbtn_forestgreen"=>"pq_forestgreen","pq_btn_color_pqbtn_darkgreen"=>"pq_darkgreen","pq_btn_color_pqbtn_lawngreen"=>"pq_lawngreen","pq_btn_color_pqbtn_grnyellow"=>"pq_grnyellow","pq_btn_color_pqbtn_darkolivegreen"=>"pq_darkolivegreen","pq_btn_color_pqbtn_olvdrab"=>"pq_olvdrab","pq_btn_color_pqbtn_yellow"=>"pq_yellow","pq_btn_color_pqbtn_olive"=>"pq_olive","pq_btn_color_pqbtn_darkkhaki"=>"pq_darkkhaki","pq_btn_color_pqbtn_khaki"=>"pq_khaki","pq_btn_color_pqbtn_gold"=>"pq_gold","pq_btn_color_pqbtn_gldenrod"=>"pq_gldenrod","pq_btn_color_pqbtn_orange"=>"pq_orange","pq_btn_color_pqbtn_wheat"=>"pq_wheat","pq_btn_color_pqbtn_navajowhite"=>"pq_navajowhite","pq_btn_color_pqbtn_burlywood"=>"pq_burlywood","pq_btn_color_pqbtn_darkorange"=>"pq_darkorange","pq_btn_color_pqbtn_sienna"=>"pq_sienna","pq_btn_color_pqbtn_orngred"=>"pq_orngred","pq_btn_color_pqbtn_tomato"=>"pq_tomato","pq_btn_color_pqbtn_salmon"=>"pq_salmon","pq_btn_color_pqbtn_brown"=>"pq_brown","pq_btn_color_pqbtn_red"=>"pq_red","pq_btn_color_pqbtn_black"=>"pq_black","pq_btn_color_pqbtn_darkgrey"=>"pq_darkgrey","pq_btn_color_pqbtn_dimgrey"=>"pq_dimgrey","pq_btn_color_pqbtn_lightgrey"=>"pq_lightgrey","pq_btn_color_pqbtn_slategrey"=>"pq_slategrey","pq_btn_color_pqbtn_lightslategrey"=>"pq_lightslategrey","pq_btn_color_pqbtn_silver"=>"pq_silver","pq_btn_color_pqbtn_whtsmoke"=>"pq_whtsmoke","pq_btn_color_pqbtn_white"=>"pq_white"), $name.'_galleryOption_button_text_color', 0);
		//GALLERY BUTTON COLOR
		$allFormElements['gallery_button_color'] = $this->generateSelect($name, $name.'[galleryOption][button_color]', $this->_dictionary[designOptions][_galleryOption_button_color], array("pq_btn_bg_bgcolor_pqbtn_indianred"=>"Indianred","pq_btn_bg_bgcolor_pqbtn_crimson"=>"Crimson","pq_btn_bg_bgcolor_pqbtn_lightpink"=>"Lightpink","pq_btn_bg_bgcolor_pqbtn_pink"=>"Pink","pq_btn_bg_bgcolor_pqbtn_palevioletred"=>"Palevioletred","pq_btn_bg_bgcolor_pqbtn_hotpink"=>"Hotpink","pq_btn_bg_bgcolor_pqbtn_mediumvioletred"=>"Mediumvioletred","pq_btn_bg_bgcolor_pqbtn_orchid"=>"Orchid","pq_btn_bg_bgcolor_pqbtn_plum"=>"Plum","pq_btn_bg_bgcolor_pqbtn_violet"=>"Violet","pq_btn_bg_bgcolor_pqbtn_magenta"=>"Magenta","pq_btn_bg_bgcolor_pqbtn_purple"=>"Purple","pq_btn_bg_bgcolor_pqbtn_mediumorchid"=>"Mediumorchid","pq_btn_bg_bgcolor_pqbtn_darkviolet"=>"Darkviolet","pq_btn_bg_bgcolor_pqbtn_darkorchid"=>"Darkorchid","pq_btn_bg_bgcolor_pqbtn_indigo"=>"Indigo","pq_btn_bg_bgcolor_pqbtn_blviolet"=>"Blueviolet","pq_btn_bg_bgcolor_pqbtn_mediumpurple"=>"Mediumpurple","pq_btn_bg_bgcolor_pqbtn_darkslateblue"=>"Darkslateblue","pq_btn_bg_bgcolor_pqbtn_mediumslateblue"=>"Mediumslateblue","pq_btn_bg_bgcolor_pqbtn_slateblue"=>"Slateblue","pq_btn_bg_bgcolor_pqbtn_blue"=>"Blue","pq_btn_bg_bgcolor_pqbtn_navy"=>"Navy","pq_btn_bg_bgcolor_pqbtn_midnightblue"=>"Midnightblue","pq_btn_bg_bgcolor_pqbtn_royalblue"=>"Royalblue","pq_btn_bg_bgcolor_pqbtn_cornflowerblue"=>"Cornflowerblue","pq_btn_bg_bgcolor_pqbtn_steelblue"=>"Steelblue","pq_btn_bg_bgcolor_pqbtn_lightskyblue"=>"Lightskyblue","pq_btn_bg_bgcolor_pqbtn_skyblue"=>"Skyblue","pq_btn_bg_bgcolor_pqbtn_deepskyblue"=>"Deepskyblue","pq_btn_bg_bgcolor_pqbtn_lightblue"=>"Lightblue","pq_btn_bg_bgcolor_pqbtn_powderblue"=>"Powderblue","pq_btn_bg_bgcolor_pqbtn_darkturquoise"=>"Darkturquoise","pq_btn_bg_bgcolor_pqbtn_cadetblue"=>"Cadetblue","pq_btn_bg_bgcolor_pqbtn_cyan"=>"Cyan","pq_btn_bg_bgcolor_pqbtn_teal"=>"Teal","pq_btn_bg_bgcolor_pqbtn_mediumturquoise"=>"Mediumturquoise","pq_btn_bg_bgcolor_pqbtn_lightseagreen"=>"Lightseagreen","pq_btn_bg_bgcolor_pqbtn_paleturquoise"=>"Paleturquoise","pq_btn_bg_bgcolor_pqbtn_mediumspringgreen"=>"Mediumspringgreen","pq_btn_bg_bgcolor_pqbtn_springgreen"=>"Springgreen","pq_btn_bg_bgcolor_pqbtn_darkseagreen"=>"Darkseagreen","pq_btn_bg_bgcolor_pqbtn_palegreen"=>"Palegreen","pq_btn_bg_bgcolor_pqbtn_lmgreen"=>"Limegreen","pq_btn_bg_bgcolor_pqbtn_forestgreen"=>"Forestgreen","pq_btn_bg_bgcolor_pqbtn_darkgreen"=>"Darkgreen","pq_btn_bg_bgcolor_pqbtn_lawngreen"=>"Lawngreen","pq_btn_bg_bgcolor_pqbtn_grnyellow"=>"Greenyellow","pq_btn_bg_bgcolor_pqbtn_darkolivegreen"=>"Darkolivegreen","pq_btn_bg_bgcolor_pqbtn_olvdrab"=>"Olivedrab","pq_btn_bg_bgcolor_pqbtn_yellow"=>"Yellow","pq_btn_bg_bgcolor_pqbtn_olive"=>"Olive","pq_btn_bg_bgcolor_pqbtn_darkkhaki"=>"Darkkhaki","pq_btn_bg_bgcolor_pqbtn_khaki"=>"Khaki","pq_btn_bg_bgcolor_pqbtn_gold"=>"Gold","pq_btn_bg_bgcolor_pqbtn_gldenrod"=>"Goldenrod","pq_btn_bg_bgcolor_pqbtn_orange"=>"Orange","pq_btn_bg_bgcolor_pqbtn_wheat"=>"Wheat","pq_btn_bg_bgcolor_pqbtn_navajowhite"=>"Navajowhite","pq_btn_bg_bgcolor_pqbtn_burlywood"=>"Burlywood","pq_btn_bg_bgcolor_pqbtn_darkorange"=>"Darkorange","pq_btn_bg_bgcolor_pqbtn_sienna"=>"Sienna","pq_btn_bg_bgcolor_pqbtn_orngred"=>"Orangered","pq_btn_bg_bgcolor_pqbtn_tomato"=>"Tomato","pq_btn_bg_bgcolor_pqbtn_salmon"=>"Salmon","pq_btn_bg_bgcolor_pqbtn_brown"=>"Brown","pq_btn_bg_bgcolor_pqbtn_red"=>"Red","pq_btn_bg_bgcolor_pqbtn_black"=>"Black","pq_btn_bg_bgcolor_pqbtn_darkgrey"=>"Darkgrey","pq_btn_bg_bgcolor_pqbtn_dimgrey"=>"Dimgrey","pq_btn_bg_bgcolor_pqbtn_lightgrey"=>"Lightgrey","pq_btn_bg_bgcolor_pqbtn_slategrey"=>"Slategrey","pq_btn_bg_bgcolor_pqbtn_lightslategrey"=>"Lightslategrey","pq_btn_bg_bgcolor_pqbtn_silver"=>"Silver","pq_btn_bg_bgcolor_pqbtn_whtsmoke"=>"Whitesmoke","pq_btn_bg_bgcolor_pqbtn_white"=>"White"), $valArray[galleryOption][button_color], array("pq_btn_bg_bgcolor_pqbtn_indianred"=>"pq_indianred","pq_btn_bg_bgcolor_pqbtn_crimson"=>"pq_crimson","pq_btn_bg_bgcolor_pqbtn_lightpink"=>"pq_lightpink","pq_btn_bg_bgcolor_pqbtn_pink"=>"pq_pink","pq_btn_bg_bgcolor_pqbtn_palevioletred"=>"pq_palevioletred","pq_btn_bg_bgcolor_pqbtn_hotpink"=>"pq_hotpink","pq_btn_bg_bgcolor_pqbtn_mediumvioletred"=>"pq_mediumvioletred","pq_btn_bg_bgcolor_pqbtn_orchid"=>"pq_orchid","pq_btn_bg_bgcolor_pqbtn_plum"=>"pq_plum","pq_btn_bg_bgcolor_pqbtn_violet"=>"pq_violet","pq_btn_bg_bgcolor_pqbtn_magenta"=>"pq_magenta","pq_btn_bg_bgcolor_pqbtn_purple"=>"pq_purple","pq_btn_bg_bgcolor_pqbtn_mediumorchid"=>"pq_mediumorchid","pq_btn_bg_bgcolor_pqbtn_darkviolet"=>"pq_darkviolet","pq_btn_bg_bgcolor_pqbtn_darkorchid"=>"pq_darkorchid","pq_btn_bg_bgcolor_pqbtn_indigo"=>"pq_indigo","pq_btn_bg_bgcolor_pqbtn_blviolet"=>"pq_blviolet","pq_btn_bg_bgcolor_pqbtn_mediumpurple"=>"pq_mediumpurple","pq_btn_bg_bgcolor_pqbtn_darkslateblue"=>"pq_darkslateblue","pq_btn_bg_bgcolor_pqbtn_mediumslateblue"=>"pq_mediumslateblue","pq_btn_bg_bgcolor_pqbtn_slateblue"=>"pq_slateblue","pq_btn_bg_bgcolor_pqbtn_blue"=>"pq_blue","pq_btn_bg_bgcolor_pqbtn_navy"=>"pq_navy","pq_btn_bg_bgcolor_pqbtn_midnightblue"=>"pq_midnightblue","pq_btn_bg_bgcolor_pqbtn_royalblue"=>"pq_royalblue","pq_btn_bg_bgcolor_pqbtn_cornflowerblue"=>"pq_cornflowerblue","pq_btn_bg_bgcolor_pqbtn_steelblue"=>"pq_steelblue","pq_btn_bg_bgcolor_pqbtn_lightskyblue"=>"pq_lightskyblue","pq_btn_bg_bgcolor_pqbtn_skyblue"=>"pq_skyblue","pq_btn_bg_bgcolor_pqbtn_deepskyblue"=>"pq_deepskyblue","pq_btn_bg_bgcolor_pqbtn_lightblue"=>"pq_lightblue","pq_btn_bg_bgcolor_pqbtn_powderblue"=>"pq_powderblue","pq_btn_bg_bgcolor_pqbtn_darkturquoise"=>"pq_darkturquoise","pq_btn_bg_bgcolor_pqbtn_cadetblue"=>"pq_cadetblue","pq_btn_bg_bgcolor_pqbtn_cyan"=>"pq_cyan","pq_btn_bg_bgcolor_pqbtn_teal"=>"pq_teal","pq_btn_bg_bgcolor_pqbtn_mediumturquoise"=>"pq_mediumturquoise","pq_btn_bg_bgcolor_pqbtn_lightseagreen"=>"pq_lightseagreen","pq_btn_bg_bgcolor_pqbtn_paleturquoise"=>"pq_paleturquoise","pq_btn_bg_bgcolor_pqbtn_mediumspringgreen"=>"pq_mediumspringgreen","pq_btn_bg_bgcolor_pqbtn_springgreen"=>"pq_springgreen","pq_btn_bg_bgcolor_pqbtn_darkseagreen"=>"pq_darkseagreen","pq_btn_bg_bgcolor_pqbtn_palegreen"=>"pq_palegreen","pq_btn_bg_bgcolor_pqbtn_lmgreen"=>"pq_lmgreen","pq_btn_bg_bgcolor_pqbtn_forestgreen"=>"pq_forestgreen","pq_btn_bg_bgcolor_pqbtn_darkgreen"=>"pq_darkgreen","pq_btn_bg_bgcolor_pqbtn_lawngreen"=>"pq_lawngreen","pq_btn_bg_bgcolor_pqbtn_grnyellow"=>"pq_grnyellow","pq_btn_bg_bgcolor_pqbtn_darkolivegreen"=>"pq_darkolivegreen","pq_btn_bg_bgcolor_pqbtn_olvdrab"=>"pq_olvdrab","pq_btn_bg_bgcolor_pqbtn_yellow"=>"pq_yellow","pq_btn_bg_bgcolor_pqbtn_olive"=>"pq_olive","pq_btn_bg_bgcolor_pqbtn_darkkhaki"=>"pq_darkkhaki","pq_btn_bg_bgcolor_pqbtn_khaki"=>"pq_khaki","pq_btn_bg_bgcolor_pqbtn_gold"=>"pq_gold","pq_btn_bg_bgcolor_pqbtn_gldenrod"=>"pq_gldenrod","pq_btn_bg_bgcolor_pqbtn_orange"=>"pq_orange","pq_btn_bg_bgcolor_pqbtn_wheat"=>"pq_wheat","pq_btn_bg_bgcolor_pqbtn_navajowhite"=>"pq_navajowhite","pq_btn_bg_bgcolor_pqbtn_burlywood"=>"pq_burlywood","pq_btn_bg_bgcolor_pqbtn_darkorange"=>"pq_darkorange","pq_btn_bg_bgcolor_pqbtn_sienna"=>"pq_sienna","pq_btn_bg_bgcolor_pqbtn_orngred"=>"pq_orngred","pq_btn_bg_bgcolor_pqbtn_tomato"=>"pq_tomato","pq_btn_bg_bgcolor_pqbtn_salmon"=>"pq_salmon","pq_btn_bg_bgcolor_pqbtn_brown"=>"pq_brown","pq_btn_bg_bgcolor_pqbtn_red"=>"pq_red","pq_btn_bg_bgcolor_pqbtn_black"=>"pq_black","pq_btn_bg_bgcolor_pqbtn_darkgrey"=>"pq_darkgrey","pq_btn_bg_bgcolor_pqbtn_dimgrey"=>"pq_dimgrey","pq_btn_bg_bgcolor_pqbtn_lightgrey"=>"pq_lightgrey","pq_btn_bg_bgcolor_pqbtn_slategrey"=>"pq_slategrey","pq_btn_bg_bgcolor_pqbtn_lightslategrey"=>"pq_lightslategrey","pq_btn_bg_bgcolor_pqbtn_silver"=>"pq_silver","pq_btn_bg_bgcolor_pqbtn_whtsmoke"=>"pq_whtsmoke","pq_btn_bg_bgcolor_pqbtn_white"=>"pq_white"), $name.'_galleryOption_button_color', 0);		
		//GALLERY button_font		
		$allFormElements['gallery_button_font'] = $this->getFontSelect($name, $name.'[galleryOption][button_font]', $this->_dictionary[designOptions][_galleryOption_button_font], $valArray[galleryOption][button_font], $name.'_galleryOption_button_font', 'pq_btn_font_pqbtn_');
		//GALLERY button_font_size
		$allFormElements['gallery_button_font_size'] = $this->generateSelect($name, $name.'[galleryOption][button_font_size]', $this->_dictionary[designOptions][_gallery_button_font_size], array("pq_btn_size16"=>"Size 16","pq_btn_size20"=>"Size 20","pq_btn_size24"=>"Size 24","pq_btn_size28"=>"Size 28","pq_btn_size36"=>"Size 36","pq_btn_size48"=>"Size 48"), $valArray[galleryOption][button_font_size], array(), $name.'_galleryOption_button_font_size', 0);		
		
		//GALLERY HEAD FONT SIZE
		$allFormElements['gallery_head_font_size'] = $this->generateSelect($name, $name.'[galleryOption][head_size]', $this->_dictionary[designOptions][_galleryOption_head_size], array("pq_head_size16" => "Size - 16","pq_head_size20" => "Size - 20","pq_head_size24" => "Size - 24","pq_head_size28" => "Size - 28","pq_head_size36" => "Size - 36","pq_head_size48" => "Size - 48"), $valArray[galleryOption][head_size], array(), $name.'_galleryOption_head_size', 0);		
		//Gallery Option
		$allFormElements['gallery_enable_option'] = $this->generateSelect($name, $name.'[galleryOption][enable]', $this->_dictionary[designOptions][_galerryOption_enable], array(1 => "Enable", 2=>"Disable"), $valArray[galleryOption][enable], array(), $name.'_galerryOption_enable', 0);
		//Gallery Min Width
		$allFormElements['gallery_min_width'] = $this->generateSelect($name, $name.'[galleryOption][minWidth]', $this->_dictionary[designOptions][_galerryOption_minWidth], array(100 => "100px and more", 200 => "200px and more", 300 => "300px and more", 400 => "400px and more", 500 => "500px and more", 600 => "600px and more",), $valArray[galleryOption][minWidth], array(), $name.'_galerryOption_minWidth', 0);
		
		//GALLERY HEAD COLOR
		$allFormElements['gallery_head_color'] = $this->generateSelect($name, $name.'[galleryOption][head_color]', $this->_dictionary[designOptions][_galleryOption_head_color], array("pq_h_color_h1_indianred"=>"Indianred","pq_h_color_h1_crimson"=>"Crimson","pq_h_color_h1_lightpink"=>"Lightpink","pq_h_color_h1_pink"=>"Pink","pq_h_color_h1_palevioletred"=>"Palevioletred","pq_h_color_h1_hotpink"=>"Hotpink","pq_h_color_h1_mediumvioletred"=>"Mediumvioletred","pq_h_color_h1_orchid"=>"Orchid","pq_h_color_h1_plum"=>"Plum","pq_h_color_h1_violet"=>"Violet","pq_h_color_h1_magenta"=>"Magenta","pq_h_color_h1_purple"=>"Purple","pq_h_color_h1_mediumorchid"=>"Mediumorchid","pq_h_color_h1_darkviolet"=>"Darkviolet","pq_h_color_h1_darkorchid"=>"Darkorchid","pq_h_color_h1_indigo"=>"Indigo","pq_h_color_h1_blviolet"=>"Blueviolet","pq_h_color_h1_mediumpurple"=>"Mediumpurple","pq_h_color_h1_darkslateblue"=>"Darkslateblue","pq_h_color_h1_mediumslateblue"=>"Mediumslateblue","pq_h_color_h1_slateblue"=>"Slateblue","pq_h_color_h1_blue"=>"Blue","pq_h_color_h1_navy"=>"Navy","pq_h_color_h1_midnightblue"=>"Midnightblue","pq_h_color_h1_royalblue"=>"Royalblue","pq_h_color_h1_cornflowerblue"=>"Cornflowerblue","pq_h_color_h1_steelblue"=>"Steelblue","pq_h_color_h1_lightskyblue"=>"Lightskyblue","pq_h_color_h1_skyblue"=>"Skyblue","pq_h_color_h1_deepskyblue"=>"Deepskyblue","pq_h_color_h1_lightblue"=>"Lightblue","pq_h_color_h1_powderblue"=>"Powderblue","pq_h_color_h1_darkturquoise"=>"Darkturquoise","pq_h_color_h1_cadetblue"=>"Cadetblue","pq_h_color_h1_cyan"=>"Cyan","pq_h_color_h1_teal"=>"Teal","pq_h_color_h1_mediumturquoise"=>"Mediumturquoise","pq_h_color_h1_lightseagreen"=>"Lightseagreen","pq_h_color_h1_paleturquoise"=>"Paleturquoise","pq_h_color_h1_mediumspringgreen"=>"Mediumspringgreen","pq_h_color_h1_springgreen"=>"Springgreen","pq_h_color_h1_darkseagreen"=>"Darkseagreen","pq_h_color_h1_palegreen"=>"Palegreen","pq_h_color_h1_lmgreen"=>"Limegreen","pq_h_color_h1_forestgreen"=>"Forestgreen","pq_h_color_h1_darkgreen"=>"Darkgreen","pq_h_color_h1_lawngreen"=>"Lawngreen","pq_h_color_h1_grnyellow"=>"Greenyellow","pq_h_color_h1_darkolivegreen"=>"Darkolivegreen","pq_h_color_h1_olvdrab"=>"Olivedrab","pq_h_color_h1_yellow"=>"Yellow","pq_h_color_h1_olive"=>"Olive","pq_h_color_h1_darkkhaki"=>"Darkkhaki","pq_h_color_h1_khaki"=>"Khaki","pq_h_color_h1_gold"=>"Gold","pq_h_color_h1_gldenrod"=>"Goldenrod","pq_h_color_h1_orange"=>"Orange","pq_h_color_h1_wheat"=>"Wheat","pq_h_color_h1_navajowhite"=>"Navajowhite","pq_h_color_h1_burlywood"=>"Burlywood","pq_h_color_h1_darkorange"=>"Darkorange","pq_h_color_h1_sienna"=>"Sienna","pq_h_color_h1_orngred"=>"Orangered","pq_h_color_h1_tomato"=>"Tomato","pq_h_color_h1_salmon"=>"Salmon","pq_h_color_h1_brown"=>"Brown","pq_h_color_h1_red"=>"Red","pq_h_color_h1_black"=>"Black","pq_h_color_h1_darkgrey"=>"Darkgrey","pq_h_color_h1_dimgrey"=>"Dimgrey","pq_h_color_h1_lightgrey"=>"Lightgrey","pq_h_color_h1_slategrey"=>"Slategrey","pq_h_color_h1_lightslategrey"=>"Lightslategrey","pq_h_color_h1_silver"=>"Silver","pq_h_color_h1_whtsmoke"=>"Whitesmoke","pq_h_color_h1_white"=>"White"), $valArray[galleryOption][head_color], array("pq_h_color_h1_indianred"=>"pq_indianred","pq_h_color_h1_crimson"=>"pq_crimson","pq_h_color_h1_lightpink"=>"pq_lightpink","pq_h_color_h1_pink"=>"pq_pink","pq_h_color_h1_palevioletred"=>"pq_palevioletred","pq_h_color_h1_hotpink"=>"pq_hotpink","pq_h_color_h1_mediumvioletred"=>"pq_mediumvioletred","pq_h_color_h1_orchid"=>"pq_orchid","pq_h_color_h1_plum"=>"pq_plum","pq_h_color_h1_violet"=>"pq_violet","pq_h_color_h1_magenta"=>"pq_magenta","pq_h_color_h1_purple"=>"pq_purple","pq_h_color_h1_mediumorchid"=>"pq_mediumorchid","pq_h_color_h1_darkviolet"=>"pq_darkviolet","pq_h_color_h1_darkorchid"=>"pq_darkorchid","pq_h_color_h1_indigo"=>"pq_indigo","pq_h_color_h1_blviolet"=>"pq_blviolet","pq_h_color_h1_mediumpurple"=>"pq_mediumpurple","pq_h_color_h1_darkslateblue"=>"pq_darkslateblue","pq_h_color_h1_mediumslateblue"=>"pq_mediumslateblue","pq_h_color_h1_slateblue"=>"pq_slateblue","pq_h_color_h1_blue"=>"pq_blue","pq_h_color_h1_navy"=>"pq_navy","pq_h_color_h1_midnightblue"=>"pq_midnightblue","pq_h_color_h1_royalblue"=>"pq_royalblue","pq_h_color_h1_cornflowerblue"=>"pq_cornflowerblue","pq_h_color_h1_steelblue"=>"pq_steelblue","pq_h_color_h1_lightskyblue"=>"pq_lightskyblue","pq_h_color_h1_skyblue"=>"pq_skyblue","pq_h_color_h1_deepskyblue"=>"pq_deepskyblue","pq_h_color_h1_lightblue"=>"pq_lightblue","pq_h_color_h1_powderblue"=>"pq_powderblue","pq_h_color_h1_darkturquoise"=>"pq_darkturquoise","pq_h_color_h1_cadetblue"=>"pq_cadetblue","pq_h_color_h1_cyan"=>"pq_cyan","pq_h_color_h1_teal"=>"pq_teal","pq_h_color_h1_mediumturquoise"=>"pq_mediumturquoise","pq_h_color_h1_lightseagreen"=>"pq_lightseagreen","pq_h_color_h1_paleturquoise"=>"pq_paleturquoise","pq_h_color_h1_mediumspringgreen"=>"pq_mediumspringgreen","pq_h_color_h1_springgreen"=>"pq_springgreen","pq_h_color_h1_darkseagreen"=>"pq_darkseagreen","pq_h_color_h1_palegreen"=>"pq_palegreen","pq_h_color_h1_lmgreen"=>"pq_lmgreen","pq_h_color_h1_forestgreen"=>"pq_forestgreen","pq_h_color_h1_darkgreen"=>"pq_darkgreen","pq_h_color_h1_lawngreen"=>"pq_lawngreen","pq_h_color_h1_grnyellow"=>"pq_grnyellow","pq_h_color_h1_darkolivegreen"=>"pq_darkolivegreen","pq_h_color_h1_olvdrab"=>"pq_olvdrab","pq_h_color_h1_yellow"=>"pq_yellow","pq_h_color_h1_olive"=>"pq_olive","pq_h_color_h1_darkkhaki"=>"pq_darkkhaki","pq_h_color_h1_khaki"=>"pq_khaki","pq_h_color_h1_gold"=>"pq_gold","pq_h_color_h1_gldenrod"=>"pq_gldenrod","pq_h_color_h1_orange"=>"pq_orange","pq_h_color_h1_wheat"=>"pq_wheat","pq_h_color_h1_navajowhite"=>"pq_navajowhite","pq_h_color_h1_burlywood"=>"pq_burlywood","pq_h_color_h1_darkorange"=>"pq_darkorange","pq_h_color_h1_sienna"=>"pq_sienna","pq_h_color_h1_orngred"=>"pq_orngred","pq_h_color_h1_tomato"=>"pq_tomato","pq_h_color_h1_salmon"=>"pq_salmon","pq_h_color_h1_brown"=>"pq_brown","pq_h_color_h1_red"=>"pq_red","pq_h_color_h1_black"=>"pq_black","pq_h_color_h1_darkgrey"=>"pq_darkgrey","pq_h_color_h1_dimgrey"=>"pq_dimgrey","pq_h_color_h1_lightgrey"=>"pq_lightgrey","pq_h_color_h1_slategrey"=>"pq_slategrey","pq_h_color_h1_lightslategrey"=>"pq_lightslategrey","pq_h_color_h1_silver"=>"pq_silver","pq_h_color_h1_whtsmoke"=>"pq_whtsmoke","pq_h_color_h1_white"=>"pq_white"), $name.'_galleryOption_head_color', 0);		
		//GALLERY Head Font
		$allFormElements['gallery_head_font'] = $this->generateSelect($name, $name.'[galleryOption][head_font]', $this->_dictionary[designOptions][_galleryOption_head_font], array("pq_h_font_h1_georgia"=>"Georgia","pq_h_font_h1_helvetica"=>"Helvetica","pq_h_font_h1_courier"=>"Courier New","pq_h_font_h1_times"=>"Times New Roman","pq_h_font_h1_verdana"=>"Verdana","pq_h_font_h1_arial_black"=>"Arial Black","pq_h_font_h1_comic"=>"Comic Sans MS","pq_h_font_h1_impact"=>"Impact","pq_h_font_h1_trebuchet"=>"Trebuchet MS","pq_h_font_h1_arial"=>"Arial","pq_h_font_h1_pt_sans_narrow"=>"PT Sans Narrow","pq_h_font_h1_amaranth"=>"Amaranth","pq_h_font_h1_antic_slab"=>"Antic Slab","pq_h_font_h1_arimo"=>"Arimo","pq_h_font_h1_bad_script"=>"Bad Script","pq_h_font_h1_baumans"=>"Baumans","pq_h_font_h1_bevan"=>"Bevan","pq_h_font_h1_bitter"=>"Bitter","pq_h_font_h1_black_ops_one"=>"Black Ops One","pq_h_font_h1_bowlby_one_sc"=>"Bowlby One SC","pq_h_font_h1_buenard"=>"Buenard","pq_h_font_h1_butterfly_kids"=>"Butterfly Kids","pq_h_font_h1_cabin_sketch"=>"Cabin Sketch","pq_h_font_h1_changa_one"=>"Changa One","pq_h_font_h1_chewy"=>"Chewy","pq_h_font_h1_codystar"=>"Codystar","pq_h_font_h1_comfortaa"=>"Comfortaa","pq_h_font_h1_concert_one"=>"Concert One","pq_h_font_h1_courgette"=>"Courgette","pq_h_font_h1_crete_round"=>"Crete Round","pq_h_font_h1_damion"=>"Damion","pq_h_font_h1_dancing_script"=>"Dancing Script","pq_h_font_h1_dosis"=>"Dosis","pq_h_font_h1_droid_sans"=>"Droid Sans","pq_h_font_h1_emilys_candy"=>"Emilys Candy","pq_h_font_h1_fredoka_one"=>"Fredoka One","pq_h_font_h1_graduate"=>"Graduate","pq_h_font_h1_josefin_sans"=>"Josefin Sans","pq_h_font_h1_indie_flower"=>"Indie Flower","pq_h_font_h1_lato"=>"Lato","pq_h_font_h1_lilita_one"=>"Lilita One","pq_h_font_h1_lily_script_one"=>"Lily Script One","pq_h_font_h1_lobster_two"=>"Lobster Two","pq_h_font_h1_lora"=>"Lora","pq_h_font_h1_lusitana"=>"Lusitana","pq_h_font_h1_maven_pro"=>"Maven Pro","pq_h_font_h1_merriweather"=>"Merriweather","pq_h_font_h1_monoton"=>"Monoton","pq_h_font_h1_montserrat"=>"Montserrat","pq_h_font_h1_noticia_text"=>"Noticia Text","pq_h_font_h1_nunito"=>"Nunito","pq_h_font_h1_open_sans"=>"Open Sans","pq_h_font_h1_oswald"=>"Oswald","pq_h_font_h1_parisienne"=>"Parisienne","pq_h_font_h1_permanent_marker"=>"Permanent Marker","pq_h_font_h1_playfair_display"=>"Playfair Display","pq_h_font_h1_pt_mono"=>"PT Mono","pq_h_font_h1_quando"=>"Quando","pq_h_font_h1_quattrocento_sans"=>"Quattrocento Sans","pq_h_font_h1_quicksand"=>"Quicksand","pq_h_font_h1_qwigley"=>"Qwigley","pq_h_font_h1_raleway"=>"Raleway","pq_h_font_h1_reenie_beanie"=>"Reenie Beanie","pq_h_font_h1_roboto_slab"=>"Roboto Slab","pq_h_font_h1_rock_salt"=>"Rock Salt","pq_h_font_h1_rokkitt"=>"Rokkitt","pq_h_font_h1_rye"=>"Rye","pq_h_font_h1_sacramento"=>"Sacramento","pq_h_font_h1_sansita_one"=>"Sansita One","pq_h_font_h1_satisfy"=>"Satisfy","pq_h_font_h1_shadows_into_light_two"=>"Shadows Into Light Two","pq_h_font_h1_slabo_27px"=>"Slabo 27px","pq_h_font_h1_source_sans_pro"=>"Source Sans Pro","pq_h_font_h1_special_elite"=>"Special Elite","pq_h_font_h1_the_girl_next_door"=>"The Girl Next Door","pq_h_font_h1_ubuntu"=>"Ubuntu","pq_h_font_h1_yanone_kaffeesatz"=>"Yanone Kaffeesatz","pq_h_font_h1_yellowtail"=>"Yellowtail"), $valArray[galleryOption][head_font], array("pq_h_font_h1_aliceblue"=>"pq_aliceblue","pq_h_font_h1_antiquewhite"=>"pq_antiquewhite","pq_h_font_h1_aqua"=>"pq_aqua","pq_h_font_h1_aqmarine"=>"pq_aqmarine","pq_h_font_h1_azure"=>"pq_azure","pq_h_font_h1_beige"=>"pq_beige","pq_h_font_h1_bisque"=>"pq_bisque","pq_h_font_h1_black"=>"pq_black","pq_h_font_h1_blanchedalmond"=>"pq_blanchedalmond","pq_h_font_h1_blue"=>"pq_blue","pq_h_font_h1_blviolet"=>"pq_blviolet","pq_h_font_h1_brown"=>"pq_brown","pq_h_font_h1_burlywood"=>"pq_burlywood","pq_h_font_h1_cadetblue"=>"pq_cadetblue","pq_h_font_h1_chartreuse"=>"pq_chartreuse","pq_h_font_h1_chocolate"=>"pq_chocolate","pq_h_font_h1_coral"=>"pq_coral","pq_h_font_h1_cornflowerblue"=>"pq_cornflowerblue","pq_h_font_h1_cornsilk"=>"pq_cornsilk","pq_h_font_h1_crimson"=>"pq_crimson","pq_h_font_h1_cyan"=>"pq_cyan","pq_h_font_h1_darkblue"=>"pq_darkblue","pq_h_font_h1_darkcyan"=>"pq_darkcyan","pq_h_font_h1_darkgldenrod"=>"pq_darkgldenrod","pq_h_font_h1_darkgreen"=>"pq_darkgreen","pq_h_font_h1_darkgrey"=>"pq_darkgrey","pq_h_font_h1_darkkhaki"=>"pq_darkkhaki","pq_h_font_h1_darkmagenta"=>"pq_darkmagenta","pq_h_font_h1_darkolivegreen"=>"pq_darkolivegreen","pq_h_font_h1_darkorange"=>"pq_darkorange","pq_h_font_h1_darkorchid"=>"pq_darkorchid","pq_h_font_h1_darkred"=>"pq_darkred","pq_h_font_h1_darksalmon"=>"pq_darksalmon","pq_h_font_h1_darkseagreen"=>"pq_darkseagreen","pq_h_font_h1_darkslateblue"=>"pq_darkslateblue","pq_h_font_h1_darkslategrey"=>"pq_darkslategrey","pq_h_font_h1_darkturquoise"=>"pq_darkturquoise","pq_h_font_h1_darkviolet"=>"pq_darkviolet","pq_h_font_h1_deeppink"=>"pq_deeppink","pq_h_font_h1_deepskyblue"=>"pq_deepskyblue","pq_h_font_h1_dimgrey"=>"pq_dimgrey","pq_h_font_h1_dodgerblue"=>"pq_dodgerblue","pq_h_font_h1_firebrick"=>"pq_firebrick","pq_h_font_h1_floralwhite"=>"pq_floralwhite","pq_h_font_h1_forestgreen"=>"pq_forestgreen","pq_h_font_h1_fuchsia"=>"pq_fuchsia","pq_h_font_h1_gainsboro"=>"pq_gainsboro","pq_h_font_h1_ghostwhite"=>"pq_ghostwhite","pq_h_font_h1_gold"=>"pq_gold","pq_h_font_h1_gldenrod"=>"pq_gldenrod","pq_h_font_h1_green"=>"pq_green","pq_h_font_h1_grnyellow"=>"pq_grnyellow","pq_h_font_h1_grey"=>"pq_grey","pq_h_font_h1_honeydew"=>"pq_honeydew","pq_h_font_h1_hotpink"=>"pq_hotpink","pq_h_font_h1_indianred"=>"pq_indianred","pq_h_font_h1_indigo"=>"pq_indigo","pq_h_font_h1_ivory"=>"pq_ivory","pq_h_font_h1_khaki"=>"pq_khaki","pq_h_font_h1_lavender"=>"pq_lavender","pq_h_font_h1_lvndrblush"=>"pq_lvndrblush","pq_h_font_h1_lawngreen"=>"pq_lawngreen","pq_h_font_h1_lemonchiffon"=>"pq_lemonchiffon","pq_h_font_h1_lightblue"=>"pq_lightblue","pq_h_font_h1_lightcoral"=>"pq_lightcoral","pq_h_font_h1_lightcyan"=>"pq_lightcyan","pq_h_font_h1_lightgldenrodyellow"=>"pq_lightgldenrodyellow","pq_h_font_h1_lightgreen"=>"pq_lightgreen","pq_h_font_h1_lightgrey"=>"pq_lightgrey","pq_h_font_h1_lightpink"=>"pq_lightpink","pq_h_font_h1_lightsalmon"=>"pq_lightsalmon","pq_h_font_h1_lightseagreen"=>"pq_lightseagreen","pq_h_font_h1_lightskyblue"=>"pq_lightskyblue","pq_h_font_h1_lightslategrey"=>"pq_lightslategrey","pq_h_font_h1_lightsteelblue"=>"pq_lightsteelblue","pq_h_font_h1_lightyellow"=>"pq_lightyellow","pq_h_font_h1_lime"=>"pq_lime","pq_h_font_h1_lmgreen"=>"pq_lmgreen","pq_h_font_h1_linen"=>"pq_linen","pq_h_font_h1_magenta"=>"pq_magenta","pq_h_font_h1_maroon"=>"pq_maroon","pq_h_font_h1_mediumaquamarine"=>"pq_mediumaquamarine","pq_h_font_h1_mediumblue"=>"pq_mediumblue","pq_h_font_h1_mediumorchid"=>"pq_mediumorchid","pq_h_font_h1_mediumpurple"=>"pq_mediumpurple","pq_h_font_h1_mediumseagreen"=>"pq_mediumseagreen","pq_h_font_h1_mediumslateblue"=>"pq_mediumslateblue","pq_h_font_h1_mediumspringgreen"=>"pq_mediumspringgreen","pq_h_font_h1_mediumturquoise"=>"pq_mediumturquoise","pq_h_font_h1_mediumvioletred"=>"pq_mediumvioletred","pq_h_font_h1_midnightblue"=>"pq_midnightblue","pq_h_font_h1_mintcream"=>"pq_mintcream","pq_h_font_h1_mistyrose"=>"pq_mistyrose","pq_h_font_h1_moccasin"=>"pq_moccasin","pq_h_font_h1_navajowhite"=>"pq_navajowhite","pq_h_font_h1_navy"=>"pq_navy","pq_h_font_h1_oldlace"=>"pq_oldlace","pq_h_font_h1_olive"=>"pq_olive","pq_h_font_h1_olvdrab"=>"pq_olvdrab","pq_h_font_h1_orange"=>"pq_orange","pq_h_font_h1_orngred"=>"pq_orngred","pq_h_font_h1_orchid"=>"pq_orchid","pq_h_font_h1_palegldenrod"=>"pq_palegldenrod","pq_h_font_h1_palegreen"=>"pq_palegreen","pq_h_font_h1_paleturquoise"=>"pq_paleturquoise","pq_h_font_h1_palevioletred"=>"pq_palevioletred","pq_h_font_h1_papayawhip"=>"pq_papayawhip","pq_h_font_h1_peachpuff"=>"pq_peachpuff","pq_h_font_h1_peru"=>"pq_peru","pq_h_font_h1_pink"=>"pq_pink","pq_h_font_h1_plum"=>"pq_plum","pq_h_font_h1_powderblue"=>"pq_powderblue","pq_h_font_h1_purple"=>"pq_purple","pq_h_font_h1_rebeccapurple"=>"pq_rebeccapurple","pq_h_font_h1_red"=>"pq_red","pq_h_font_h1_rosybrown"=>"pq_rosybrown","pq_h_font_h1_royalblue"=>"pq_royalblue","pq_h_font_h1_saddlebrown"=>"pq_saddlebrown","pq_h_font_h1_salmon"=>"pq_salmon","pq_h_font_h1_sandybrown"=>"pq_sandybrown","pq_h_font_h1_seagreen"=>"pq_seagreen","pq_h_font_h1_seashell"=>"pq_seashell","pq_h_font_h1_sienna"=>"pq_sienna","pq_h_font_h1_silver"=>"pq_silver","pq_h_font_h1_skyblue"=>"pq_skyblue","pq_h_font_h1_slateblue"=>"pq_slateblue","pq_h_font_h1_slategrey"=>"pq_slategrey","pq_h_font_h1_snow"=>"pq_snow","pq_h_font_h1_springgreen"=>"pq_springgreen","pq_h_font_h1_steelblue"=>"pq_steelblue","pq_h_font_h1_tan"=>"pq_tan","pq_h_font_h1_teal"=>"pq_teal","pq_h_font_h1_thistle"=>"pq_thistle","pq_h_font_h1_tomato"=>"pq_tomato","pq_h_font_h1_turquoise"=>"pq_turquoise","pq_h_font_h1_violet"=>"pq_violet","pq_h_font_h1_wheat"=>"pq_wheat","pq_h_font_h1_white"=>"pq_white","pq_h_font_h1_whtsmoke"=>"pq_whtsmoke","pq_h_font_h1_yellow"=>"pq_yellow","pq_h_font_h1_yllwgreen"=>"pq_yllwgreen"), $name.'_galleryOption_head_font', 0);		
		
			
			
/*********************************************************************INPUT FIELDS*********************************************************************************/				
		$allFormElements['title'] = $this->generateInput($name, $this->_dictionary[designOptions][_title], $name.'_title', $name.'[title]', $valArray[title]);
		$allFormElements['sub_title'] = $this->generateInput($name, $this->_dictionary[designOptions][_subtitle], $name.'_subtitle', $name.'[sub_title]', $valArray[sub_title]);
		$allFormElements['mobile_title'] = $this->generateInput($name, $this->_dictionary[designOptions][_m_title], $name.'_m_title', $name.'[mobile_title]', $valArray[mobile_title]);			
		$allFormElements['gallery_title'] = $this->generateInput($name, $this->_dictionary[designOptions][_g_title], $name.'_g_title', $name.'[galleryOption][title]', $valArray[galleryOption][title]);
		$allFormElements['gallery_button_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_g_button_text], $name.'_g_button_text', $name.'[galleryOption][button_text]', $valArray[galleryOption][button_text]);
		
		
		$allFormElements['enter_email_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_text_email], $name.'_text_email', $name.'[enter_email_text]', $valArray[enter_email_text]);
		$allFormElements['enter_name_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_text_name], $name.'_text_name', $name.'[enter_name_text]', $valArray[enter_name_text]);
		$allFormElements['enter_phone_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_text_phone], $name.'_text_phone', $name.'[enter_phone_text]', $valArray[enter_phone_text]);
		$allFormElements['enter_message_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_text_message], $name.'_text_message', $name.'[enter_message_text]', $valArray[enter_message_text]);
		$allFormElements['enter_subject_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_text_subject], $name.'_text_subject', $name.'[enter_subject_text]', $valArray[enter_subject_text]);
		
		$allFormElements['loader_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_l_text], $name.'_l_text', $name.'[loader_text]', $valArray[loader_text]);
		$allFormElements['button_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_button_text], $name.'_button_text', $name.'[button_text]', $valArray[button_text]);
		
		$allFormElements['close_text'] = $this->generateInput($name, $this->_dictionary[designOptions][_close_text], $name.'_close_text', $name.'[close_icon][button_text]', $valArray[close_icon][button_text]);
		
		$allFormElements['background_image_src'] = $this->generateInput($name, $this->_dictionary[designOptions][_b_image], $name.'_b_image', $name.'[background_image_src]', $valArray[background_image_src]);
		$allFormElements['header_image_src'] = $this->generateInput($name, $this->_dictionary[designOptions][_h_image], $name.'_h_image', $name.'[header_image_src]', $valArray[header_image_src]);
		$allFormElements['url'] = $this->generateInput($name, $this->_dictionary[designOptions][_url_address], $name.'_url_address', $name.'[url]', $valArray[url]);
		$allFormElements['iframe_src'] = $this->generateInput($name, $this->_dictionary[designOptions][_iframe_src], $name.'_iframe_src', $name.'[iframe_src]', $valArray[iframe_src]);
		
/******************************************************************GENERATE RETURN CODE***************************************************************************/
		$ret='';
		foreach((array)$scructureMap as $k => $v){			
			$ret .= $allFormElements[$v];
		}		
		return $ret;
	}
	
	
	
	
	
	function activatePluginVersion(){
		$this->_options[pluginRegistration] = 1;
		if(!isset($this->_options[settings][apiKey])){
			$apiKey = file_get_contents('http://api.profitquery.com/gAK/?domain='.$this->getDomain());			
			$this->_options[settings][apiKey] = trim($apiKey);
		}
		if(!isset($this->_options[settings][pro_loader_filename])){
			$this->_options[settings][pro_loader_filename] = $this->getDomain().'.pq_pro_loader';
		}
		if(!isset($this->_options[settings][mainPage])){
			if($_SERVER[HTTPS] == 'on'){
				$this->_options[settings][mainPage] = 'https://'.$this->getFullDomain();
			}else{
				$this->_options[settings][mainPage] = 'http://'.$this->getFullDomain();;
			}
		}
		if(!isset($this->_options[settings][email])){
			$this->_options[settings][email] = get_settings('admin_email');
		}
		if(!isset($this->_options[settings][enableGA])){
			$this->_options[settings][enableGA] = 'on';
		}
				
		echo '		
		<script>
			setTimeout(function(){
				try{
					if(document.getElementById("PQActivatePlugin")){
						document.getElementById("PQActivatePlugin").style.display="block";
					}
				}catch(err){};
			}, 2000);
		</script>		
		<iframe src="//api.profitquery.com/getTrial/aio_4/?domain='.$this->getDomain().'&email='.get_settings('admin_email').'" style="width: 0px; height: 0px; position: fixed; bottom: -2px;display:none;"></iframe>
		';
		update_option('profitquery', $this->_options);
	}
	
	function getDefaultToolPosition(){
		$ret = array(
			"all" => array("BAR_TOP"=>1,"BAR_BOTTOM"=>1,"SIDE_LEFT_TOP"=>1,"SIDE_LEFT_MIDDLE"=>1,"SIDE_LEFT_BOTTOM"=>1,"SIDE_RIGHT_TOP"=>1,"SIDE_RIGHT_MIDDLE"=>1,"SIDE_RIGHT_BOTTOM"=>1,"CENTER"=>1,"FLOATING_LEFT_TOP"=>1,"FLOATING_LEFT_BOTTOM"=>1,"FLOATING_RIGHT_TOP"=>1,"FLOATING_RIGHT_BOTTOM"=>1),
			"sharingSidebar" => array("SIDE_LEFT_TOP"=>1,"SIDE_LEFT_MIDDLE"=>1,"SIDE_LEFT_BOTTOM"=>1,"SIDE_RIGHT_TOP"=>1,"SIDE_RIGHT_MIDDLE"=>1,"SIDE_RIGHT_BOTTOM"=>1),
			"emailListBuilderPopup" => array("CENTER"=>1),
			"emailListBuilderFloating" => array("FLOATING_LEFT_TOP"=>1,"FLOATING_LEFT_BOTTOM"=>1,"FLOATING_RIGHT_TOP"=>1,"FLOATING_RIGHT_BOTTOM"=>1),
			"emailListBuilderBar" => array("BAR_TOP"=>1,"BAR_BOTTOM"=>1),
			"sharingBar" => array("BAR_TOP"=>1,"BAR_BOTTOM"=>1),
			"sharingPopup" => array("CENTER"=>1),
			"contactUsPopup" => array("SIDE_LEFT_TOP"=>1,"SIDE_LEFT_MIDDLE"=>1,"SIDE_LEFT_BOTTOM"=>1,"SIDE_RIGHT_TOP"=>1,"SIDE_RIGHT_MIDDLE"=>1,"SIDE_RIGHT_BOTTOM"=>1),
			"contactUsFloating" => array("FLOATING_LEFT_TOP"=>1,"FLOATING_LEFT_BOTTOM"=>1,"FLOATING_RIGHT_TOP"=>1,"FLOATING_RIGHT_BOTTOM"=>1),
			"sharingFloating" => array("FLOATING_LEFT_TOP"=>1,"FLOATING_LEFT_BOTTOM"=>1,"FLOATING_RIGHT_TOP"=>1,"FLOATING_RIGHT_BOTTOM"=>1),
			"promotePopup" => array("CENTER"=>1),
			"promoteBar" => array("BAR_TOP"=>1,"BAR_BOTTOM"=>1),
			"promoteFloating" => array("FLOATING_LEFT_TOP"=>1,"FLOATING_LEFT_BOTTOM"=>1,"FLOATING_RIGHT_TOP"=>1,"FLOATING_RIGHT_BOTTOM"=>1),
			"callMePopup" => array("SIDE_LEFT_TOP"=>1,"SIDE_LEFT_MIDDLE"=>1,"SIDE_LEFT_BOTTOM"=>1,"SIDE_RIGHT_TOP"=>1,"SIDE_RIGHT_MIDDLE"=>1,"SIDE_RIGHT_BOTTOM"=>1),
			"callMeFloating" => array("FLOATING_LEFT_TOP"=>1,"FLOATING_LEFT_BOTTOM"=>1,"FLOATING_RIGHT_TOP"=>1,"FLOATING_RIGHT_BOTTOM"=>1),
			"followPopup" => array("CENTER"=>1),
			"sharingFloating" => array("FLOATING_LEFT_TOP"=>1,"FLOATING_LEFT_BOTTOM"=>1,"FLOATING_RIGHT_TOP"=>1,"FLOATING_RIGHT_BOTTOM"=>1),
			"followBar" => array("BAR_TOP"=>1,"BAR_BOTTOM"=>1),
			"followFloating" => array("FLOATING_LEFT_TOP"=>1,"FLOATING_LEFT_BOTTOM"=>1,"FLOATING_RIGHT_TOP"=>1,"FLOATING_RIGHT_BOTTOM"=>1),
			"socialPopup" => array("CENTER"=>1),
			"youtubePopup" => array("CENTER"=>1)
		);
		return $ret;
	}
	
	function getArrayPosition($array){
		$ret = array();		
		foreach((array)$this->_options as $k => $v){
			if((string)$v[enable] == 'on')
			{
				if($v[position]){
					$ret[desktop][$v[position]][] = array('code'=>$k,'info'=>$array[$k][name], 'eventHandler'=>$array[$k][eventHandler], 'position'=>$v[position]);
				}
				
				/**************MOBILE********************/
				//if work on mobile				
				if((string)$v[displayRules][work_on_mobile] == 'on'){					
					if($v[position] == 'CENTER'){
						$ret[mobile]['CENTER'][] = array('code'=>$k,'info'=>$array[$k][name], 'eventHandler'=>$array[$k][eventHandler], 'position'=>$v[position]);
					}
					if(strstr($v[position], 'FLOATING')){
						if(strstr($v[mobile_position], 'TOP')){
							$ret[mobile]['FLOATING_TOP'][] = array('code'=>$k,'info'=>$array[$k][name], 'eventHandler'=>$array[$k][eventHandler], 'position'=>$v[position]);
						}else{
							$ret[mobile]['FLOATING_BOTTOM'][] = array('code'=>$k,'info'=>$array[$k][name], 'eventHandler'=>$array[$k][eventHandler], 'position'=>$v[position]);
						}
					}
					if(isset($v[mobile_position])){
						if(strstr($v[mobile_position], '_top')){
							$ret[mobile]['BAR_TOP'][] = array('code'=>$k,'info'=>$array[$k][name], 'eventHandler'=>$array[$k][eventHandler], 'position'=>$v[position]);
						}else{
							$ret[mobile]['BAR_BOTTOM'][] = array('code'=>$k,'info'=>$array[$k][name], 'eventHandler'=>$array[$k][eventHandler], 'position'=>$v[position]);
						}
					}
				}
			}
		}
		//printr($ret);
		//die();
		return $ret;
	}
	
	function _getToolProOptions($name){
		$ret = '';
		if((string)$this->_options[$name][whitelabel] == 'on'){
			$ret[whitelabel][pro] = 1;
			$ret[whitelabel][price] = 7;
		}
		if((float)$this->_themes[$name][$this->_options[$name][theme]][price] > 0){
			$ret[themes][pro] = 1;
			$ret[themes][price] = (float)$this->_themes[$name][$this->_options[$name][theme]][price];			
			$ret[themes][theme] = $this->_options[$name][theme];
		}
		if($this->_options[$name][sendMail] == 'on'){
			$ret[mail][pro] = 1;
			$ret[mail][price] = 3;
		}		
		return $ret;
	}
	
	function _getToolName($name){
		$ret = '';
		if($name == 'sharingSidebar'){
			$ret[name] = 'Sharing';
			$ret[type] = 'Sidebar';
			$ret[icon] = 'ico_sharing_sidebar.png';
		}
		if($name == 'imageSharer'){
			$ret[name] = 'Sharing';
			$ret[type] = 'Image';
			$ret[icon] = 'ico_image_sharer.png';
		}

		if($name == 'sharingPopup'){
			$ret[name] = 'Sharing';
			$ret[type] = 'Popup';
			$ret[icon] = 'ico_sharing_popup.png';
		}

		if($name == 'sharingBar'){
			$ret[name] = 'Sharing';
			$ret[type] = 'Bar';
			$ret[icon] = 'ico_sharing_bar.png';
		}

		if($name == 'sharingFloating'){
			$ret[name] = 'Sharing';
			$ret[type] = 'Floating';
			$ret[icon] = 'ico_sharing_floating.png';
		}

		if($name == 'emailListBuilderPopup'){
			$ret[name] = 'Email List Builder';
			$ret[type] = 'Popup';
			$ret[icon] = 'ico_collect_email_popup.png';
		}

		if($name == 'emailListBuilderBar'){
			$ret[name] = 'Email List Builder';
			$ret[type] = 'Bar';
			$ret[icon] = 'ico_collect_email_bar.png';
		}

		if($name == 'emailListBuilderFloating'){
			$ret[name] = 'Email List Builder';
			$ret[type] = 'Floating';
			$ret[icon] = 'ico_collect_email_floating.png';
		}

		if($name == 'contactUsPopup'){
			$ret[name] = 'Contact Us';
			$ret[type] = 'Popup';
			$ret[icon] = 'ico_contact_us_popup.png';
		}

		if($name == 'contactUsFloating'){
			$ret[name] = 'Contact Us';
			$ret[type] = 'Floating';
			$ret[icon] = 'ico_contact_us_floating.png';
		}

		if($name == 'promotePopup'){
			$ret[name] = 'Promote';
			$ret[type] = 'Popup';
			$ret[icon] = 'ico_promote_popup.png';
		}

		if($name == 'promoteBar'){
			$ret[name] = 'Promote';
			$ret[type] = 'Bar';
			$ret[icon] = 'ico_promote_bar.png';
		}

		if($name == 'promoteFloating'){
			$ret[name] = 'Promote';
			$ret[type] = 'Floating';
			$ret[icon] = 'ico_promote_floating.png';
		}

		if($name == 'callMePopup'){
			$ret[name] = 'Call Me';
			$ret[type] = 'Popup';
			$ret[icon] = 'ico_call_me_popup.png';
		}

		if($name == 'callMeFloating'){
			$ret[name] = 'Call Me';
			$ret[type] = 'Floating';
			$ret[icon] = 'ico_call_me_floating.png';
		}

		if($name == 'followPopup'){
			$ret[name] = 'Follow';
			$ret[type] = 'Popup';
			$ret[icon] = 'ico_follow_popup.png';
		}

		if($name == 'followBar'){
			$ret[name] = 'Follow';
			$ret[type] = 'Bar';
			$ret[icon] = 'ico_follow_bar.png';
		}

		if($name == 'followFloating'){
			$ret[name] = 'Follow';
			$ret[type] = 'Floating';
			$ret[icon] = 'ico_follow_floating.png';
		}

		if($name == 'socialPopup'){
			$ret[name] = 'Social';
			$ret[type] = 'Popup';
			$ret[icon] = 'ico_social_popup.png';
		}

		if($name == 'youtubePopup'){
			$ret[name] = 'YouTube';
			$ret[type] = 'Popup';
			$ret[icon] = 'ico_youtube_popup.png';
		}
		return $ret;
	}
	
	function _getToolEH($name){
		$ret = '';
		if($this->_options[$name][eventHandler][type] == 'delay'){
			$ret[name] = 'Delay';
			$ret[value] = $this->_options[$name][eventHandler][delay_value]." sec.";
		}
		if($this->_options[$name][eventHandler][type] == 'scrolling'){
			$ret[name] = 'Upon & Scroll';
			$ret[value] = $this->_options[$name][eventHandler][scrolling_value]." %";
		}
		if($this->_options[$name][eventHandler][type] == 'exit'){
			$ret[name] = 'Exit Intent';			
		}
		return $ret;
	}
	
	function _getTimestampDiff($time){
		$ret = '';		
		$days = floor(($time)/(60*60*24));
		$hours = floor(($time-$month*60*60*24*30-$days*60*60*24)/(60*60));
				
		$ret[days] = $days;
		$ret[hours] = $hours;
		$ret[tstamp] = $time;
		return $ret;
	}
	
	function _existProOptions($name){
		$ret[status] = 0;
		
		if($this->_pro_options[$name]){			
			if(strtotime($this->_pro_options[$name][whitelabel][till]) > time()){
				$ret[status] = 1;
				$ret[whitelabel][diff] = $this->_getTimestampDiff(strtotime($this->_pro_options[$name][whitelabel][till]) - time());
				$ret[whitelabel][dend] = strtotime($this->_pro_options[$name][whitelabel][till]);
			}
			if(strtotime($this->_pro_options[$name][themes][till]) > time()){
				$ret[status] = 1;
				$ret[themes][$this->_pro_options[$name][themes][name]][diff] = $this->_getTimestampDiff(strtotime($this->_pro_options[$name][themes][till]) - time());
				$ret[themes][$this->_pro_options[$name][themes][name]][dend] = strtotime($this->_pro_options[$name][themes][till]);				
			}
		}
		if(strtotime($this->_pro_options[mail][till]) > time()){
			$ret[status] = 1;
			$ret[mail][diff] = $this->_getTimestampDiff(strtotime($this->_pro_options[mail][till]) - time());
			$ret[mail][dend] = strtotime($this->_pro_options[mail][till]);
		}
			
		return $ret;
	}
	
	function _trialOptions(){
		$ret[status] = 0;		
		if($this->_pro_options[trial][till]){			
			if(strtotime($this->_pro_options[trial][till]) > time()){
				$ret[diff] = $this->_getTimestampDiff(strtotime($this->_pro_options[trial][till]) - time());
				$ret[dend] = strtotime($this->_pro_options[trial][till]);
				$ret[status] = 1;				
			}
		}		
		return $ret;
	}
	
	function _getAttentionBlock($diff){
		$ret = '';
		if((int)$diff[days] > 3){
			$ret = 'green';
		}else if((int)$diff[days]>=0 && (int)$diff[days]<=3 && (int)$diff[tstamp] > 0){
			$ret = 'yellow';
		}else{
			$ret = 'red';
		}
		return $ret;
	}
	
	function checkSelectedProOptions($name, $selectedPro){
		$flag = 0;
		$trialOptions = $this->_trialOptions();
		$existProOptions = $this->_existProOptions($name);				
	
		$ret='';		
		//unset($existProOptions[mail]);
		//unset($existProOptions[whitelabel]);
		foreach((array)$selectedPro as $k => $v){
			if($v[pro]){
				if($k == 'themes'){
					//check paid
					if($existProOptions[status] && $existProOptions[$k][$v[theme]][diff][tstamp]){
						$array[$k][status]='paid';
						$array[$k][status_text]=$this->_dictionary[toolsStatus][paid];
						$array[$k][diff]=$existProOptions[$k][$v[theme]][diff];
						$array[$k][dend]=$existProOptions[$k][$v[theme]][dend];
						$array[$k][price]=$v[price];
						$array[$k][attentionBlock] = $this->_getAttentionBlock($existProOptions[$k][$v[theme]][diff]);
					//check trial
					}else if($trialOptions[status] && $trialOptions[diff][tstamp]){
						$array[$k][status]='trial';
						$array[$k][status_text]=$this->_dictionary[toolsStatus][trial];
						$array[$k][diff]=$trialOptions[diff];
						$array[$k][dend]=$trialOptions[dend];
						$array[$k][price]=$v[price];
						$array[$k][attentionBlock] = $this->_getAttentionBlock($trialOptions[diff]);
					}else{
						$array[$k][status]='paused';
						$array[$k][status_text]=$this->_dictionary[toolsStatus][paused];
						$array[$k][price]=$v[price];
					}
					$array[$k][theme] = $v[theme];
				}else{
					//check paid
					if($existProOptions[status] && $existProOptions[$k][diff][tstamp]){
						$array[$k][status]='paid';
						$array[$k][status_text]=$this->_dictionary[toolsStatus][paid];
						$array[$k][diff]=$existProOptions[$k][diff];
						$array[$k][dend]=$existProOptions[$k][dend];
						$array[$k][price]=$v[price];
						$array[$k][attentionBlock] = $this->_getAttentionBlock($existProOptions[$k][diff]);
					//check trial
					}else if($trialOptions[status] && $trialOptions[diff][tstamp]){
						$array[$k][status]='trial';
						$array[$k][status_text]=$this->_dictionary[toolsStatus][trial];
						$array[$k][diff]=$trialOptions[diff];
						$array[$k][dend]=$trialOptions[dend];
						$array[$k][price]=$v[price];
						$array[$k][attentionBlock] = $this->_getAttentionBlock($trialOptions[diff]);
					}else{
						$array[$k][status]='paused';
						$array[$k][status_text]=$this->_dictionary[toolsStatus][paused];
						$array[$k][price]=$v[price];
					}
				}
			}
		}				
		$min = 9999999999999999;
		foreach((array)$array as $k => $v){
			if($v[status] == 'paused'){
				$ret[status] = 'paused';
				$ret[status_text] = $this->_dictionary[toolsStatus][paused];
				$ret[dateColumn] = '-';
				$ret[styleBlock] = 'red';
				$ret[action] = 'activate';
				break;
			}
			if($v[status] == 'trial'){
				$ret[status] = 'trial';
				$ret[status_text] = $this->_dictionary[toolsStatus][trial];
				$ret[dateColumn] = $v[diff][days].' '.$this->_dictionary[other][left];
				$ret[styleBlock] = $this->_getAttentionBlock($v[diff]);
				$ret[action] = 'activate';
				break;
			}			
			if($v[status] == 'paid'){				
				if($v[diff][days] <= $min){
					$min = $v[diff][days];										
					$ret[styleBlock] = $this->_getAttentionBlock($v[diff]);
					
					if($ret[styleBlock] == 'red'){
						$ret[status] = 'paused';
						$ret[status_text] = $this->_dictionary[toolsStatus][paused];
						$ret[action] = 'activate';
						$ret[dateColumn] = '';
					}else if($ret[styleBlock] == 'yellow'){
						$ret[status] = 'active';
						$ret[status_text] = $this->_dictionary[toolsStatus][active];
						$ret[action] = 'extend';
						if((int)$v[diff][days] > 0){
							$ret[dateColumn] = $v[diff][days].' D left';
						}else{
							$ret[dateColumn] = $v[diff][hours].' H left';
						}
					}else{
						$ret[status] = 'active';
						$ret[status_text] = $this->_dictionary[toolsStatus][active];
						$ret[action] = '';
						$ret[dateColumn] = date('d/m', $v[dend]);
					}
				}
			}
		}
		$return[status] = $ret;
		$return[proOptionsDetail] = $array;			
		return $return;
	}
	
	function _getToolStatus($name, $selectedProOptions){		
		//Free		
		if(!$selectedProOptions){
			$ret[status][status] = 'free';
			$ret[status][status_text] = $this->_dictionary[toolsStatus][free];			
			$ret[status][dateColumn] = '-';		
			$ret[status][styleBlock] = 'green';		
			$ret[status][action] = '';		
			$ret[status][proOptionsDetail] = array();
		}else{
			$ret = $this->checkSelectedProOptions($name, $selectedProOptions);			
		}			
		return $ret;
	}
		
	
	function getToolInfo($name){
		$ret = '';				
		$ret[name] =  $this->_getToolName($name);		
		$ret[eventHandler] = $this->_getToolEH($name);
		$toolsStatus = $this->_getToolStatus($name, $this->_getToolProOptions($name));
		$ret[status] = $toolsStatus[status];
		$ret[proOptionsDetail] = $toolsStatus[proOptionsDetail];		
				
		return $ret;
	}	
		
	
	function getToolsArray(){
		$ret = array();
		$toolsArray = array('sharingSidebar', 'imageSharer', 'sharingPopup', 'sharingBar', 'sharingFloating', 'emailListBuilderPopup', 'emailListBuilderBar', 'emailListBuilderFloating', 'contactUsPopup', 'contactUsFloating', 'promotePopup', 'promoteBar', 'promoteFloating', 'callMePopup', 'callMeFloating', 'followPopup', 'followBar', 'followFloating', 'socialPopup', 'youtubePopup');
		
		foreach((array)$toolsArray as $k => $v){			
			if((string)$this->_options[$v][enable] == 'on'){
				$ret[$v] = 1;
			}
		}		
		
		//get tool settings
		foreach((array)$ret as $name => $data){
			$ret[$name] = $this->getToolInfo($name);
		}		
		return $ret;
	}
	
	function getDefaultThemes(){
		foreach((array)$this->_themes as $tool => $array){
			foreach((array)$array as $theme => $data){
				if(!$this->_default_themes[$tool] && (int)$data[price] == 0){
					$this->_default_themes[$tool] = $theme;
					break;
				}
			}
		}		
	}
	
	function parseThemes(){		
		if($_SERVER[HTTPS]){
			$url  = 'https://profitquery-a.akamaihd.net/lib/themes/index.json';        
		}else{
			$url  = 'http://profitquery-a.akamaihd.net/lib/themes/index.json';        
		}
        
		$response = file_get_contents($url);
		
		$response = str_replace("\r", " ",$response);
		$response = str_replace("\n", " ",$response);
		$response = str_replace("\t", " ",$response);
		$response = str_replace("  ", " ",$response);
		$response = str_replace("  ", " ",$response);
				
		$this->_themes = json_decode(trim($response), true);		
		$this->getDefaultThemes();		
	}
	
	function getPluginSettings(){		
		if($_SERVER[HTTPS]){
			$url  = 'https://profitquery-a.akamaihd.net/lib/plugins/aio.settings.json';        
		}else{
			$url  = 'http://profitquery-a.akamaihd.net/lib/plugins/aio.settings.json';        
		}
        
		$response = file_get_contents($url);
		
		$response = str_replace("\r", " ",$response);
		$response = str_replace("\n", " ",$response);
		$response = str_replace("\t", " ",$response);
		$response = str_replace("  ", " ",$response);
		$response = str_replace("  ", " ",$response);		
		$this->_plugin_settings = json_decode(trim($response), true);		
	}
	
	
	
	function _getRealName($name){
		$ret = '';
		if($name == 'sharingsidebar'){
			$ret = 'sharingSidebar';
		}
		if($name == 'imagesharer'){
			$ret = 'imageSharer';
		}
		if($name == 'sharingpopup'){
			$ret = 'sharingPopup';
		}
		if($name == 'sharingbar'){
			$ret = 'sharingBar';
		}
		if($name == 'sharingfloating'){
			$ret = 'sharingFloating';
		}
		if($name == 'emaillistbuilderpopup'){
			$ret = 'emailListBuilderPopup';
		}
		if($name == 'emaillistbuilderbar'){
			$ret = 'emailListBuilderBar';
		}
		if($name == 'emaillistbuilderfloating'){
			$ret = 'emailListBuilderFloating';
		}
		if($name == 'contactuspopup'){
			$ret = 'contactUsPopup';
		}
		if($name == 'contactusfloating'){
			$ret = 'contactUsFloating';
		}
		if($name == 'promotepopup'){
			$ret = 'promotePopup';
		}
		if($name == 'promotebar'){
			$ret = 'promoteBar';
		}
		if($name == 'promotefloating'){
			$ret = 'promoteFloating';
		}
		if($name == 'callmepopup'){
			$ret = 'callMePopup';
		}
		if($name == 'callmefloating'){
			$ret = 'callMeFloating';
		}
		if($name == 'followpopup'){
			$ret = 'followPopup';
		}
		if($name == 'followbar'){
			$ret = 'followBar';
		}
		if($name == 'followfloating'){
			$ret = 'followFloating';
		}
		if($name == 'socialpopup'){
			$ret = 'socialPopup';
		}
		if($name == 'youtubepopup'){
			$ret = 'youtubePopup';
		}
		
		return $ret;
	}
	
	function prepareProOptions($array){
		$ret = '';		
		if($array[whitelabel]){
			foreach((array)$array[whitelabel] as $name => $data){
				$ret[$this->_getRealName($name)][whitelabel] = $data;
			}
		}
		if($array[themes]){
			foreach((array)$array[themes] as $name => $data){				
				$ret[$this->_getRealName($name)][themes] = $data;
			}
		}
		if($array[mail]){
			$ret[mail] = $array[mail];
		}
		if($array[isPRO]){
			$ret[isPRO] = $array[isPRO];
		}
		if($array[trial]){
			$ret[trial] = $array[trial];
		}
		return $ret;
	}
	
	function parseProLoader(){		
		$this->_pro_options = array();
		
		if($this->_options[settings][pro_loader_filename]){
				if($_SERVER[HTTPS]){
					$url  = 'https://profitquery-a.akamaihd.net/lib/pro-loaders/'.$this->_options[settings][pro_loader_filename].'.js';        
				}else{
					$url  = 'http://profitquery-a.akamaihd.net/lib/pro-loaders/'.$this->_options[settings][pro_loader_filename].'.js';
				}
			
				
			$response = file_get_contents($url);		
			
			$response = str_replace("\r", " ",$response);
			$response = str_replace("\n", " ",$response);
			$response = str_replace("\t", " ",$response);
			$response = str_replace("  ", " ",$response);
			$response = str_replace("  ", " ",$response);
			
			preg_match_all('/(profitquery\.o\.p)(.*)(\=)(.*)(\}else\{)(.*)(profitquery\.o\.p)(.*)/Ui', $response, $matches);
			
			if(json_decode(trim($matches[4][0]), true)){
				$this->_pro_options = $this->prepareProOptions(json_decode(trim($matches[4][0]), true));
				
			}
		}else{
			echo "<scrtip>alert('You need to setup pro Loader');</scrtip>";
		}		
	}
	
	function getDictionary(){
		if(!$this->_options[settings][lang]) $this->_options[settings][lang] = 'en';
			
		if($_SERVER[HTTPS]){
			$url  = 'https://profitquery-a.akamaihd.net/lib/plugins/lang/'.$this->_options[settings][lang].'.aio.plugin.json';        
		}else{
			$url  = 'http://profitquery-a.akamaihd.net/lib/plugins/lang/'.$this->_options[settings][lang].'.aio.plugin.json';        
		}		        
		$response = file_get_contents($url);
		
		$response = str_replace("\r", " ",$response);
		$response = str_replace("\n", " ",$response);
		$response = str_replace("\t", " ",$response);
		$response = str_replace("  ", " ",$response);
		$response = str_replace("  ", " ",$response);
		        
		
		if(strstr($response, '<h1>Not Found</h1>')){			
			$this->_options[settings][lang] = 'en';		
			if($_SERVER[HTTPS]){
				$url  = 'https://profitquery-a.akamaihd.net/lib/plugins/lang/'.$this->_options[settings][lang].'.aio.plugin.json';        
			}else{
				$url  = 'http://profitquery-a.akamaihd.net/lib/plugins/lang/'.$this->_options[settings][lang].'.aio.plugin.json';        
			}
			
			$response = file_get_contents($url);
			$response = str_replace("\r", " ",$response);
			$response = str_replace("\n", " ",$response);
			$response = str_replace("\t", " ",$response);
			$response = str_replace("  ", " ",$response);
			$response = str_replace("  ", " ",$response);			
		}		
		$this->_dictionary = json_decode(trim($response), true);				
	}
	
	function getArrayProOptionsSelected($array){
		$ret = array();		
		foreach((array)$array as $tool => $data){			
			$proOptions = $this->_getToolProOptions($tool);
			if($proOptions){
				$ret[$tool][name] = $data[name];			
				$ret[$tool][pro_options] = $proOptions;
			}
		}		
		return $ret;
	}
	
	function _proceedSharingSocnetError($array){
		$ret = $temp = array();
		foreach((array)$array as $k => $v){
			if($v){
				if(!$temp[$v]){
					$temp[$v] = 1;
				}else{
					$ret[$v] = 1;
				}
			}
		}
		return $ret;
	}
	
	function getSharingSocnetErrorArray(){
		$ret = array();
		foreach((array)$this->_options as $toolName => $array){
			if(isset($this->_options[$toolName]['socnet_with_pos'])){				
				$ret[$toolName]['error_socnet'] = $this->_proceedSharingSocnetError($this->_options[$toolName]['socnet_with_pos']);
			}
			if(isset($this->_options[$toolName]['thank']['socnet_with_pos'])){
				$ret[$toolName]['thank']['error_socnet'] = $this->_proceedSharingSocnetError($this->_options[$toolName]['thank']['socnet_with_pos']);
			}
		}
		return $ret;
	}
	
	function PQsanitizeAllData($array){
		if(is_array($array)){
			if($array){
				foreach((array)$array as $k => $v){
					if(is_array($v)){
						$array[$k] = $this->PQsanitizeAllData($v);
					}else{
						if($k == 'providerForm'){
							$array[$k] = $v;
						}else if($k == 'url' || $k == 'iframe_src' || $k == 'header_image_src' || $k == 'background_image_src'){
							$array[$k] = addslashes($v);
						}else{
							$array[$k] = sanitize_text_field($v);
						}
						
					}
				}
			}
		}
		return $array;
	}
	
	 /**
     * Manages the WP settings page
     * 
     * @return null
     */
    function ProfitquerySmartWidgetsOptions()
    {
        if (!current_user_can('manage_options')) {
            wp_die(
                __('You do not have sufficient permissions to access this page.')
            );
        }
		
		date_default_timezone_set('Europe/Berlin');
		echo "			
			<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
			
			<link href='https://fonts.googleapis.com/css?family=Aguafina+Script|Amita|Anonymous+Pro|Arimo|Averia+Gruesa+Libre|Berkshire+Swash|Bigelow+Rules|Bitter|Black+Ops+One|Butterfly+Kids|Cantata+One|Chango|Chela+One|Chicle|Clicker+Script|Codystar|Combo|Comfortaa|Croissant+One|Dynalight|Eagle+Lake|Elsie|Elsie+Swash+Caps|Emilys+Candy|Esteban|Euphoria+Script|Exo|Gruppo|Great+Vibes|Happy+Monkey|Julius+Sans+One|Kavoon|Lato|Lancelot|Life+Savers|Lilita+One|Lily+Script+One|Limelight|Lobster|McLaren|Metamorphous|Milonga|Modak|Molle:400italic|Mystery+Quest|Neuton|Nosifer|Oleo+Script+Swash+Caps|Open+Sans|Oregano|Overlock|Oswald|PT+Sans|PT+Sans+Narrow|Passion+One|Pathway+Gothic+One|Petit+Formal+Script|Playball|Playfair+Display|Poiret+One|Pragati+Narrow|Princess+Sofia|Prosto+One|Purple+Purse|Quattrocento|Quintessential|Racing+Sans+One|Righteous|Roboto|Rubik+Mono+One|Sacramento|Shadows+Into+Light+Two|Signika|Simonetta|Sniglet|Source+Sans+Pro|Stint+Ultra+Expanded|Teko|Text+Me+One|Titan+One|Trocchi|Ubuntu|Unica+One|Varela|Viga|Warnes&subset=latin-ext' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Open+Sans|Roboto|PT+Sans|Open+Sans+Condensed:300|Ubuntu|PT+Sans+Narrow|Noto+Serif|Play|Fira+Sans|EB+Garamond|Comfortaa|Tinos|Didact+Gothic|Jura|PT+Serif+Caption|Ubuntu+Mono|Oranienbaum|PT+Mono|Andika|Cousine|Poiret+One|Lobster|Playfair+Display|Exo+2|Philosopher|Russo+One|Marmelad|Neucha|Prosto+One|Yeseva+One|Ruslan+Display|Rubik+Mono+One|Rubik+One&subset=cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Lateef|Amiri:400,700|Scheherazade&subset=arabic' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Lateef|Amiri|Scheherazade|Noto+Sans|Hind|Glegoo|Halant|Ek+Mukta|Rajdhani|Khula|Khand|Karma|Teko|Cambay|Palanquin+Dark|Yantramanav|Biryani|Sarpanch|Martel|Kurale|Laila|Rozha+One|Dekko|Vesper+Libre|Amita|Tillana|Arya|Pragati+Narrow|Inknut+Antiqua|Sahitya&subset=devanagari' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Roboto|Open+Sans|Roboto+Condensed|Ubuntu|Open+Sans+Condensed:300|Arimo|Play|Fira+Sans|Tinos|Comfortaa|Cardo|Advent+Pro|Caudex|Cousine|Ubuntu+Mono|GFS+Didot|Fira+Mono|Nova+Mono|GFS+Neohellenic|Didact+Gothic&subset=greek' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Arimo|Tinos|Alef|Cousine&subset=hebrew' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Hanuman|Khmer|Suwannaphum|Koulen|Bokor|Battambang|Angkor|Nokora|Siemreap|Dangrek|Freehand|Moul|Kantumruy|Taprom|Bayon&subset=khmer' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Catamaran&subset=tamil' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Timmana|Mallanna|Ramabhadra|Ramaraja|Mandali|Gurajada|Lakki+Reddy|NTR|Suranna|Tenali+Ramakrishna|Sree+Krushnadevaraya|Peddana&subset=telugu' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Itim|Chonburi&subset=thai' rel='stylesheet' type='text/css'>
			<link href='https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Source+Sans+Pro|Open+Sans+Condensed:300|Arimo|Lobster|Noto+Sans|EB+Garamond|Noticia+Text|Patrick+Hand|Tinos|Alegreya+Sans+SC|Chonburi|Itim|Patrick+Hand+SC&subset=vietnamese' rel='stylesheet' type='text/css'>
			<link rel='stylesheet'  href='".plugins_url()."/".PROFITQUERY_SMART_WIDGETS_PLUGIN_NAME."/".PROFITQUERY_SMART_WIDGETS_ADMIN_CSS_PATH."pq_wordpress_v4.1.1.css' type='text/css' media='all' />			
			<style>
				.pq_aliceblue {background:aliceblue;}
				.pq_antiquewhite {background:antiquewhite;}
				.pq_aqua {background:aqua;}
				.pq_aqmarine {background:aquamarine;}
				.pq_azure {background:azure;}
				.pq_beige {background:beige;}
				.pq_bisque {background:bisque;}
				.pq_black {background:black;}
				.pq_blanchedalmond {background:blanchedalmond;}
				.pq_blue {background:blue;}
				.pq_blviolet {background:blueviolet;}
				.pq_brown {background:brown;}
				.pq_burlywood {background:burlywood;}
				.pq_cadetblue {background:cadetblue;}
				.pq_chartreuse {background:chartreuse;}
				.pq_chocolate {background:chocolate;}
				.pq_coral {background:coral;}
				.pq_cornflowerblue {background:cornflowerblue;}
				.pq_cornsilk {background:cornsilk;}
				.pq_crimson {background:crimson;}
				.pq_cyan {background:cyan;}
				.pq_darkblue {background:darkblue;}
				.pq_darkcyan {background:darkcyan;}
				.pq_darkgoldenrod {background:darkgoldenrod;}
				.pq_darkgreen {background:darkgreen;}
				.pq_darkgrey {background:darkgrey;}
				.pq_darkkhaki {background:darkkhaki;}
				.pq_darkmagenta {background:darkmagenta;}
				.pq_darkolivegreen {background:darkolivegreen;}
				.pq_darkorange {background:darkorange;}
				.pq_darkorchid {background:darkorchid;}
				.pq_darkred {background:darkred;}
				.pq_darksalmon {background:darksalmon;}
				.pq_darkseagreen {background:darkseagreen;}
				.pq_darkslateblue {background:darkslateblue;}
				.pq_darkslategrey {background:darkslategrey;}
				.pq_darkturquoise {background:darkturquoise;}
				.pq_darkviolet {background:darkviolet;}
				.pq_deeppink {background:deeppink;}
				.pq_deepskyblue {background:deepskyblue;}
				.pq_dimgrey {background:dimgrey;}
				.pq_dodgerblue {background:dodgerblue;}
				.pq_firebrick {background:firebrick;}
				.pq_floralwhite {background:floralwhite;}
				.pq_forestgreen {background:forestgreen;}
				.pq_fuchsia {background:fuchsia;}
				.pq_gainsboro {background:gainsboro;}
				.pq_ghostwhite {background:ghostwhite;}
				.pq_gold {background:gold;}
				.pq_gldenrod {background:goldenrod;}
				.pq_green {background:green;}
				.pq_grnyellow {background:greenyellow;}
				.pq_grey {background:grey;}
				.pq_honeydew {background:honeydew;}
				.pq_hotpink {background:hotpink;}
				.pq_indianred {background:indianred;}
				.pq_indigo {background:indigo;}
				.pq_ivory {background:ivory;}
				.pq_khaki {background:khaki;}
				.pq_lavender {background:lavender;}
				.pq_lvndrblush {background:lavenderblush;}
				.pq_lawngreen {background:lawngreen;}
				.pq_lemonchiffon {background:lemonchiffon;}
				.pq_lightblue {background:lightblue;}
				.pq_lightcoral {background:lightcoral;}
				.pq_lightcyan {background:lightcyan;}
				.pq_lightgoldenrodyellow {background:lightgoldenrodyellow;}
				.pq_lightgreen {background:lightgreen;}
				.pq_lightgrey {background:lightgrey;}
				.pq_lightpink {background:lightpink;}
				.pq_lightsalmon {background:lightsalmon;}
				.pq_lightseagreen {background:lightseagreen;}
				.pq_lightskyblue {background:lightskyblue;}
				.pq_lightslategrey {background:lightslategrey;}
				.pq_lightsteelblue {background:lightsteelblue;}
				.pq_lightyellow {background:lightyellow;}
				.pq_lime {background:lime;}
				.pq_lmgreen {background:limegreen;}
				.pq_linen {background:linen;}
				.pq_magenta {background:magenta;}
				.pq_maroon {background:maroon;}
				.pq_mediumaquamarine {background:mediumaquamarine;}
				.pq_mediumblue {background:mediumblue;}
				.pq_mediumorchid {background:mediumorchid;}
				.pq_mediumpurple {background:mediumpurple;}
				.pq_mediumseagreen {background:mediumseagreen;}
				.pq_mediumslateblue {background:mediumslateblue;}
				.pq_mediumspringgreen {background:mediumspringgreen;}
				.pq_mediumturquoise {background:mediumturquoise;}
				.pq_mediumvioletred {background:mediumvioletred;}
				.pq_midnightblue {background:midnightblue;}
				.pq_mintcream {background:mintcream;}
				.pq_mistyrose {background:mistyrose;}
				.pq_moccasin {background:moccasin;}
				.pq_navajowhite {background:navajowhite;}
				.pq_navy {background:navy;}
				.pq_oldlace {background:oldlace;}
				.pq_olive {background:olive;}
				.pq_olvdrab {background:olivedrab;}
				.pq_orange {background:orange;}
				.pq_orngred {background:orangered;}
				.pq_orchid {background:orchid;}
				.pq_palegoldenrod {background:palegoldenrod;}
				.pq_palegreen {background:palegreen;}
				.pq_paleturquoise {background:paleturquoise;}
				.pq_palevioletred {background:palevioletred;}
				.pq_papayawhip {background:papayawhip;}
				.pq_peachpuff {background:peachpuff;}
				.pq_peru {background:peru;}
				.pq_pink {background:pink;}
				.pq_plum {background:plum;}
				.pq_powderblue {background:powderblue;}
				.pq_purple {background:purple;}
				.pq_rebeccapurple {background:rebeccapurple;}
				.pq_red {background:red;}
				.pq_rosybrown {background:rosybrown;}
				.pq_royalblue {background:royalblue;}
				.pq_saddlebrown {background:saddlebrown;}
				.pq_salmon {background:salmon;}
				.pq_sandybrown {background:sandybrown;}
				.pq_seagreen {background:seagreen;}
				.pq_seashell {background:seashell;}
				.pq_sienna {background:sienna;}
				.pq_silver {background:silver;}
				.pq_skyblue {background:skyblue;}
				.pq_slateblue {background:slateblue;}
				.pq_slategrey {background:slategrey;}
				.pq_snow {background:snow;}
				.pq_springgreen {background:springgreen;}
				.pq_steelblue {background:steelblue;}
				.pq_tan {background:tan;}
				.pq_teal {background:teal;}
				.pq_thistle {background:thistle;}
				.pq_tomato {background:tomato;}
				.pq_turquoise {background:turquoise;}
				.pq_violet {background:violet;}
				.pq_wheat {background:wheat;}
				.pq_white {background:white;}
				.pq_whtsmoke {background:whitesmoke;}
				.pq_yellow {background:yellow;}
				.pq_yllwgreen {background:yellowgreen;}
			</style>
		<noscript>				
				<p>Please enable JavaScript in your browser.</p>				
		</noscript>
		";
				
		//clear_all
		if($_GET[act] == 'clear_XXXSdf'){
			$this->_options = array();			
			update_option('profitquery', $this->_options);			
		}
		
		if($_POST[action] == 'i_agree'){
			$this->_options = array();
			$this->_options[i_agree] = 1;			
			update_option('profitquery', $this->_options);
			$this->activatePluginVersion();
			$this->setDefaultProductData();
		}
		
		$canStart = 0;
		if((int)$this->_options[i_agree] == 1){
			$canStart = 1;
		}
		
		//Start Main Plugin
		if($canStart)
		{
			$successSendEmail = 0;
			if($_POST[action] == 'needHelp'){
				if(trim($_POST[message])){
					$message .= "Massge:".$_POST[message]."<br>";
					$message .= "From:".stripslashes($this->_options[settings][email])."<br>";
					$message .= "Domain:".$this->getFullDomain()."<br>";
					wp_mail( 'support@profitquery.com', '[WP]'.$this->getFullDomain().' Support', $message );
					$successSendEmail = 1;
				}
			}
			
			//sanitize
			if($_POST){
				$_POST = $this->PQsanitizeAllData($_POST);				
			}			
			/*********************SAVE***********************/
			if($_POST[action] == "setLang"){
				$this->_options[settings][lang] = $_POST[lang];
				update_option('profitquery', $this->_options);
			}
			
			if($_POST[action] == 'settingsSave'){
				$this->_options[settings] = $_POST[settings];
				update_option('profitquery', $this->_options);
			}
			
			/*******************Get APi Key******************/
			if($_GET[apiKey]){
				$this->_options[settings][apiKey] = sanitize_text_field($_GET[apiKey]);
				update_option('profitquery', $this->_options);
			}
					
			/**********************SAVE TOOLS**************************/
			$this->parseThemes();
			
			if($_POST[action] == 'sharingSidebarSave'){					
				$this->_options[sharingSidebar] = $_POST[sharingSidebar];
				$this->_options[sharingSidebar][enable] = 'on';
				
				if(!$this->_options[sharingSidebar][displayRules][display_on_main_page]) $this->_options[sharingSidebar][displayRules][display_on_main_page] = '';
				if(!$this->_options[sharingSidebar][displayRules][work_on_mobile]) $this->_options[sharingSidebar][displayRules][work_on_mobile] = '';
					
				
				$this->_options[sharingSidebar][themeClass] = $this->_themes[sharingSidebar][$_POST[sharingSidebar][theme]][addClass];
				if($_POST[sharingSidebar][sendMailWindow][enable] == 'on' || $_POST[sharingSidebar][sendMail] == 'on'){
					$this->_options[sharingSidebar][sendMailWindow][enable] = $this->_options[sharingSidebar][sendMail] = 'on';
				}else{
					$this->_options[sharingSidebar][sendMailWindow][enable] = $this->_options[sharingSidebar][sendMail] = '';
				}				
				update_option('profitquery', $this->_options);
			}
			
			//IMAGE SHARER
			if($_POST[action] == 'imageSharerSave'){			
				$this->_options[imageSharer] = $_POST[imageSharer];
				$this->_options[imageSharer][enable] = 'on';
				
				if(!$this->_options[imageSharer][displayRules][display_on_main_page]) $this->_options[imageSharer][displayRules][display_on_main_page] = '';
				if(!$this->_options[imageSharer][displayRules][work_on_mobile]) $this->_options[imageSharer][displayRules][work_on_mobile] = '';
				
				$this->_options[imageSharer][themeClass] = $this->_themes[imageSharer][$_POST[imageSharer][theme]][addClass];
				if($_POST[imageSharer][sendMailWindow][enable] == 'on' || $_POST[imageSharer][sendMail] == 'on'){
					$this->_options[imageSharer][sendMailWindow][enable] = $this->_options[imageSharer][sendMail] = 'on';
				}else{
					$this->_options[imageSharer][sendMailWindow][enable] = $this->_options[imageSharer][sendMail] = '';
				}
				update_option('profitquery', $this->_options);			
			}

			//SHARING POPUP
			if($_POST[action] == 'sharingPopupSave'){
				$this->_options[sharingPopup] = $_POST[sharingPopup];
				$this->_options[sharingPopup][enable] = 'on';
				
				if(!$this->_options[sharingPopup][displayRules][display_on_main_page]) $this->_options[sharingPopup][displayRules][display_on_main_page] = '';
				if(!$this->_options[sharingPopup][displayRules][work_on_mobile]) $this->_options[sharingPopup][displayRules][work_on_mobile] = '';
				
				$this->_options[sharingPopup][themeClass] = $this->_themes[sharingPopup][$_POST[sharingPopup][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//sharingBar
			if($_POST[action] == 'sharingBarSave'){
				$this->_options[sharingBar] = $_POST[sharingBar];
				$this->_options[sharingBar][enable] = 'on';
				
				if(!$this->_options[sharingBar][displayRules][display_on_main_page]) $this->_options[sharingBar][displayRules][display_on_main_page] = '';
				if(!$this->_options[sharingBar][displayRules][work_on_mobile]) $this->_options[sharingBar][displayRules][work_on_mobile] = '';
				
				$this->_options[sharingBar][themeClass] = $this->_themes[sharingBar][$_POST[sharingBar][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//sharingFloating
			if($_POST[action] == 'sharingFloatingSave'){
				$this->_options[sharingFloating] = $_POST[sharingFloating];
				$this->_options[sharingFloating][enable] = 'on';
				
				if(!$this->_options[sharingFloating][displayRules][display_on_main_page]) $this->_options[sharingFloating][displayRules][display_on_main_page] = '';
				if(!$this->_options[sharingFloating][displayRules][work_on_mobile]) $this->_options[sharingFloating][displayRules][work_on_mobile] = '';
				
				$this->_options[sharingFloating][themeClass] = $this->_themes[sharingFloating][$_POST[sharingFloating][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}
			
			$providerRedirectByError = '';
			
			//emailListBuilderPopup
			if($_POST[action] == 'emailListBuilderPopupSave'){
				$this->_options[emailListBuilderPopup] = $_POST[emailListBuilderPopup];
				
				if(!$this->_options[emailListBuilderPopup][displayRules][display_on_main_page]) $this->_options[emailListBuilderPopup][displayRules][display_on_main_page] = '';
				if(!$this->_options[emailListBuilderPopup][displayRules][work_on_mobile]) $this->_options[emailListBuilderPopup][displayRules][work_on_mobile] = '';
				
				if($_POST[emailListBuilderPopup][providerForm]){
					$data = $this->parseSubscribeProviderForm(trim($_POST[emailListBuilderPopup][provider]), $_POST[emailListBuilderPopup][providerForm]);
					if((int)$data[is_error]){
						$this->_options[emailListBuilderPopup][enable] = '';
						$providerRedirectByError = 'emailListBuilderPopup';
						$this->_options[emailListBuilderPopup][providerOption][is_error] = 1;
					}else{					
						$this->_options[emailListBuilderPopup][providerOption] = $data;
						$this->_options[emailListBuilderPopup][themeClass] = $this->_themes[emailListBuilderPopup][$_POST[emailListBuilderPopup][theme]][addClass];
						$this->_options[emailListBuilderPopup][enable] = 'on';
					}
				}else{
					$providerRedirectByError = 'emailListBuilderPopup';
					$this->_options[emailListBuilderPopup][enable] = '';
					$this->_options[emailListBuilderPopup][providerOption][is_error] = 1;
				}			
				update_option('profitquery', $this->_options);			
			}

			//emailListBuilderBar
			if($_POST[action] == 'emailListBuilderBarSave'){
				$this->_options[emailListBuilderBar] = $_POST[emailListBuilderBar];				
				if(!$this->_options[emailListBuilderBar][displayRules][display_on_main_page]) $this->_options[emailListBuilderBar][displayRules][display_on_main_page] = '';
				if(!$this->_options[emailListBuilderBar][displayRules][work_on_mobile]) $this->_options[emailListBuilderBar][displayRules][work_on_mobile] = '';
				
				if($_POST[emailListBuilderBar][providerForm]){
					$data = $this->parseSubscribeProviderForm(trim($_POST[emailListBuilderBar][provider]), $_POST[emailListBuilderBar][providerForm]);
					if((int)$data[is_error]){
						$this->_options[emailListBuilderBar][enable] = '';
						$providerRedirectByError = 'emailListBuilderBar';
						$this->_options[emailListBuilderBar][providerOption][is_error] = 1;
					}else{					
						$this->_options[emailListBuilderBar][providerOption] = $data;
						$this->_options[emailListBuilderBar][themeClass] = $this->_themes[emailListBuilderBar][$_POST[emailListBuilderBar][theme]][addClass];
						$this->_options[emailListBuilderBar][enable] = 'on';
					}
				}else{
					$providerRedirectByError = 'emailListBuilderBar';
					$this->_options[emailListBuilderBar][enable] = '';
					$this->_options[emailListBuilderBar][providerOption][is_error] = 1;
				}			
				update_option('profitquery', $this->_options);				
			}

			//emailListBuilderFloating
			if($_POST[action] == 'emailListBuilderFloatingSave'){
				$this->_options[emailListBuilderFloating] = $_POST[emailListBuilderFloating];
				
				if(!$this->_options[emailListBuilderFloating][displayRules][display_on_main_page]) $this->_options[emailListBuilderFloating][displayRules][display_on_main_page] = '';
				if(!$this->_options[emailListBuilderFloating][displayRules][work_on_mobile]) $this->_options[emailListBuilderFloating][displayRules][work_on_mobile] = '';
				
				if($_POST[emailListBuilderFloating][providerForm]){
					
					$data = $this->parseSubscribeProviderForm(trim($_POST[emailListBuilderFloating][provider]), $_POST[emailListBuilderFloating][providerForm]);
					if((int)$data[is_error]){
						$this->_options[emailListBuilderFloating][enable] = '';
						$providerRedirectByError = 'emailListBuilderFloating';
						$this->_options[emailListBuilderFloating][providerOption][is_error] = 1;
					}else{					
						$this->_options[emailListBuilderFloating][providerOption] = $data;
						$this->_options[emailListBuilderFloating][themeClass] = $this->_themes[emailListBuilderFloating][$_POST[emailListBuilderFloating][theme]][addClass];
						$this->_options[emailListBuilderFloating][enable] = 'on';
					}
				}else{
					$providerRedirectByError = 'emailListBuilderFloating';
					$this->_options[emailListBuilderFloating][enable] = '';
					$this->_options[emailListBuilderFloating][providerOption][is_error] = 1;
				}			
				update_option('profitquery', $this->_options);			
			}

			//contactUsPopup
			if($_POST[action] == 'contactUsPopupSave'){
				$this->_options[contactUsPopup] = $_POST[contactUsPopup];
				$this->_options[contactUsPopup][enable] = 'on';
				
				if(!$this->_options[contactUsPopup][displayRules][display_on_main_page]) $this->_options[contactUsPopup][displayRules][display_on_main_page] = '';
				if(!$this->_options[contactUsPopup][displayRules][work_on_mobile]) $this->_options[contactUsPopup][displayRules][work_on_mobile] = '';
				
				$this->_options[contactUsPopup][themeClass] = $this->_themes[contactUsPopup][$_POST[contactUsPopup][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//contactUsFloating
			if($_POST[action] == 'contactUsFloatingSave'){
				$this->_options[contactUsFloating] = $_POST[contactUsFloating];
				$this->_options[contactUsFloating][enable] = 'on';
				
				if(!$this->_options[contactUsFloating][displayRules][display_on_main_page]) $this->_options[contactUsFloating][displayRules][display_on_main_page] = '';
				if(!$this->_options[contactUsFloating][displayRules][work_on_mobile]) $this->_options[contactUsFloating][displayRules][work_on_mobile] = '';
				
				$this->_options[contactUsFloating][themeClass] = $this->_themes[contactUsFloating][$_POST[contactUsFloating][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//promotePopup
			if($_POST[action] == 'promotePopupSave'){
				$this->_options[promotePopup] = $_POST[promotePopup];
				$this->_options[promotePopup][enable] = 'on';
				
				if(!$this->_options[promotePopup][displayRules][display_on_main_page]) $this->_options[promotePopup][displayRules][display_on_main_page] = '';
				if(!$this->_options[promotePopup][displayRules][work_on_mobile]) $this->_options[promotePopup][displayRules][work_on_mobile] = '';
				
				$this->_options[promotePopup][themeClass] = $this->_themes[promotePopup][$_POST[promotePopup][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//promoteBar
			if($_POST[action] == 'promoteBarSave'){
				$this->_options[promoteBar] = $_POST[promoteBar];
				$this->_options[promoteBar][enable] = 'on';
				
				if(!$this->_options[promoteBar][displayRules][display_on_main_page]) $this->_options[promoteBar][displayRules][display_on_main_page] = '';
				if(!$this->_options[promoteBar][displayRules][work_on_mobile]) $this->_options[promoteBar][displayRules][work_on_mobile] = '';
				
				$this->_options[promoteBar][themeClass] = $this->_themes[promoteBar][$_POST[promoteBar][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//promoteFloating
			if($_POST[action] == 'promoteFloatingSave'){
				$this->_options[promoteFloating] = $_POST[promoteFloating];
				$this->_options[promoteFloating][enable] = 'on';
				
				if(!$this->_options[promoteFloating][displayRules][display_on_main_page]) $this->_options[promoteFloating][displayRules][display_on_main_page] = '';
				if(!$this->_options[promoteFloating][displayRules][work_on_mobile]) $this->_options[promoteFloating][displayRules][work_on_mobile] = '';
				
				$this->_options[promoteFloating][themeClass] = $this->_themes[promoteFloating][$_POST[promoteFloating][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//callMePopup
			if($_POST[action] == 'callMePopupSave'){
				$this->_options[callMePopup] = $_POST[callMePopup];
				$this->_options[callMePopup][enable] = 'on';
				
				if(!$this->_options[callMePopup][displayRules][display_on_main_page]) $this->_options[callMePopup][displayRules][display_on_main_page] = '';
				if(!$this->_options[callMePopup][displayRules][work_on_mobile]) $this->_options[callMePopup][displayRules][work_on_mobile] = '';
				
				$this->_options[callMePopup][themeClass] = $this->_themes[callMePopup][$_POST[callMePopup][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//callMeFloating
			if($_POST[action] == 'callMeFloatingSave'){
				$this->_options[callMeFloating] = $_POST[callMeFloating];
				$this->_options[callMeFloating][enable] = 'on';
				
				if(!$this->_options[callMeFloating][displayRules][display_on_main_page]) $this->_options[callMeFloating][displayRules][display_on_main_page] = '';
				if(!$this->_options[callMeFloating][displayRules][work_on_mobile]) $this->_options[callMeFloating][displayRules][work_on_mobile] = '';
				
				$this->_options[callMeFloating][themeClass] = $this->_themes[callMeFloating][$_POST[callMeFloating][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//followPopup
			if($_POST[action] == 'followPopupSave'){
				$this->_options[followPopup] = $_POST[followPopup];
				$this->_options[followPopup][enable] = 'on';
				
				if(!$this->_options[followPopup][displayRules][display_on_main_page]) $this->_options[followPopup][displayRules][display_on_main_page] = '';
				if(!$this->_options[followPopup][displayRules][work_on_mobile]) $this->_options[followPopup][displayRules][work_on_mobile] = '';
				
				$this->_options[followPopup][themeClass] = $this->_themes[followPopup][$_POST[followPopup][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//followBar
			if($_POST[action] == 'followBarSave'){
				$this->_options[followBar] = $_POST[followBar];
				$this->_options[followBar][enable] = 'on';
				
				if(!$this->_options[followBar][displayRules][display_on_main_page]) $this->_options[followBar][displayRules][display_on_main_page] = '';
				if(!$this->_options[followBar][displayRules][work_on_mobile]) $this->_options[followBar][displayRules][work_on_mobile] = '';
				
				$this->_options[followBar][themeClass] = $this->_themes[followBar][$_POST[followBar][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//followFloating
			if($_POST[action] == 'followFloatingSave'){
				$this->_options[followFloating] = $_POST[followFloating];
				$this->_options[followFloating][enable] = 'on';
				
				if(!$this->_options[followFloating][displayRules][display_on_main_page]) $this->_options[followFloating][displayRules][display_on_main_page] = '';
				if(!$this->_options[followFloating][displayRules][work_on_mobile]) $this->_options[followFloating][displayRules][work_on_mobile] = '';
				
				$this->_options[followFloating][themeClass] = $this->_themes[followFloating][$_POST[followFloating][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//socialPopup
			if($_POST[action] == 'socialPopupSave'){
				$this->_options[socialPopup] = $_POST[socialPopup];
				$this->_options[socialPopup][enable] = 'on';
				
				if(!$this->_options[socialPopup][displayRules][display_on_main_page]) $this->_options[socialPopup][displayRules][display_on_main_page] = '';
				if(!$this->_options[socialPopup][displayRules][work_on_mobile]) $this->_options[socialPopup][displayRules][work_on_mobile] = '';
				
				$this->_options[socialPopup][themeClass] = $this->_themes[socialPopup][$_POST[socialPopup][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}

			//youtubePopup
			if($_POST[action] == 'youtubePopupSave'){
				$this->_options[youtubePopup] = $_POST[youtubePopup];
				
				$this->_options[youtubePopup][iframe_src] = addslashes(str_replace('watch?v=', 'embed/', stripslashes($this->_options[youtubePopup][iframe_src])));				
				
				$this->_options[youtubePopup][enable] = 'on';
				
				if(!$this->_options[youtubePopup][displayRules][display_on_main_page]) $this->_options[youtubePopup][displayRules][display_on_main_page] = '';
				if(!$this->_options[youtubePopup][displayRules][work_on_mobile]) $this->_options[youtubePopup][displayRules][work_on_mobile] = '';
				
				$this->_options[youtubePopup][themeClass] = $this->_themes[youtubePopup][$_POST[youtubePopup][theme]][addClass];
				update_option('profitquery', $this->_options);			
			}
					
			
						
			$this->parseProLoader();
			$this->getPluginSettings();
			$this->getDictionary();
			
			/***********************DISABLE OPTION*****************************/
			$needOpenActivatePopup = 0;
			$needOpenProInfoPopup = 0;
			if($_POST[action] == 'disableOption'){			
				if($this->_options[$_POST[tool]]){
					if($_POST[optionName] == 'whitelabel'){
						$this->_options[$_POST[tool]][whitelabel] = '';
					}elseif($_POST[optionName] == 'mail'){
						$this->_options[$_POST[tool]][sendMail] = '';
						$this->_options[$_POST[tool]][sendMailWindow][enable] = '';
					}elseif($_POST[optionName] == 'themes' && $this->_themes[$_POST[tool]][$_POST[themeName]]){
						if($this->_default_themes[$_POST[tool]] != $_POST[themeName]){
							$this->_options[$_POST[tool]][theme] = $this->_default_themes[$_POST[tool]];
						}
					}
					update_option('profitquery', $this->_options);
				}
				if($_POST[currentLocation] == 'activate_popup'){
					$needOpenActivatePopup = 1;
				}
							
				if($_POST[currentLocation] == 'pro_info'){
					$needOpenProInfoPopup = 1;
				}
			}									
			
			
			
			//disable Tools
			if($_POST[action] == 'disable' && $_POST[toolsID]){
				$this->_options[$_POST[toolsID]][enable] = 0;
				update_option('profitquery', $this->_options);	
			}				
			
									
			/*Check for dublicate sharingSidebar socnet_with_pos*/		
			$sharingSocnetErrorArray = $this->getSharingSocnetErrorArray();		
			
			$toolsArray = $this->getToolsArray();		
			//printr($toolsArray);
			//printr($this->_themes);
			//die();
			
			$positionArray = $this->getArrayPosition($toolsArray);		
			$selectedProOptions = $this->getArrayProOptionsSelected($toolsArray);		
		//	printr($selectedProOptions);
		//	die();
			$defaultToolPosition = $this->getDefaultToolPosition();						
	?>
			
	<script>
	
	
	function enableBlockByCheckboxClick(t, id){
		if(t){
			document.getElementById(id).style.display = 'block';
		}else{
			document.getElementById(id).style.display = 'none';
		}
	}

	function enableAdditionalBlock(val, id_to_block, arr){
		for(var i in arr){
			try{		
				document.getElementById(id_to_block+arr[i]).style.display = 'none';		
			}catch(err){};
		}
		try{			
			if(val != "") document.getElementById(id_to_block+val).style.display = 'block';
		}catch(err){};
	}

	function PQdeserialize (data) {
		data === "" && (data = '""');
		try {
			eval("var __tempPQRetData");
			eval("__tempPQRetData=" + data)
		} catch (b) {}
		return __tempPQRetData;
	}


	function clearClassName(str){
		return str.split(' ')[0];
	}

	function setEHBlockActive(id, type){
		try{
			if(!document.getElementById(id+'_eventHandler_'+type).disabled){
				document.getElementById(id+'_eventHandler_'+type).checked = true;	
			}
		}catch(err){};
	}

	function changeStatusToRadio(partId, current, arr){
		for(var i in arr){		
			try{document.getElementById(partId+arr[i]).checked = false;}catch(err){};			
		}	
		try{
			document.getElementById(partId+current).checked = true;			
			}catch(err){};	
	}

	var PQThemes = PQdeserialize('<?php echo json_encode($this->_themes);?>');
	var PQDefaultPosition = PQdeserialize('<?php echo json_encode($defaultToolPosition);?>');
	var PQToolsPosition = PQdeserialize('<?php echo json_encode($positionArray);?>');
	var PQPluginSettings = PQdeserialize('<?php echo json_encode($this->_plugin_settings);?>');
	var PQPluginDict = PQdeserialize('<?php echo json_encode($this->_dictionary);?>');	
	var PQPositionValues = {
			"BAR_TOP":'pq_top',
			"BAR_BOTTOM":'pq_bottom',
			"SIDE_LEFT_TOP":'pq_left pq_top',
			"SIDE_LEFT_MIDDLE":'pq_left pq_middle',
			"SIDE_LEFT_BOTTOM":'pq_left pq_bottom',
			"SIDE_RIGHT_TOP":'pq_right pq_top',
			"SIDE_RIGHT_MIDDLE":"pq_right pq_middle",
			"SIDE_RIGHT_BOTTOM":'pq_right pq_bottom',
			"CENTER":'',
			"FLOATING_LEFT_TOP":'pq_left pq_top',
			"FLOATING_LEFT_BOTTOM":'pq_left pq_bottom',
			"FLOATING_RIGHT_TOP":'pq_right pq_top',
			"FLOATING_RIGHT_BOTTOM":'pq_right pq_bottom'
	}
	//*************************************CHECKOUT SCRIPTS
	//TO EXTEND
	function changeExtendCheckoutPeriod(){
		try{
			var val = document.getElementById('PQExtendPopup_ExtendPeriod').value;	
			var summ = 0;
			var discounts = PQPluginSettings.discounts[val]||0;
			var arrayWorkedID = [];
			var ifMailAlreadyChecked = false;
			var mailSave = 0;
			var save = 0;
			var allSumm = 0;
			for(var i in document.getElementById('PQtoExtendForm').getElementsByTagName('input')){
				var obj = document.getElementById('PQtoExtendForm').getElementsByTagName('input');
				try{
					if(obj[i].id.indexOf('PQToolsToExtend_') == 0 && !arrayWorkedID[obj[i].id]){
						if(obj[i].checked){
							arrayWorkedID[obj[i].id]=1;
							//if mail
							if(obj[i].getAttribute('data-pq-mail-options') == 1){
								if(!ifMailAlreadyChecked){
									summ += Number(obj[i].getAttribute('data-pq-price'));
									ifMailAlreadyChecked = true;
								}else{
									mailSave += Number(obj[i].getAttribute('data-pq-price'));
								}
							}else{
								summ += Number(obj[i].getAttribute('data-pq-price'));
							}						
						}											
					}
				}catch(err){};
			}
			
			allSumm = Math.round((summ+mailSave)*val);
			summ = Math.round((summ-summ*discounts/100)*val);
			save = allSumm-summ;
			
			
			if(discounts > 0 || save > 0){
				if(discounts > 0) document.getElementById('PQExtendPopup_Discount_Text').innerHTML = val+' '+PQPluginDict.other.months+' - '+PQPluginDict.other.discount+' '+discounts+'%'; else document.getElementById('PQExtendPopup_Discount_Text').innerHTML = val+' '+PQPluginDict.other.month;
				if(save > 0) document.getElementById('PQExtendPopup_Save_Text').innerHTML = PQPluginDict.other.you_save+' $'+save; else document.getElementById('PQExtendPopup_Save_Text').innerHTML = '';
			}else{
				document.getElementById('PQExtendPopup_Discount_Text').innerHTML = val+' '+PQPluginDict.other.month;
				document.getElementById('PQExtendPopup_Save_Text').innerHTML = '';
			}
			document.getElementById('PQExtendPopup_Price').innerHTML = '$'+summ;
			document.getElementById('PQtoExtendForm_Submit').value = PQPluginDict.action.pay+' $'+summ;
			if(summ > 0){
				document.getElementById('PQtoExtendForm_Submit').disabled=false;
			}else{
				document.getElementById('PQtoExtendForm_Submit').disabled=true;
			}
		}catch(err){};
	}
	//TO CHECKOUT
	function changeActivateCheckoutPeriod(){
		try{
			var val = document.getElementById('PQActivatePopup_Period').value;	
			var summ = 0;
			var discounts = PQPluginSettings.discounts[val]||0;
			var arrayWorkedID = [];
			var ifMailAlreadyChecked = false;
			var mailSave = 0;
			var save = 0;
			var allSumm = 0;
			for(var i in document.getElementById('PQtoActivateForm').getElementsByTagName('input')){
				var obj = document.getElementById('PQtoActivateForm').getElementsByTagName('input');							
				try{
					if(obj[i].id.indexOf('PQToolsToActivate_') == 0 && !arrayWorkedID[obj[i].id]){																
						arrayWorkedID[obj[i].id]=1;
						//if mail
						if(obj[i].getAttribute('data-pq-mail-options') == 1){
							if(!ifMailAlreadyChecked){
								summ += Number(obj[i].getAttribute('data-pq-price'));
								ifMailAlreadyChecked = true;
							}else{
								mailSave += Number(obj[i].getAttribute('data-pq-price'));
							}
						}else{
							summ += Number(obj[i].getAttribute('data-pq-price'));
						}
					}
				}catch(err){};							
			}		
			
			allSumm = Math.round((summ+mailSave)*val);
			summ = Math.round((summ-summ*discounts/100)*val);
			save = allSumm-summ;
			
			
			
			if(discounts > 0 || save > 0){
				if(discounts > 0) document.getElementById('PQActivatePopup_Discount_Text').innerHTML = val+' '+PQPluginDict.other.months+' - '+PQPluginDict.other.discount+' '+discounts+'%'; else document.getElementById('PQExtendPopup_Discount_Text').innerHTML = val+' '+PQPluginDict.other.month;
				if(save > 0) document.getElementById('PQActivatePopup_Save_Text').innerHTML = PQPluginDict.other.you_save+' $'+save; else document.getElementById('PQActivatePopup_Save_Text').innerHTML = '';
			}else{
				document.getElementById('PQActivatePopup_Discount_Text').innerHTML = val+' '+PQPluginDict.other.month;
				document.getElementById('PQActivatePopup_Save_Text').innerHTML = '';
			}
			document.getElementById('PQActivatePopup_Price').innerHTML = '= $'+summ;
			document.getElementById('PQActivateForm_Submit').value = PQPluginDict.action.pay+' $'+summ;
			if(summ > 0){
				document.getElementById('PQActivateForm_Submit').disabled=false;
			}else{
				document.getElementById('PQActivateForm_Submit').disabled=true;
			}
		}catch(err){};
	}






	//**************************************PreviewThemeScript
	function getNextElemFromObject(obj, current){
		var ret = {};
		var _flagFindCurrent = false;
		for(var i in obj){
			if(_flagFindCurrent){
				return {theme:i, obj:obj[i]};
			}else if(i == current){
				_flagFindCurrent = true;
			}
		}
		return 0;
	}

	function getPrevElemFromObject(obj, current){
		var ret = 0;
		var _flagFindCurrent = false;
		for(var i in obj){				
			if(i == current){
				return ret;
			}else{
				ret = {theme:i, obj:obj[i]};
			}
		}
		return 0;
	}

	function getPrevArrow(themeObjectId, theme){
		if(getPrevElemFromObject(PQThemes[themeObjectId], theme) == 0){
			document.getElementById('Popup_ThemePreview_Prev').innerHTML = '';			
		}else{
			document.getElementById('Popup_ThemePreview_Prev').innerHTML = '<img src="<?php echo plugins_url('i/scroll_left.png', __FILE__);?>" />';
		}
	}

	function getNextArrow(themeObjectId, theme){
		if(getNextElemFromObject(PQThemes[themeObjectId], theme) == 0){
			document.getElementById('Popup_ThemePreview_Next').innerHTML = '';			
		}else{
			document.getElementById('Popup_ThemePreview_Next').innerHTML = '<img src="<?php echo plugins_url('i/scroll_right.png', __FILE__);?>" />';
		}
	}

	function setThemePreviewData(obj){
		document.getElementById('PQThemeSelectPreviewPopup').style.display = 'block';
		document.getElementById('Popup_ThemePreview_big_src').src = obj.src
		document.getElementById(document.getElementById('PQThemeSelectCurrentToolID').value+'_CurrentThemeForPreview').value = obj.theme
		document.getElementById('Popup_ThemePreview_Title').innerHTML = obj.title
		document.getElementById('Popup_ThemePreview_Description').innerHTML = obj.description
		document.getElementById('Popup_ThemePreview_DIV').className = clearClassName(document.getElementById('Popup_ThemePreview_DIV').className);	
		if(Number(obj.price)>0){
			document.getElementById('Popup_ThemePreview_DIV').className += ' pq_premium';
		}
	}

	function previewThemeNext(){	
		var currentTheme = document.getElementById(document.getElementById('PQThemeSelectCurrentToolID').value+'_CurrentThemeForPreview').value;	
		var themeObjectId = document.getElementById('PQThemeSelectCurrentID').value;
		var nextObj = getNextElemFromObject(PQThemes[themeObjectId], currentTheme);	
		if(nextObj != 0){
			setThemePreviewData({src:nextObj.obj.preview_image_big, title:nextObj.obj.title, description:nextObj.obj.description, theme:nextObj.theme, price:nextObj.obj.price});
			
			document.getElementById(document.getElementById('PQThemeSelectCurrentToolID').value+'_CurrentThemeForPreview').value = nextObj.theme;
			
			getNextArrow(themeObjectId, nextObj.theme);
			document.getElementById('Popup_ThemePreview_Prev').innerHTML = '<img src="<?php echo plugins_url('i/scroll_left.png', __FILE__);?>" />';
		}
	}



	function previewThemePrev(){
		var currentTheme = document.getElementById(document.getElementById('PQThemeSelectCurrentToolID').value+'_CurrentThemeForPreview').value;
		var themeObjectId = document.getElementById('PQThemeSelectCurrentID').value;
		var prevObject = getPrevElemFromObject(PQThemes[themeObjectId], currentTheme);	
		if(prevObject != 0){
			setThemePreviewData({src:prevObject.obj.preview_image_big, title:prevObject.obj.title, description:prevObject.obj.description, theme:prevObject.theme, price:prevObject.obj.price});
			document.getElementById(document.getElementById('PQThemeSelectCurrentToolID').value+'_CurrentThemeForPreview').value = prevObject.theme;
			
			getPrevArrow(themeObjectId, prevObject.theme)
			document.getElementById('Popup_ThemePreview_Next').innerHTML = '<img src="<?php echo plugins_url('i/scroll_right.png', __FILE__);?>" />';
		}
	}

	function previewThemeSelect(tool, themeObjectId, theme){
		document.getElementById('PQThemeSelectCurrentToolID').value=tool;
		document.getElementById('PQThemeSelectCurrentID').value=themeObjectId;
		setThemePreviewData({src:PQThemes[themeObjectId][theme].preview_image_big, title:PQThemes[themeObjectId][theme].title, description:PQThemes[themeObjectId][theme].description, theme:theme, price:PQThemes[themeObjectId][theme].price});
		
		getPrevArrow(themeObjectId, theme);
		getNextArrow(themeObjectId, theme);
	}
	
	function checkTheme(id, toolID){
		try{
			var toolTheme = document.getElementById(toolID+'_Current_Theme').value;		
			if(typeof PQThemes[id][toolTheme] != 'undefined'){
				if(Number(PQThemes[id][toolTheme].price) == 0){
					lockProOptionFromForm(id, toolID);
				}else{
					unlockProOptionFromForm(id, toolID);
				}
			}else{
				lockProOptionFromForm(id, toolID);
			}
		}catch(err){};
	}
	
	function lockProOptionFromForm(id, toolID){		
		try{document.getElementById(id+'_border_color').disabled=true;}catch(err){};
		try{document.getElementById(id+'_border_type').disabled=true;}catch(err){};
		try{document.getElementById(id+'_border_depth').disabled=true;}catch(err){};
		try{document.getElementById(id+'_b_image').disabled=true;}catch(err){};		
		
		try{document.getElementById(id+'_thank_border_color').disabled=true;}catch(err){};
		try{document.getElementById(id+'_thank_border_type').disabled=true;}catch(err){};
		try{document.getElementById(id+'_thank_border_depth').disabled=true;}catch(err){};
		try{document.getElementById(id+'_thank_b_image').disabled=true;}catch(err){};		
		
		try{document.getElementById(id+'_sendMailWindow_border_color').disabled=true;}catch(err){};
		try{document.getElementById(id+'_sendMailWindow_border_type').disabled=true;}catch(err){};
		try{document.getElementById(id+'_sendMailWindow_border_depth').disabled=true;}catch(err){};
		try{document.getElementById(id+'_sendMailWindow_b_image').disabled=true;}catch(err){};		
	}
	
	function unlockProOptionFromForm(id, toolID){		
		try{document.getElementById(id+'_border_color').disabled=false;}catch(err){};
		try{document.getElementById(id+'_border_type').disabled=false;}catch(err){};
		try{document.getElementById(id+'_border_depth').disabled=false;}catch(err){};
		try{document.getElementById(id+'_b_image').disabled=false;}catch(err){};		
		
		try{document.getElementById(id+'_thank_border_color').disabled=false;}catch(err){};
		try{document.getElementById(id+'_thank_border_type').disabled=false;}catch(err){};
		try{document.getElementById(id+'_thank_border_depth').disabled=false;}catch(err){};
		try{document.getElementById(id+'_thank_b_image').disabled=false;}catch(err){};		
		
		try{document.getElementById(id+'_sendMailWindow_border_color').disabled=false;}catch(err){};
		try{document.getElementById(id+'_sendMailWindow_border_type').disabled=false;}catch(err){};
		try{document.getElementById(id+'_sendMailWindow_border_depth').disabled=false;}catch(err){};
		try{document.getElementById(id+'_sendMailWindow_b_image').disabled=false;}catch(err){};		
	}
	
	function disableAllDesignOpt(id){
		try{document.getElementById(id+'_b_image').value='';}catch(err){};
		try{document.getElementById(id+'_h_image').value='';}catch(err){};
		try{document.getElementById(id+'_typeWindow').value='';}catch(err){};
		try{document.getElementById(id+'_background').value='';}catch(err){};
		try{document.getElementById(id+'_second_background').value='';}catch(err){};
		try{document.getElementById(id+'_overlay').value='';}catch(err){};
		try{document.getElementById(id+'_button_text_color').value='';}catch(err){};
		try{document.getElementById(id+'_button_color').value='';}catch(err){};
		try{document.getElementById(id+'_icon_design').value='';}catch(err){};
		try{document.getElementById(id+'_icon_form').value='';}catch(err){};
		try{document.getElementById(id+'_icon_size').value='';}catch(err){};
		try{document.getElementById(id+'_icon_space').value='';}catch(err){};
		try{document.getElementById(id+'_icon_shadow').value='';}catch(err){};
		try{document.getElementById(id+'_icon_position').value='';}catch(err){};
		try{document.getElementById(id+'_icon_animation').value='';}catch(err){};		
		try{document.getElementById(id+'_head_size').value='';}catch(err){};
		try{document.getElementById(id+'_text_size').value='';}catch(err){};
		try{document.getElementById(id+'_head_color').value='';}catch(err){};
		try{document.getElementById(id+'_text_color').value='';}catch(err){};
		try{document.getElementById(id+'_border_color').value='';}catch(err){};
		try{document.getElementById(id+'_popup_form').value='';}catch(err){};
		try{document.getElementById(id+'_bookmark_background').value='';}catch(err){};
		try{document.getElementById(id+'_bookmark_text_color').value='';}catch(err){};
		try{document.getElementById(id+'_font_size').value='';}catch(err){};
		try{document.getElementById(id+'_border_type').value='';}catch(err){};
		try{document.getElementById(id+'_border_depth').value='';}catch(err){};
		try{document.getElementById(id+'_button_font_size').value='';}catch(err){};
		try{document.getElementById(id+'_head_font').value='';}catch(err){};
		try{document.getElementById(id+'_text_font').value='';}catch(err){};
		try{document.getElementById(id+'_button_font').value='';}catch(err){};
		try{document.getElementById(id+'_animation_close_icon').value='';}catch(err){};
		try{document.getElementById(id+'_close_icon_type').value='';}catch(err){};
		try{document.getElementById(id+'_close_icon_color').value='';}catch(err){};		
	}
	
	function setValueTOFormElements(id, obj, toolID){		
		disableAllDesignOpt(id);
		for(var i in obj){		
			try{
				if(obj[i] == 'pq_checked'){
					document.getElementById(id+i).checked = true;
				}else{
					document.getElementById(id+i).value=obj[i];
				}
			}catch(err){}		
		}
	}	
	function setThemeSettings(toolID, id){	
		var theme = document.getElementById(toolID+'_Current_Theme').value = document.getElementById(toolID+'_CurrentThemeForPreview').value;	
		if(theme && PQThemes[id][theme].designOptions){						
			setValueTOFormElements(id, PQThemes[id][theme].designOptions, toolID);
			if(Number(PQThemes[id][theme].price) == 0){
				lockProOptionFromForm(id, toolID);
			}else{
				unlockProOptionFromForm(id, toolID);
			}
		}
	}

	/**********************************************************************/

	function selectSubscribeProvider(partId, val){
		var url = '';
		var title = '';
		if(val == 'aweber'){
			url = 'aweber.html';
			title = 'Aweber';
		}
		
		if(val == 'mailchimp'){
			url = 'mailchimp.html';
			title = 'Mailchimp';
		}
		
		
		if(val == 'newsletter2go'){
			url = 'newsletter2go.html';
			title = 'Newsletter2go';
		}
	
		if(val == 'madmini'){
			url = 'madmini.html';
			title = 'Mad Mini';
		}
		if(val == 'acampaign'){
			url = 'acampaign.html';
			title = 'Active Campaign';
		}
		if(val == 'getresponse'){
			url = 'getresponse.html';
			title = 'GetResponse';
		}
		if(val == 'klickmail'){
			url = 'klickmail.html';
			title = 'Klick Mmail';
		}	
		
		try{
			if(url){
				document.getElementById(partId+'_provier_help_url').href="http://profitquery.com/"+url;
				document.getElementById(partId+'_provier_title').innerHTML = title;
			}
		}catch(err){};	
	}


	function _proceedDisableAllTools(name){
		try{
			for (var i = 1; i <= 10; i++) {			
				document.getElementById('SelectTool_'+name+'_'+i).style.display = 'none';
			}
		}catch(err){};
	}

	function enableToolsByName(name){
		try{
			for (var i = 1; i <= 10; i++) {
				document.getElementById('SelectTool_'+name+'_'+i).style.display = 'inline-block';
			}
		}catch(err){};
	}

	function enableAllTools(){
		enableToolsByName('Sharing');
		enableToolsByName('EmailListBuilder');
		enableToolsByName('ContactUs');
		enableToolsByName('Promote');
		enableToolsByName('CallMe');
		enableToolsByName('Follow');
		enableToolsByName('Any');
		enableToolsByName('Other');
	}

	function disableAllTools(){
		_proceedDisableAllTools('Sharing');
		_proceedDisableAllTools('EmailListBuilder');
		_proceedDisableAllTools('ContactUs');
		_proceedDisableAllTools('Promote');
		_proceedDisableAllTools('CallMe');
		_proceedDisableAllTools('Follow');
		_proceedDisableAllTools('Any');	
		_proceedDisableAllTools('Other');
	}
	function sortTools(val){
		if(val == ''){		
			enableAllTools();
		}else{
			disableAllTools();
			enableToolsByName(val);
		}
	}



	function disableCurrentTool(id, name, src){
		document.getElementById('PQ_Disable_Popup').style.display = 'block';
		var title = '<?php echo $this->_dictionary[disableDialog][title];?>';	
		
		document.getElementById('PQ_Disable_Popup_title').innerHTML = title.replace(new RegExp("(\%s)",'g'),name);	
		document.getElementById('PQ_Disable_Popup_toolsID').value = id;	
		document.getElementById('PQ_Disable_Popup_src').src = src;	
	}



	/**********************************PREVIEW SCRIPT**********************************************/
	
	function getPreviewByActiveForm(toolID, id){
		var thank = 0;
		var email = 0;
		var tool = 0;
		var func = '';
		try{if(document.getElementById(id+'_DesignToolSwitch_main').className.indexOf('pq_active') != -1) tool = 1;}catch(err){};
		try{if(document.getElementById(id+'_DesignToolSwitch_thank').className.indexOf('pq_active') != -1) thank = 1;}catch(err){};
		try{if(document.getElementById(id+'_DesignToolSwitch_email').className.indexOf('pq_active') != -1) email = 1;}catch(err){};
		if(tool == 1){
			func = toolID+'Preview()';
		}
		if(email == 1){
			func = toolID+'sendMailWindowPreview("'+id+'")';
		}
		if(thank == 1){
			func = toolID+'thankPreview("'+id+'")';
		}
		if(func) eval(func);							
	}					
	
	function thankPreview(id, desktop, whitelabel){		
		var design = '';
		
		design += ' pq_animated '+document.getElementById(id+'_thank_animation').value+' '+document.getElementById(id+'_thank_typeWindow').value+' '+document.getElementById(id+'_thank_popup_form').value+' '+document.getElementById(id+'_thank_second_background').value;
		design += ' '+document.getElementById(id+'_thank_background').value+' '+document.getElementById(id+'_thank_head_font').value+' '+document.getElementById(id+'_thank_head_size').value;
		design += ' '+document.getElementById(id+'_thank_head_color').value+' '+document.getElementById(id+'_thank_text_font').value+' '+document.getElementById(id+'_thank_font_size').value+' '+document.getElementById(id+'_thank_text_color').value;		
		design += ' '+document.getElementById(id+'_thank_close_icon_type').value;
		design += ' '+document.getElementById(id+'_thank_close_icon_color').value;
		design += ' '+document.getElementById(id+'_thank_border_type').value+' '+document.getElementById(id+'_thank_border_depth').value;
		design += ' '+document.getElementById(id+'_thank_border_color').value;
		
		var overlay = document.getElementById(id+'_thank_overlay').value||'';
		var close_animation = document.getElementById(id+'_thank_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById(id+'_thank_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById(id+'_thank_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById(id+'_thank_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById(id+'_thank_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById(id+'_thank_title').value)||'';		
	
										
		//ICONS			
		design += ' '+document.getElementById(id+'_thank_icon_design').value||'';
		design += ' '+document.getElementById(id+'_thank_icon_form').value||'';
		design += ' '+document.getElementById(id+'_thank_icon_size').value||'';
		design += ' '+document.getElementById(id+'_thank_icon_space').value||'';
		var icon_animation = document.getElementById(id+'_thank_icon_animation').value||'';
		
		//socnet
		var socnetIcons = '';
		var socnet_type = '';
		if(document.getElementById(id+'_thank_socnet_block_type').value != ''){
			if(document.getElementById(id+'_thank_socnet_block_type').value == 'follow'){
				socnet_type = 'follow';
				if(document.getElementById(id+'_thank_follow_icon_FB').value) socnetIcons += 'FB|';
				if(document.getElementById(id+'_thank_follow_icon_TW').value) socnetIcons += 'TW|';
				if(document.getElementById(id+'_thank_follow_icon_GP').value) socnetIcons += 'GP|';
				if(document.getElementById(id+'_thank_follow_icon_PI').value) socnetIcons += 'PI|';
				if(document.getElementById(id+'_thank_follow_icon_YT').value) socnetIcons += 'YT|';
				if(document.getElementById(id+'_thank_follow_icon_OD').value) socnetIcons += 'OD|';
				if(document.getElementById(id+'_thank_follow_icon_IG').value) socnetIcons += 'IG|';
				if(document.getElementById(id+'_thank_follow_icon_RSS').value) socnetIcons += 'RSS|';
			}
			if(document.getElementById(id+'_thank_socnet_block_type').value == 'share'){				
				socnet_type = 'share';
				for(var i=0; i<=9; i++){
					if(document.getElementById(id+'_thank_sharing_icon_'+i).value != ''){
						socnetIcons += document.getElementById(id+'_thank_sharing_icon_'+i).value+'|';
					}
				}
			}
		}
	
		//BUTTON
		var button_text = '';
		var button_action = '';
		var button_url = '';
		if(document.getElementById(id+'_thank_buttonBlock_type').value=='redirect'){
			button_action = 'redirect';
			button_url = encodeURIComponent(document.getElementById(id+'_thank_buttonBlock_url_address').value);
			design += ' '+document.getElementById(id+'_thank_buttonBlock_button_font').value+' '+document.getElementById(id+'_thank_buttonBlock_button_font_size').value+' '+document.getElementById(id+'_thank_buttonBlock_button_text_color').value+' '+document.getElementById(id+'_thank_buttonBlock_button_color').value
			var button_text = encodeURIComponent(document.getElementById(id+'_thank_buttonBlock_button_text').value)||'';
		}
		
		if(document.getElementById(id+'_thank_enable').checked ){
			if(desktop == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=thank&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&icon_animation='+icon_animation+'&socnet='+socnetIcons+'&socnet_type='+socnet_type+'&button_action='+button_action+'&button_text='+button_text+'&button_url='+button_url;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=thank&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&icon_animation='+icon_animation+'&socnet='+socnetIcons+'&socnet_type='+socnet_type+'&button_action='+button_action+'&button_text='+button_text+'&button_url='+button_url;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		}else{			
			document.getElementById('PQPreviewID').src = 'about:blank';
			document.getElementById('PQPreviewID').className='';
			document.getElementById('PQIframePreviewBlock').className='frame';
		}			
	}
	
	
	function sharingSidebarthankPreview(){
		var desktopView = 0;		
		if(document.getElementById('PQSharingSidebar_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQSharingSidebar_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		if(document.getElementById('sharingSidebar_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		thankPreview('sharingSidebar', desktopView, whitelabel);		
	}
	function imageSharerthankPreview(){
		var desktopView = 0;		
		if(document.getElementById('PQImageSharer_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQImageSharer_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		if(document.getElementById('imageSharer_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		thankPreview('imageSharer', desktopView, whitelabel);		
	}
	function sharingPopupthankPreview(){
		var desktopView = 0;		
		if(document.getElementById('PQSharingPopup_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQSharingPopup_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		if(document.getElementById('sharingPopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		thankPreview('sharingPopup', desktopView, whitelabel);		
	}
	function sharingBarthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQSharingBar_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQSharingBar_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			if(document.getElementById('sharingBar_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('sharingBar', desktopView, whitelabel);		
		}
	function sharingFloatingthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQSharingFloating_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQSharingFloating_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			if(document.getElementById('sharingFloating_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('sharingFloating', desktopView, whitelabel);		
		}
	function emailListBuilderPopupthankPreview(){			
			var desktopView = 0;		
			if(document.getElementById('PQEmailListBuilderPopup_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQEmailListBuilderPopup_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			if(document.getElementById('emailListBuilderPopup_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('emailListBuilderPopup', desktopView, whitelabel);		
		}
	function emailListBuilderBarthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQEmailListBuilderBar_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQEmailListBuilderBar_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('emailListBuilderBar_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('emailListBuilderBar', desktopView, whitelabel);		
		}
	function emailListBuilderFloatingthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQEmailListBuilderFloating_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQEmailListBuilderFloating_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('emailListBuilderFloating_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('emailListBuilderFloating', desktopView, whitelabel);		
		}
	function contactUsPopupthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQcontactUsPopup_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQcontactUsPopup_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('contactUsPopup_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('contactUsPopup', desktopView, whitelabel);		
		}
	function contactUsFloatingthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQcontactUsFloating_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQcontactUsFloating_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('contactUsFloating_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('contactUsFloating', desktopView, whitelabel);		
		}
	function promotePopupthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQPromotePopup_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQPromotePopup_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('promotePopup_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('promotePopup', desktopView, whitelabel);		
		}
	function promoteBarthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQPromoteBar_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQPromoteBar_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('promoteBar_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('promoteBar', desktopView, whitelabel);		
		}
	function promoteFloatingthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQPromoteFloating_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQPromoteFloating_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('promoteFloating_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('promoteFloating', desktopView, whitelabel);		
		}
	function callMePopupthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQCallMePopup_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQCallMePopup_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('callMePopup_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('callMePopup', desktopView, whitelabel);		
		}
	function callMeFloatingthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQCallMeFloating_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQCallMeFloating_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('callMeFloating_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('callMeFloating', desktopView, whitelabel);		
		}
	function followPopupthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQFollowPopup_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQFollowPopup_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('followPopup_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('followPopup', desktopView, whitelabel);		
		}
	function followBarthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQFollowBar_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQFollowBar_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('followBar_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('followBar', desktopView, whitelabel);		
		}
	function followFloatingthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQFollowFloating_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQFollowFloating_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('followFloating_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('followFloating', desktopView, whitelabel);		
		}
	function socialPopupthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQSocialPopup_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQSocialPopup_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('socialPopup_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('socialPopup', desktopView, whitelabel);		
		}
	function youtubePopupthankPreview(){
			var desktopView = 0;		
			if(document.getElementById('PQYoutubePopup_thank_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
			if(document.getElementById('PQYoutubePopup_thank_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
			
			if(document.getElementById('youtubePopup_whitelabel').checked){
				var whitelabel = 1;
			}else{
				var whitelabel = 0;
			}
			
			thankPreview('youtubePopup', desktopView, whitelabel);		
		}
	
	function imageSharersendMailWindowPreview(){
		var desktopView = 0;		
		if(document.getElementById('PQImageSharer_email_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQImageSharer_email_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = '';
		
		design += ' pq_animated '+document.getElementById('imageSharer_sendMailWindow_animation').value+' '+document.getElementById('imageSharer_sendMailWindow_typeWindow').value+' '+document.getElementById('imageSharer_sendMailWindow_popup_form').value+' '+document.getElementById('imageSharer_sendMailWindow_second_background').value;
		design += ' '+document.getElementById('imageSharer_sendMailWindow_background').value+' '+document.getElementById('imageSharer_sendMailWindow_head_font').value+' '+document.getElementById('imageSharer_sendMailWindow_head_size').value;
		design += ' '+document.getElementById('imageSharer_sendMailWindow_head_color').value+' '+document.getElementById('imageSharer_sendMailWindow_text_font').value+' '+document.getElementById('imageSharer_sendMailWindow_font_size').value+' '+document.getElementById('imageSharer_sendMailWindow_text_color').value;
		design += ' '+document.getElementById('imageSharer_sendMailWindow_button_font').value+' '+document.getElementById('imageSharer_sendMailWindow_button_text_color').value+' '+document.getElementById('imageSharer_sendMailWindow_button_color').value
		design += ' '+document.getElementById('imageSharer_sendMailWindow_button_font_size').value;
		design += ' '+document.getElementById('imageSharer_sendMailWindow_close_icon_type').value;
		design += ' '+document.getElementById('imageSharer_sendMailWindow_close_icon_color').value;		
		
		var overlay = document.getElementById('imageSharer_sendMailWindow_overlay').value||'';
		var close_animation = document.getElementById('imageSharer_sendMailWindow_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_title').value)||'';
		var button_text = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_button_text').value)||'';
		var name_text = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_text_name').value)||'';
		var email_text = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_text_email').value)||'';
		var subject_text = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_text_subject').value)||'';
		var message_text = encodeURIComponent(document.getElementById('imageSharer_sendMailWindow_text_message').value)||'';
		
		
		if(document.getElementById('imageSharer_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		if(document.getElementById('imageSharer_email_enable').checked){
			if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sendMail&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&name_text='+name_text+'&email_text='+email_text+'&subject_text='+subject_text+'&message_text='+message_text;
					document.getElementById('PQPreviewID').className='pq_desktop_preview';
					document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{			
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sendMail&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&name_text='+name_text+'&email_text='+email_text+'&subject_text='+subject_text+'&message_text='+message_text;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		}else{
			document.getElementById('PQPreviewID').src = 'about:blank';
			document.getElementById('PQPreviewID').className='';
			document.getElementById('PQIframePreviewBlock').className='frame';
		}
	}
	
	function sharingSidebarsendMailWindowPreview(){
		var desktopView = 0;		
		if(document.getElementById('PQSharingSidebar_email_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQSharingSidebar_email_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = '';
		
		design += ' pq_animated '+document.getElementById('sharingSidebar_sendMailWindow_animation').value+' '+document.getElementById('sharingSidebar_sendMailWindow_typeWindow').value+' '+document.getElementById('sharingSidebar_sendMailWindow_popup_form').value+' '+document.getElementById('sharingSidebar_sendMailWindow_second_background').value;
		design += ' '+document.getElementById('sharingSidebar_sendMailWindow_background').value+' '+document.getElementById('sharingSidebar_sendMailWindow_head_font').value+' '+document.getElementById('sharingSidebar_sendMailWindow_head_size').value;
		design += ' '+document.getElementById('sharingSidebar_sendMailWindow_head_color').value+' '+document.getElementById('sharingSidebar_sendMailWindow_text_font').value+' '+document.getElementById('sharingSidebar_sendMailWindow_font_size').value+' '+document.getElementById('sharingSidebar_sendMailWindow_text_color').value;
		design += ' '+document.getElementById('sharingSidebar_sendMailWindow_button_font').value+' '+document.getElementById('sharingSidebar_sendMailWindow_button_text_color').value+' '+document.getElementById('sharingSidebar_sendMailWindow_button_color').value
		design += ' '+document.getElementById('sharingSidebar_sendMailWindow_button_font_size').value;
		design += ' '+document.getElementById('sharingSidebar_sendMailWindow_close_icon_type').value;
		design += ' '+document.getElementById('sharingSidebar_sendMailWindow_close_icon_color').value;		
		
		var overlay = document.getElementById('sharingSidebar_sendMailWindow_overlay').value||'';
		var close_animation = document.getElementById('sharingSidebar_sendMailWindow_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_h_image').value)||'';		
		var sub_title = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_title').value)||'';
		var button_text = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_button_text').value)||'';
		var name_text = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_text_name').value)||'';
		var email_text = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_text_email').value)||'';
		var subject_text = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_text_subject').value)||'';
		var message_text = encodeURIComponent(document.getElementById('sharingSidebar_sendMailWindow_text_message').value)||'';
		
		
		if(document.getElementById('sharingSidebar_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		if(document.getElementById('sharingSidebar_email_enable').checked){
			if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sendMail&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&name_text='+name_text+'&email_text='+email_text+'&subject_text='+subject_text+'&message_text='+message_text;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{			
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sendMail&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&name_text='+name_text+'&email_text='+email_text+'&subject_text='+subject_text+'&message_text='+message_text;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		}else{
			document.getElementById('PQPreviewID').src = 'about:blank';
			document.getElementById('PQPreviewID').className='';
			document.getElementById('PQIframePreviewBlock').className='frame';
		}
	}
	
	function sharingSidebarPreview(){		
		var desktopView = 0;		
		if(document.getElementById('PQSharingSidebar_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQSharingSidebar_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;				
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQSharingSidebar_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQSharingSidebar_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['sharingSidebar'][theme].addClass||'';}catch(err){};
		
		design = position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('sharingSidebar_animation').value||'';		
		design += ' '+document.getElementById('sharingSidebar_icon_form').value||'';
		design += ' '+document.getElementById('sharingSidebar_icon_size').value||'';
		design += ' '+document.getElementById('sharingSidebar_icon_space').value||'';
		design += ' '+document.getElementById('sharingSidebar_mobile_type').value||'';
		design += ' '+document.getElementById('sharingSidebar_mobile_position').value||'';
		var icon_animation = document.getElementById('sharingSidebar_icon_animation').value||'';
		
		if(document.getElementById('sharingSidebar_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var counters = document.getElementById('sharingSidebar_counters').checked||'';
		var gallery_enable = document.getElementById('sharingSidebar_g_enable').checked||'';
		var mobile_title = encodeURIComponent(document.getElementById('sharingSidebar_m_title').value)||'';			
		var socnetIcons = '';
		for(var i=0; i<=9; i++){
			if(document.getElementById('sharingSidebar_sharing_icon_'+i).value != ''){
				socnetIcons += document.getElementById('sharingSidebar_sharing_icon_'+i).value+'|';
			}
		}
		
		var galleryDesign = '';
		galleryDesign += ' '+document.getElementById('sharingSidebar_galleryOption_head_font').value+' '+document.getElementById('sharingSidebar_galleryOption_head_size').value;
		galleryDesign += ' '+document.getElementById('sharingSidebar_galleryOption_button_font').value+' '+document.getElementById('sharingSidebar_galleryOption_button_font_size').value+' '+document.getElementById('sharingSidebar_galleryOption_button_text_color').value;
		galleryDesign += ' '+document.getElementById('sharingSidebar_galleryOption_button_color').value+' '+document.getElementById('sharingSidebar_galleryOption_background_color').value;
		galleryDesign += ' '+document.getElementById('sharingSidebar_galleryOption_button_font_size').value;
		
		var galleryTitle = document.getElementById('sharingSidebar_g_title').value;
		var galleryButtonText = document.getElementById('sharingSidebar_g_button_text').value;
		
		
		var moreDesign = theme+' '+themeAddClass;
		moreDesign += ' pq_animated '+document.getElementById('sharingSidebar_moreShareWindow_animation').value+' '+document.getElementById('sharingSidebar_moreShareWindow_typeWindow').value+' '+document.getElementById('sharingSidebar_moreShareWindow_popup_form').value;
		moreDesign += ' '+document.getElementById('sharingSidebar_moreShareWindow_background').value+' '+document.getElementById('sharingSidebar_moreShareWindow_head_font').value+' '+document.getElementById('sharingSidebar_moreShareWindow_head_size').value;
		moreDesign += ' '+document.getElementById('sharingSidebar_moreShareWindow_head_color').value+' '+document.getElementById('sharingSidebar_moreShareWindow_text_font').value+' '+document.getElementById('sharingSidebar_moreShareWindow_font_size').value;
		moreDesign += ' '+document.getElementById('sharingSidebar_moreShareWindow_text_color').value+' '+document.getElementById('sharingSidebar_moreShareWindow_close_icon_type').value;
		moreDesign += ' '+document.getElementById('sharingSidebar_moreShareWindow_close_icon_color').value;		
		
		var moreOverlay = document.getElementById('sharingSidebar_moreShareWindow_overlay').value||'';
		var closeAnimation = document.getElementById('sharingSidebar_moreShareWindow_animation_close_icon').value||'';
		var iconAnimation = document.getElementById('sharingSidebar_moreShareWindow_icon_animation').value||'';
		var closeText = encodeURIComponent(document.getElementById('sharingSidebar_moreShareWindow_close_text').value)||'';
		var headerImg = encodeURIComponent(document.getElementById('sharingSidebar_moreShareWindow_h_image').value)||'';
		var subTitle = encodeURIComponent(document.getElementById('sharingSidebar_moreShareWindow_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('sharingSidebar_moreShareWindow_title').value)||'';		
		
		
		if(desktopView == '1'){
			document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sharingSidebar&design='+design+'&whitelabel='+whitelabel+'&counters='+
					counters+'&gallery_enable='+gallery_enable+'&icon_animation='+icon_animation+'&socnet='+socnetIcons+'&more_design='+
					moreDesign+'&more_overlay='+moreOverlay+'&more_close_animation='+closeAnimation+'&more_close_text='+closeText+'&more_header_i='+
					headerImg+'&more_sub_title='+subTitle+'&more_title='+title+'&more_icon_animation='+iconAnimation+'&gallery_design='+
					galleryDesign+'&gallery_title='+galleryTitle+'&gallery_button_text='+galleryButtonText+'&mobile_title='+mobile_title;
			document.getElementById('PQPreviewID').className='pq_desktop_preview';
			document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';			
		}else{			
			document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sharingSidebar&design='+design+'&whitelabel='+whitelabel+'&counters='+
					counters+'&gallery_enable='+gallery_enable+'&icon_animation='+icon_animation+'&socnet='+socnetIcons+'&more_design='+
					moreDesign+'&more_overlay='+moreOverlay+'&more_close_animation='+closeAnimation+'&more_close_text='+closeText+'&more_header_i='+
					headerImg+'&more_sub_title='+subTitle+'&more_title='+title+'&more_icon_animation='+iconAnimation+'&gallery_design='+
					galleryDesign+'&gallery_title='+galleryTitle+'&gallery_button_text='+galleryButtonText+'&mobile_title='+mobile_title;
			document.getElementById('PQPreviewID').className='pq_mobile_preview';
			document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
		}
	}
	function imageSharerPreview(){
		var toolView = 0;
		if(document.getElementById('PQImageSharer_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQImageSharer_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;				
		var design = theme = themeAddClass = '';
		
		try{theme = document.getElementById('PQImageSharer_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['imageSharer'][theme].addClass||'';}catch(err){};
		
		
		design = theme+' '+themeAddClass;		
		design += ' '+document.getElementById('imageSharer_icon_form').value||'';
		design += ' '+document.getElementById('imageSharer_icon_size').value||'';
		design += ' '+document.getElementById('imageSharer_icon_shadow').value||'';
		design += ' '+document.getElementById('imageSharer_icon_space').value||'';
		design += ' '+document.getElementById('imageSharer_icon_position').value||'';
		var icon_animation = document.getElementById('imageSharer_icon_animation').value||'';
		
		if(document.getElementById('imageSharer_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		var socnetIcons = '';
		if(document.getElementById('imageSharer_provider_FB').checked) socnetIcons += 'FB|';
		if(document.getElementById('imageSharer_provider_TW').checked) socnetIcons += 'TW|';
		if(document.getElementById('imageSharer_provider_GP').checked) socnetIcons += 'GP|';
		if(document.getElementById('imageSharer_provider_PI').checked) socnetIcons += 'PI|';
		if(document.getElementById('imageSharer_provider_TR').checked) socnetIcons += 'TR|';
		if(document.getElementById('imageSharer_provider_VK').checked) socnetIcons += 'VK|';
		if(document.getElementById('imageSharer_provider_OD').checked) socnetIcons += 'OD|';
		if(document.getElementById('imageSharer_provider_WU').checked) socnetIcons += 'WU|';
		if(document.getElementById('imageSharer_provider_Mail').checked) socnetIcons += 'Mail|';	
		
		
		if(desktopView == '1'){
			document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=imageSharer&design='+design+'&whitelabel='+whitelabel+'&icon_animation='+
					icon_animation+'&socnet='+socnetIcons;
			document.getElementById('PQPreviewID').className='pq_desktop_preview';
			document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
		}else{			
			document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=imageSharer&design='+design+'&whitelabel='+whitelabel+'&icon_animation='+
					icon_animation+'&socnet='+socnetIcons;
			document.getElementById('PQPreviewID').className='pq_mobile_preview';
			document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
		}		
	}

	function sharingPopupPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQSharingPopup_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQSharingPopup_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQSharingPopup_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQSharingPopup_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['sharingPopup'][theme].addClass||'';}catch(err){};
		
		design = position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('sharingPopup_animation').value+' '+document.getElementById('sharingPopup_typeWindow').value+' '+document.getElementById('sharingPopup_popup_form').value+' '+document.getElementById('sharingPopup_second_background').value;
		design += ' '+document.getElementById('sharingPopup_background').value+' '+document.getElementById('sharingPopup_head_font').value+' '+document.getElementById('sharingPopup_head_size').value;
		design += ' '+document.getElementById('sharingPopup_head_color').value+' '+document.getElementById('sharingPopup_text_font').value+' '+document.getElementById('sharingPopup_font_size').value+' '+document.getElementById('sharingPopup_text_color').value;		
		design += ' '+document.getElementById('sharingPopup_close_icon_type').value;
		design += ' '+document.getElementById('sharingPopup_close_icon_color').value;
		design += ' '+document.getElementById('sharingPopup_border_type').value+' '+document.getElementById('sharingPopup_border_depth').value;
		design += ' '+document.getElementById('sharingPopup_border_color').value;
		
		var overlay = document.getElementById('sharingPopup_overlay').value||'';
		var close_animation = document.getElementById('sharingPopup_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('sharingPopup_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('sharingPopup_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('sharingPopup_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('sharingPopup_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('sharingPopup_title').value)||'';		
	
		
		//ICONS			
		design += ' '+document.getElementById('sharingPopup_icon_design').value||'';
		design += ' '+document.getElementById('sharingPopup_icon_form').value||'';
		design += ' '+document.getElementById('sharingPopup_icon_size').value||'';
		design += ' '+document.getElementById('sharingPopup_icon_space').value||'';
		var icon_animation = document.getElementById('sharingPopup_icon_animation').value||'';
		
		if(document.getElementById('sharingPopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		var socnetIcons = '';
		for(var i=0; i<=9; i++){
			if(document.getElementById('sharingPopup_sharing_icon_'+i).value != ''){
				socnetIcons += document.getElementById('sharingPopup_sharing_icon_'+i).value+'|';
			}
		}
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sharingPopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sharingPopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	function sharingBarPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQSharingBar_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQSharingBar_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQSharingBar_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQSharingBar_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['sharingBar'][theme].addClass||'';}catch(err){};
		
		design = 'pq_bar '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('sharingBar_animation').value+' '+document.getElementById('sharingBar_second_background').value;
		design += ' '+document.getElementById('sharingBar_background').value+' '+document.getElementById('sharingBar_head_font').value+' '+document.getElementById('sharingBar_head_size').value;
		design += ' '+document.getElementById('sharingBar_head_color').value;		
		design += ' '+document.getElementById('sharingBar_close_icon_type').value;
		design += ' '+document.getElementById('sharingBar_close_icon_color').value;
		design += ' '+document.getElementById('sharingBar_border_type').value+' '+document.getElementById('sharingBar_border_depth').value;
		design += ' '+document.getElementById('sharingBar_border_color').value+' '+document.getElementById('sharingBar_mobile_position').value;
		
				
		var close_animation = document.getElementById('sharingBar_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('sharingBar_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('sharingBar_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('sharingBar_b_image').value)||'';		
		var title = encodeURIComponent(document.getElementById('sharingBar_title').value)||'';
		var m_title = encodeURIComponent(document.getElementById('sharingBar_m_title').value)||'';
	
		
		//ICONS			
		design += ' '+document.getElementById('sharingBar_icon_design').value||'';
		design += ' '+document.getElementById('sharingBar_icon_form').value||'';
		design += ' '+document.getElementById('sharingBar_icon_size').value||'';
		design += ' '+document.getElementById('sharingBar_icon_space').value||'';
		var icon_animation = document.getElementById('sharingBar_icon_animation').value||'';
		
		if(document.getElementById('sharingBar_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		var socnetIcons = '';
		for(var i=0; i<=9; i++){
			if(document.getElementById('sharingBar_sharing_icon_'+i).value != ''){
				socnetIcons += document.getElementById('sharingBar_sharing_icon_'+i).value+'|';
			}
		}
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sharingBar&design='+design+'&whitelabel='+whitelabel+'&close_animation='+
					close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons+'&mobile_title='+m_title;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_bar_iframe pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sharingBar&design='+design+'&whitelabel='+whitelabel+'&close_animation='+
					close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons+'&mobile_title='+m_title;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_bar_iframe pq_mobile_preview';
			}
		
	}
	
	//sharingFloating
	function sharingFloatingPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQSharingFloating_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQSharingFloating_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQSharingFloating_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQSharingFloating_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['sharingFloating'][theme].addClass||'';}catch(err){};
		
		design = 'pq_fixed '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('sharingFloating_animation').value+' '+document.getElementById('sharingFloating_typeWindow').value+' '+document.getElementById('sharingFloating_popup_form').value+' '+document.getElementById('sharingFloating_second_background').value;
		design += ' '+document.getElementById('sharingFloating_background').value+' '+document.getElementById('sharingFloating_head_font').value+' '+document.getElementById('sharingFloating_head_size').value;
		design += ' '+document.getElementById('sharingFloating_head_color').value+' '+document.getElementById('sharingFloating_text_font').value+' '+document.getElementById('sharingFloating_font_size').value+' '+document.getElementById('sharingFloating_text_color').value;
		design += ' '+document.getElementById('sharingFloating_close_icon_type').value;
		design += ' '+document.getElementById('sharingFloating_close_icon_color').value;
		design += ' '+document.getElementById('sharingFloating_border_type').value+' '+document.getElementById('sharingFloating_border_depth').value;
		design += ' '+document.getElementById('sharingFloating_border_color').value;
		
		var overlay = document.getElementById('sharingFloating_overlay').value||'';
		var close_animation = document.getElementById('sharingFloating_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('sharingFloating_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('sharingFloating_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('sharingFloating_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('sharingFloating_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('sharingFloating_title').value)||'';		
	
		
		//ICONS			
		design += ' '+document.getElementById('sharingFloating_icon_design').value||'';
		design += ' '+document.getElementById('sharingFloating_icon_form').value||'';
		design += ' '+document.getElementById('sharingFloating_icon_size').value||'';
		design += ' '+document.getElementById('sharingFloating_icon_space').value||'';
		var icon_animation = document.getElementById('sharingFloating_icon_animation').value||'';
		
		if(document.getElementById('sharingFloating_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		var socnetIcons = '';
		for(var i=0; i<=9; i++){
			if(document.getElementById('sharingFloating_sharing_icon_'+i).value != ''){
				socnetIcons += document.getElementById('sharingFloating_sharing_icon_'+i).value+'|';
			}
		}
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sharingFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=sharingFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	
	function callMeFloatingPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQCallMeFloating_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQCallMeFloating_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQCallMeFloating_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQCallMeFloating_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['callMeFloating'][theme].addClass||'';}catch(err){};
		
		design = 'pq_fixed '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('callMeFloating_animation').value+' '+document.getElementById('callMeFloating_typeWindow').value+' '+document.getElementById('callMeFloating_popup_form').value+' '+document.getElementById('callMeFloating_second_background').value;
		design += ' '+document.getElementById('callMeFloating_background').value+' '+document.getElementById('callMeFloating_head_font').value+' '+document.getElementById('callMeFloating_head_size').value;
		design += ' '+document.getElementById('callMeFloating_head_color').value+' '+document.getElementById('callMeFloating_text_font').value+' '+document.getElementById('callMeFloating_font_size').value+' '+document.getElementById('callMeFloating_text_color').value;
		design += ' '+document.getElementById('callMeFloating_close_icon_type').value;
		design += ' '+document.getElementById('callMeFloating_close_icon_color').value;
		design += ' '+document.getElementById('callMeFloating_border_type').value+' '+document.getElementById('callMeFloating_border_depth').value;
		design += ' '+document.getElementById('callMeFloating_border_color').value;
		
		var overlay = document.getElementById('callMeFloating_overlay').value||'';
		var close_animation = document.getElementById('callMeFloating_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('callMeFloating_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('callMeFloating_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('callMeFloating_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('callMeFloating_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('callMeFloating_title').value)||'';
		
		if(document.getElementById('callMeFloating_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var phone_text = encodeURIComponent(document.getElementById('callMeFloating_text_phone').value)||'';
		var name_text = encodeURIComponent(document.getElementById('callMeFloating_text_name').value)||'';		
		
				
		design += ' '+document.getElementById('callMeFloating_button_font').value+' '+document.getElementById('callMeFloating_button_font_size').value+' '+document.getElementById('callMeFloating_button_text_color').value+' '+document.getElementById('callMeFloating_button_color').value
		var button_text = encodeURIComponent(document.getElementById('callMeFloating_button_text').value)||'';		
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=callMeFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&phone_text='+phone_text+'&name_text='+name_text;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=callMeFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&phone_text='+phone_text+'&name_text='+name_text;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	
	function socialPopupPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQSocialPopup_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQSocialPopup_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQSocialPopup_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQSocialPopup_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['socialPopup'][theme].addClass||'';}catch(err){};
		
		design = position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('socialPopup_animation').value+' '+document.getElementById('socialPopup_typeWindow').value+' '+document.getElementById('socialPopup_popup_form').value+' '+document.getElementById('socialPopup_second_background').value;
		design += ' '+document.getElementById('socialPopup_background').value+' '+document.getElementById('socialPopup_head_font').value+' '+document.getElementById('socialPopup_head_size').value;
		design += ' '+document.getElementById('socialPopup_head_color').value+' '+document.getElementById('socialPopup_text_font').value+' '+document.getElementById('socialPopup_font_size').value+' '+document.getElementById('socialPopup_text_color').value;
		design += ' '+document.getElementById('socialPopup_close_icon_type').value;
		design += ' '+document.getElementById('socialPopup_close_icon_color').value;
		design += ' '+document.getElementById('socialPopup_border_type').value+' '+document.getElementById('socialPopup_border_depth').value;
		design += ' '+document.getElementById('socialPopup_border_color').value;
		
		var overlay = document.getElementById('socialPopup_overlay').value||'';
		var close_animation = document.getElementById('socialPopup_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('socialPopup_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('socialPopup_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('socialPopup_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('socialPopup_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('socialPopup_title').value)||'';
		
		if(document.getElementById('socialPopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var iframe_src = encodeURIComponent(document.getElementById('socialPopup_iframe_src').value)||'';
				
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=socialPopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&iframe_src='+iframe_src;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=socialPopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&iframe_src='+iframe_src;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	function youtubePopupPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQYoutubePopup_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQYoutubePopup_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQYoutubePopup_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQYoutubePopup_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['youtubePopup'][theme].addClass||'';}catch(err){};
		
		design = position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('youtubePopup_animation').value+' '+document.getElementById('youtubePopup_typeWindow').value+' '+document.getElementById('youtubePopup_popup_form').value+' '+document.getElementById('youtubePopup_second_background').value;
		design += ' '+document.getElementById('youtubePopup_background').value+' '+document.getElementById('youtubePopup_head_font').value+' '+document.getElementById('youtubePopup_head_size').value;
		design += ' '+document.getElementById('youtubePopup_head_color').value+' '+document.getElementById('youtubePopup_text_font').value+' '+document.getElementById('youtubePopup_font_size').value+' '+document.getElementById('youtubePopup_text_color').value;		
		design += ' '+document.getElementById('youtubePopup_close_icon_type').value;
		design += ' '+document.getElementById('youtubePopup_close_icon_color').value;
		design += ' '+document.getElementById('youtubePopup_border_type').value+' '+document.getElementById('youtubePopup_border_depth').value;
		design += ' '+document.getElementById('youtubePopup_border_color').value;
		
		var overlay = document.getElementById('youtubePopup_overlay').value||'';
		var close_animation = document.getElementById('youtubePopup_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('youtubePopup_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('youtubePopup_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('youtubePopup_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('youtubePopup_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('youtubePopup_title').value)||'';
		
		if(document.getElementById('youtubePopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var iframe_src = encodeURIComponent(document.getElementById('youtubePopup_iframe_src').value)||'';
		
		
		try{
			iframe_src = iframe_src.toString().replace('watch%3Fv%3D', 'embed%2F');			
		}catch(err){}
				
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=youtubePopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&iframe_src='+iframe_src;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=youtubePopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&iframe_src='+iframe_src;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	function contactUsFloatingPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQcontactUsFloating_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQcontactUsFloating_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQcontactUsFloating_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQcontactUsFloating_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['contactUsFloating'][theme].addClass||'';}catch(err){};
		
		design = 'pq_fixed '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('contactUsFloating_animation').value+' '+document.getElementById('contactUsFloating_typeWindow').value+' '+document.getElementById('contactUsFloating_popup_form').value+' '+document.getElementById('contactUsFloating_second_background').value;
		design += ' '+document.getElementById('contactUsFloating_background').value+' '+document.getElementById('contactUsFloating_head_font').value+' '+document.getElementById('contactUsFloating_head_size').value;
		design += ' '+document.getElementById('contactUsFloating_head_color').value+' '+document.getElementById('contactUsFloating_text_font').value+' '+document.getElementById('contactUsFloating_font_size').value+' '+document.getElementById('contactUsFloating_text_color').value;		
		design += ' '+document.getElementById('contactUsFloating_close_icon_type').value;
		design += ' '+document.getElementById('contactUsFloating_close_icon_color').value;
		design += ' '+document.getElementById('contactUsFloating_border_type').value+' '+document.getElementById('contactUsFloating_border_depth').value;
		design += ' '+document.getElementById('contactUsFloating_border_color').value;
		
		var overlay = document.getElementById('contactUsFloating_overlay').value||'';
		var close_animation = document.getElementById('contactUsFloating_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('contactUsFloating_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('contactUsFloating_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('contactUsFloating_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('contactUsFloating_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('contactUsFloating_title').value)||'';
		
		if(document.getElementById('contactUsFloating_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var email_text = encodeURIComponent(document.getElementById('contactUsFloating_text_email').value)||'';
		var name_text = encodeURIComponent(document.getElementById('contactUsFloating_text_name').value)||'';
		var message_text = encodeURIComponent(document.getElementById('contactUsFloating_text_message').value)||'';
		
				
		design += ' '+document.getElementById('contactUsFloating_button_font').value+' '+document.getElementById('contactUsFloating_button_font_size').value+' '+document.getElementById('contactUsFloating_button_text_color').value+' '+document.getElementById('contactUsFloating_button_color').value
		var button_text = encodeURIComponent(document.getElementById('contactUsFloating_button_text').value)||'';		
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=contactUsFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&email_text='+email_text+'&name_text='+name_text+'&message_text='+message_text;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=contactUsFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&email_text='+email_text+'&name_text='+name_text+'&message_text='+message_text;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	function callMePopupPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQCallMePopup_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQCallMePopup_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQCallMePopup_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQCallMePopup_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['callMePopup'][theme].addClass||'';}catch(err){};
		
		design = 'pq_stick pq_call '+position+' '+theme+' '+themeAddClass+' '+document.getElementById('callMePopup_bookmark_background').value+' '+document.getElementById('callMePopup_bookmark_text_color').value;
		
		
		var windowDesign = '';
		windowDesign += ' pq_animated '+theme+' '+themeAddClass+' '+theme+' '+themeAddClass+' '+document.getElementById('callMePopup_animation').value+' '+document.getElementById('callMePopup_typeWindow').value+' '+document.getElementById('callMePopup_popup_form').value+' '+document.getElementById('callMePopup_second_background').value;
		windowDesign += ' '+document.getElementById('callMePopup_background').value+' '+document.getElementById('callMePopup_head_font').value+' '+document.getElementById('callMePopup_head_size').value;
		windowDesign += ' '+document.getElementById('callMePopup_head_color').value+' '+document.getElementById('callMePopup_text_font').value+' '+document.getElementById('callMePopup_font_size').value+' '+document.getElementById('callMePopup_text_color').value;		
		windowDesign += ' '+document.getElementById('callMePopup_close_icon_type').value;
		windowDesign += ' '+document.getElementById('callMePopup_close_icon_color').value;
		windowDesign += ' '+document.getElementById('callMePopup_border_type').value+' '+document.getElementById('callMePopup_border_depth').value;
		windowDesign += ' '+document.getElementById('callMePopup_border_color').value;		
		windowDesign += ' '+document.getElementById('callMePopup_button_font').value+' '+document.getElementById('callMePopup_button_font_size').value+' '+document.getElementById('callMePopup_button_text_color').value+' '+document.getElementById('callMePopup_button_color').value
											 
		
		if(document.getElementById('callMePopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var loader_text = document.getElementById('callMePopup_l_text').value||'';
		var overlay = document.getElementById('callMePopup_overlay').value||'';
		var close_animation = document.getElementById('callMePopup_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('callMePopup_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('callMePopup_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('callMePopup_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('callMePopup_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('callMePopup_title').value)||'';		
		var button_text = encodeURIComponent(document.getElementById('callMePopup_button_text').value)||'';
		var phone_text = encodeURIComponent(document.getElementById('callMePopup_text_phone').value)||'';
		var name_text = encodeURIComponent(document.getElementById('callMePopup_text_name').value)||'';		
			
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=callMePopup&design='+design+'&windowDesign='+windowDesign+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&loader_text='+loader_text+'&button_text='+button_text+'&phone_text='+phone_text+'&name_text='+name_text;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{				
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=callMePopup&design='+design+'&windowDesign='+windowDesign+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&loader_text='+loader_text+'&button_text='+button_text+'&phone_text='+phone_text+'&name_text='+name_text;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	
	function contactUsPopupPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQcontactUsPopup_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQcontactUsPopup_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQcontactUsPopup_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQcontactUsPopup_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['contactUsPopup'][theme].addClass||'';}catch(err){};
		
		design = 'pq_stick pq_contact '+position+' '+theme+' '+themeAddClass+' '+document.getElementById('contactUsPopup_bookmark_background').value+' '+document.getElementById('contactUsPopup_bookmark_text_color').value;
		
		
		var windowDesign = '';
		windowDesign += ' pq_animated '+theme+' '+themeAddClass+' '+document.getElementById('contactUsPopup_animation').value+' '+document.getElementById('contactUsPopup_typeWindow').value+' '+document.getElementById('contactUsPopup_popup_form').value+' '+document.getElementById('contactUsPopup_second_background').value;
		windowDesign += ' '+document.getElementById('contactUsPopup_background').value+' '+document.getElementById('contactUsPopup_head_font').value+' '+document.getElementById('contactUsPopup_head_size').value;
		windowDesign += ' '+document.getElementById('contactUsPopup_head_color').value+' '+document.getElementById('contactUsPopup_text_font').value+' '+document.getElementById('contactUsPopup_font_size').value+' '+document.getElementById('contactUsPopup_text_color').value;		
		windowDesign += ' '+document.getElementById('contactUsPopup_close_icon_type').value;
		windowDesign += ' '+document.getElementById('contactUsPopup_close_icon_color').value;
		windowDesign += ' '+document.getElementById('contactUsPopup_border_type').value+' '+document.getElementById('contactUsPopup_border_depth').value;
		windowDesign += ' '+document.getElementById('contactUsPopup_border_color').value;		
		windowDesign += ' '+document.getElementById('contactUsPopup_button_font').value+' '+document.getElementById('contactUsPopup_button_font_size').value+' '+document.getElementById('contactUsPopup_button_text_color').value+' '+document.getElementById('contactUsPopup_button_color').value
											 
		
		if(document.getElementById('contactUsPopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var loader_text = document.getElementById('contactUsPopup_l_text').value||'';
		var overlay = document.getElementById('contactUsPopup_overlay').value||'';
		var close_animation = document.getElementById('contactUsPopup_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('contactUsPopup_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('contactUsPopup_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('contactUsPopup_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('contactUsPopup_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('contactUsPopup_title').value)||'';		
		var button_text = encodeURIComponent(document.getElementById('contactUsPopup_button_text').value)||'';
		var email_text = encodeURIComponent(document.getElementById('contactUsPopup_text_email').value)||'';
		var name_text = encodeURIComponent(document.getElementById('contactUsPopup_text_name').value)||'';
		var message_text = encodeURIComponent(document.getElementById('contactUsPopup_text_message').value)||'';
			
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=contactUsPopup&design='+design+'&windowDesign='+windowDesign+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&loader_text='+loader_text+'&button_text='+button_text+'&email_text='+email_text+'&name_text='+name_text+'&message_text='+message_text;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{				
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=contactUsPopup&design='+design+'&windowDesign='+windowDesign+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&loader_text='+loader_text+'&button_text='+button_text+'&email_text='+email_text+'&name_text='+name_text+'&message_text='+message_text;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	function emailListBuilderBarPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQEmailListBuilderBar_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQEmailListBuilderBar_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQEmailListBuilderBar_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQEmailListBuilderBar_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['emailListBuilderBar'][theme].addClass||'';}catch(err){};
		
		design = 'pq_bar '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('emailListBuilderBar_animation').value+' '+document.getElementById('emailListBuilderBar_mobile_position').value;
		design += ' '+document.getElementById('emailListBuilderBar_background').value+' '+document.getElementById('emailListBuilderBar_head_font').value+' '+document.getElementById('emailListBuilderBar_head_size').value;
		design += ' '+document.getElementById('emailListBuilderBar_head_color').value;		
		design += ' '+document.getElementById('emailListBuilderBar_close_icon_type').value;
		design += ' '+document.getElementById('emailListBuilderBar_close_icon_color').value;
		design += ' '+document.getElementById('emailListBuilderBar_border_type').value+' '+document.getElementById('emailListBuilderBar_border_depth').value;
		design += ' '+document.getElementById('emailListBuilderBar_border_color').value;
		
		
		var close_animation = document.getElementById('emailListBuilderBar_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('emailListBuilderBar_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('emailListBuilderBar_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('emailListBuilderBar_b_image').value)||'';
		var m_title = encodeURIComponent(document.getElementById('emailListBuilderBar_m_title').value)||'';
		
		var title = encodeURIComponent(document.getElementById('emailListBuilderBar_title').value)||'';		
		var text_email = encodeURIComponent(document.getElementById('emailListBuilderBar_text_email').value)||'';		
		var text_name = encodeURIComponent(document.getElementById('emailListBuilderBar_text_name').value)||'';		
		
		var provider = 'mailchimp';
		if(document.getElementById('emailListBuilderBar_provider_mailchimp').checked) provider = 'mailchimp';
		if(document.getElementById('emailListBuilderBar_provider_getresponse').checked) provider = 'getresponse';
		if(document.getElementById('emailListBuilderBar_provider_aweber').checked) provider = 'aweber';
		//if(document.getElementById('emailListBuilderBar_provider_newsletter2go').checked) provider = 'newsletter2go';
		if(document.getElementById('emailListBuilderBar_provider_madmini').checked) provider = 'madmini';
		if(document.getElementById('emailListBuilderBar_provider_acampaign').checked) provider = 'acampaign';
		if(document.getElementById('emailListBuilderBar_provider_klickmail').checked) provider = 'klickmail';		
		
		
		
		if(document.getElementById('emailListBuilderBar_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		design += ' '+document.getElementById('emailListBuilderBar_button_font').value+' '+document.getElementById('emailListBuilderBar_button_font_size').value+' '+document.getElementById('emailListBuilderBar_button_text_color').value+' '+document.getElementById('emailListBuilderBar_button_color').value
		var button_text = encodeURIComponent(document.getElementById('emailListBuilderBar_button_text').value)||'';		
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=emailListBuilderBar&design='+design+'&whitelabel='+whitelabel+'&close_animation='+
					close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&title='+
					title+'&button_text='+button_text+'&text_email='+text_email+'&text_name='+text_name+'&provider='+provider+'&mobile_title='+m_title;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_bar_iframe pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=emailListBuilderBar&design='+design+'&whitelabel='+whitelabel+'&close_animation='+
					close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&title='+
					title+'&button_text='+button_text+'&text_email='+text_email+'&text_name='+text_name+'&provider='+provider+'&mobile_title='+m_title;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_bar_iframe pq_mobile_preview';
			}
		
	}
	
	
	function emailListBuilderFloatingPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQEmailListBuilderFloating_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQEmailListBuilderFloating_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQEmailListBuilderFloating_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQEmailListBuilderFloating_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['emailListBuilderFloating'][theme].addClass||'';}catch(err){};
		
		design = 'pq_fixed '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('emailListBuilderFloating_animation').value+' '+document.getElementById('emailListBuilderFloating_typeWindow').value+' '+document.getElementById('emailListBuilderFloating_popup_form').value+' '+document.getElementById('emailListBuilderFloating_second_background').value;
		design += ' '+document.getElementById('emailListBuilderFloating_background').value+' '+document.getElementById('emailListBuilderFloating_head_font').value+' '+document.getElementById('emailListBuilderFloating_head_size').value;
		design += ' '+document.getElementById('emailListBuilderFloating_head_color').value+' '+document.getElementById('emailListBuilderFloating_text_font').value+' '+document.getElementById('emailListBuilderFloating_font_size').value+' '+document.getElementById('emailListBuilderFloating_text_color').value;		
		design += ' '+document.getElementById('emailListBuilderFloating_close_icon_type').value;
		design += ' '+document.getElementById('emailListBuilderFloating_close_icon_color').value;
		design += ' '+document.getElementById('emailListBuilderFloating_border_type').value+' '+document.getElementById('emailListBuilderFloating_border_depth').value;
		design += ' '+document.getElementById('emailListBuilderFloating_border_color').value;
		
		var overlay = document.getElementById('emailListBuilderFloating_overlay').value||'';
		var close_animation = document.getElementById('emailListBuilderFloating_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('emailListBuilderFloating_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('emailListBuilderFloating_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('emailListBuilderFloating_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('emailListBuilderFloating_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('emailListBuilderFloating_title').value)||'';		
		var text_email = encodeURIComponent(document.getElementById('emailListBuilderFloating_text_email').value)||'';		
		var text_name = encodeURIComponent(document.getElementById('emailListBuilderFloating_text_name').value)||'';		
		
		var provider = 'mailchimp';
		if(document.getElementById('emailListBuilderFloating_provider_mailchimp').checked) provider = 'mailchimp';
		if(document.getElementById('emailListBuilderFloating_provider_getresponse').checked) provider = 'getresponse';
		if(document.getElementById('emailListBuilderFloating_provider_aweber').checked) provider = 'aweber';
		//if(document.getElementById('emailListBuilderFloating_provider_newsletter2go').checked) provider = 'newsletter2go';
		if(document.getElementById('emailListBuilderFloating_provider_madmini').checked) provider = 'madmini';
		if(document.getElementById('emailListBuilderFloating_provider_acampaign').checked) provider = 'acampaign';
		if(document.getElementById('emailListBuilderFloating_provider_klickmail').checked) provider = 'klickmail';		
		
		
		
		if(document.getElementById('emailListBuilderFloating_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		design += ' '+document.getElementById('emailListBuilderFloating_button_font').value+' '+document.getElementById('emailListBuilderFloating_button_font_size').value+' '+document.getElementById('emailListBuilderFloating_button_text_color').value+' '+document.getElementById('emailListBuilderFloating_button_color').value
		var button_text = encodeURIComponent(document.getElementById('emailListBuilderFloating_button_text').value)||'';		
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=emailListBuilderFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&text_email='+text_email+'&text_name='+text_name+'&provider='+provider;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=emailListBuilderFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&text_email='+text_email+'&text_name='+text_name+'&provider='+provider;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	function emailListBuilderPopupPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQEmailListBuilderPopup_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQEmailListBuilderPopup_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQEmailListBuilderPopup_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQEmailListBuilderPopup_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['emailListBuilderPopup'][theme].addClass||'';}catch(err){};
		
		design = position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('emailListBuilderPopup_animation').value+' '+document.getElementById('emailListBuilderPopup_typeWindow').value+' '+document.getElementById('emailListBuilderPopup_popup_form').value+' '+document.getElementById('emailListBuilderPopup_second_background').value;
		design += ' '+document.getElementById('emailListBuilderPopup_background').value+' '+document.getElementById('emailListBuilderPopup_head_font').value+' '+document.getElementById('emailListBuilderPopup_head_size').value;
		design += ' '+document.getElementById('emailListBuilderPopup_head_color').value+' '+document.getElementById('emailListBuilderPopup_text_font').value+' '+document.getElementById('emailListBuilderPopup_font_size').value+' '+document.getElementById('emailListBuilderPopup_text_color').value;		
		design += ' '+document.getElementById('emailListBuilderPopup_close_icon_type').value;
		design += ' '+document.getElementById('emailListBuilderPopup_close_icon_color').value;
		design += ' '+document.getElementById('emailListBuilderPopup_border_type').value+' '+document.getElementById('emailListBuilderPopup_border_depth').value;
		design += ' '+document.getElementById('emailListBuilderPopup_border_color').value;
		
		var overlay = document.getElementById('emailListBuilderPopup_overlay').value||'';
		var close_animation = document.getElementById('emailListBuilderPopup_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('emailListBuilderPopup_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('emailListBuilderPopup_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('emailListBuilderPopup_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('emailListBuilderPopup_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('emailListBuilderPopup_title').value)||'';		
		var text_email = encodeURIComponent(document.getElementById('emailListBuilderPopup_text_email').value)||'';		
		var text_name = encodeURIComponent(document.getElementById('emailListBuilderPopup_text_name').value)||'';		
		
		var provider = 'mailchimp';
		if(document.getElementById('emailListBuilderPopup_provider_mailchimp').checked) provider = 'mailchimp';
		if(document.getElementById('emailListBuilderPopup_provider_getresponse').checked) provider = 'getresponse';
		if(document.getElementById('emailListBuilderPopup_provider_aweber').checked) provider = 'aweber';
		//if(document.getElementById('emailListBuilderPopup_provider_newsletter2go').checked) provider = 'newsletter2go';
		if(document.getElementById('emailListBuilderPopup_provider_madmini').checked) provider = 'madmini';
		if(document.getElementById('emailListBuilderPopup_provider_acampaign').checked) provider = 'acampaign';
		if(document.getElementById('emailListBuilderPopup_provider_klickmail').checked) provider = 'klickmail';		
		
		
		
		if(document.getElementById('emailListBuilderPopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		design += ' '+document.getElementById('emailListBuilderPopup_button_font').value+' '+document.getElementById('emailListBuilderPopup_button_font_size').value+' '+document.getElementById('emailListBuilderPopup_button_text_color').value+' '+document.getElementById('emailListBuilderPopup_button_color').value
		var button_text = encodeURIComponent(document.getElementById('emailListBuilderPopup_button_text').value)||'';		
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=emailListBuilderPopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&text_email='+text_email+'&text_name='+text_name+'&provider='+provider;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=emailListBuilderPopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&button_text='+button_text+'&text_email='+text_email+'&text_name='+text_name+'&provider='+provider;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	
	
	
	function promotePopupPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQPromotePopup_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQPromotePopup_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQPromotePopup_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQPromotePopup_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['promotePopup'][theme].addClass||'';}catch(err){};
		
		design = position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('promotePopup_animation').value+' '+document.getElementById('promotePopup_typeWindow').value+' '+document.getElementById('promotePopup_popup_form').value+' '+document.getElementById('promotePopup_second_background').value;
		design += ' '+document.getElementById('promotePopup_background').value+' '+document.getElementById('promotePopup_head_font').value+' '+document.getElementById('promotePopup_head_size').value;
		design += ' '+document.getElementById('promotePopup_head_color').value+' '+document.getElementById('promotePopup_text_font').value+' '+document.getElementById('promotePopup_font_size').value+' '+document.getElementById('promotePopup_text_color').value;		
		design += ' '+document.getElementById('promotePopup_close_icon_type').value;
		design += ' '+document.getElementById('promotePopup_close_icon_color').value;
		design += ' '+document.getElementById('promotePopup_border_type').value+' '+document.getElementById('promotePopup_border_depth').value;
		design += ' '+document.getElementById('promotePopup_border_color').value;
		
		var overlay = document.getElementById('promotePopup_overlay').value||'';
		var close_animation = document.getElementById('promotePopup_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('promotePopup_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('promotePopup_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('promotePopup_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('promotePopup_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('promotePopup_title').value)||'';
		
		if(document.getElementById('promotePopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var url = encodeURIComponent(document.getElementById('promotePopup_url_address').value)||'';
		
				
		design += ' '+document.getElementById('promotePopup_button_font').value+' '+document.getElementById('promotePopup_button_font_size').value+' '+document.getElementById('promotePopup_button_text_color').value+' '+document.getElementById('promotePopup_button_color').value
		var button_text = encodeURIComponent(document.getElementById('promotePopup_button_text').value)||'';		
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=promotePopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&url='+url+'&button_text='+button_text;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=promotePopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&url='+url+'&button_text='+button_text;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	
	function promoteBarPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQPromoteBar_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQPromoteBar_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQPromoteBar_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQPromoteBar_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['promoteBar'][theme].addClass||'';}catch(err){};
		
		design = 'pq_bar '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('promoteBar_animation').value;
		design += ' '+document.getElementById('promoteBar_background').value+' '+document.getElementById('promoteBar_head_font').value+' '+document.getElementById('promoteBar_head_size').value;
		design += ' '+document.getElementById('promoteBar_head_color').value;		
		design += ' '+document.getElementById('promoteBar_close_icon_type').value;
		design += ' '+document.getElementById('promoteBar_close_icon_color').value;
		design += ' '+document.getElementById('promoteBar_border_type').value+' '+document.getElementById('promoteBar_border_depth').value;
		design += ' '+document.getElementById('promoteBar_border_color').value+' '+document.getElementById('promoteBar_mobile_position').value;
		
		
		var close_animation = document.getElementById('promoteBar_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('promoteBar_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('promoteBar_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('promoteBar_b_image').value)||'';		
		var title = encodeURIComponent(document.getElementById('promoteBar_title').value)||'';
		
		if(document.getElementById('promoteBar_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var url = encodeURIComponent(document.getElementById('promoteBar_url_address').value)||'';
		var m_title = encodeURIComponent(document.getElementById('promoteBar_m_title').value)||'';
		
				
		design += ' '+document.getElementById('promoteBar_button_font').value+' '+document.getElementById('promoteBar_button_font_size').value+' '+document.getElementById('promoteBar_button_text_color').value+' '+document.getElementById('promoteBar_button_color').value
		var button_text = encodeURIComponent(document.getElementById('promoteBar_button_text').value)||'';		
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=promoteBar&design='+design+'&whitelabel='+whitelabel+'&close_animation='+
					close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&title='+
					title+'&url='+url+'&button_text='+button_text+'&mobile_title='+m_title;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_bar_iframe pq_desktop_preview';
			}else{				
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=promoteBar&design='+design+'&whitelabel='+whitelabel+'&close_animation='+
					close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&title='+
					title+'&url='+url+'&button_text='+button_text+'&mobile_title='+m_title;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_bar_iframe pq_mobile_preview';
			}
		
	}
	
	function promoteFloatingPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQPromoteFloating_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQPromoteFloating_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQPromoteFloating_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQPromoteFloating_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['promoteFloating'][theme].addClass||'';}catch(err){};
		
		design = 'pq_fixed '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('promoteFloating_animation').value+' '+document.getElementById('promoteFloating_typeWindow').value+' '+document.getElementById('promoteFloating_popup_form').value+' '+document.getElementById('promoteFloating_second_background').value;
		design += ' '+document.getElementById('promoteFloating_background').value+' '+document.getElementById('promoteFloating_head_font').value+' '+document.getElementById('promoteFloating_head_size').value;
		design += ' '+document.getElementById('promoteFloating_head_color').value+' '+document.getElementById('promoteFloating_text_font').value+' '+document.getElementById('promoteFloating_font_size').value+' '+document.getElementById('promoteFloating_text_color').value;		
		design += ' '+document.getElementById('promoteFloating_close_icon_type').value;
		design += ' '+document.getElementById('promoteFloating_close_icon_color').value;
		design += ' '+document.getElementById('promoteFloating_border_type').value+' '+document.getElementById('promoteFloating_border_depth').value;
		design += ' '+document.getElementById('promoteFloating_border_color').value;
		
		var overlay = document.getElementById('promoteFloating_overlay').value||'';
		var close_animation = document.getElementById('promoteFloating_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('promoteFloating_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('promoteFloating_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('promoteFloating_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('promoteFloating_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('promoteFloating_title').value)||'';
		
		if(document.getElementById('promoteFloating_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		var url = encodeURIComponent(document.getElementById('promoteFloating_url_address').value)||'';
		
				
		design += ' '+document.getElementById('promoteFloating_button_font').value+' '+document.getElementById('promoteFloating_button_font_size').value+' '+document.getElementById('promoteFloating_button_text_color').value+' '+document.getElementById('promoteFloating_button_color').value
		var button_text = encodeURIComponent(document.getElementById('promoteFloating_button_text').value)||'';		
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=promoteFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&url='+url+'&button_text='+button_text;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=promoteFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&url='+url+'&button_text='+button_text;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	
	
	function followPopupPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQFollowPopup_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQFollowPopup_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQFollowPopup_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQFollowPopup_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['followPopup'][theme].addClass||'';}catch(err){};
		
		design = position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('followPopup_animation').value+' '+document.getElementById('followPopup_typeWindow').value+' '+document.getElementById('followPopup_popup_form').value+' '+document.getElementById('followPopup_second_background').value;
		design += ' '+document.getElementById('followPopup_background').value+' '+document.getElementById('followPopup_head_font').value+' '+document.getElementById('followPopup_head_size').value;
		design += ' '+document.getElementById('followPopup_head_color').value+' '+document.getElementById('followPopup_text_font').value+' '+document.getElementById('followPopup_font_size').value+' '+document.getElementById('followPopup_text_color').value;		
		design += ' '+document.getElementById('followPopup_close_icon_type').value;
		design += ' '+document.getElementById('followPopup_close_icon_color').value;
		design += ' '+document.getElementById('followPopup_border_type').value+' '+document.getElementById('followPopup_border_depth').value;
		design += ' '+document.getElementById('followPopup_border_color').value;
		
		var overlay = document.getElementById('followPopup_overlay').value||'';
		var close_animation = document.getElementById('followPopup_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('followPopup_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('followPopup_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('followPopup_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('followPopup_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('followPopup_title').value)||'';		
	
		
		//ICONS			
		design += ' '+document.getElementById('followPopup_icon_design').value||'';
		design += ' '+document.getElementById('followPopup_icon_form').value||'';
		design += ' '+document.getElementById('followPopup_icon_size').value||'';
		design += ' '+document.getElementById('followPopup_icon_space').value||'';
		var icon_animation = document.getElementById('followPopup_icon_animation').value||'';
		
		if(document.getElementById('followPopup_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		var socnetIcons = '';
		if(document.getElementById('followPopup_follow_icon_FB').value) socnetIcons += 'FB|';
		if(document.getElementById('followPopup_follow_icon_TW').value) socnetIcons += 'TW|';
		if(document.getElementById('followPopup_follow_icon_GP').value) socnetIcons += 'GP|';
		if(document.getElementById('followPopup_follow_icon_PI').value) socnetIcons += 'PI|';
		if(document.getElementById('followPopup_follow_icon_YT').value) socnetIcons += 'YT|';
		if(document.getElementById('followPopup_follow_icon_OD').value) socnetIcons += 'OD|';
		if(document.getElementById('followPopup_follow_icon_IG').value) socnetIcons += 'IG|';
		if(document.getElementById('followPopup_follow_icon_RSS').value) socnetIcons += 'RSS|';
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=followPopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=followPopup&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}
	
	function followBarPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQFollowBar_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQFollowBar_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQFollowBar_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQFollowBar_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['followBar'][theme].addClass||'';}catch(err){};
		
		design = 'pq_bar '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('followBar_animation').value+' '+document.getElementById('followBar_second_background').value;
		design += ' '+document.getElementById('followBar_background').value+' '+document.getElementById('followBar_head_font').value+' '+document.getElementById('followBar_head_size').value;
		design += ' '+document.getElementById('followBar_head_color').value;		
		design += ' '+document.getElementById('followBar_close_icon_type').value;
		design += ' '+document.getElementById('followBar_close_icon_color').value;
		design += ' '+document.getElementById('followBar_border_type').value+' '+document.getElementById('followBar_border_depth').value;
		design += ' '+document.getElementById('followBar_border_color').value+' '+document.getElementById('followBar_mobile_position').value;
		
				
		var close_animation = document.getElementById('followBar_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('followBar_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('followBar_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('followBar_b_image').value)||'';		
		var title = encodeURIComponent(document.getElementById('followBar_title').value)||'';		
		var m_title = encodeURIComponent(document.getElementById('followBar_m_title').value)||'';		
	
		
		//ICONS			
		design += ' '+document.getElementById('followBar_icon_design').value||'';
		design += ' '+document.getElementById('followBar_icon_form').value||'';
		design += ' '+document.getElementById('followBar_icon_size').value||'';
		design += ' '+document.getElementById('followBar_icon_space').value||'';
		var icon_animation = document.getElementById('followBar_icon_animation').value||'';
		
		if(document.getElementById('followBar_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		var socnetIcons = '';
		if(document.getElementById('followBar_follow_icon_FB').value) socnetIcons += 'FB|';
		if(document.getElementById('followBar_follow_icon_TW').value) socnetIcons += 'TW|';
		if(document.getElementById('followBar_follow_icon_GP').value) socnetIcons += 'GP|';
		if(document.getElementById('followBar_follow_icon_PI').value) socnetIcons += 'PI|';
		if(document.getElementById('followBar_follow_icon_YT').value) socnetIcons += 'YT|';
		if(document.getElementById('followBar_follow_icon_OD').value) socnetIcons += 'OD|';
		if(document.getElementById('followBar_follow_icon_IG').value) socnetIcons += 'IG|';
		if(document.getElementById('followBar_follow_icon_RSS').value) socnetIcons += 'RSS|';
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=followBar&design='+design+'&whitelabel='+whitelabel+'&close_animation='+
					close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons+'&mobile_title='+m_title;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_bar_iframe pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=followBar&design='+design+'&whitelabel='+whitelabel+'&close_animation='+
					close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons+'&mobile_title='+m_title;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_bar_iframe pq_mobile_preview';
			}
		
	}
	
	//followFloating
	function followFloatingPreview(){
		var toolView = 0;
		var desktopView = 0;		
		if(document.getElementById('PQFollowFloating_main_DesignToolSwitch_desktop').className.indexOf('pq_active') != -1) desktopView = 1;
		if(document.getElementById('PQFollowFloating_main_DesignToolSwitch_mobile').className.indexOf('pq_active') != -1) desktopView = 0;
		
		var design = position = theme = themeAddClass = '';
		
		try{position = PQPositionValues[document.getElementById('PQFollowFloating_position').value]||'';}catch(err){};
		try{theme = document.getElementById('PQFollowFloating_Current_Theme').value||'';}catch(err){};
		try{themeAddClass = PQThemes['followFloating'][theme].addClass||'';}catch(err){};
		
		design = 'pq_fixed '+position+' '+theme+' '+themeAddClass;
		
		design += ' pq_animated '+document.getElementById('followFloating_animation').value+' '+document.getElementById('followFloating_typeWindow').value+' '+document.getElementById('followFloating_popup_form').value+' '+document.getElementById('followFloating_second_background').value;
		design += ' '+document.getElementById('followFloating_background').value+' '+document.getElementById('followFloating_head_font').value+' '+document.getElementById('followFloating_head_size').value;
		design += ' '+document.getElementById('followFloating_head_color').value+' '+document.getElementById('followFloating_text_font').value+' '+document.getElementById('followFloating_font_size').value+' '+document.getElementById('followFloating_text_color').value;		
		design += ' '+document.getElementById('followFloating_close_icon_type').value;
		design += ' '+document.getElementById('followFloating_close_icon_color').value;
		design += ' '+document.getElementById('followFloating_border_type').value+' '+document.getElementById('followFloating_border_depth').value;
		design += ' '+document.getElementById('followFloating_border_color').value;
		
		var overlay = document.getElementById('followFloating_overlay').value||'';
		var close_animation = document.getElementById('followFloating_animation_close_icon').value||'';		
		var close_text = encodeURIComponent(document.getElementById('followFloating_close_text').value)||'';
		var header_i = encodeURIComponent(document.getElementById('followFloating_h_image').value)||'';
		var background_i = encodeURIComponent(document.getElementById('followFloating_b_image').value)||'';
		var sub_title = encodeURIComponent(document.getElementById('followFloating_subtitle').value)||'';
		var title = encodeURIComponent(document.getElementById('followFloating_title').value)||'';		
	
		
		//ICONS			
		design += ' '+document.getElementById('followFloating_icon_design').value||'';
		design += ' '+document.getElementById('followFloating_icon_form').value||'';
		design += ' '+document.getElementById('followFloating_icon_size').value||'';
		design += ' '+document.getElementById('followFloating_icon_space').value||'';
		var icon_animation = document.getElementById('followFloating_icon_animation').value||'';
		
		if(document.getElementById('followFloating_whitelabel').checked){
			var whitelabel = 1;
		}else{
			var whitelabel = 0;
		}
		
		var socnetIcons = '';
		if(document.getElementById('followFloating_follow_icon_FB').value) socnetIcons += 'FB|';
		if(document.getElementById('followFloating_follow_icon_TW').value) socnetIcons += 'TW|';
		if(document.getElementById('followFloating_follow_icon_GP').value) socnetIcons += 'GP|';
		if(document.getElementById('followFloating_follow_icon_PI').value) socnetIcons += 'PI|';
		if(document.getElementById('followFloating_follow_icon_YT').value) socnetIcons += 'YT|';
		if(document.getElementById('followFloating_follow_icon_OD').value) socnetIcons += 'OD|';
		if(document.getElementById('followFloating_follow_icon_IG').value) socnetIcons += 'IG|';
		if(document.getElementById('followFloating_follow_icon_RSS').value) socnetIcons += 'RSS|';
		
		if(desktopView == '1'){
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=followFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons;
				document.getElementById('PQPreviewID').className='pq_desktop_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_desktop_preview';
			}else{
				document.getElementById('PQPreviewID').src = '//profitquery.com/preview/iframe_demo_v4.html?tool=followFloating&design='+design+'&whitelabel='+whitelabel+'&overlay='+
					overlay+'&close_animation='+close_animation+'&close_text='+close_text+'&header_i='+header_i+'&background_i='+background_i+'&sub_title='+sub_title+'&title='+
					title+'&icon_animation='+icon_animation+'&socnet='+socnetIcons;
				document.getElementById('PQPreviewID').className='pq_mobile_preview';
				document.getElementById('PQIframePreviewBlock').className='frame pq_mobile_preview';
			}
		
	}

	</script>
	<div id="profitquery">
		<div id="PQLoader" style="position: absolute;width: 32px;height: 32px;padding: 30px;background-color: white;left: 0;right: 0;top: 20%;margin: 0 auto;box-shadow: 0 0 1px 0px #9C9C9D;z-index: 12;display:none;"><img src="<?php echo plugins_url('i/loader.gif', __FILE__);?>"></div>
	<?php
		if($_GET[s_t]){
			echo "<script>document.getElementById('PQLoader').style.display='block';</script>";
		}
	?>	
		<div class="pq2">
		<div class="pq1" id="PQmainHeaderBlock">
			<a href="http://profitquery.com/?utm-campaign=wp_aio_widgets_logo" target="_blank"> <img src="<?php echo plugins_url('i/profitquery_logo.png', __FILE__);?>"></a>						
			<a class="pq_adds" href="javascript:void(0)" onclick="document.getElementById('PQ_lang').style.display='block';"><?php if(!$this->_options[settings][lang]) echo 'en'; else echo $this->_options[settings][lang];?></a>
			<a class="pro" type="button" onclick="document.getElementById('PQSettingsPopup').style.display='block';"><img src="<?php echo plugins_url('i/settings_site.png', __FILE__);?>" /></a>
			<a class="pq_info" href="javascript:void(0)" onclick="document.getElementById('PQ_info_popup').style.display='block';"><span>10</span><img src="<?php echo plugins_url('i/info.png', __FILE__);?>"></a>
			<a class="pq_your_pro" href="javascript:void(0)" onclick="document.getElementById('PQ_pro_info').style.display='block';"><?php echo $this->_dictionary[navigation][pro_options];?></a>
		</div>
			<div class="f_wrapper pq_li1" id="PQMainTable">
				<h1><?php echo $this->_dictionary[mainPage][title];?></h1>
				<div class="pq_help">
					<input type="button" value="<?php echo $this->_dictionary[mainPage][need_help];?>" onclick="document.getElementById('PQNeedHelpPopup').style.display='block';" class="need_help">
					<input type="button" value="<?php echo $this->_dictionary[mainPage][add_new_tool];?>" onclick="document.getElementById('PQMainTable').style.display='none';document.getElementById('PQmainHeaderBlock').style.display='none';document.getElementById('PQSelectTools').style.display='block';" class="add_new">
					<input type="button" value="<?php echo $this->_dictionary[mainPage][tools_map];?>" onclick="document.getElementById('View_tools_map').style.display='block';" class="view_tools_maps">
				</div>
				<div class="clear"></div>
				<!--div class="pq_alert">
					<p>Activate</p>
					<input type="button" value="Upgrade Plan" onclick="document.getElementById('PQActivatePopup').style.display='block';">
				</div>
				<div class="pq_alert_soft">
					<p>Upgrade</p>
					<input type="button" value="Extend Plan" href="javascript:void(0)" onclick="document.getElementById('PQExtendPopup').style.display='block';">
				</div-->
				<table width="90%" style="margin:0px auto 30px; padding:0;border-spacing: 0px 5px; font-family: 'Open Sans', Helvetica, Arial, sans-serif;
		font-size: 15px;">
					<tbody>
					<?php						
						if($toolsArray){
						
						foreach((array)$toolsArray as $k => $v)
						{
					?>
						<tr class="<?php if($v[status][styleBlock] == 'red') echo 'pq_paused'; elseif($v[status][styleBlock] == 'yellow') echo 'pq_attantion';?>">
							<td><img src="<?php echo plugins_url('i/'.$v[name][icon], __FILE__);?>"></td>
							<td><p><?php echo $v[name][name];?></p></td>
							<td><p><?php echo $v[name][type];?></p></td>
							<td><p><?php echo $v[eventHandler][name].' '.$v[eventHandler][value];?></p></td>
							<td><p class="<?php if($v[status][status] == 'active') echo 'pq_active'; else echo 'pq_paused';?>"><?php echo $v[status][status_text];?></p></td>
							<td><p><?php if($v[proOptionsDetail]) echo $this->_dictionary[toolsStatus][premium]; else echo $this->_dictionary[toolsStatus][free];?></p></td>
							<td><p><?php echo $v[status][dateColumn];?></p></td>
							<td>
								<?php
									if($v[status][action] == 'activate'){									
										echo '<a href="javascript:void(0);" onclick="document.getElementById(\'PQActivatePopup\').style.display=\'block\';">'.$this->_dictionary[action][activate].'</a>';
									}else if($v[status][action] == 'extend'){									
										echo '<a href="javascript:void(0);" onclick="document.getElementById(\'PQExtendPopup\').style.display=\'block\';">'.$this->_dictionary[action][extend].'</a>';									
									}else{
										echo '&nbsp;';
									}
								?>														
							</td>
							<td><a href="<?php echo $this->getSettingsPageUrl();?>&s_t=<?php echo $k;?>&pos=<?php echo stripslashes($this->_options[$k][position]);?>"><img src="<?php echo plugins_url('i/settings_tool.png', __FILE__);?>"></a></td>
							<td><a href="javascript:void(0)" onclick="disableCurrentTool('<?php echo $k?>', '<?php echo $v[name][name]." ".$v[name][type];?>', '<?php echo plugins_url('i/'.$v[name][icon], __FILE__);?>');"><img src="<?php echo plugins_url('i/disable_tool.png', __FILE__);?>"></a></td>
						</tr>					
					<?php
						}
					?>
					<?php
						}else{
					?>
						<tr>
							<td><p><?php echo $this->_dictionary[mainPage][no_tool];?></p></td>						
						</tr>
					<?php
						}
					?>
					</tbody>
				</table>
				<input type="button" class="add_new_big" onclick="document.getElementById('PQMainTable').style.display='none';document.getElementById('PQmainHeaderBlock').style.display='none';document.getElementById('PQSelectTools').style.display='block';" value="<?php echo $this->_dictionary[mainPage][add_new_tool];?>">
				<!--input type="button" name="clr" value="Clear All" onclick="location.href='<?php echo $this->getSettingsPageUrl();?>&act=clear'" style="display: block;    margin: 0 auto; border: 0; background-color: transparent; color: #707679; cursor: pointer;"-->
				
							
				<!-- VIEW TOOL MAP -->
					
				<div class="pq_footer_left">				
					<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.4&appId=580260312066700";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-like" data-href="https://www.facebook.com/profitquery/" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
				<p><a href="javascript:void(0)" onclick="document.getElementById('PQNeedHelpPopup').style.display='block';">Support Center</a> | <a href="http://profitquery.com/privacy.html" target="_blank">Privacy Policy</a> | <a href="http://profitquery.com/terms.html" target="_blank">Terms of Service</a></p>
				</div>
				<div class="pq_footer_right"><p><?php printf($this->_dictionary[other][footer_text], 'href="https://wordpress.org/support/view/plugin-reviews/share-subscribe-contact-aio-widget" target="review"');?></p><img src="<?php echo plugins_url('i/rating.png', __FILE__);?>"></div>
			</div>
			
			<!-- Select Tools Div -->
			<div id="PQSelectTools" style="display:none;" class="f_wrapper pq_li2">
				<div class="pq_nav">
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>
					<span class="active"><?php echo $this->_dictionary[navigation][your_tools];?></span>	
				</div>
				<h1><?php echo $this->_dictionary[selectToolsDialog][title];?></h1>
				<p><?php echo $this->_dictionary[selectToolsDialog][description];?></p>
				<div class="pq_help">
					<div><span><?php echo $this->_dictionary[selectToolsDialog][search_title];?></span>
					<select onchange="sortTools(this.value)">
						<option value="">All</option>
						<option value="Sharing">Sharing</option>
						<option value="EmailListBuilder">Email List Builder</option>
						<option value="ContactUs">Contact Us</option>
						<option value="Promote">Promote</option>
						<option value="CallMe">Call Me</option>
						<option value="Follow">Follow</option>
					</select></div>				
					<input type="button" value="<?php echo $this->_dictionary[selectToolsDialog][button_help];?>" onclick="document.getElementById('PQNeedHelpPopup').style.display='block';" class="need_help" style="margin: 10px 5px;">
				</div>
				<div class="pq_clear"></div>
				<div style="overflow-y: auto; position: absolute; bottom: 0; top: 200px; left: 0; right: 0;">
				<div class="pq_label" id="SelectTool_Sharing_1">
					<div><img src="<?php echo plugins_url('i/tool_sharing_sidebar.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_sharing_sidebar.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Sharing Sidebar</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=sharingSidebar&pos=<?php echo $this->_options[sharingSidebar][position];?>" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=sharingSidebar&pos=<?php echo $this->_options[sharingSidebar][position];?>" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>				
					<img src="<?php echo plugins_url('i/ico_sharing_sidebar.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Sharing_2">
					<div><img src="<?php echo plugins_url('i/tool_image_sharer.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_image_sharer.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Image Sharer</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=imageSharer" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=imageSharer" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>				
					<img src="<?php echo plugins_url('i/ico_image_sharer.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Sharing_3">
					<div><img src="<?php echo plugins_url('i/tool_sharing_popup.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_sharing_popup.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Sharing Popup</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=sharingPopup" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=sharingPopup" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_sharing_popup.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Sharing_4">
					<div><img src="<?php echo plugins_url('i/tool_sharing_bar.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_sharing_bar.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Sharing Bar</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=sharingBar" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=sharingBar" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_sharing_bar.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Sharing_5">
					<div><img src="<?php echo plugins_url('i/tool_sharing_floating.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_sharing_floating.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Sharing Floating</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=sharingFloating" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=sharingFloating" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_sharing_floating.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_EmailListBuilder_1">
					<div><img src="<?php echo plugins_url('i/tool_collect_email_popup.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_collect_email_popup.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Email List Builder Popup</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=emailListBuilderPopup" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=emailListBuilderPopup" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_collect_email_popup.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_EmailListBuilder_2">
					<div><img src="<?php echo plugins_url('i/tool_collect_email_bar.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_collect_email_bar.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Email List Builder Bar</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=emailListBuilderBar" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=emailListBuilderBar" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_collect_email_bar.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_EmailListBuilder_3">
					<div><img src="<?php echo plugins_url('i/tool_collect_email_floating.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_collect_email_floating.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Email List Builder Floating</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=emailListBuilderFloating" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=emailListBuilderFloating" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_collect_email_floating.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_ContactUs_1">
					<div><img src="<?php echo plugins_url('i/tool_contact_us_popup.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_contact_us_popup.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Contact Us Popup</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=contactUsPopup" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=contactUsPopup" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_contact_us_popup.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_ContactUs_2">
					<div><img src="<?php echo plugins_url('i/tool_contact_us_floating.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_contact_us_floating.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Contact Us Floating</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=contactUsFloating" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=contactUsFloating" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_contact_us_floating.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Promote_1">
					<div><img src="<?php echo plugins_url('i/tool_promote_popup.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_promote_popup.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Promote Popup</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=promotePopup" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=promotePopup" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_promote_popup.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Promote_2">
					<div><img src="<?php echo plugins_url('i/tool_promote_bar.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_promote_bar.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Promote Bar</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=promoteBar" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=promoteBar" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_promote_bar.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Promote_3">
					<div><img src="<?php echo plugins_url('i/tool_promote_floating.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_promote_floating.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Promote Floating</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=promoteFloating" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=promoteFloating" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_promote_floating.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_CallMe_1">
					<div><img src="<?php echo plugins_url('i/tool_call_me_popup.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_call_me_popup.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Call Me Popup</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=callMePopup" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=callMePopup" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_call_me_popup.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_CallMe_2">
					<div><img src="<?php echo plugins_url('i/tool_call_me_floating.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_call_me_floating.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Call Me Floating</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=callMeFloating" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=callMeFloating" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_call_me_floating.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Follow_1">
					<div><img src="<?php echo plugins_url('i/tool_follow_popup.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_follow_popup.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Follow Popup</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=followPopup" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=followPopup" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_follow_popup.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Follow_2">
					<div><img src="<?php echo plugins_url('i/tool_follow_bar.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_follow_bar.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Follow Bar</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=followBar" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=followBar" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_follow_bar.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Follow_3">
					<div><img src="<?php echo plugins_url('i/tool_follow_floating.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_follow_floating.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Follow Floating</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=followFloating" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=followFloating" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_follow_floating.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Other_1">
					<div><img src="<?php echo plugins_url('i/tool_social_popup.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_social_popup.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>Social Popup</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=socialPopup" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=socialPopup" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_social_popup.png', __FILE__);?>" class="pq_ico">
				</div>
				<div class="pq_label" id="SelectTool_Other_2">
					<div><img src="<?php echo plugins_url('i/tool_youtube_popup.png', __FILE__);?>"><img src="<?php echo plugins_url('i/tool_youtube_popup.gif', __FILE__);?>" class="pq_label_hover"></div>
					<p>YouTube Popup</p>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=youtubePopup" class="pq_enable"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="<?php echo $this->getSettingsPageUrl();?>&s_t=youtubePopup" class="pq_settings"><?php echo $this->_dictionary[selectToolsDialog][settings];?></a>
					<a href="javascript:void(0)" class="pq_disable">Disable</a>
					<img src="<?php echo plugins_url('i/ico_youtube_popup.png', __FILE__);?>" class="pq_ico">
				</div>		
				</div>
			</div>
			<!-- End Select Tools Div -->	
			
			
			
			
	<!-- ***********************************************************************START TOOLS*********************************************************************************** -->		
	<script>



	function clearLastChangesProceed(){		
		location.href='<?php echo $this->getSettingsPageUrl();?>&act=clearLastChanges&s_t='+document.getElementById('PQCLearLastChangesTool').value;
	}

	function clearLastChanges(id){	
		document.getElementById('PQClearLastChangesPopup').style.display='block';	
		document.getElementById('PQCLearLastChangesTool').value=id;	
		
	}

	function returnToProviderStep(id){
		disableAllDialog();			
		if(id == 'emailListBuilderPopup'){			
			startTool('PQEmailListBuilderPopup');
			selectStep('PQEmailListBuilderPopup', 4);					
		}
		
		if(id == 'emailListBuilderFloating'){
			startTool('PQEmailListBuilderFloating');
			selectStep('PQEmailListBuilderFloating', 4);
		}
		
		if(id == 'emailListBuilderBar'){
			startTool('PQEmailListBuilderBar');
			selectStep('PQEmailListBuilderBar', 4);
		}
	}

	function returnToDesignStep(id){	
		disableAllDialog();
		if(id == 'sharingSidebar'){		
			startTool('PQSharingSidebar');
			selectStep('PQSharingSidebar', 2);
			enablePreviewIframe('PQSharingSidebar');
			selectForm('PQSharingSidebar', 'main');
			//sharingSidebarPreview();
		}
		if(id == 'imageSharer'){
			startTool('PQImageSharer');
			selectStep('PQImageSharer', 2);
			enablePreviewIframe('PQImageSharer');
			selectForm('PQImageSharer', 'main');
			//imageSharerPreview();
		}

		if(id == 'sharingPopup'){
			startTool('PQSharingPopup');
			selectStep('PQSharingPopup', 2);
			enablePreviewIframe('PQSharingPopup');
			selectForm('PQSharingPopup', 'main');
			//sharingPopupPreview();
		}

		if(id == 'sharingBar'){
			startTool('PQSharingBar');
			selectStep('PQSharingBar', 2);
			enablePreviewIframe('PQSharingBar');
			selectForm('PQSharingBar', 'main');
			//sharingBarPreview();
		}

		if(id == 'sharingFloating'){
			startTool('PQSharingFloating');
			selectStep('PQSharingFloating', 2);
			enablePreviewIframe('PQSharingFloating');
			selectForm('PQSharingFloating', 'main');
			//sharingFloatingPreview();
		}

		if(id == 'emailListBuilderPopup'){
			startTool('PQEmailListBuilderPopup');
			selectStep('PQEmailListBuilderPopup', 2);
			enablePreviewIframe('PQEmailListBuilderPopup');
			selectForm('PQEmailListBuilderPopup', 'main');
			//emailListBuilderPopupPreview();
		}

		if(id == 'emailListBuilderBar'){
			startTool('PQEmailListBuilderBar');
			selectStep('PQEmailListBuilderBar', 2);
			enablePreviewIframe('PQEmailListBuilderBar');
			selectForm('PQEmailListBuilderBar', 'main');
			//emailListBuilderBarPreview();
		}

		if(id == 'emailListBuilderFloating'){
			startTool('PQEmailListBuilderFloating');
			selectStep('PQEmailListBuilderFloating', 2);
			enablePreviewIframe('PQEmailListBuilderFloating');
			selectForm('PQEmailListBuilderFloating', 'main');
			//emailListBuilderFloatingPreview();
		}

		if(id == 'contactUsPopup'){
			startTool('PQcontactUsPopup');
			selectStep('PQcontactUsPopup', 2);
			enablePreviewIframe('PQcontactUsPopup');
			selectForm('PQcontactUsPopup', 'main');
			//contactUsPopupPreview();
		}

		if(id == 'contactUsFloating'){
			startTool('PQcontactUsFloating');
			selectStep('PQcontactUsFloating', 2);
			enablePreviewIframe('PQcontactUsFloating');
			selectForm('PQcontactUsFloating', 'main');
			//contactUsFloatingPreview();
		}

		if(id == 'promotePopup'){
			startTool('PQPromotePopup');
			selectStep('PQPromotePopup', 2);
			enablePreviewIframe('PQPromotePopup');
			selectForm('PQPromotePopup', 'main');
			//promotePopupPreview();
		}

		if(id == 'promoteBar'){
			startTool('PQPromoteBar');
			selectStep('PQPromoteBar', 2);
			enablePreviewIframe('PQPromoteBar');
			selectForm('PQPromoteBar', 'main');
			//promoteBarPreview();
		}

		if(id == 'promoteFloating'){
			startTool('PQPromoteFloating');
			selectStep('PQPromoteFloating', 2);
			enablePreviewIframe('PQPromoteFloating');
			selectForm('PQPromoteFloating', 'main');
			//promoteFloatingPreview();
		}

		if(id == 'callMePopup'){
			startTool('PQCallMePopup');
			selectStep('PQCallMePopup', 2);
			enablePreviewIframe('PQCallMePopup');
			selectForm('PQCallMePopup', 'main');
			//callMePopupPreview();
		}

		if(id == 'callMeFloating'){
			startTool('PQCallMeFloating');
			selectStep('PQCallMeFloating', 2);
			enablePreviewIframe('PQCallMeFloating');
			selectForm('PQCallMeFloating', 'main');
			//callMeFloatingPreview();
		}

		if(id == 'followPopup'){
			startTool('PQFollowPopup');
			selectStep('PQFollowPopup', 2);
			enablePreviewIframe('PQFollowPopup');
			selectForm('PQFollowPopup', 'main');
			//followPopupPreview();
		}

		if(id == 'followBar'){
			startTool('PQFollowBar');
			selectStep('PQFollowBar', 2);
			enablePreviewIframe('PQFollowBar');
			selectForm('PQFollowBar', 'main');
			//followBarPreview();
		}

		if(id == 'followFloating'){
			startTool('PQFollowFloating');
			selectStep('PQFollowFloating', 2);
			enablePreviewIframe('PQFollowFloating');
			selectForm('PQFollowFloating', 'main');
			//followFloatingPreview();
		}

		if(id == 'socialPopup'){
			startTool('PQSocialPopup');
			selectStep('PQSocialPopup', 2);
			enablePreviewIframe('PQSocialPopup');
			selectForm('PQSocialPopup', 'main');
			//socialPopupPreview();
		}

		if(id == 'youtubePopup'){
			startTool('PQYoutubePopup');
			selectStep('PQYoutubePopup', 2);
			enablePreviewIframe('PQYoutubePopup');
			selectForm('PQYoutubePopup', 'main');
			//youtubePopupPreview();
		}
	}


	function selectSubForm(id, type, subtype){	
		try{
			document.getElementById(id+'_form_main_mobile').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_main_desktop').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_thank_mobile').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_thank_desktop').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_email_mobile').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_email_desktop').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_'+type+'_'+subtype).style.display = 'block';
		}catch(err){};
	}

	function selectForm(id, type){
		try{
			document.getElementById(id+'_form_main').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_email').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_thank').style.display = 'none';
		}catch(err){};
		try{
			document.getElementById(id+'_form_'+type).style.display = 'block';
		}catch(err){};
	}

	function disableAllDialog(){
		try{document.getElementById('PQMainTable').style.display='none';}catch(err){};
		try{document.getElementById('PQSelectTools').style.display='none';}catch(err){};	
		try{document.getElementById('PQIframePreviewBlock').style.display='none';}catch(err){};
		try{document.getElementById('PQPositionBlock').style.display='none';}catch(err){};
		try{document.getElementById('PQThemeSelectPreviewPopup').style.display='none';}catch(err){};
		
		try{document.getElementById('PQmainHeaderBlock').style.display='none';}catch(err){};		
		
		try{document.getElementById('PQContinueCheckoutForm').style.display='none';}catch(err){};
		try{document.getElementById('PQExtendPopup').style.display='none';}catch(err){};
		try{document.getElementById('PQActivatePopup').style.display='none';}catch(err){};
		
		//tools
		try{document.getElementById('PQSharingSidebar').style.display='none';}catch(err){};
		try{document.getElementById('PQImageSharer').style.display='none';}catch(err){};
		try{document.getElementById('PQSharingPopup').style.display='none';}catch(err){};
		try{document.getElementById('PQSharingBar').style.display='none';}catch(err){};
		try{document.getElementById('PQSharingFloating').style.display='none';}catch(err){};
		try{document.getElementById('PQEmailListBuilderPopup').style.display='none';}catch(err){};
		try{document.getElementById('PQEmailListBuilderBar').style.display='none';}catch(err){};
		try{document.getElementById('PQEmailListBuilderFloating').style.display='none';}catch(err){};
		try{document.getElementById('PQcontactUsPopup').style.display='none';}catch(err){};
		try{document.getElementById('PQcontactUsFloating').style.display='none';}catch(err){};
		try{document.getElementById('PQPromotePopup').style.display='none';}catch(err){};
		try{document.getElementById('PQPromoteBar').style.display='none';}catch(err){};
		try{document.getElementById('PQPromoteFloating').style.display='none';}catch(err){};
		try{document.getElementById('PQCallMePopup').style.display='none';}catch(err){};
		try{document.getElementById('PQCallMeFloating').style.display='none';}catch(err){};
		try{document.getElementById('PQFollowPopup').style.display='none';}catch(err){};
		try{document.getElementById('PQFollowBar').style.display='none';}catch(err){};
		try{document.getElementById('PQFollowFloating').style.display='none';}catch(err){};
		try{document.getElementById('PQSocialPopup').style.display='none';}catch(err){};
		try{document.getElementById('PQYoutubePopup').style.display='none';}catch(err){};
	}

	function mainPage(){
		disableAllDialog();
		document.getElementById('PQMainTable').style.display='block';		
		document.getElementById('PQmainHeaderBlock').style.display='block';		
	}

	function enablePreviewIframe(id){
		document.getElementById('PQIframePreviewBlock').style.display='block';	
		document.getElementById('PQPreviewID').style.display='block';
		
		selectForm(id, 'main', ['desktop', 'mobile']);
		addClassToSwitcher(id, 'main', 'pq_active',['main', 'thank']);
		
		if(id == 'PQSharingSidebar'){sharingSidebarPreview();}
		if(id == 'PQImageSharer'){imageSharerPreview();}
		if(id == 'PQSharingPopup'){sharingPopupPreview();}
		if(id == 'PQSharingBar'){sharingBarPreview();}
		if(id == 'PQSharingFloating'){sharingFloatingPreview();}
		if(id == 'PQEmailListBuilderPopup'){emailListBuilderPopupPreview();}
		if(id == 'PQEmailListBuilderBar'){emailListBuilderBarPreview();}
		if(id == 'PQEmailListBuilderFloating'){emailListBuilderFloatingPreview();}
		if(id == 'PQcontactUsPopup'){contactUsPopupPreview();}
		if(id == 'PQcontactUsFloating'){contactUsFloatingPreview();}
		if(id == 'PQPromotePopup'){promotePopupPreview();}
		if(id == 'PQPromoteBar'){promoteBarPreview();}
		if(id == 'PQPromoteFloating'){promoteFloatingPreview();}
		if(id == 'PQCallMePopup'){callMePopupPreview();}
		if(id == 'PQCallMeFloating'){callMeFloatingPreview();}
		if(id == 'PQFollowPopup'){followPopupPreview();}
		if(id == 'PQFollowBar'){followBarPreview();}
		if(id == 'PQFollowFloating'){followFloatingPreview();}
		if(id == 'PQSocialPopup'){socialPopupPreview();}
		if(id == 'PQYoutubePopup'){youtubePopupPreview();}
		
		
	}

	function disableAllToolsStep(id){
		try{
			for (var i = 1; i <= 10; i++) {
				document.getElementById(id+'_step_'+i).style.display = 'none';
				document.getElementById(id+'_nav_step_'+i).className = '';
			}
		}catch(err){};
	}

	function selectStep(id, step){	
		disableAllToolsStep(id);
			try{			
				if(step>=2) {				
					document.getElementById(id+'_submit_button').disabled = false;
					document.getElementById(id+'_submit_button').style.backgroundColor = '#00e676';								
				}else{
					document.getElementById(id+'_submit_button').disabled = true;
					document.getElementById(id+'_submit_button').style.Color = '#7F7F80';
				}
			}catch(err){}
		try{
			document.getElementById(id+'_step_'+step).style.display = 'block';
			document.getElementById(id+'_nav_step_'+step).className = 'active';
		}catch(err){};
	}



	function disableAllPositionExept(structureName){	
		for(var i in PQDefaultPosition.all){		
			document.getElementById(i).className="";
			if(PQDefaultPosition[structureName][i]){			
				document.getElementById(i).disabled=false;			
			}else{
				document.getElementById(i).disabled=true;			
			}
		}
	}

	function nextStepFromTheme(){	
		startTool(document.getElementById('CURRENT_TOOL').value);
	}

	function selectPosition(tool, structureName){	
		disableAllPositionExept(structureName);
			
		document.getElementById('PQPositionBlock').style.display='block';			
		document.getElementById('CURRENT_TOOL').value=tool;
	}

	function setPosition(t){		
		setToolPosition(document.getElementById('CURRENT_TOOL').value, t.id)
		startTool(document.getElementById('CURRENT_TOOL').value);	
	}

	function setToolPosition(id, position){
		try{		
			document.getElementById(id+'_position').value=position;
		}catch(err){};	
	}

	function startTool(id){	
		document.getElementById('PQPositionBlock').style.display='none';		
		//start view
		document.getElementById(id).style.display='block';
		selectStep(id, 1);
	}

	function addClassToSwitcher(id, current, clsN, arr){
		try{
			for(var i in arr){
				document.getElementById(id+'_DesignToolSwitch_'+arr[i]).className = '';
			}
		}catch(err){};	
		document.getElementById(id+'_DesignToolSwitch_'+current).className = clsN;	
	}


	/****************************TOOLS LOADER JS********************************/
	//SHARING SIDEBAR
	function sharingSidebarSettings(pos){		
		disableAllDialog();		
		selectPosition('PQSharingSidebar', 'sharingSidebar');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//EMAIl LIST BUILDER POPUP
	function emailListBuilderPopupSettings(pos){		
		disableAllDialog();		
		selectPosition('PQEmailListBuilderPopup', 'emailListBuilderPopup');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//EMAIl LIST BUILDER FLOATING
	function emailListBuilderFloatingSettings(pos){		
		disableAllDialog();		
		selectPosition('PQEmailListBuilderFloating', 'emailListBuilderFloating');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//EMAIl LIST BUILDER BAR
	function emailListBuilderBarSettings(pos){		
		disableAllDialog();		
		selectPosition('PQEmailListBuilderBar', 'emailListBuilderBar');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//SHARING BAR
	function sharingBarSettings(pos){		
		disableAllDialog();		
		selectPosition('PQSharingBar', 'sharingBar');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//SHARING POPUP
	function sharingPopupSettings(pos){		
		disableAllDialog();		
		selectPosition('PQSharingPopup', 'sharingPopup');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//SHARING FLOATING
	function sharingFloatingSettings(pos){		
		disableAllDialog();		
		selectPosition('PQSharingFloating', 'sharingFloating');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//CONTACT US POPUP
	function contactUsPopupSettings(pos){		
		disableAllDialog();		
		selectPosition('PQcontactUsPopup', 'contactUsPopup');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//CONTACT US FLOATING FLOATING
	function contactUsFloatingSettings(pos){		
		disableAllDialog();		
		selectPosition('PQcontactUsFloating', 'contactUsFloating');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//PROMOTE POPUP
	function promotePopupSettings(pos){		
		disableAllDialog();		
		selectPosition('PQPromotePopup', 'promotePopup');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//PROMOTE BAR
	function promoteBarSettings(pos){		
		disableAllDialog();		
		selectPosition('PQPromoteBar', 'promoteBar');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//PROMOTE FLOATING
	function promoteFloatingSettings(pos){		
		disableAllDialog();		
		selectPosition('PQPromoteFloating', 'promoteFloating');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//CALL ME POPUP
	function callMePopupSettings(pos){		
		disableAllDialog();		
		selectPosition('PQCallMePopup', 'callMePopup');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//CALL ME FLOATING
	function callMeFloatingSettings(pos){		
		disableAllDialog();		
		selectPosition('PQCallMeFloating', 'callMeFloating');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//FOLLOW POPUP
	function followPopupSettings(pos){		
		disableAllDialog();		
		selectPosition('PQFollowPopup', 'followPopup');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//FOLLOW BAR
	function followBarSettings(pos){		
		disableAllDialog();		
		selectPosition('PQFollowBar', 'followBar');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//SHARING FLOATING
	function followFloatingSettings(pos){		
		disableAllDialog();		
		selectPosition('PQFollowFloating', 'followFloating');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//SOCIAL POPUP
	function socialPopupSettings(pos){		
		disableAllDialog();		
		selectPosition('PQSocialPopup', 'socialPopup');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}

	//YOUTUBE POPUP
	function youtubePopupSettings(pos){		
		disableAllDialog();		
		selectPosition('PQYoutubePopup', 'youtubePopup');
		document.getElementById('PQPositionBlockSkipButton').disabled = true;
		if(typeof pos != 'undefined'){
			document.getElementById('PQPositionBlockSkipButton').disabled = false;
			
			if(pos == 'BAR_TOP') document.getElementById('BAR_TOP').className = 'pq_checked';		
			if(pos == 'BAR_BOTTOM') document.getElementById('BAR_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_TOP') document.getElementById('SIDE_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_MIDDLE') document.getElementById('SIDE_LEFT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_LEFT_BOTTOM') document.getElementById('SIDE_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_TOP') document.getElementById('SIDE_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_MIDDLE') document.getElementById('SIDE_RIGHT_MIDDLE').className = 'pq_checked';		
			if(pos == 'SIDE_RIGHT_BOTTOM') document.getElementById('SIDE_RIGHT_BOTTOM').className = 'pq_checked';		
			if(pos == 'CENTER') document.getElementById('CENTER').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_TOP') document.getElementById('FLOATING_LEFT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_LEFT_BOTTOM') document.getElementById('FLOATING_LEFT_BOTTOM').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_TOP') document.getElementById('FLOATING_RIGHT_TOP').className = 'pq_checked';		
			if(pos == 'FLOATING_RIGHT_BOTTOM') document.getElementById('FLOATING_RIGHT_BOTTOM').className = 'pq_checked';
		}
	}


	//IMAGE SHARER
	function imageSharerSettings(pos){		
		disableAllDialog();
		startTool('PQImageSharer');	
	}
	/****************************END TOOLS LOADER JS****************************/
	
	
	function checkNameFieldsByProvider(id){	
		if(id){
			var provider = 'mailchimp';
			try{if(document.getElementById(id+'_provider_mailchimp').checked) provider = 'mailchimp';}catch(err){}
			try{if(document.getElementById(id+'_provider_getresponse').checked) provider = 'getresponse';}catch(err){}
			try{if(document.getElementById(id+'_provider_aweber').checked) provider = 'aweber';}catch(err){}
			try{if(document.getElementById(id+'_provider_madmini').checked) provider = 'madmini';}catch(err){}
			try{if(document.getElementById(id+'_provider_acampaign').checked) provider = 'acampaign';}catch(err){}
			try{if(document.getElementById(id+'_provider_klickmail').checked) provider = 'klickmail';}catch(err){}
			
			try{
				if(provider == 'aweber' || provider == 'getresponse' || provider == 'acampaign' || provider == 'klickmail'){
					document.getElementById(id+'_text_name').disabled = false;
				}else{
					document.getElementById(id+'_text_name').disabled = true;
				}
			}catch(err){}
		}		
	}
	</script>

	<!-- Preview Theme Popup -->
			<div id="PQThemeSelectPreviewPopup" style="display:none;">
			<input type="hidden" id="PQThemeSelectCurrentToolID" value="">
			<input type="hidden" id="PQThemeSelectCurrentID" value="">		
				<p id="Popup_ThemePreview_Title"></p>
				<div class="pq_clear"></div>
				<div style="display: block; position: absolute; top: 0px; bottom: 0; left: 0; right: 0; overflow-y: auto;">
				<div class="pq_preview_full" id="Popup_ThemePreview_DIV">
					<span id="Popup_ThemePreview_Prev" onclick="previewThemePrev();"></span>
					<img id="Popup_ThemePreview_big_src" src="" />
					<span id="Popup_ThemePreview_Next" onclick="previewThemeNext();"></span>
				<div><p id="Popup_ThemePreview_Description"></p><input type="button" value="<?php echo $this->_dictionary[choseTheme][choose_button];?>" onclick="document.getElementById('PQThemeSelectPreviewPopup').style.display='none';selectStep(document.getElementById('PQThemeSelectCurrentToolID').value, 2);setThemeSettings(document.getElementById('PQThemeSelectCurrentToolID').value,document.getElementById('PQThemeSelectCurrentID').value);enablePreviewIframe(document.getElementById('PQThemeSelectCurrentToolID').value);"></div>
				<a class="pq_close" onclick="document.getElementById('PQThemeSelectPreviewPopup').style.display='none';"><?php echo $this->_dictionary[action][close];?></a>
				</div>
				</div>
			</div>		

			<!-- Position -->
			<div id="PQPositionBlock" style="display:none">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>				
					<span class="active" ><?php echo $this->_dictionary[navigation][location];?></span>
					<input type="button" disabled style="color: #7F7F80; border: 1px solid #7F7F80; background-color: transparent!important;" id="PQPositionBlockSkipButton"  name="" onclick="nextStepFromTheme()" value="<?php echo $this->_dictionary[navigation][next_step];?>">
				</div>
				<h1><?php echo $this->_dictionary[positionsBlock][title];?></h1>
				<p><?php echo $this->_dictionary[positionsBlock][description];?></p>			
				<div style="overflow: hidden; display: inline-block; float: none; position: relative; max-width: 98%;"><img src="<?php echo plugins_url('i/tools_map.png', __FILE__);?>"  style="max-width: 100%;" />
				<div class="pq_clear"></div>
					<input type="hidden" id="CURRENT_TOOL" name="current_tool" value="">
					<label class="bar_top" ><input type="checkbox" id="BAR_TOP" name="position"  onclick="setPosition(this)">BAR TOP<div></div></label>
					<label class="bar_bottom" ><input type="checkbox" id="BAR_BOTTOM" name="position"  onclick="setPosition(this)">BAR BOTTOM<div></div></label>
					<label class="side_left_top" ><input type="checkbox" id="SIDE_LEFT_TOP" name="position"  onclick="setPosition(this)">SIDE_LEFT_TOP<div></div></label>
					<label class="side_left_middle" ><input type="checkbox" id="SIDE_LEFT_MIDDLE" name="position"  onclick="setPosition(this)">SIDE_LEFT_MIDDLE<div></div></label>
					<label class="side_left_bottom" ><input type="checkbox" id="SIDE_LEFT_BOTTOM" name="position"  onclick="setPosition(this)">SIDE_LEFT_BOTTOM<div></div></label>
					<label class="side_right_top" ><input type="checkbox" id="SIDE_RIGHT_TOP" name="position"  onclick="setPosition(this)">SIDE_RIGHT_TOP<div></div></label>
					<label class="side_right_middle" ><input type="checkbox" id="SIDE_RIGHT_MIDDLE" name="position"  onclick="setPosition(this)">SIDE_RIGHT_MIDDLE<div></div></label>
					<label class="side_right_bottom" ><input type="checkbox" id="SIDE_RIGHT_BOTTOM" name="position"  onclick="setPosition(this)">SIDE_RIGHT_BOTTOM<div></div></label>
					<label class="popup_center" ><input type="checkbox" id="CENTER" name="position"  onclick="setPosition(this)">CENTER<div></div></label>
					<label class="floating_left_top" ><input type="checkbox" id="FLOATING_LEFT_TOP" name="position" onclick="setPosition(this)">FLOATING_LEFT_TOP<div></div></label>			
					<label class="floating_left_bottom" ><input type="checkbox" id="FLOATING_LEFT_BOTTOM" name="position" onclick="setPosition(this)">FLOATING_LEFT_BOTTOM<div></div></label>
					<label class="floating_right_top" ><input type="checkbox" disabled id="FLOATING_RIGHT_TOP" name="position"  onclick="setPosition(this)">FLOATING_RIGHT_TOP<div></div></label>			
					<label class="floating_right_bottom" ><input type="checkbox" checked="checked" disabled id="FLOATING_RIGHT_BOTTOM" name="position" onclick="setPosition(this)">FLOATING_RIGHT_BOTTOM<div></div></label>
				</div>
			</div>

	<?php
		if($_GET[s_t] || trim($providerRedirectByError)){
			if($_GET[s_t] == 'sharingSidebar'){
				?>
				<!-- ****************** Sharing sidebar Div -->
				<div id="PQSharingSidebar" style="display:none;" class="f_wrapper pq_li3">
				<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
				<input type="hidden" name="action" value="sharingSidebarSave">
				<input type="hidden" id="PQSharingSidebar_position" name="sharingSidebar[position]" value="<?php echo stripslashes($this->_options[sharingSidebar][position]);?>">
					<div class="pq_nav" >				
						<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>'"><?php echo $this->_dictionary[navigation][index];?></span>							
						<span  onclick="sharingSidebarSettings('<?php echo stripslashes($this->_options[sharingSidebar][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
						<span id="PQSharingSidebar_nav_step_1" onclick="selectStep('PQSharingSidebar', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
						<span id="PQSharingSidebar_nav_step_2" onclick="selectStep('PQSharingSidebar', 2);enablePreviewIframe('PQSharingSidebar');selectForm('PQSharingSidebar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
						<span id="PQSharingSidebar_nav_step_3" onclick="selectStep('PQSharingSidebar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
						<span id="PQSharingSidebar_nav_step_4" onclick="selectStep('PQSharingSidebar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
						<input type="submit" id="PQSharingSidebar_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					</div>
					<div id="PQSharingSidebar_step_1" style="display:none;" class="pq_themes">
					<?php
						if(!$this->_options[sharingSidebar][theme]) $this->_options[sharingSidebar][theme] = $this->_default_themes[sharingSidebar];
					?>
					<input type="hidden" id="PQSharingSidebar_Current_Theme" name="sharingSidebar[theme]" value="<?php echo stripslashes($this->_options[sharingSidebar][theme]);?>">
					<input type="hidden" id="PQSharingSidebar_CurrentThemeForPreview" value="">
							<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
							<p><?php echo $this->_dictionary[choseTheme][description];?></p>
							<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
							<?php foreach((array)$this->_themes[sharingSidebar] as $k => $v){
									$isActive = '';
									if($k == stripslashes($this->_options[sharingSidebar][theme])){
										$isActive = 'pq_active';
									}
							?>
								<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
									<?php
											if($isActive){
												echo "selectStep('PQSharingSidebar', 2);enablePreviewIframe('PQSharingSidebar');selectForm('PQSharingSidebar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
											}else{
												echo "previewThemeSelect('PQSharingSidebar','sharingSidebar','".$k."', 'current');";
											}
										?>
								"/>
									<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
								</div>
							<?php }?>
							</div>				
						<div class="pq_close"></div>							
					</div>
					<div id="PQSharingSidebar_step_2" style="display:block;" class="design_settings">
						
						<div class="pq_thank_select">
							<span id="PQSharingSidebar_DesignToolSwitch_main" onclick="selectForm('PQSharingSidebar', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQSharingSidebar', 'main', 'pq_active',['main', 'thank', 'email']);sharingSidebarPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>							
							<span id="PQSharingSidebar_DesignToolSwitch_thank" onclick="selectForm('PQSharingSidebar', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQSharingSidebar', 'thank', 'pq_active',['main', 'thank', 'email']);sharingSidebarthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
							<span id="PQSharingSidebar_DesignToolSwitch_email" onclick="selectForm('PQSharingSidebar', 'email', ['desktop', 'mobile']);addClassToSwitcher('PQSharingSidebar', 'email', 'pq_active',['main', 'thank', 'email']);sharingSidebarsendMailWindowPreview('sharingSidebar');"><?php echo $this->_dictionary[navigation][email];?></span>
							
						</div>
						
						<div id="PQSharingSidebar_form_main" style="display:block;" class="pq_design">
							<div class="pq_desktop_mobile">
								<div id="PQSharingSidebar_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQSharingSidebar', 'main', 'desktop');addClassToSwitcher('PQSharingSidebar_main', 'desktop', 'pq_active',['desktop', 'mobile']);sharingSidebarPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
								<div id="PQSharingSidebar_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQSharingSidebar', 'main', 'mobile');addClassToSwitcher('PQSharingSidebar_main', 'mobile', 'pq_active',['desktop', 'mobile']);sharingSidebarPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
							</div>
							<h3>SHARING SIDEBAR</h3>					
							<!-- DESKTOP -->
							<div id="PQSharingSidebar_form_main_desktop" style="display:block;">					
								<?php
									echo $this->getSharingIcons('sharingSidebar', $this->_options[sharingSidebar][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[sharingSidebar]));
									
									echo $this->getFormCodeForTool(
									array(										
										'form_icons',
										'size_icons',
										'space_icons',								
										'animation_icons',
										'animation'
										), 'sharingSidebar', $this->_options[sharingSidebar]);
								?>								
								<div class="pq_switch_for">
								<?php echo $this->_dictionary[designOptions][_counters];?>
								<input type="checkbox" class="pq_switch" name="sharingSidebar[withCounter]" onclick="sharingSidebarPreview();" id="sharingSidebar_counters" <?php if($this->_options[sharingSidebar][withCounter] == 'on') echo 'checked';?>>
								<label for="sharingSidebar_counters" class="pq_switch_label"></label>
								</div>
								<div class="pq_switch_for">
								<?php echo $this->_dictionary[designOptions][_galerryOption_enable];?>
								<input type="checkbox" class="pq_switch" name="sharingSidebar[galleryOption][enable]" onclick="sharingSidebarPreview();" id="sharingSidebar_g_enable" <?php if($this->_options[sharingSidebar][galleryOption][enable] == 'on') echo 'checked';?> onclick="enableBlockByCheckboxClick(this.checked, 'PQSharingSidebar_form_gallery_proceed');">
								<label for="sharingSidebar_g_enable" class="pq_switch_label"></label>
								</div>
								<div id="PQSharingSidebar_form_gallery_proceed" style="display:none">
									<?php
										echo $this->getFormCodeForTool(
											array(						
												'gallery_min_width',
												'gallery_title',
												'gallery_head_font',
												'gallery_head_font_size',
												'gallery_button_text',
												'gallery_button_font',
												'gallery_button_font_size',
												'gallery_button_text_color',
												'gallery_button_color',
												'gallery_background_color'												
												), 'sharingSidebar', $this->_options[sharingSidebar]);
									?>
								</div>					
								<script>
									enableBlockByCheckboxClick('<?php echo $this->_options[sharingSidebar][galleryOption][enable];?>', 'PQSharingSidebar_form_gallery_proceed');
								</script>
								<br><br>
								<h3><?php echo $this->_dictionary[navigation][more_share_window];?></h3>
								<?php					
									echo $this->getFormCodeForTool(
									array(
										'size_window',
										'popup_form',
										'background_color',										
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',  										
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',										
										'animation_icons',
										'animation',
										'overlay_color'
									)
									, 'sharingSidebar[moreShareWindow]', $this->_options[sharingSidebar][moreShareWindow]);
								?>
								
								
							</div>
							<!-- MOBILE -->
							<div id="PQSharingSidebar_form_main_mobile" style="display:none;">						
								<?php							
									echo $this->getFormCodeForTool(
									array(														
										'mobile_view_type',
										'mobile_position',
										'mobile_title'								
										), 'sharingSidebar', $this->_options[sharingSidebar]);
								?>						
							</div>
							
							
						</div>
						<div id="PQSharingSidebar_form_email" style="display:none;" class="pq_design">
							<div class="pq_desktop_mobile">
								<div id="PQSharingSidebar_email_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQSharingSidebar_email', 'desktop', 'pq_active',['desktop', 'mobile']);sharingSidebarsendMailWindowPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
								<div id="PQSharingSidebar_email_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQSharingSidebar_email', 'mobile', 'pq_active',['desktop', 'mobile']);sharingSidebarsendMailWindowPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
							</div>
							<div class="pq_switch_for">
							<?php echo $this->_dictionary[action][enable];?>
							<input type="checkbox" class="pq_switch" name="sharingSidebar[sendMailWindow][enable]" id="sharingSidebar_email_enable" <?php if($this->_options[sharingSidebar][sendMailWindow][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQSharingSidebar_form_email_proceed');sharingSidebarsendMailWindowPreview();">
							<label for="sharingSidebar_email_enable" class="pq_switch_label"></label>
							</div>
							<div id="PQSharingSidebar_form_email_proceed" style="display:none">	
								<?php
									echo $this->getFormCodeForTool(
									array(										  
										  'size_window',
										  'popup_form',
										  'background_color',
										  'second_background',										  
										  'title',
										  'head_font',
										  'head_font_size',
										  'head_color',
										  'sub_title',
										  'text_font',
										  'font_size',
										  'text_color', 
										  'enter_email_text',
										  'enter_name_text',  										  
										  'enter_subject_text',  
										  'enter_message_text',
										  'button_text',
										  'button_font',
										  'button_font_size',
										  'button_text_color',
										  'button_color',									  
										  'header_image_src',          
										  'close_icon_type', 
										  'close_text',
										  'close_icon_color',
										  'close_icon_animation',										  
										  'animation',
										  'overlay_color'
										), 'sharingSidebar[sendMailWindow]', $this->_options[sharingSidebar][sendMailWindow]);
									?>
							</div>
							<script>
								enableBlockByCheckboxClick('<?php echo $this->_options[sharingSidebar][sendMailWindow][enable];?>', 'PQSharingSidebar_form_email_proceed');
							</script>
						</div>
						<div id="PQSharingSidebar_form_thank" style="display:none;" class="pq_design">
							<div class="pq_desktop_mobile">
								<div id="PQSharingSidebar_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQSharingSidebar_thank', 'desktop', 'pq_active',['desktop', 'mobile']);sharingSidebarthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
								<div id="PQSharingSidebar_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQSharingSidebar_thank', 'mobile', 'pq_active',['desktop', 'mobile']);sharingSidebarthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
							</div>
							
							<div class="pq_switch_for">
							<?php echo $this->_dictionary[action][enable];?>
							<input type="checkbox" class="pq_switch" name="sharingSidebar[thank][enable]" id="sharingSidebar_thank_enable" <?php if($this->_options[sharingSidebar][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQSharingSidebar_form_thank_proceed');sharingSidebarthankPreview();">
							<label for="sharingSidebar_thank_enable" class="pq_switch_label"></label>
							</div>
							<div id="PQSharingSidebar_form_thank_proceed" style="display:none">						
								<?php					
									echo $this->getFormCodeForTool(
									array(										
										'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',  										
										'header_image_src',
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',
										'animation',
										'overlay_color'									
									)
									, 'sharingSidebar[thank]', $this->_options[sharingSidebar][thank]);
								?>
								<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
								
								<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
								<select name="sharingSidebar[thank][socnet_block_type]" id="sharingSidebar_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQSharingSidebar_thank_socnetIconsBlock_',['follow', 'share']);">
									<option value="" selected>None</option>
									<option value="follow" <?php if($this->_options[sharingSidebar][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
									<option value="share" <?php if($this->_options[sharingSidebar][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
								</select>
								
								<br>
								<div id="PQSharingSidebar_thank_socnetIconsBlock_follow" style="display:none">
									<?php
										echo $this->getFollowIcons('sharingSidebar[thank]', $this->_options[sharingSidebar][thank]);										
									?>
								</div>					
								<br>
								<div id="PQSharingSidebar_thank_socnetIconsBlock_share" style="display:none">
									<?php
										echo $this->getSharingIcons('sharingSidebar[thank]', $this->_options[sharingSidebar][thank][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[sharingSidebar][thank]), 1);
									?>
								</div>
								<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'sharingSidebar[thank]', $this->_options[sharingSidebar][thank]);
								?>
								<script>
									enableAdditionalBlock('<?php echo $this->_options[sharingSidebar][thank][socnet_block_type];?>', 'PQSharingSidebar_thank_socnetIconsBlock_',['follow', 'share']);
								</script>
								<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
								
								<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
								<select name="sharingSidebar[thank][buttonBlock][type]" id="sharingSidebar_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQSharingSidebar_thank_buttonBlock_', ['redirect']);sharingSidebarthankPreview();">
									<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
									<option value="redirect" <?php if($this->_options[sharingSidebar][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
								</select>
								<br>
								<div id="PQSharingSidebar_thank_buttonBlock_redirect" style="display:none">
									<?php
										echo $this->getFormCodeForTool(
										array(
											'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'											
										)
										, 'sharingSidebar[thank][buttonBlock]', $this->_options[sharingSidebar][thank][buttonBlock]);
									?>						
								</div>
								<script>
									enableAdditionalBlock('<?php echo $this->_options[sharingSidebar][thank][buttonBlock][type];?>', 'PQSharingSidebar_thank_buttonBlock_', ['redirect']);
								</script>
							</div>
							<script>
								enableBlockByCheckboxClick('<?php echo $this->_options[sharingSidebar][thank][enable];?>', 'PQSharingSidebar_form_thank_proceed');
							</script>
						</div>
						<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="sharingSidebar[whitelabel]" onclick="getPreviewByActiveForm('sharingSidebar', 'PQSharingSidebar');" id="sharingSidebar_whitelabel" <?php if($this->_options[sharingSidebar][whitelabel] == 'on') echo 'checked';?>>
						<label for="sharingSidebar_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQSharingSidebar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('sharingSidebar');" <?php if(trim($this->_options[sharingSidebar][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>					
					</div>
					<div id="PQSharingSidebar_step_3" style="display:none;" class="pq_rules">
						<h1><img src="<?php echo plugins_url('i/ico_sharing_sidebar.png', __FILE__);?>"> Sharing Sidebar</h1>
						<p><?php echo $this->_dictionary[displayRules][description];?></p>
						<div class="pq_clear"></div>
						<?php
							echo $this->getEventHandlerBlock('sharingSidebar', array('delay', 'scrolling'),  $this->_options[sharingSidebar]);
						?>
						<?php
							echo $this->getPageOptions('sharingSidebar', $this->_options[sharingSidebar]);
						?>
						<?php
							echo $this->getGadgetRules('sharingSidebar', $this->_options[sharingSidebar]);
						?>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQSharingSidebar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
						
					</div>
					<div id="PQSharingSidebar_step_4" style="display:none;" class="provider_settings">				
						<h1><img src="<?php echo plugins_url('i/ico_sharing_sidebar.png', __FILE__);?>"> Sharing Sidebar</h1>
						<p><?php echo $this->_dictionary[providerSettings][description];?></p>
						<?php
							echo $this->setProMailForm('sharingSidebar', 0, $this->_options[sharingSidebar]);
						?>						
					</div>
				</form>	
				</div>
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQSharingSidebar\');
								selectStep(\'PQSharingSidebar\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								sharingSidebarSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					document.getElementById('PQLoader').style.display='none';
					checkTheme('sharingSidebar', 'PQSharingSidebar');
				</script>
				<!-- ********* End Sharing sidebar Div -->
				<?php
			}
			if($_GET[s_t] == 'imageSharer'){
				?>
				
	<!-- ****************** IMAGE SHARER -->
			<div id="PQImageSharer" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="imageSharerSave">
			<input type="hidden" id="PQImageSharer_position" name="imageSharer[position]" value="<?php echo stripslashes($this->_options[imageSharer][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>'"><?php echo $this->_dictionary[navigation][index];?></span>				
					<span id="PQImageSharer_nav_step_1" onclick="selectStep('PQImageSharer', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQImageSharer_nav_step_2" onclick="selectStep('PQImageSharer', 2);enablePreviewIframe('PQImageSharer');selectForm('PQImageSharer', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQImageSharer_nav_step_3" onclick="selectStep('PQImageSharer', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQImageSharer_nav_step_4" onclick="selectStep('PQImageSharer', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQImageSharer_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQImageSharer_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[imageSharer][theme]) $this->_options[imageSharer][theme] = $this->_default_themes[imageSharer];
				?>
				<input type="hidden" id="PQImageSharer_Current_Theme" name="imageSharer[theme]" value="<?php echo stripslashes($this->_options[imageSharer][theme]);?>">
				<input type="hidden" id="PQImageSharer_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[imageSharer] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[imageSharer][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQImageSharer', 2);enablePreviewIframe('PQImageSharer');selectForm('PQImageSharer', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQImageSharer','imageSharer','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQImageSharer_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQImageSharer_DesignToolSwitch_main" onclick="selectForm('PQImageSharer', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQImageSharer', 'main', 'pq_active',['main', 'thank', 'email']);imageSharerPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQImageSharer_DesignToolSwitch_thank" onclick="selectForm('PQImageSharer', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQImageSharer', 'thank', 'pq_active',['main', 'thank', 'email']);imageSharerthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>
						<span id="PQImageSharer_DesignToolSwitch_email" onclick="selectForm('PQImageSharer', 'email', ['desktop', 'mobile']);addClassToSwitcher('PQImageSharer', 'email', 'pq_active',['main', 'thank', 'email']);imageSharersendMailWindowPreview();"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQImageSharer_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQImageSharer_main_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQImageSharer_main', 'desktop', 'pq_active',['desktop', 'mobile']);imageSharerPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQImageSharer_main_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQImageSharer_main', 'mobile', 'pq_active',['desktop', 'mobile']);imageSharerPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>IMAGE SHARER</h3>					
						<!-- DESKTOP -->
						<div id="PQImageSharer_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getImageSharingIcons('imageSharer', $this->_options[imageSharer]);
								
								echo $this->getFormCodeForTool(
								array(															
									'form_icons',
									'size_icons',
									'space_icons',
									'shadow_icons',
									'animation_icons',									
									'position_icons'
									), 'imageSharer', $this->_options[imageSharer]);
							?>
							
						</div>					
					</div>
					<div id="PQImageSharer_form_email" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQImageSharer_email_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQImageSharer_email', 'desktop', 'pq_active',['desktop', 'mobile']);imageSharersendMailWindowPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQImageSharer_email_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQImageSharer_email', 'mobile', 'pq_active',['desktop', 'mobile']);imageSharersendMailWindowPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="imageSharer[sendMailWindow][enable]" id="imageSharer_email_enable" <?php if($this->_options[imageSharer][sendMailWindow][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQImageSharer_form_email_proceed');imageSharersendMailWindowPreview();">
						<label for="imageSharer_email_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQImageSharer_form_email_proceed" style="display:none">	
							<?php
								echo $this->getFormCodeForTool(
								array(										  
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',									  
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'enter_email_text',
									  'enter_name_text',									  
									  'enter_subject_text',  
									  'enter_message_text',
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',									  
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',									  
									  'animation',
									  'overlay_color'
									), 'imageSharer[sendMailWindow]', $this->_options[imageSharer][sendMailWindow]);
								?>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[imageSharer][sendMailWindow][enable];?>', 'PQImageSharer_form_email_proceed');
						</script>
					</div>
					<div id="PQImageSharer_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQImageSharer_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQImageSharer_thank', 'desktop', 'pq_active',['desktop', 'mobile']);imageSharerthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQImageSharer_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQImageSharer_thank', 'mobile', 'pq_active',['desktop', 'mobile']);imageSharerthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" id="imageSharer_thank_enable" name="imageSharer[thank][enable]" <?php if($this->_options[imageSharer][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQImageSharer_form_thank_proceed');imageSharerthankPreview();">
						<label for="imageSharer_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQImageSharer_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   										
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'imageSharer[thank]', $this->_options[imageSharer][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="imageSharer[thank][socnet_block_type]" id="imageSharer_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQImageSharer_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[imageSharer][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[imageSharer][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQImageSharer_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('imageSharer[thank]', $this->_options[imageSharer][thank]);
								?>
							</div>					
							<br>
							<div id="PQImageSharer_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('imageSharer[thank]', $this->_options[imageSharer][thank][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[imageSharer][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'imageSharer[thank]', $this->_options[imageSharer][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[imageSharer][thank][socnet_block_type];?>', 'PQImageSharer_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="imageSharer[thank][buttonBlock][type]" id="imageSharer_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQImageSharer_thank_buttonBlock_', ['redirect']);imageSharerthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[imageSharer][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQImageSharer_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'imageSharer[thank][buttonBlock]', $this->_options[imageSharer][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[imageSharer][thank][buttonBlock][type];?>', 'PQImageSharer_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[imageSharer][thank][enable];?>', 'PQImageSharer_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="imageSharer[whitelabel]" onclick="getPreviewByActiveForm('imageSharer', 'PQImageSharer');" id="imageSharer_whitelabel" <?php if($this->_options[imageSharer][whitelabel] == 'on') echo 'checked';?>>
						<label for="imageSharer_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQImageSharer', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">	
						<a onclick="clearLastChanges('imageSharer');" <?php if(trim($this->_options[imageSharer][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQImageSharer_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_image_sharer.png', __FILE__);?>"> Image Sharer</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>				
					<?php
						echo $this->getPageOptions('imageSharer', $this->_options[imageSharer]);
					?>
					<?php
						echo $this->getGadgetRules('imageSharer', $this->_options[imageSharer]);
					?>
					<div class="pq_desable_url">
						<h2><?php echo $this->_dictionary[pageSettings][extension_title]?></h2>
						<div class="pq_clear"></div>
						<input type="text" name="imageSharer[displayRules][allowedExtensions][0]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedExtensions][0]);?>">
						<input type="text" name="imageSharer[displayRules][allowedExtensions][1]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedExtensions][1]);?>">
						<input type="text" name="imageSharer[displayRules][allowedExtensions][2]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedExtensions][2]);?>">
						<input type="text" name="imageSharer[displayRules][allowedExtensions][3]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedExtensions][3]);?>">
						<input type="text" name="imageSharer[displayRules][allowedExtensions][4]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedExtensions][4]);?>">
						<input type="text" name="imageSharer[displayRules][allowedExtensions][5]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedExtensions][5]);?>">
					</div>
					
					<div class="pq_desable_url">
						<h2><?php echo $this->_dictionary[pageSettings][image_url_title]?></h2>
						<div class="pq_clear"></div>
						<input type="text" name="imageSharer[displayRules][allowedImageAddress][0]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedImageAddress][0]);?>">
						<input type="text" name="imageSharer[displayRules][allowedImageAddress][1]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedImageAddress][1]);?>">
						<input type="text" name="imageSharer[displayRules][allowedImageAddress][2]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedImageAddress][2]);?>">
						<input type="text" name="imageSharer[displayRules][allowedImageAddress][3]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedImageAddress][3]);?>">
						<input type="text" name="imageSharer[displayRules][allowedImageAddress][4]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedImageAddress][4]);?>">
						<input type="text" name="imageSharer[displayRules][allowedImageAddress][5]" value="<?php echo stripslashes($this->_options[imageSharer][displayRules][allowedImageAddress][5]);?>">
					</div>
					
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQImageSharer', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQImageSharer_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_image_sharer.png', __FILE__);?>"> Image Sharer</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getFormCodeForTool(
								array(						
									'min_width',
									'min_height'
									), 'imageSharer', $this->_options[imageSharer]);
					?>
					<?php
						echo $this->setProMailForm('imageSharer', 0, $this->_options[imageSharer]);
					?>
					
				</div>
			</form>	
			</div>
	<!-- ********* End IMAGE SHARER -->

				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQImageSharer\');
								selectStep(\'PQImageSharer\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								imageSharerSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					document.getElementById('PQLoader').style.display='none';
					checkTheme('imageSharer', 'PQImageSharer');
				</script>
				<?php
			}

			if($_GET[s_t] == 'sharingPopup'){
				?>
				<!-- ********* Sharing Popup Div -->
			<div id="PQSharingPopup" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="sharingPopupSave">
			<input type="hidden" id="PQSharingPopup_position" name="sharingPopup[position]" value="<?php echo stripslashes($this->_options[sharingPopup][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="sharingPopupSettings('<?php echo stripslashes($this->_options[sharingPopup][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQSharingPopup_nav_step_1" onclick="selectStep('PQSharingPopup', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQSharingPopup_nav_step_2" onclick="selectStep('PQSharingPopup', 2);enablePreviewIframe('PQSharingPopup');selectForm('PQSharingPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQSharingPopup_nav_step_3" onclick="selectStep('PQSharingPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQSharingPopup_nav_step_4" onclick="selectStep('PQSharingPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQSharingPopup_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQSharingPopup_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[sharingPopup][theme]) $this->_options[sharingPopup][theme] = $this->_default_themes[sharingPopup];
				?>
				<input type="hidden" id="PQSharingPopup_Current_Theme" name="sharingPopup[theme]" value="<?php echo stripslashes($this->_options[sharingPopup][theme]);?>">
				<input type="hidden" id="PQSharingPopup_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[sharingPopup] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[sharingPopup][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQSharingPopup', 2);enablePreviewIframe('PQSharingPopup');selectForm('PQSharingPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQSharingPopup','sharingPopup','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQSharingPopup_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQSharingPopup_DesignToolSwitch_main" onclick="selectForm('PQSharingPopup', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQSharingPopup', 'main', 'pq_active',['main', 'thank']);sharingPopupPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQSharingPopup_DesignToolSwitch_thank" onclick="selectForm('PQSharingPopup', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQSharingPopup', 'thank', 'pq_active',['main', 'thank']);sharingPopupthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQSharingPopup_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQSharingPopup_main_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQSharingPopup_main', 'desktop', 'pq_active',['desktop', 'mobile']);sharingPopupPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQSharingPopup_main_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQSharingPopup_main', 'mobile', 'pq_active',['desktop', 'mobile']);sharingPopupPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>SHARING POPUP</h3>					
						<!-- DESKTOP -->
						<div id="PQSharingPopup_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getSharingIcons('sharingPopup', $this->_options[sharingPopup][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[sharingPopup]),1);
								echo $this->getFormCodeForTool(
								array(
									 'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color',
									  'design_icons',
									  'form_icons',
									  'size_icons',
									  'space_icons',
									  'animation_icons',									  
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'								
									), 'sharingPopup', $this->_options[sharingPopup]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQSharingPopup_form_main_mobile" style="display:none;">					
						</div>
					</div>
					<div id="PQSharingPopup_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQSharingPopup_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQSharingPopup_thank', 'desktop', 'pq_active',['desktop', 'mobile']);sharingPopupthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQSharingPopup_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQSharingPopup_thank', 'mobile', 'pq_active',['desktop', 'mobile']);sharingPopupthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="sharingPopup[thank][enable]" id="sharingPopup_thank_enable" <?php if($this->_options[sharingPopup][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQSharingPopup_form_thank_proceed');sharingPopupthankPreview();">
						<label for="sharingPopup_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQSharingPopup_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   										
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',										
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'										
								)
								, 'sharingPopup[thank]', $this->_options[sharingPopup][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="sharingPopup[thank][socnet_block_type]" id="sharingPopup_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQSharingPopup_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[sharingPopup][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[sharingPopup][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQSharingPopup_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('sharingPopup[thank]', $this->_options[sharingPopup][thank]);
								?>
							</div>					
							<br>
							<div id="PQSharingPopup_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('sharingPopup[thank]', $this->_options[sharingPopup][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[sharingPopup][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'sharingPopup[thank]', $this->_options[sharingPopup][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[sharingPopup][thank][socnet_block_type];?>', 'PQSharingPopup_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="sharingPopup[thank][buttonBlock][type]" id="sharingPopup_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQSharingPopup_thank_buttonBlock_', ['redirect']);sharingPopupthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[sharingPopup][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQSharingPopup_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'									
									)
									, 'sharingPopup[thank][buttonBlock]', $this->_options[sharingPopup][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[sharingPopup][thank][buttonBlock][type];?>', 'PQSharingPopup_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[sharingPopup][thank][enable];?>', 'PQSharingPopup_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="sharingPopup[whitelabel]" onclick="getPreviewByActiveForm('sharingPopup', 'PQSharingPopup');" id="sharingPopup_whitelabel" <?php if($this->_options[sharingPopup][whitelabel] == 'on') echo 'checked';?>>
						<label for="sharingPopup_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQSharingPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('sharingPopup');" <?php if(trim($this->_options[sharingPopup][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQSharingPopup_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_sharing_popup.png', __FILE__);?>"> Sharing Popup</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('sharingPopup', array('delay', 'exit', 'scrolling'),  $this->_options[sharingPopup]);
					?>
					<?php
						echo $this->getPageOptions('sharingPopup', $this->_options[sharingPopup]);
					?>
					<?php
						echo $this->getGadgetRules('sharingPopup', $this->_options[sharingPopup]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQSharingPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQSharingPopup_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_sharing_popup.png', __FILE__);?>"> Sharing Popup</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->setProMailForm('sharingPopup', 0, $this->_options[sharingPopup]);
					?>
					<?php
						echo $this->getLockBlock('sharingPopup', $this->_options[sharingPopup]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END Sharing Popup Div -->
				<script>
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQSharingPopup\');
								selectStep(\'PQSharingPopup\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								sharingPopupSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					document.getElementById('PQLoader').style.display='none';
					checkTheme('sharingPopup', 'PQSharingPopup');
				</script>
				<?php
			}

			if($_GET[s_t] == 'sharingBar'){
				?>
				<!-- ********* Sharing Bar Div -->
			<div id="PQSharingBar" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="sharingBarSave">
			<input type="hidden" id="PQSharingBar_position" name="sharingBar[position]" value="<?php echo stripslashes($this->_options[sharingBar][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="sharingBarSettings('<?php echo stripslashes($this->_options[sharingBar][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQSharingBar_nav_step_1" onclick="selectStep('PQSharingBar', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQSharingBar_nav_step_2" onclick="selectStep('PQSharingBar', 2);enablePreviewIframe('PQSharingBar');selectForm('PQSharingBar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQSharingBar_nav_step_3" onclick="selectStep('PQSharingBar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQSharingBar_nav_step_4" onclick="selectStep('PQSharingBar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQSharingBar_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQSharingBar_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[sharingBar][theme]) $this->_options[sharingBar][theme] = $this->_default_themes[sharingBar];
				?>
				<input type="hidden" id="PQSharingBar_Current_Theme" name="sharingBar[theme]" value="<?php echo stripslashes($this->_options[sharingBar][theme]);?>">
				<input type="hidden" id="PQSharingBar_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[sharingBar] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[sharingBar][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQSharingBar', 2);enablePreviewIframe('PQSharingBar');selectForm('PQSharingBar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQSharingBar','sharingBar','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQSharingBar_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQSharingBar_DesignToolSwitch_main" onclick="selectForm('PQSharingBar', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQSharingBar', 'main', 'pq_active',['main', 'thank']);sharingBarPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQSharingBar_DesignToolSwitch_thank" onclick="selectForm('PQSharingBar', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQSharingBar', 'thank', 'pq_active',['main', 'thank']);sharingBarthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQSharingBar_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQSharingBar_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQSharingBar', 'main', 'desktop');addClassToSwitcher('PQSharingBar_main', 'desktop', 'pq_active',['desktop', 'mobile']);sharingBarPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQSharingBar_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQSharingBar', 'main', 'mobile');addClassToSwitcher('PQSharingBar_main', 'mobile', 'pq_active',['desktop', 'mobile']);sharingBarPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>SHARING BAR</h3>					
						<!-- DESKTOP -->
						<div id="PQSharingBar_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getSharingIcons('sharingBar', $this->_options[sharingBar][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[sharingBar]),1);
								echo $this->getFormCodeForTool(
								array(						
								  'background_color',
								  'second_background',
								  'background_image_src',
								  'title',
								  'head_font',
								  'head_font_size',
								  'head_color',          
								  'design_icons',
								  'form_icons',
								  'size_icons',
								  'space_icons',
								  'animation_icons',								  
								  'header_image_src',          
								  'close_icon_type', 
								  'close_text',
								  'close_icon_color',
								  'close_icon_animation',
								  'border_type',
								  'border_depth', 
								  'border_color',
								  'animation'
									), 'sharingBar', $this->_options[sharingBar]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQSharingBar_form_main_mobile" style="display:none;">
							<?php
								echo $this->getFormCodeForTool(
								array(						
									'mobile_position',
									'mobile_title'
									), 'sharingBar', $this->_options[sharingBar]);
							?>
						</div>
					</div>
					<div id="PQSharingBar_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQSharingBar_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQSharingBar_thank', 'desktop', 'pq_active',['desktop', 'mobile']);sharingBarthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQSharingBar_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQSharingBar_thank', 'mobile', 'pq_active',['desktop', 'mobile']);sharingBarthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="sharingBar[thank][enable]" id="sharingBar_thank_enable" <?php if($this->_options[sharingBar][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQSharingBar_form_thank_proceed');sharingBarthankPreview();">
						<label for="sharingBar_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQSharingBar_form_thank_proceed" style="display:none">						
							<?php					
								
								echo $this->getFormCodeForTool(
								array(
								'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   										
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'sharingBar[thank]', $this->_options[sharingBar][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="sharingBar[thank][socnet_block_type]" id="sharingBar_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQSharingBar_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[sharingBar][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[sharingBar][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQSharingBar_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('sharingBar[thank]', $this->_options[sharingBar][thank]);
								?>
							</div>					
							<br>
							<div id="PQSharingBar_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('sharingBar[thank]', $this->_options[sharingBar][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[sharingBar][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'sharingBar[thank]', $this->_options[sharingBar][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[sharingBar][thank][socnet_block_type];?>', 'PQSharingBar_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="sharingBar[thank][buttonBlock][type]" id="sharingBar_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQSharingBar_thank_buttonBlock_', ['redirect']);sharingBarthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[sharingBar][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQSharingBar_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'sharingBar[thank][buttonBlock]', $this->_options[sharingBar][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[sharingBar][thank][buttonBlock][type];?>', 'PQSharingBar_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[sharingBar][thank][enable];?>', 'PQSharingBar_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="sharingBar[whitelabel]" onclick="getPreviewByActiveForm('sharingBar', 'PQSharingBar');" id="sharingBar_whitelabel" <?php if($this->_options[sharingBar][whitelabel] == 'on') echo 'checked';?>>
						<label for="sharingBar_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQSharingBar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('sharingBar');" <?php if(trim($this->_options[sharingBar][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQSharingBar_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_sharing_bar.png', __FILE__);?>"> Sharing Bar</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('sharingBar', array('delay', 'scrolling'),  $this->_options[sharingBar]);
					?>
					<?php
						echo $this->getPageOptions('sharingBar', $this->_options[sharingBar]);
					?>
					<?php
						echo $this->getGadgetRules('sharingBar', $this->_options[sharingBar]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQSharingBar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQSharingBar_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_sharing_bar.png', __FILE__);?>"> Sharing Bar</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('sharingBar', $this->_options[sharingBar]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END Sharing Bar Div -->

				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQSharingBar\');
								selectStep(\'PQSharingBar\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								sharingBarSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					document.getElementById('PQLoader').style.display='none';
					checkTheme('sharingBar', 'PQSharingBar');
				</script>
				<?php
			}

			if($_GET[s_t] == 'sharingFloating'){
				?>
				
	<!-- ********* Sharing Floating Div -->
			<div id="PQSharingFloating" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="sharingFloatingSave">
			<input type="hidden" id="PQSharingFloating_position" name="sharingFloating[position]" value="<?php echo stripslashes($this->_options[sharingFloating][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="sharingFloatingSettings('<?php echo stripslashes($this->_options[sharingFloating][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQSharingFloating_nav_step_1" onclick="selectStep('PQSharingFloating', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQSharingFloating_nav_step_2" onclick="selectStep('PQSharingFloating', 2);enablePreviewIframe('PQSharingFloating');selectForm('PQSharingFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQSharingFloating_nav_step_3" onclick="selectStep('PQSharingFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQSharingFloating_nav_step_4" onclick="selectStep('PQSharingFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQSharingFloating_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQSharingFloating_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[sharingFloating][theme]) $this->_options[sharingFloating][theme] = $this->_default_themes[sharingFloating];
				?>
				<input type="hidden" id="PQSharingFloating_Current_Theme" name="sharingFloating[theme]" value="<?php echo stripslashes($this->_options[sharingFloating][theme]);?>">
				<input type="hidden" id="PQSharingFloating_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[sharingFloating] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[sharingFloating][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQSharingFloating', 2);enablePreviewIframe('PQSharingFloating');selectForm('PQSharingFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQSharingFloating','sharingFloating','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQSharingFloating_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQSharingFloating_DesignToolSwitch_main" onclick="selectForm('PQSharingFloating', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQSharingFloating', 'main', 'pq_active',['main', 'thank']);sharingFloatingPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQSharingFloating_DesignToolSwitch_thank" onclick="selectForm('PQSharingFloating', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQSharingFloating', 'thank', 'pq_active',['main', 'thank']);sharingFloatingthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQSharingFloating_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQSharingFloating_main_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQSharingFloating_main', 'desktop', 'pq_active',['desktop', 'mobile']);sharingFloatingPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQSharingFloating_main_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQSharingFloating_main', 'mobile', 'pq_active',['desktop', 'mobile']);sharingFloatingPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>SHARING FLOATING</h3>					
						<!-- DESKTOP -->
						<div id="PQSharingFloating_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getSharingIcons('sharingFloating', $this->_options[sharingFloating][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[sharingFloating]),1);
								echo $this->getFormCodeForTool(
								array(
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color',  									  
									 'design_icons',
									  'form_icons',
									  'size_icons',
									  'space_icons',
									  'animation_icons',									  
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'sharingFloating', $this->_options[sharingFloating]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQSharingFloating_form_main_mobile" style="display:none;">
							
						</div>
					</div>
					<div id="PQSharingFloating_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQSharingFloating_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQSharingFloating_thank', 'desktop', 'pq_active',['desktop', 'mobile']);sharingFloatingthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQSharingFloating_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQSharingFloating_thank', 'mobile', 'pq_active',['desktop', 'mobile']);sharingFloatingthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="sharingFloating[thank][enable]" id="sharingFloating_thank_enable" <?php if($this->_options[sharingFloating][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQSharingFloating_form_thank_proceed');sharingFloatingthankPreview();">
						<label for="sharingFloating_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQSharingFloating_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   										
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'sharingFloating[thank]', $this->_options[sharingFloating][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="sharingFloating[thank][socnet_block_type]" id="sharingFloating_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQSharingFloating_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[sharingFloating][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[sharingFloating][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQSharingFloating_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('sharingFloating[thank]', $this->_options[sharingFloating][thank]);
								?>
							</div>					
							<br>
							<div id="PQSharingFloating_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('sharingFloating[thank]', $this->_options[sharingFloating][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[sharingFloating][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'sharingFloating[thank]', $this->_options[sharingFloating][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[sharingFloating][thank][socnet_block_type];?>', 'PQSharingFloating_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="sharingFloating[thank][buttonBlock][type]" id="sharingFloating_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQSharingFloating_thank_buttonBlock_', ['redirect']);sharingFloatingthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[sharingFloating][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQSharingFloating_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'sharingFloating[thank][buttonBlock]', $this->_options[sharingFloating][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[sharingFloating][thank][buttonBlock][type];?>', 'PQSharingFloating_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[sharingFloating][thank][enable];?>', 'PQSharingFloating_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="sharingFloating[whitelabel]" onclick="getPreviewByActiveForm('sharingFloating', 'PQSharingFloating');" id="sharingFloating_whitelabel" <?php if($this->_options[sharingFloating][whitelabel] == 'on') echo 'checked';?>>
						<label for="sharingFloating_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQSharingFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('sharingFloating');" <?php if(trim($this->_options[sharingFloating][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>					
					</div>
				</div>
				<div id="PQSharingFloating_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_sharing_floating.png', __FILE__);?>"> Sharing Floating</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('sharingFloating', array('delay', 'scrolling'),  $this->_options[sharingFloating]);
					?>
					<?php
						echo $this->getPageOptions('sharingFloating', $this->_options[sharingFloating]);
					?>
					<?php
						echo $this->getGadgetRules('sharingFloating', $this->_options[sharingFloating]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQSharingFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQSharingFloating_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_sharing_floating.png', __FILE__);?>"> Sharing Floating</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('sharingFloating', $this->_options[sharingFloating]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END Sharing Floating Div -->
				<script>
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQSharingFloating\');
								selectStep(\'PQSharingFloating\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								sharingFloatingSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					document.getElementById('PQLoader').style.display='none';
					checkTheme('sharingFloating', 'PQSharingFloating');
				</script>
				<?php
			}

			if($_GET[s_t] == 'emailListBuilderPopup' || trim($providerRedirectByError)){
				?>
				<!-- ********* Email List Builder Div -->
			<div id="PQEmailListBuilderPopup" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="emailListBuilderPopupSave">
			<input type="hidden" id="PQEmailListBuilderPopup_position" name="emailListBuilderPopup[position]" value="<?php echo stripslashes($this->_options[emailListBuilderPopup][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="emailListBuilderPopupSettings('<?php echo stripslashes($this->_options[emailListBuilderPopup][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQEmailListBuilderPopup_nav_step_1" onclick="selectStep('PQEmailListBuilderPopup', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQEmailListBuilderPopup_nav_step_2" onclick="selectStep('PQEmailListBuilderPopup', 2);enablePreviewIframe('PQEmailListBuilderPopup');selectForm('PQEmailListBuilderPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQEmailListBuilderPopup_nav_step_3" onclick="selectStep('PQEmailListBuilderPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQEmailListBuilderPopup_nav_step_4" onclick="selectStep('PQEmailListBuilderPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQEmailListBuilderPopup_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQEmailListBuilderPopup_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[emailListBuilderPopup][theme]) $this->_options[emailListBuilderPopup][theme] = $this->_default_themes[emailListBuilderPopup];
				?>
				<input type="hidden" id="PQEmailListBuilderPopup_Current_Theme" name="emailListBuilderPopup[theme]" value="<?php echo stripslashes($this->_options[emailListBuilderPopup][theme]);?>">
				<input type="hidden" id="PQEmailListBuilderPopup_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[emailListBuilderPopup] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[emailListBuilderPopup][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQEmailListBuilderPopup', 2);enablePreviewIframe('PQEmailListBuilderPopup');selectForm('PQEmailListBuilderPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQEmailListBuilderPopup','emailListBuilderPopup','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQEmailListBuilderPopup_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQEmailListBuilderPopup_DesignToolSwitch_main" onclick="selectForm('PQEmailListBuilderPopup', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQEmailListBuilderPopup', 'main', 'pq_active',['main', 'thank']);emailListBuilderPopupPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQEmailListBuilderPopup_DesignToolSwitch_thank" onclick="selectForm('PQEmailListBuilderPopup', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQEmailListBuilderPopup', 'thank', 'pq_active',['main', 'thank']);emailListBuilderPopupthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQEmailListBuilderPopup_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQEmailListBuilderPopup_main_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQEmailListBuilderPopup_main', 'desktop', 'pq_active',['desktop', 'mobile']);emailListBuilderPopupPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQEmailListBuilderPopup_main_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQEmailListBuilderPopup_main', 'mobile', 'pq_active',['desktop', 'mobile']);emailListBuilderPopupPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>EMAIL LIST BUILDER POPUP</h3>					
						<!-- DESKTOP -->
						<div id="PQEmailListBuilderPopup_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'enter_email_text',  
									  'enter_name_text',  
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',									  
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'emailListBuilderPopup', $this->_options[emailListBuilderPopup]);
							?>							
						</div>
						<!-- MOBILE -->
						<div id="PQEmailListBuilderPopup_form_main_mobile" style="display:none;">											
						</div>
					</div>
					<div id="PQEmailListBuilderPopup_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQEmailListBuilderPopup_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQEmailListBuilderPopup_thank', 'desktop', 'pq_active',['desktop', 'mobile']);emailListBuilderPopupthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQEmailListBuilderPopup_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQEmailListBuilderPopup_thank', 'mobile', 'pq_active',['desktop', 'mobile']);emailListBuilderPopupthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="emailListBuilderPopup[thank][enable]" id="emailListBuilderPopup_thank_enable" <?php if($this->_options[emailListBuilderPopup][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQEmailListBuilderPopup_form_thank_proceed');emailListBuilderPopupthankPreview();">
						<label for="emailListBuilderPopup_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQEmailListBuilderPopup_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'emailListBuilderPopup[thank]', $this->_options[emailListBuilderPopup][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="emailListBuilderPopup[thank][socnet_block_type]" id="emailListBuilderPopup_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQEmailListBuilderPopup_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[emailListBuilderPopup][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[emailListBuilderPopup][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQEmailListBuilderPopup_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('emailListBuilderPopup[thank]', $this->_options[emailListBuilderPopup][thank]);
								?>
							</div>					
							<br>
							<div id="PQEmailListBuilderPopup_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('emailListBuilderPopup[thank]', $this->_options[emailListBuilderPopup][thank][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[emailListBuilderPopup][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'emailListBuilderPopup[thank]', $this->_options[emailListBuilderPopup][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[emailListBuilderPopup][thank][socnet_block_type];?>', 'PQEmailListBuilderPopup_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="emailListBuilderPopup[thank][buttonBlock][type]" id="emailListBuilderPopup_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQEmailListBuilderPopup_thank_buttonBlock_', ['redirect']);emailListBuilderPopupthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[emailListBuilderPopup][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQEmailListBuilderPopup_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'emailListBuilderPopup[thank][buttonBlock]', $this->_options[emailListBuilderPopup][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[emailListBuilderPopup][thank][buttonBlock][type];?>', 'PQEmailListBuilderPopup_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[emailListBuilderPopup][thank][enable];?>', 'PQEmailListBuilderPopup_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="emailListBuilderPopup[whitelabel]" onclick="getPreviewByActiveForm('emailListBuilderPopup', 'PQEmailListBuilderPopup');" id="emailListBuilderPopup_whitelabel" <?php if($this->_options[emailListBuilderPopup][whitelabel] == 'on') echo 'checked';?>>
						<label for="emailListBuilderPopup_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQEmailListBuilderPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('emailListBuilderPopup');" <?php if(trim($this->_options[emailListBuilderPopup][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>				
					</div>
				</div>
				<div id="PQEmailListBuilderPopup_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_collect_email_popup.png', __FILE__);?>"> Email List Builder Popup</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('emailListBuilderPopup', array('delay', 'exit','scrolling'),  $this->_options[emailListBuilderPopup]);
					?>
					<?php
						echo $this->getPageOptions('emailListBuilderPopup', $this->_options[emailListBuilderPopup]);
					?>
					<?php
						echo $this->getGadgetRules('emailListBuilderPopup', $this->_options[emailListBuilderPopup]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQEmailListBuilderPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQEmailListBuilderPopup_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_collect_email_popup.png', __FILE__);?>"> Email List Builder Popup</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getProviderBlock('emailListBuilderPopup', $this->_options[emailListBuilderPopup]);
					?>					
					<?php
						echo $this->getLockBlock('emailListBuilderPopup', $this->_options[emailListBuilderPopup]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* End Email List Builder Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQEmailListBuilderPopup\');
								selectStep(\'PQEmailListBuilderPopup\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								emailListBuilderPopupSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('emailListBuilderPopup', 'PQEmailListBuilderPopup');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'emailListBuilderBar' || trim($providerRedirectByError)){
				?>
				<!-- ********* Email List Builder Bar Div -->
			<div id="PQEmailListBuilderBar" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="emailListBuilderBarSave">
			<input type="hidden" id="PQEmailListBuilderBar_position" name="emailListBuilderBar[position]" value="<?php echo stripslashes($this->_options[emailListBuilderBar][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="emailListBuilderBarSettings('<?php echo stripslashes($this->_options[emailListBuilderBar][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQEmailListBuilderBar_nav_step_1" onclick="selectStep('PQEmailListBuilderBar', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQEmailListBuilderBar_nav_step_2" onclick="selectStep('PQEmailListBuilderBar', 2);enablePreviewIframe('PQEmailListBuilderBar');selectForm('PQEmailListBuilderBar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQEmailListBuilderBar_nav_step_3" onclick="selectStep('PQEmailListBuilderBar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQEmailListBuilderBar_nav_step_4" onclick="selectStep('PQEmailListBuilderBar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQEmailListBuilderBar_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQEmailListBuilderBar_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[emailListBuilderBar][theme]) $this->_options[emailListBuilderBar][theme] = $this->_default_themes[emailListBuilderBar];
				?>
				<input type="hidden" id="PQEmailListBuilderBar_Current_Theme" name="emailListBuilderBar[theme]" value="<?php echo stripslashes($this->_options[emailListBuilderBar][theme]);?>">
				<input type="hidden" id="PQEmailListBuilderBar_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[emailListBuilderBar] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[emailListBuilderBar][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQEmailListBuilderBar', 2);enablePreviewIframe('PQEmailListBuilderBar');selectForm('PQEmailListBuilderBar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQEmailListBuilderBar','emailListBuilderBar','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQEmailListBuilderBar_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQEmailListBuilderBar_DesignToolSwitch_main" onclick="selectForm('PQEmailListBuilderBar', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQEmailListBuilderBar', 'main', 'pq_active',['main', 'thank']);emailListBuilderBarPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQEmailListBuilderBar_DesignToolSwitch_thank" onclick="selectForm('PQEmailListBuilderBar', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQEmailListBuilderBar', 'thank', 'pq_active',['main', 'thank']);emailListBuilderBarthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQEmailListBuilderBar_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQEmailListBuilderBar_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQEmailListBuilderBar', 'main', 'desktop');addClassToSwitcher('PQEmailListBuilderBar_main', 'desktop', 'pq_active',['desktop', 'mobile']);emailListBuilderBarPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQEmailListBuilderBar_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQEmailListBuilderBar', 'main', 'mobile');addClassToSwitcher('PQEmailListBuilderBar_main', 'mobile', 'pq_active',['desktop', 'mobile']);emailListBuilderBarPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>EMAIL LIST BUILDER BAR</h3>					
						<!-- DESKTOP -->
						<div id="PQEmailListBuilderBar_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getFormCodeForTool(
								array(						
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
										'head_font',
										'head_font_size',
										'head_color',
									  'enter_email_text',  
									  'enter_name_text',  
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation'

									), 'emailListBuilderBar', $this->_options[emailListBuilderBar]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQEmailListBuilderBar_form_main_mobile" style="display:none;">
							<?php
								echo $this->getFormCodeForTool(
								array(						
									'mobile_position',
									'mobile_title'
									), 'emailListBuilderBar', $this->_options[emailListBuilderBar]);
							?>
						</div>
					</div>
					<div id="PQEmailListBuilderBar_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQEmailListBuilderBar_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQEmailListBuilderBar_thank', 'desktop', 'pq_active',['desktop', 'mobile']);emailListBuilderBarthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQEmailListBuilderBar_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQEmailListBuilderBar_thank', 'mobile', 'pq_active',['desktop', 'mobile']);emailListBuilderBarthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="emailListBuilderBar[thank][enable]" id="emailListBuilderBar_thank_enable" <?php if($this->_options[emailListBuilderBar][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQEmailListBuilderBar_form_thank_proceed');emailListBuilderBarthankPreview();">
						<label for="emailListBuilderBar_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQEmailListBuilderBar_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'emailListBuilderBar[thank]', $this->_options[emailListBuilderBar][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="emailListBuilderBar[thank][socnet_block_type]" id="emailListBuilderBar_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQEmailListBuilderBar_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[emailListBuilderBar][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[emailListBuilderBar][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQEmailListBuilderBar_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('emailListBuilderBar[thank]', $this->_options[emailListBuilderBar][thank]);
								?>
							</div>					
							<br>
							<div id="PQEmailListBuilderBar_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('emailListBuilderBar[thank]', $this->_options[emailListBuilderBar][thank][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[emailListBuilderBar][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'emailListBuilderBar[thank]', $this->_options[emailListBuilderBar][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[emailListBuilderBar][thank][socnet_block_type];?>', 'PQEmailListBuilderBar_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="emailListBuilderBar[thank][buttonBlock][type]" id="emailListBuilderBar_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQEmailListBuilderBar_thank_buttonBlock_', ['redirect']);emailListBuilderBarthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[emailListBuilderBar][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQEmailListBuilderBar_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'emailListBuilderBar[thank][buttonBlock]', $this->_options[emailListBuilderBar][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[emailListBuilderBar][thank][buttonBlock][type];?>', 'PQEmailListBuilderBar_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[emailListBuilderBar][thank][enable];?>', 'PQEmailListBuilderBar_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="emailListBuilderBar[whitelabel]" onclick="getPreviewByActiveForm('emailListBuilderBar', 'PQEmailListBuilderBar');" id="emailListBuilderBar_whitelabel" <?php if($this->_options[emailListBuilderBar][whitelabel] == 'on') echo 'checked';?>>
						<label for="emailListBuilderBar_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQEmailListBuilderBar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('emailListBuilderBar');" <?php if(trim($this->_options[emailListBuilderBar][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>				
					</div>
				</div>
				<div id="PQEmailListBuilderBar_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_collect_email_bar.png', __FILE__);?>"> Email List Builder Bar</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('emailListBuilderBar', array('delay', 'scrolling'),  $this->_options[emailListBuilderBar]);
					?>
					<?php
						echo $this->getPageOptions('emailListBuilderBar', $this->_options[emailListBuilderBar]);
					?>
					<?php
						echo $this->getGadgetRules('emailListBuilderBar', $this->_options[emailListBuilderBar]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQEmailListBuilderBar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQEmailListBuilderBar_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_collect_email_bar.png', __FILE__);?>"> Email List Builder Bar</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getProviderBlock('emailListBuilderBar', $this->_options[emailListBuilderBar]);
					?>
					<?php
						echo $this->getLockBlock('emailListBuilderBar', $this->_options[emailListBuilderBar]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* End Email List Builder Bar Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQEmailListBuilderBar\');
								selectStep(\'PQEmailListBuilderBar\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								emailListBuilderBarSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('emailListBuilderBar', 'PQEmailListBuilderBar');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'emailListBuilderFloating' || trim($providerRedirectByError)){
				?>
				<!-- ********* Email List Builder Floating Div -->
			<div id="PQEmailListBuilderFloating" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="emailListBuilderFloatingSave">
			<input type="hidden" id="PQEmailListBuilderFloating_position" name="emailListBuilderFloating[position]" value="<?php echo stripslashes($this->_options[emailListBuilderFloating][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="emailListBuilderFloatingSettings('<?php echo stripslashes($this->_options[emailListBuilderFloating][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQEmailListBuilderFloating_nav_step_1" onclick="selectStep('PQEmailListBuilderFloating', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQEmailListBuilderFloating_nav_step_2" onclick="selectStep('PQEmailListBuilderFloating', 2);enablePreviewIframe('PQEmailListBuilderFloating');selectForm('PQEmailListBuilderFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQEmailListBuilderFloating_nav_step_3" onclick="selectStep('PQEmailListBuilderFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQEmailListBuilderFloating_nav_step_4" onclick="selectStep('PQEmailListBuilderFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQEmailListBuilderFloating_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQEmailListBuilderFloating_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[emailListBuilderFloating][theme]) $this->_options[emailListBuilderFloating][theme] = $this->_default_themes[emailListBuilderFloating];
				?>
				<input type="hidden" id="PQEmailListBuilderFloating_Current_Theme" name="emailListBuilderFloating[theme]" value="<?php echo stripslashes($this->_options[emailListBuilderFloating][theme]);?>">
				<input type="hidden" id="PQEmailListBuilderFloating_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[emailListBuilderFloating] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[emailListBuilderFloating][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQEmailListBuilderFloating', 2);enablePreviewIframe('PQEmailListBuilderFloating');selectForm('PQEmailListBuilderFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQEmailListBuilderFloating','emailListBuilderFloating','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQEmailListBuilderFloating_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQEmailListBuilderFloating_DesignToolSwitch_main" onclick="selectForm('PQEmailListBuilderFloating', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQEmailListBuilderFloating', 'main', 'pq_active',['main', 'thank']);emailListBuilderFloatingPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQEmailListBuilderFloating_DesignToolSwitch_thank" onclick="selectForm('PQEmailListBuilderFloating', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQEmailListBuilderFloating', 'thank', 'pq_active',['main', 'thank']);emailListBuilderFloatingthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQEmailListBuilderFloating_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQEmailListBuilderFloating_main_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQEmailListBuilderFloating_main', 'desktop', 'pq_active',['desktop', 'mobile']);emailListBuilderFloatingPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQEmailListBuilderFloating_main_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQEmailListBuilderFloating_main', 'mobile', 'pq_active',['desktop', 'mobile']);emailListBuilderFloatingPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>EMAIL LIST BUILDER FLOATING</h3>					
						<!-- DESKTOP -->
						<div id="PQEmailListBuilderFloating_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'enter_email_text',  
									  'enter_name_text',  
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'emailListBuilderFloating', $this->_options[emailListBuilderFloating]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQEmailListBuilderFloating_form_main_mobile" style="display:none;">											
						</div>
					</div>
					<div id="PQEmailListBuilderFloating_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQEmailListBuilderFloating_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQEmailListBuilderFloating_thank', 'desktop', 'pq_active',['desktop', 'mobile']);emailListBuilderFloatingthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQEmailListBuilderFloating_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQEmailListBuilderFloating_thank', 'mobile', 'pq_active',['desktop', 'mobile']);emailListBuilderFloatingthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="emailListBuilderFloating[thank][enable]" id="emailListBuilderFloating_thank_enable" <?php if($this->_options[emailListBuilderFloating][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQEmailListBuilderFloating_form_thank_proceed');emailListBuilderFloatingthankPreview();">
						<label for="emailListBuilderFloating_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQEmailListBuilderFloating_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'emailListBuilderFloating[thank]', $this->_options[emailListBuilderFloating][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="emailListBuilderFloating[thank][socnet_block_type]" id="emailListBuilderFloating_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQEmailListBuilderFloating_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[emailListBuilderFloating][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[emailListBuilderFloating][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQEmailListBuilderFloating_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('emailListBuilderFloating[thank]', $this->_options[emailListBuilderFloating][thank]);
								?>
							</div>					
							<br>
							<div id="PQEmailListBuilderFloating_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('emailListBuilderFloating[thank]', $this->_options[emailListBuilderFloating][thank][socnet_with_pos], array('socnet_with_pos_error' => $sharingSocnetErrorArray[emailListBuilderFloating][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'emailListBuilderFloating[thank]', $this->_options[emailListBuilderFloating][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[emailListBuilderFloating][thank][socnet_block_type];?>', 'PQEmailListBuilderFloating_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="emailListBuilderFloating[thank][buttonBlock][type]" id="emailListBuilderFloating_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQEmailListBuilderFloating_thank_buttonBlock_', ['redirect']);emailListBuilderFloatingthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[emailListBuilderFloating][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQEmailListBuilderFloating_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'emailListBuilderFloating[thank][buttonBlock]', $this->_options[emailListBuilderFloating][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[emailListBuilderFloating][thank][buttonBlock][type];?>', 'PQEmailListBuilderFloating_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[emailListBuilderFloating][thank][enable];?>', 'PQEmailListBuilderFloating_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="emailListBuilderFloating[whitelabel]" onclick="getPreviewByActiveForm('emailListBuilderFloating', 'PQEmailListBuilderFloating');" id="emailListBuilderFloating_whitelabel" <?php if($this->_options[emailListBuilderFloating][whitelabel] == 'on') echo 'checked';?>>
						<label for="emailListBuilderFloating_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQEmailListBuilderFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('emailListBuilderFloating');" <?php if(trim($this->_options[emailListBuilderFloating][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>				
					</div>
				</div>
				<div id="PQEmailListBuilderFloating_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_sharing_floating.png', __FILE__);?>"> Email List Builder Floating</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('emailListBuilderFloating', array('delay', 'scrolling'),  $this->_options[emailListBuilderFloating]);
					?>
					<?php
						echo $this->getPageOptions('emailListBuilderFloating', $this->_options[emailListBuilderFloating]);
					?>
					<?php
						echo $this->getGadgetRules('emailListBuilderFloating', $this->_options[emailListBuilderFloating]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQEmailListBuilderFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQEmailListBuilderFloating_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_sharing_floating.png', __FILE__);?>"> Email List Builder Floating</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getProviderBlock('emailListBuilderFloating', $this->_options[emailListBuilderFloating]);
					?>
					<?php
						echo $this->getLockBlock('emailListBuilderFloating', $this->_options[emailListBuilderFloating]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* End Email List Builder Floating Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQEmailListBuilderFloating\');
								selectStep(\'PQEmailListBuilderFloating\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								emailListBuilderFloatingSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('emailListBuilderFloating', 'PQEmailListBuilderFloating');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'contactUsPopup' || trim($providerRedirectByError)){
				?>
				<!-- ********* Contact Us Popup Div -->
			<div id="PQcontactUsPopup" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="contactUsPopupSave">
			<input type="hidden" id="PQcontactUsPopup_position" name="contactUsPopup[position]" value="<?php echo stripslashes($this->_options[contactUsPopup][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="contactUsPopupSettings('<?php echo stripslashes($this->_options[contactUsPopup][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQcontactUsPopup_nav_step_1" onclick="selectStep('PQcontactUsPopup', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQcontactUsPopup_nav_step_2" onclick="selectStep('PQcontactUsPopup', 2);enablePreviewIframe('PQcontactUsPopup');selectForm('PQcontactUsPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQcontactUsPopup_nav_step_3" onclick="selectStep('PQcontactUsPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQcontactUsPopup_nav_step_4" onclick="selectStep('PQcontactUsPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQcontactUsPopup_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQcontactUsPopup_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[contactUsPopup][theme]) $this->_options[contactUsPopup][theme] = $this->_default_themes[contactUsPopup];
				?>
				<input type="hidden" id="PQcontactUsPopup_Current_Theme" name="contactUsPopup[theme]" value="<?php echo stripslashes($this->_options[contactUsPopup][theme]);?>">
				<input type="hidden" id="PQcontactUsPopup_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[contactUsPopup] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[contactUsPopup][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQcontactUsPopup', 2);enablePreviewIframe('PQcontactUsPopup');selectForm('PQcontactUsPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQcontactUsPopup','contactUsPopup','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQcontactUsPopup_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQcontactUsPopup_DesignToolSwitch_main" onclick="selectForm('PQcontactUsPopup', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQcontactUsPopup', 'main', 'pq_active',['main', 'thank']);contactUsPopupPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQcontactUsPopup_DesignToolSwitch_thank" onclick="selectForm('PQcontactUsPopup', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQcontactUsPopup', 'thank', 'pq_active',['main', 'thank']);contactUsPopupthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQcontactUsPopup_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQcontactUsPopup_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQcontactUsPopup', 'main', 'desktop');addClassToSwitcher('PQcontactUsPopup_main', 'desktop', 'pq_active',['desktop', 'mobile']);contactUsPopupPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQcontactUsPopup_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQcontactUsPopup', 'main', 'desktop');addClassToSwitcher('PQcontactUsPopup_main', 'mobile', 'pq_active',['desktop', 'mobile']);contactUsPopupPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>CONTACT US POPUP</h3>					
						<!-- DESKTOP -->
						<div id="PQcontactUsPopup_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'loader_text',    
									  'bookmark_background',
									  'bookmark_text_color',
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'enter_email_text',
									  'enter_name_text',  									    
									  'enter_message_text',  
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'contactUsPopup', $this->_options[contactUsPopup]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQcontactUsPopup_form_main_mobile" style="display:none;">
							
						</div>
					</div>
					<div id="PQcontactUsPopup_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQcontactUsPopup_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQcontactUsPopup_thank', 'desktop', 'pq_active',['desktop', 'mobile']);contactUsPopupthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQcontactUsPopup_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQcontactUsPopup_thank', 'mobile', 'pq_active',['desktop', 'mobile']);contactUsPopupthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="contactUsPopup[thank][enable]" id="contactUsPopup_thank_enable" <?php if($this->_options[contactUsPopup][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQcontactUsPopup_form_thank_proceed');contactUsPopupthankPreview();">
						<label for="contactUsPopup_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQcontactUsPopup_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'contactUsPopup[thank]', $this->_options[contactUsPopup][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="contactUsPopup[thank][socnet_block_type]" id="contactUsPopup_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQcontactUsPopup_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[contactUsPopup][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[contactUsPopup][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQcontactUsPopup_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('contactUsPopup[thank]', $this->_options[contactUsPopup][thank]);
								?>
							</div>					
							<br>
							<div id="PQcontactUsPopup_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('contactUsPopup[thank]', $this->_options[contactUsPopup][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[contactUsPopup][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'contactUsPopup[thank]', $this->_options[contactUsPopup][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[contactUsPopup][thank][socnet_block_type];?>', 'PQcontactUsPopup_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="contactUsPopup[thank][buttonBlock][type]" id="contactUsPopup_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQcontactUsPopup_thank_buttonBlock_', ['redirect']);contactUsPopupthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[contactUsPopup][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQcontactUsPopup_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'contactUsPopup[thank][buttonBlock]', $this->_options[contactUsPopup][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[contactUsPopup][thank][buttonBlock][type];?>', 'PQcontactUsPopup_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[contactUsPopup][thank][enable];?>', 'PQcontactUsPopup_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="contactUsPopup[whitelabel]" onclick="getPreviewByActiveForm('contactUsPopup', 'PQcontactUsPopup');" id="contactUsPopup_whitelabel" <?php if($this->_options[contactUsPopup][whitelabel] == 'on') echo 'checked';?>>
						<label for="contactUsPopup_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQcontactUsPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('contactUsPopup');" <?php if(trim($this->_options[contactUsPopup][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQcontactUsPopup_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_contact_us_popup.png', __FILE__);?>"> Contact Us Popup</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('contactUsPopup', array('delay'),  $this->_options[contactUsPopup]);
					?>
					<?php
						echo $this->getPageOptions('contactUsPopup', $this->_options[contactUsPopup]);
					?>
					<?php
						echo $this->getGadgetRules('contactUsPopup', $this->_options[contactUsPopup]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQcontactUsPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQcontactUsPopup_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_contact_us_popup.png', __FILE__);?>"> Contact Us Popup</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>
					<?php
							echo $this->setProMailForm('contactUsPopup', 1, $this->_options[contactUsPopup]);
					?>
					<?php
						echo $this->getLockBlock('contactUsPopup', $this->_options[contactUsPopup]);
					?>
					
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END Contact Us Popup Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQcontactUsPopup\');
								selectStep(\'PQcontactUsPopup\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								contactUsPopupSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('contactUsPopup', 'PQcontactUsPopup');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'contactUsFloating'){
				?>
				
	<!-- ********* Contact Us Floating Div -->
			<div id="PQcontactUsFloating" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="contactUsFloatingSave">
			<input type="hidden" id="PQcontactUsFloating_position" name="contactUsFloating[position]" value="<?php echo stripslashes($this->_options[contactUsFloating][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="contactUsFloatingSettings('<?php echo stripslashes($this->_options[contactUsFloating][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQcontactUsFloating_nav_step_1" onclick="selectStep('PQcontactUsFloating', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQcontactUsFloating_nav_step_2" onclick="selectStep('PQcontactUsFloating', 2);enablePreviewIframe('PQcontactUsFloating');selectForm('PQcontactUsFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQcontactUsFloating_nav_step_3" onclick="selectStep('PQcontactUsFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQcontactUsFloating_nav_step_4" onclick="selectStep('PQcontactUsFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQcontactUsFloating_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQcontactUsFloating_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[contactUsFloating][theme]) $this->_options[contactUsFloating][theme] = $this->_default_themes[contactUsFloating];
				?>
				<input type="hidden" id="PQcontactUsFloating_Current_Theme" name="contactUsFloating[theme]" value="<?php echo stripslashes($this->_options[contactUsFloating][theme]);?>">
				<input type="hidden" id="PQcontactUsFloating_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[contactUsFloating] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[contactUsFloating][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQcontactUsFloating', 2);enablePreviewIframe('PQcontactUsFloating');selectForm('PQcontactUsFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQcontactUsFloating','contactUsFloating','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQcontactUsFloating_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQcontactUsFloating_DesignToolSwitch_main" onclick="selectForm('PQcontactUsFloating', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQcontactUsFloating', 'main', 'pq_active',['main', 'thank']);contactUsFloatingPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQcontactUsFloating_DesignToolSwitch_thank" onclick="selectForm('PQcontactUsFloating', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQcontactUsFloating', 'thank', 'pq_active',['main', 'thank']);contactUsFloatingthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQcontactUsFloating_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQcontactUsFloating_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQcontactUsFloating', 'main', 'desktop');addClassToSwitcher('PQcontactUsFloating_main', 'desktop', 'pq_active',['desktop', 'mobile']);contactUsFloatingPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQcontactUsFloating_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQcontactUsFloating', 'main', 'desktop');addClassToSwitcher('PQcontactUsFloating_main', 'mobile', 'pq_active',['desktop', 'mobile']);contactUsFloatingPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>CONTACT US FLOATING</h3>					
						<!-- DESKTOP -->
						<div id="PQcontactUsFloating_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'enter_email_text',
									  'enter_name_text',  									    
									  'enter_message_text',  
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'contactUsFloating', $this->_options[contactUsFloating]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQcontactUsFloating_form_main_mobile" style="display:none;">
							
						</div>
					</div>
					<div id="PQcontactUsFloating_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQcontactUsFloating_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQcontactUsFloating_thank', 'desktop', 'pq_active',['desktop', 'mobile']);contactUsFloatingthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQcontactUsFloating_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQcontactUsFloating_thank', 'mobile', 'pq_active',['desktop', 'mobile']);contactUsFloatingthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="contactUsFloating[thank][enable]" id="contactUsFloating_thank_enable" <?php if($this->_options[contactUsFloating][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQcontactUsFloating_form_thank_proceed');contactUsFloatingthankPreview();">
						<label for="contactUsFloating_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQcontactUsFloating_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'contactUsFloating[thank]', $this->_options[contactUsFloating][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="contactUsFloating[thank][socnet_block_type]" id="contactUsFloating_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQcontactUsFloating_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[contactUsFloating][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[contactUsFloating][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQcontactUsFloating_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('contactUsFloating[thank]', $this->_options[contactUsFloating][thank]);
								?>
							</div>					
							<br>
							<div id="PQcontactUsFloating_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('contactUsFloating[thank]', $this->_options[contactUsFloating][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[contactUsFloating][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'contactUsFloating[thank]', $this->_options[contactUsFloating][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[contactUsFloating][thank][socnet_block_type];?>', 'PQcontactUsFloating_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="contactUsFloating[thank][buttonBlock][type]" id="contactUsFloating_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQcontactUsFloating_thank_buttonBlock_', ['redirect']);contactUsFloatingthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[contactUsFloating][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQcontactUsFloating_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'contactUsFloating[thank][buttonBlock]', $this->_options[contactUsFloating][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[contactUsFloating][thank][buttonBlock][type];?>', 'PQcontactUsFloating_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[contactUsFloating][thank][enable];?>', 'PQcontactUsFloating_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="contactUsFloating[whitelabel]" onclick="getPreviewByActiveForm('contactUsFloating', 'PQcontactUsFloating');" id="contactUsFloating_whitelabel" <?php if($this->_options[contactUsFloating][whitelabel] == 'on') echo 'checked';?>>
						<label for="contactUsFloating_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQcontactUsFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('contactUsFloating');" <?php if(trim($this->_options[contactUsFloating][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>				
					</div>
				</div>
				<div id="PQcontactUsFloating_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_contact_us_floating.png', __FILE__);?>"> Contact Us Floating</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('contactUsFloating', array('delay', 'scrolling'),  $this->_options[contactUsFloating]);
					?>
					<?php
						echo $this->getPageOptions('contactUsFloating', $this->_options[contactUsFloating]);
					?>
					<?php
						echo $this->getGadgetRules('contactUsFloating', $this->_options[contactUsFloating]);
					?>				
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQcontactUsFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQcontactUsFloating_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_contact_us_floating.png', __FILE__);?>"> Contact Us Floating</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
							echo $this->setProMailForm('contactUsFloating', 1, $this->_options[contactUsFloating]);
					?>
					<?php
						echo $this->getLockBlock('contactUsFloating', $this->_options[contactUsFloating]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END Contact Us FLOATING Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQcontactUsFloating\');
								selectStep(\'PQcontactUsFloating\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								contactUsFloatingSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('contactUsFloating', 'PQcontactUsFloating');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'promotePopup'){
				?>
				<!-- ********* promotePopup Div -->
			<div id="PQPromotePopup" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="promotePopupSave">
			<input type="hidden" id="PQPromotePopup_position" name="promotePopup[position]" value="<?php echo stripslashes($this->_options[promotePopup][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="promotePopupSettings('<?php echo stripslashes($this->_options[promotePopup][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQPromotePopup_nav_step_1" onclick="selectStep('PQPromotePopup', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQPromotePopup_nav_step_2" onclick="selectStep('PQPromotePopup', 2);enablePreviewIframe('PQPromotePopup');selectForm('PQPromotePopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQPromotePopup_nav_step_3" onclick="selectStep('PQPromotePopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQPromotePopup_nav_step_4" onclick="selectStep('PQPromotePopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQPromotePopup_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQPromotePopup_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[promotePopup][theme]) $this->_options[promotePopup][theme] = $this->_default_themes[promotePopup];
				?>
				<input type="hidden" id="PQPromotePopup_Current_Theme" name="promotePopup[theme]" value="<?php echo stripslashes($this->_options[promotePopup][theme]);?>">
				<input type="hidden" id="PQPromotePopup_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[promotePopup] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[promotePopup][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQPromotePopup', 2);enablePreviewIframe('PQPromotePopup');selectForm('PQPromotePopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQPromotePopup','promotePopup','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQPromotePopup_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQPromotePopup_DesignToolSwitch_main" onclick="selectForm('PQPromotePopup', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQPromotePopup', 'main', 'pq_active',['main', 'thank']);promotePopupPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQPromotePopup_DesignToolSwitch_thank" onclick="selectForm('PQPromotePopup', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQPromotePopup', 'thank', 'pq_active',['main', 'thank']);promotePopupthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQPromotePopup_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQPromotePopup_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQPromotePopup', 'main', 'desktop');addClassToSwitcher('PQPromotePopup_main', 'desktop', 'pq_active',['desktop', 'mobile']);promotePopupPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQPromotePopup_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQPromotePopup', 'main', 'desktop');addClassToSwitcher('PQPromotePopup_main', 'mobile', 'pq_active',['desktop', 'mobile']);promotePopupPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>PROMOTE POPUP</h3>					
						<!-- DESKTOP -->
						<div id="PQPromotePopup_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'url',
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'promotePopup', $this->_options[promotePopup]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQPromotePopup_form_main_mobile" style="display:none;">
							
						</div>
					</div>
					<div id="PQPromotePopup_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQPromotePopup_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQPromotePopup_thank', 'desktop', 'pq_active',['desktop', 'mobile']);promotePopupthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQPromotePopup_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQPromotePopup_thank', 'mobile', 'pq_active',['desktop', 'mobile']);promotePopupthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="promotePopup[thank][enable]" id="promotePopup_thank_enable" <?php if($this->_options[promotePopup][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQPromotePopup_form_thank_proceed');promotePopupthankPreview();">
						<label for="promotePopup_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQPromotePopup_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'promotePopup[thank]', $this->_options[promotePopup][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="promotePopup[thank][socnet_block_type]" id="promotePopup_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQPromotePopup_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[promotePopup][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[promotePopup][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQPromotePopup_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('promotePopup[thank]', $this->_options[promotePopup][thank]);
								?>
							</div>					
							<br>
							<div id="PQPromotePopup_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('promotePopup[thank]', $this->_options[promotePopup][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[promotePopup][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'promotePopup[thank]', $this->_options[promotePopup][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[promotePopup][thank][socnet_block_type];?>', 'PQPromotePopup_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="promotePopup[thank][buttonBlock][type]" id="promotePopup_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQPromotePopup_thank_buttonBlock_', ['redirect']);promotePopupthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[promotePopup][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQPromotePopup_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'promotePopup[thank][buttonBlock]', $this->_options[promotePopup][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[promotePopup][thank][buttonBlock][type];?>', 'PQPromotePopup_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[promotePopup][thank][enable];?>', 'PQPromotePopup_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="promotePopup[whitelabel]" onclick="getPreviewByActiveForm('promotePopup', 'PQPromotePopup');" id="promotePopup_whitelabel" <?php if($this->_options[promotePopup][whitelabel] == 'on') echo 'checked';?>>
						<label for="promotePopup_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQPromotePopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('promotePopup');" <?php if(trim($this->_options[promotePopup][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQPromotePopup_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_promote_popup.png', __FILE__);?>"> Promote Popup</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('promotePopup', array('delay', 'exit','scrolling'),  $this->_options[promotePopup]);
					?>
					<?php
						echo $this->getPageOptions('promotePopup', $this->_options[promotePopup]);
					?>
					<?php
						echo $this->getGadgetRules('promotePopup', $this->_options[promotePopup]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQPromotePopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQPromotePopup_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_promote_popup.png', __FILE__);?>"> Promote Popup</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('promotePopup', $this->_options[promotePopup]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END promotePopup Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQPromotePopup\');
								selectStep(\'PQPromotePopup\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								promotePopupSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('promotePopup', 'PQPromotePopup');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'promoteBar'){
				?>
				<!-- ********* promoteBar Div -->
			<div id="PQPromoteBar" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="promoteBarSave">
			<input type="hidden" id="PQPromoteBar_position" name="promoteBar[position]" value="<?php echo stripslashes($this->_options[promoteBar][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="promoteBarSettings('<?php echo stripslashes($this->_options[promoteBar][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQPromoteBar_nav_step_1" onclick="selectStep('PQPromoteBar', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQPromoteBar_nav_step_2" onclick="selectStep('PQPromoteBar', 2);enablePreviewIframe('PQPromoteBar');selectForm('PQPromoteBar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQPromoteBar_nav_step_3" onclick="selectStep('PQPromoteBar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQPromoteBar_nav_step_4" onclick="selectStep('PQPromoteBar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQPromoteBar_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQPromoteBar_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[promoteBar][theme]) $this->_options[promoteBar][theme] = $this->_default_themes[promoteBar];
				?>
				<input type="hidden" id="PQPromoteBar_Current_Theme" name="promoteBar[theme]" value="<?php echo stripslashes($this->_options[promoteBar][theme]);?>">
				<input type="hidden" id="PQPromoteBar_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[promoteBar] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[promoteBar][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQPromoteBar', 2);enablePreviewIframe('PQPromoteBar');selectForm('PQPromoteBar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQPromoteBar','promoteBar','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQPromoteBar_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQPromoteBar_DesignToolSwitch_main" onclick="selectForm('PQPromoteBar', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQPromoteBar', 'main', 'pq_active',['main', 'thank']);promoteBarPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQPromoteBar_DesignToolSwitch_thank" onclick="selectForm('PQPromoteBar', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQPromoteBar', 'thank', 'pq_active',['main', 'thank']);promoteBarthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQPromoteBar_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQPromoteBar_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQPromoteBar', 'main', 'desktop');addClassToSwitcher('PQPromoteBar_main', 'desktop', 'pq_active',['desktop', 'mobile']);promoteBarPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQPromoteBar_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQPromoteBar', 'main', 'mobile');addClassToSwitcher('PQPromoteBar_main', 'mobile', 'pq_active',['desktop', 'mobile']);promoteBarPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>PROMOTE BAR</h3>					
						<!-- DESKTOP -->
						<div id="PQPromoteBar_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
										'head_font',
										'head_font_size',
										'head_color',
									  'url',
									   'button_text',
									   'button_font',
									   'button_font_size',
									   'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									), 'promoteBar', $this->_options[promoteBar]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQPromoteBar_form_main_mobile" style="display:none;">
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									'mobile_position',
									 'mobile_title'
									), 'promoteBar', $this->_options[promoteBar]);
							?>
						</div>
					</div>
					<div id="PQPromoteBar_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQPromoteBar_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQPromoteBar_thank', 'desktop', 'pq_active',['desktop', 'mobile']);promoteBarthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQPromoteBar_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQPromoteBar_thank', 'mobile', 'pq_active',['desktop', 'mobile']);promoteBarthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="promoteBar[thank][enable]" id="promoteBar_thank_enable" <?php if($this->_options[promoteBar][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQPromoteBar_form_thank_proceed');promoteBarthankPreview();">
						<label for="promoteBar_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQPromoteBar_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'promoteBar[thank]', $this->_options[promoteBar][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="promoteBar[thank][socnet_block_type]" id="promoteBar_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQPromoteBar_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[promoteBar][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[promoteBar][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQPromoteBar_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('promoteBar[thank]', $this->_options[promoteBar][thank]);
								?>
							</div>					
							<br>
							<div id="PQPromoteBar_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('promoteBar[thank]', $this->_options[promoteBar][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[promoteBar][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'promoteBar[thank]', $this->_options[promoteBar][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[promoteBar][thank][socnet_block_type];?>', 'PQPromoteBar_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="promoteBar[thank][buttonBlock][type]" id="promoteBar_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQPromoteBar_thank_buttonBlock_', ['redirect']);promoteBarthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[promoteBar][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQPromoteBar_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'promoteBar[thank][buttonBlock]', $this->_options[promoteBar][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[promoteBar][thank][buttonBlock][type];?>', 'PQPromoteBar_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[promoteBar][thank][enable];?>', 'PQPromoteBar_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="promoteBar[whitelabel]" onclick="getPreviewByActiveForm('promoteBar', 'PQPromoteBar');" id="promoteBar_whitelabel" <?php if($this->_options[promoteBar][whitelabel] == 'on') echo 'checked';?>>
						<label for="promoteBar_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQPromoteBar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('promoteBar');" <?php if(trim($this->_options[promoteBar][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQPromoteBar_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_promote_bar.png', __FILE__);?>"> Promote Bar</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('promoteBar', array('delay','scrolling'),  $this->_options[promoteBar]);
					?>
					<?php
						echo $this->getPageOptions('promoteBar', $this->_options[promoteBar]);
					?>
					<?php
						echo $this->getGadgetRules('promoteBar', $this->_options[promoteBar]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQPromoteBar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQPromoteBar_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_promote_bar.png', __FILE__);?>"> Promote Bar</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('promoteBar', $this->_options[promoteBar]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END promoteBar Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQPromoteBar\');
								selectStep(\'PQPromoteBar\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								promoteBarSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('promoteBar', 'PQPromoteBar');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'promoteFloating'){
				?>
				<!-- ********* promoteFloating Div -->
			<div id="PQPromoteFloating" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="promoteFloatingSave">
			<input type="hidden" id="PQPromoteFloating_position" name="promoteFloating[position]" value="<?php echo stripslashes($this->_options[promoteFloating][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="promoteFloatingSettings('<?php echo stripslashes($this->_options[promoteFloating][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQPromoteFloating_nav_step_1" onclick="selectStep('PQPromoteFloating', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQPromoteFloating_nav_step_2" onclick="selectStep('PQPromoteFloating', 2);enablePreviewIframe('PQPromoteFloating');selectForm('PQPromoteFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQPromoteFloating_nav_step_3" onclick="selectStep('PQPromoteFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQPromoteFloating_nav_step_4" onclick="selectStep('PQPromoteFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQPromoteFloating_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQPromoteFloating_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[promoteFloating][theme]) $this->_options[promoteFloating][theme] = $this->_default_themes[promoteFloating];
				?>
				<input type="hidden" id="PQPromoteFloating_Current_Theme" name="promoteFloating[theme]" value="<?php echo stripslashes($this->_options[promoteFloating][theme]);?>">
				<input type="hidden" id="PQPromoteFloating_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[promoteFloating] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[promoteFloating][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQPromoteFloating', 2);enablePreviewIframe('PQPromoteFloating');selectForm('PQPromoteFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQPromoteFloating','promoteFloating','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQPromoteFloating_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQPromoteFloating_DesignToolSwitch_main" onclick="selectForm('PQPromoteFloating', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQPromoteFloating', 'main', 'pq_active',['main', 'thank']);promoteFloatingPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQPromoteFloating_DesignToolSwitch_thank" onclick="selectForm('PQPromoteFloating', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQPromoteFloating', 'thank', 'pq_active',['main', 'thank']);promoteFloatingthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQPromoteFloating_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQPromoteFloating_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQPromoteFloating', 'main', 'desktop');addClassToSwitcher('PQPromoteFloating_main', 'desktop', 'pq_active',['desktop', 'mobile']);promoteFloatingPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQPromoteFloating_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQPromoteFloating', 'main', 'desktop');addClassToSwitcher('PQPromoteFloating_main', 'mobile', 'pq_active',['desktop', 'mobile']);promoteFloatingPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>PROMOTE FLOATING</h3>					
						<!-- DESKTOP -->
						<div id="PQPromoteFloating_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'url',
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'promoteFloating', $this->_options[promoteFloating]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQPromoteFloating_form_main_mobile" style="display:none;">
							
						</div>
					</div>
					<div id="PQPromoteFloating_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQPromoteFloating_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQPromoteFloating_thank', 'desktop', 'pq_active',['desktop', 'mobile']);promoteFloatingthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQPromoteFloating_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQPromoteFloating_thank', 'mobile', 'pq_active',['desktop', 'mobile']);promoteFloatingthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="promoteFloating[thank][enable]" id="promoteFloating_thank_enable" <?php if($this->_options[promoteFloating][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQPromoteFloating_form_thank_proceed');promoteFloatingthankPreview();">
						<label for="promoteFloating_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQPromoteFloating_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'promoteFloating[thank]', $this->_options[promoteFloating][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="promoteFloating[thank][socnet_block_type]" id="promoteFloating_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQPromoteFloating_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[promoteFloating][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[promoteFloating][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQPromoteFloating_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('promoteFloating[thank]', $this->_options[promoteFloating][thank]);
								?>
							</div>					
							<br>
							<div id="PQPromoteFloating_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('promoteFloating[thank]', $this->_options[promoteFloating][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[promoteFloating][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'promoteFloating[thank]', $this->_options[promoteFloating][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[promoteFloating][thank][socnet_block_type];?>', 'PQPromoteFloating_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="promoteFloating[thank][buttonBlock][type]" id="promoteFloating_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQPromoteFloating_thank_buttonBlock_', ['redirect']);promoteFloatingthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[promoteFloating][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQPromoteFloating_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'promoteFloating[thank][buttonBlock]', $this->_options[promoteFloating][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[promoteFloating][thank][buttonBlock][type];?>', 'PQPromoteFloating_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[promoteFloating][thank][enable];?>', 'PQPromoteFloating_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="promoteFloating[whitelabel]" onclick="getPreviewByActiveForm('promoteFloating', 'PQPromoteFloating');" id="promoteFloating_whitelabel" <?php if($this->_options[promoteFloating][whitelabel] == 'on') echo 'checked';?>>
						<label for="promoteFloating_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQPromoteFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">	
						<a onclick="clearLastChanges('promoteFloating');" <?php if(trim($this->_options[promoteFloating][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>					
					</div>
				</div>
				<div id="PQPromoteFloating_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_promote_floating.png', __FILE__);?>"> Promote Floating</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('promoteFloating', array('delay', 'scrolling'),  $this->_options[promoteFloating]);
					?>
					<?php
						echo $this->getPageOptions('promoteFloating', $this->_options[promoteFloating]);
					?>
					<?php
						echo $this->getGadgetRules('promoteFloating', $this->_options[promoteFloating]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQPromoteFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQPromoteFloating_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_promote_floating.png', __FILE__);?>"> Promote Floating</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('promoteFloating', $this->_options[promoteFloating]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END promoteFloating Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQPromoteFloating\');
								selectStep(\'PQPromoteFloating\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								promoteFloatingSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('promoteFloating', 'PQPromoteFloating');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'callMePopup'){
				?>
				
	<!-- ********* callMePopup Div -->
			<div id="PQCallMePopup" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="callMePopupSave">
			<input type="hidden" id="PQCallMePopup_position" name="callMePopup[position]" value="<?php echo stripslashes($this->_options[callMePopup][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="callMePopupSettings('<?php echo stripslashes($this->_options[callMePopup][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQCallMePopup_nav_step_1" onclick="selectStep('PQCallMePopup', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQCallMePopup_nav_step_2" onclick="selectStep('PQCallMePopup', 2);enablePreviewIframe('PQCallMePopup');selectForm('PQCallMePopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQCallMePopup_nav_step_3" onclick="selectStep('PQCallMePopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQCallMePopup_nav_step_4" onclick="selectStep('PQCallMePopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQCallMePopup_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQCallMePopup_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[callMePopup][theme]) $this->_options[callMePopup][theme] = $this->_default_themes[callMePopup];
				?>
				<input type="hidden" id="PQCallMePopup_Current_Theme" name="callMePopup[theme]" value="<?php echo stripslashes($this->_options[callMePopup][theme]);?>">
				<input type="hidden" id="PQCallMePopup_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[callMePopup] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[callMePopup][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQCallMePopup', 2);enablePreviewIframe('PQCallMePopup');selectForm('PQCallMePopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQCallMePopup','callMePopup','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQCallMePopup_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQCallMePopup_DesignToolSwitch_main" onclick="selectForm('PQCallMePopup', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQCallMePopup', 'main', 'pq_active',['main', 'thank']);callMePopupPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQCallMePopup_DesignToolSwitch_thank" onclick="selectForm('PQCallMePopup', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQCallMePopup', 'thank', 'pq_active',['main', 'thank']);callMePopupthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQCallMePopup_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQCallMePopup_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQCallMePopup', 'main', 'desktop');addClassToSwitcher('PQCallMePopup_main', 'desktop', 'pq_active',['desktop', 'mobile']);callMePopupPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQCallMePopup_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQCallMePopup', 'main', 'desktop');addClassToSwitcher('PQCallMePopup_main', 'mobile', 'pq_active',['desktop', 'mobile']);callMePopupPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>CALL ME POPUP</h3>					
						<!-- DESKTOP -->
						<div id="PQCallMePopup_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'loader_text',    
									  'bookmark_background',
									  'bookmark_text_color',
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'enter_name_text',  
									  'enter_phone_text',									  
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'

									), 'callMePopup', $this->_options[callMePopup]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQCallMePopup_form_main_mobile" style="display:none;">
							
						</div>
					</div>
					<div id="PQCallMePopup_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQCallMePopup_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQCallMePopup_thank', 'desktop', 'pq_active',['desktop', 'mobile']);callMePopupthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQCallMePopup_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQCallMePopup_thank', 'mobile', 'pq_active',['desktop', 'mobile']);callMePopupthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="callMePopup[thank][enable]" id="callMePopup_thank_enable" <?php if($this->_options[callMePopup][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQCallMePopup_form_thank_proceed');callMePopupthankPreview();">
						<label for="callMePopup_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQCallMePopup_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'callMePopup[thank]', $this->_options[callMePopup][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="callMePopup[thank][socnet_block_type]" id="callMePopup_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQCallMePopup_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[callMePopup][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[callMePopup][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQCallMePopup_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('callMePopup[thank]', $this->_options[callMePopup][thank]);
								?>
							</div>					
							<br>
							<div id="PQCallMePopup_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('callMePopup[thank]', $this->_options[callMePopup][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[callMePopup][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'callMePopup[thank]', $this->_options[callMePopup][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[callMePopup][thank][socnet_block_type];?>', 'PQCallMePopup_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="callMePopup[thank][buttonBlock][type]" id="callMePopup_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQCallMePopup_thank_buttonBlock_', ['redirect']);callMePopupthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[callMePopup][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQCallMePopup_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'callMePopup[thank][buttonBlock]', $this->_options[callMePopup][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[callMePopup][thank][buttonBlock][type];?>', 'PQCallMePopup_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[callMePopup][thank][enable];?>', 'PQCallMePopup_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="callMePopup[whitelabel]" onclick="getPreviewByActiveForm('callMePopup', 'PQCallMePopup');" id="callMePopup_whitelabel" <?php if($this->_options[callMePopup][whitelabel] == 'on') echo 'checked';?>>
						<label for="callMePopup_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQCallMePopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">	
						<a onclick="clearLastChanges('callMePopup');" <?php if(trim($this->_options[callMePopup][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQCallMePopup_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_call_me_popup.png', __FILE__);?>"> Call Me Popup</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('callMePopup', array('delay','scrolling'),  $this->_options[callMePopup]);
					?>
					<?php
						echo $this->getPageOptions('callMePopup', $this->_options[callMePopup]);
					?>
					<?php
						echo $this->getGadgetRules('callMePopup', $this->_options[callMePopup]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQCallMePopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQCallMePopup_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_call_me_popup.png', __FILE__);?>"> Call Me Popup</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>
					<?php
							echo $this->setProMailForm('callMePopup', 1, $this->_options[callMePopup]);
					?>
					<?php
						echo $this->getLockBlock('callMePopup', $this->_options[callMePopup]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END callMePopup Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQCallMePopup\');
								selectStep(\'PQCallMePopup\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								callMePopupSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('callMePopup', 'PQCallMePopup');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'callMeFloating'){
				?>
				<!-- ********* callMeFloating Div -->
			<div id="PQCallMeFloating" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="callMeFloatingSave">
			<input type="hidden" id="PQCallMeFloating_position" name="callMeFloating[position]" value="<?php echo stripslashes($this->_options[callMeFloating][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="callMeFloatingSettings('<?php echo stripslashes($this->_options[callMeFloating][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQCallMeFloating_nav_step_1" onclick="selectStep('PQCallMeFloating', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQCallMeFloating_nav_step_2" onclick="selectStep('PQCallMeFloating', 2);enablePreviewIframe('PQCallMeFloating');selectForm('PQCallMeFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQCallMeFloating_nav_step_3" onclick="selectStep('PQCallMeFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQCallMeFloating_nav_step_4" onclick="selectStep('PQCallMeFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQCallMeFloating_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQCallMeFloating_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[callMeFloating][theme]) $this->_options[callMeFloating][theme] = $this->_default_themes[callMeFloating];
				?>
				<input type="hidden" id="PQCallMeFloating_Current_Theme" name="callMeFloating[theme]" value="<?php echo stripslashes($this->_options[callMeFloating][theme]);?>">
				<input type="hidden" id="PQCallMeFloating_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[callMeFloating] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[callMeFloating][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQCallMeFloating', 2);enablePreviewIframe('PQCallMeFloating');selectForm('PQCallMeFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQCallMeFloating','callMeFloating','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQCallMeFloating_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQCallMeFloating_DesignToolSwitch_main" onclick="selectForm('PQCallMeFloating', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQCallMeFloating', 'main', 'pq_active',['main', 'thank']);callMeFloatingPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQCallMeFloating_DesignToolSwitch_thank" onclick="selectForm('PQCallMeFloating', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQCallMeFloating', 'thank', 'pq_active',['main', 'thank']);callMeFloatingthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQCallMeFloating_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQCallMeFloating_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQCallMeFloating', 'main', 'desktop');addClassToSwitcher('PQCallMeFloating_main', 'desktop', 'pq_active',['desktop', 'mobile']);callMeFloatingPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQCallMeFloating_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQCallMeFloating', 'main', 'desktop');addClassToSwitcher('PQCallMeFloating_main', 'mobile', 'pq_active',['desktop', 'mobile']);callMeFloatingPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>CALL ME FLOATING</h3>					
						<!-- DESKTOP -->
						<div id="PQCallMeFloating_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color', 
									  'enter_name_text',  
									  'enter_phone_text',									  
									  'button_text',
									  'button_font',
									  'button_font_size',
									  'button_text_color',
									  'button_color',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'callMeFloating', $this->_options[callMeFloating]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQCallMeFloating_form_main_mobile" style="display:none;">
							
						</div>
					</div>
					<div id="PQCallMeFloating_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQCallMeFloating_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQCallMeFloating_thank', 'desktop', 'pq_active',['desktop', 'mobile']);callMeFloatingthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQCallMeFloating_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQCallMeFloating_thank', 'mobile', 'pq_active',['desktop', 'mobile']);callMeFloatingthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="callMeFloating[thank][enable]" id="callMeFloating_thank_enable" <?php if($this->_options[callMeFloating][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQCallMeFloating_form_thank_proceed');callMeFloatingthankPreview();">
						<label for="callMeFloating_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQCallMeFloating_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'callMeFloating[thank]', $this->_options[callMeFloating][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="callMeFloating[thank][socnet_block_type]" id="callMeFloating_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQCallMeFloating_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[callMeFloating][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[callMeFloating][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQCallMeFloating_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('callMeFloating[thank]', $this->_options[callMeFloating][thank]);
								?>
							</div>					
							<br>
							<div id="PQCallMeFloating_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('callMeFloating[thank]', $this->_options[callMeFloating][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[callMeFloating][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'callMeFloating[thank]', $this->_options[callMeFloating][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[callMeFloating][thank][socnet_block_type];?>', 'PQCallMeFloating_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="callMeFloating[thank][buttonBlock][type]" id="callMeFloating_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQCallMeFloating_thank_buttonBlock_', ['redirect']);callMeFloatingthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[callMeFloating][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQCallMeFloating_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'callMeFloating[thank][buttonBlock]', $this->_options[callMeFloating][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[callMeFloating][thank][buttonBlock][type];?>', 'PQCallMeFloating_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[callMeFloating][thank][enable];?>', 'PQCallMeFloating_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="callMeFloating[whitelabel]" onclick="getPreviewByActiveForm('callMeFloating', 'PQCallMeFloating');" id="callMeFloating_whitelabel" <?php if($this->_options[callMeFloating][whitelabel] == 'on') echo 'checked';?>>
						<label for="callMeFloating_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQCallMeFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('callMeFloating');" <?php if(trim($this->_options[callMeFloating][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQCallMeFloating_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_call_me_floating.png', __FILE__);?>"> Call Me Floating</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('callMeFloating', array('delay', 'scrolling'),  $this->_options[callMeFloating]);
					?>
					<?php
						echo $this->getPageOptions('callMeFloating', $this->_options[callMeFloating]);
					?>
					<?php
						echo $this->getGadgetRules('callMeFloating', $this->_options[callMeFloating]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQCallMeFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQCallMeFloating_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_call_me_floating.png', __FILE__);?>"> Call Me Floating</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
							echo $this->setProMailForm('callMeFloating', 1, $this->_options[callMeFloating]);
					?>
					<?php
						echo $this->getLockBlock('callMeFloating', $this->_options[callMeFloating]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END callMeFloating Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQCallMeFloating\');
								selectStep(\'PQCallMeFloating\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								callMeFloatingSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('callMeFloating', 'PQCallMeFloating');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'followPopup'){
				?>
				
	<!-- ********* followPopup Div -->
			<div id="PQFollowPopup" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="followPopupSave">
			<input type="hidden" id="PQFollowPopup_position" name="followPopup[position]" value="<?php echo stripslashes($this->_options[followPopup][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="followPopupSettings('<?php echo stripslashes($this->_options[followPopup][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQFollowPopup_nav_step_1" onclick="selectStep('PQFollowPopup', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQFollowPopup_nav_step_2" onclick="selectStep('PQFollowPopup', 2);enablePreviewIframe('PQFollowPopup');selectForm('PQFollowPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQFollowPopup_nav_step_3" onclick="selectStep('PQFollowPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQFollowPopup_nav_step_4" onclick="selectStep('PQFollowPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQFollowPopup_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQFollowPopup_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[followPopup][theme]) $this->_options[followPopup][theme] = $this->_default_themes[followPopup];
				?>
				<input type="hidden" id="PQFollowPopup_Current_Theme" name="followPopup[theme]" value="<?php echo stripslashes($this->_options[followPopup][theme]);?>">
				<input type="hidden" id="PQFollowPopup_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[followPopup] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[followPopup][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQFollowPopup', 2);enablePreviewIframe('PQFollowPopup');selectForm('PQFollowPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQFollowPopup','followPopup','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQFollowPopup_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQFollowPopup_DesignToolSwitch_main" onclick="selectForm('PQFollowPopup', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQFollowPopup', 'main', 'pq_active',['main', 'thank']);followPopupPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQFollowPopup_DesignToolSwitch_thank" onclick="selectForm('PQFollowPopup', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQFollowPopup', 'thank', 'pq_active',['main', 'thank']);followPopupthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQFollowPopup_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQFollowPopup_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQFollowPopup', 'main', 'desktop');addClassToSwitcher('PQFollowPopup_main', 'desktop', 'pq_active',['desktop', 'mobile']);followPopupPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQFollowPopup_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQFollowPopup', 'main', 'desktop');addClassToSwitcher('PQFollowPopup_main', 'mobile', 'pq_active',['desktop', 'mobile']);followPopupPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>FOLLOW POPUP</h3>					
						<!-- DESKTOP -->
						<div id="PQFollowPopup_form_main_desktop" style="display:block;">					
							<?php	
								
								echo $this->getFollowIcons('followPopup', $this->_options[followPopup]);										
								
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color',   
									 'design_icons',
									  'form_icons',
									  'size_icons',
									  'space_icons',
									  'animation_icons',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'followPopup', $this->_options[followPopup]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQFollowPopup_form_main_mobile" style="display:none;">
							
						</div>
					</div>
					<div id="PQFollowPopup_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQFollowPopup_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQFollowPopup_thank', 'desktop', 'pq_active',['desktop', 'mobile']);followPopupthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQFollowPopup_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQFollowPopup_thank', 'mobile', 'pq_active',['desktop', 'mobile']);followPopupthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="followPopup[thank][enable]" id="followPopup_thank_enable" <?php if($this->_options[followPopup][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQFollowPopup_form_thank_proceed');followPopupthankPreview();">
						<label for="followPopup_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQFollowPopup_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'followPopup[thank]', $this->_options[followPopup][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="followPopup[thank][socnet_block_type]" id="followPopup_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQFollowPopup_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[followPopup][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[followPopup][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQFollowPopup_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('followPopup[thank]', $this->_options[followPopup][thank]);
								?>
							</div>					
							<br>
							<div id="PQFollowPopup_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('followPopup[thank]', $this->_options[followPopup][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[followPopup][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'followPopup[thank]', $this->_options[followPopup][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[followPopup][thank][socnet_block_type];?>', 'PQFollowPopup_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="followPopup[thank][buttonBlock][type]" id="followPopup_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQFollowPopup_thank_buttonBlock_', ['redirect']);followPopupthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[followPopup][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQFollowPopup_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'followPopup[thank][buttonBlock]', $this->_options[followPopup][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[followPopup][thank][buttonBlock][type];?>', 'PQFollowPopup_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[followPopup][thank][enable];?>', 'PQFollowPopup_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="followPopup[whitelabel]" onclick="getPreviewByActiveForm('followPopup', 'PQFollowPopup');" id="followPopup_whitelabel" <?php if($this->_options[followPopup][whitelabel] == 'on') echo 'checked';?>>
						<label for="followPopup_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQFollowPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('followPopup');" <?php if(trim($this->_options[followPopup][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQFollowPopup_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_follow_popup.png', __FILE__);?>"> Follow Popup</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('followPopup', array('delay','exit', 'scrolling'),  $this->_options[followPopup]);
					?>
					<?php
						echo $this->getPageOptions('followPopup', $this->_options[followPopup]);
					?>
					<?php
						echo $this->getGadgetRules('followPopup', $this->_options[followPopup]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQFollowPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQFollowPopup_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_follow_popup.png', __FILE__);?>"> Follow Popup</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('followPopup', $this->_options[followPopup]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END followPopup Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQFollowPopup\');
								selectStep(\'PQFollowPopup\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								followPopupSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('followPopup', 'PQFollowPopup');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'followBar'){
				?>
				<!-- ********* followBar Div -->
			<div id="PQFollowBar" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="followBarSave">
			<input type="hidden" id="PQFollowBar_position" name="followBar[position]" value="<?php echo stripslashes($this->_options[followBar][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="followBarSettings('<?php echo stripslashes($this->_options[followBar][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQFollowBar_nav_step_1" onclick="selectStep('PQFollowBar', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQFollowBar_nav_step_2" onclick="selectStep('PQFollowBar', 2);enablePreviewIframe('PQFollowBar');selectForm('PQFollowBar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQFollowBar_nav_step_3" onclick="selectStep('PQFollowBar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQFollowBar_nav_step_4" onclick="selectStep('PQFollowBar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQFollowBar_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQFollowBar_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[followBar][theme]) $this->_options[followBar][theme] = $this->_default_themes[followBar];
				?>
				<input type="hidden" id="PQFollowBar_Current_Theme" name="followBar[theme]" value="<?php echo stripslashes($this->_options[followBar][theme]);?>">
				<input type="hidden" id="PQFollowBar_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[followBar] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[followBar][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQFollowBar', 2);enablePreviewIframe('PQFollowBar');selectForm('PQFollowBar', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQFollowBar','followBar','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQFollowBar_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQFollowBar_DesignToolSwitch_main" onclick="selectForm('PQFollowBar', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQFollowBar', 'main', 'pq_active',['main', 'thank']);followBarPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQFollowBar_DesignToolSwitch_thank" onclick="selectForm('PQFollowBar', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQFollowBar', 'thank', 'pq_active',['main', 'thank']);followBarthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQFollowBar_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQFollowBar_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQFollowBar', 'main', 'desktop');addClassToSwitcher('PQFollowBar_main', 'desktop', 'pq_active',['desktop', 'mobile']);followBarPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQFollowBar_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQFollowBar', 'main', 'mobile');addClassToSwitcher('PQFollowBar_main', 'mobile', 'pq_active',['desktop', 'mobile']);followBarPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>FOLLOW BAR</h3>					
						<!-- DESKTOP -->
						<div id="PQFollowBar_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getFollowIcons('followBar', $this->_options[followBar]);
								echo $this->getFormCodeForTool(
								array(						
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
										'head_font',
										'head_font_size',
										'head_color',  									  
									 'design_icons',
									  'form_icons',
									  'size_icons',
									  'space_icons',
									  'animation_icons',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation'
									), 'followBar', $this->_options[followBar]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQFollowBar_form_main_mobile" style="display:none;">
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									'mobile_position',
									'mobile_title'								
									), 'followBar', $this->_options[followBar]);
							?>
						</div>
					</div>
					<div id="PQFollowBar_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQFollowBar_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQFollowBar_thank', 'desktop', 'pq_active',['desktop', 'mobile']);followBarthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQFollowBar_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQFollowBar_thank', 'mobile', 'pq_active',['desktop', 'mobile']);followBarthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="followBar[thank][enable]" id="followBar_thank_enable" <?php if($this->_options[followBar][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQFollowBar_form_thank_proceed');followBarthankPreview();">
						<label for="followBar_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQFollowBar_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'followBar[thank]', $this->_options[followBar][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="followBar[thank][socnet_block_type]" id="followBar_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQFollowBar_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[followBar][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[followBar][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQFollowBar_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('followBar[thank]', $this->_options[followBar][thank]);
								?>
							</div>					
							<br>
							<div id="PQFollowBar_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('followBar[thank]', $this->_options[followBar][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[followBar][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'followBar[thank]', $this->_options[followBar][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[followBar][thank][socnet_block_type];?>', 'PQFollowBar_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="followBar[thank][buttonBlock][type]" id="followBar_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQFollowBar_thank_buttonBlock_', ['redirect']);followBarthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[followBar][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQFollowBar_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'followBar[thank][buttonBlock]', $this->_options[followBar][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[followBar][thank][buttonBlock][type];?>', 'PQFollowBar_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[followBar][thank][enable];?>', 'PQFollowBar_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="followBar[whitelabel]" onclick="getPreviewByActiveForm('followBar', 'PQFollowBar');" id="followBar_whitelabel" <?php if($this->_options[followBar][whitelabel] == 'on') echo 'checked';?>>
								<label for="followBar_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQFollowBar', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('followBar');" <?php if(trim($this->_options[followBar][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
				</div>
				<div id="PQFollowBar_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_follow_bar.png', __FILE__);?>"> Follow Bar</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('followBar', array('delay','scrolling'),  $this->_options[followBar]);
					?>
					<?php
						echo $this->getPageOptions('followBar', $this->_options[followBar]);
					?>
					<?php
						echo $this->getGadgetRules('followBar', $this->_options[followBar]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQFollowBar', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQFollowBar_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_follow_bar.png', __FILE__);?>"> Follow Bar</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('followBar', $this->_options[followBar]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END followBar Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQFollowBar\');
								selectStep(\'PQFollowBar\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								followBarSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('followBar', 'PQFollowBar');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'followFloating'){
				?>
				<!-- ********* followFloating Div -->
			<div id="PQFollowFloating" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="followFloatingSave">
			<input type="hidden" id="PQFollowFloating_position" name="followFloating[position]" value="<?php echo stripslashes($this->_options[followFloating][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="followFloatingSettings('<?php echo stripslashes($this->_options[followFloating][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQFollowFloating_nav_step_1" onclick="selectStep('PQFollowFloating', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQFollowFloating_nav_step_2" onclick="selectStep('PQFollowFloating', 2);enablePreviewIframe('PQFollowFloating');selectForm('PQFollowFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQFollowFloating_nav_step_3" onclick="selectStep('PQFollowFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQFollowFloating_nav_step_4" onclick="selectStep('PQFollowFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQFollowFloating_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQFollowFloating_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[followFloating][theme]) $this->_options[followFloating][theme] = $this->_default_themes[followFloating];
				?>
				<input type="hidden" id="PQFollowFloating_Current_Theme" name="followFloating[theme]" value="<?php echo stripslashes($this->_options[followFloating][theme]);?>">
				<input type="hidden" id="PQFollowFloating_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[followFloating] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[followFloating][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQFollowFloating', 2);enablePreviewIframe('PQFollowFloating');selectForm('PQFollowFloating', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQFollowFloating','followFloating','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQFollowFloating_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQFollowFloating_DesignToolSwitch_main" onclick="selectForm('PQFollowFloating', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQFollowFloating', 'main', 'pq_active',['main', 'thank']);followFloatingPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQFollowFloating_DesignToolSwitch_thank" onclick="selectForm('PQFollowFloating', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQFollowFloating', 'thank', 'pq_active',['main', 'thank']);followFloatingthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQFollowFloating_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQFollowFloating_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQFollowFloating', 'main', 'desktop');addClassToSwitcher('PQFollowFloating_main', 'desktop', 'pq_active',['desktop', 'mobile']);followFloatingPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQFollowFloating_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQFollowFloating', 'main', 'desktop');addClassToSwitcher('PQFollowFloating_main', 'mobile', 'pq_active',['desktop', 'mobile']);followFloatingPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>FOLLOW FLOATING</h3>					
						<!-- DESKTOP -->
						<div id="PQFollowFloating_form_main_desktop" style="display:block;">					
							<?php
								echo $this->getFollowIcons('followFloating', $this->_options[followFloating]);
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color',   
									 'design_icons',
									  'form_icons',
									  'size_icons',
									  'space_icons',
									  'animation_icons',
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'followFloating', $this->_options[followFloating]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQFollowFloating_form_main_mobile" style="display:none;">
							<?php							
								
							?>
						</div>
					</div>
					<div id="PQFollowFloating_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQFollowFloating_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQFollowFloating_thank', 'desktop', 'pq_active',['desktop', 'mobile']);followFloatingthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQFollowFloating_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQFollowFloating_thank', 'mobile', 'pq_active',['desktop', 'mobile']);followFloatingthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="followFloating[thank][enable]" id="followFloating_thank_enable" <?php if($this->_options[followFloating][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQFollowFloating_form_thank_proceed');followFloatingthankPreview();">
						<label for="followFloating_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQFollowFloating_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'followFloating[thank]', $this->_options[followFloating][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="followFloating[thank][socnet_block_type]" id="followFloating_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQFollowFloating_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[followFloating][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[followFloating][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQFollowFloating_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('followFloating[thank]', $this->_options[followFloating][thank]);
								?>
							</div>					
							<br>
							<div id="PQFollowFloating_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('followFloating[thank]', $this->_options[followFloating][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[followFloating][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'followFloating[thank]', $this->_options[followFloating][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[followFloating][thank][socnet_block_type];?>', 'PQFollowFloating_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="followFloating[thank][buttonBlock][type]" id="followFloating_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQFollowFloating_thank_buttonBlock_', ['redirect']);followFloatingthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[followFloating][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQFollowFloating_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'followFloating[thank][buttonBlock]', $this->_options[followFloating][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[followFloating][thank][buttonBlock][type];?>', 'PQFollowFloating_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[followFloating][thank][enable];?>', 'PQFollowFloating_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="followFloating[whitelabel]" onclick="getPreviewByActiveForm('followFloating', 'PQFollowFloating');" id="followFloating_whitelabel" <?php if($this->_options[followFloating][whitelabel] == 'on') echo 'checked';?>>
						<label for="followFloating_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQFollowFloating', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('followFloating');" <?php if(trim($this->_options[followFloating][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
					<div class="pq_switch_for">
								<?php echo $this->_dictionary[designOptions][_whitelabel];?>
								
							</div>
				</div>
				<div id="PQFollowFloating_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_follow_floating.png', __FILE__);?>"> Follow Floating</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('followFloating', array('delay', 'scrolling'),  $this->_options[followFloating]);
					?>
					<?php
						echo $this->getPageOptions('followFloating', $this->_options[followFloating]);
					?>
					<?php
						echo $this->getGadgetRules('followFloating', $this->_options[followFloating]);
					?>				
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQFollowFloating', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQFollowFloating_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_follow_floating.png', __FILE__);?>"> Follow Floating</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('followFloating', $this->_options[followFloating]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END followFloating Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQFollowFloating\');
								selectStep(\'PQFollowFloating\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								followFloatingSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('followFloating', 'PQFollowFloating');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'socialPopup'){
				?>
				<!-- ********* socialPopup Div -->
			<div id="PQSocialPopup" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="socialPopupSave">
			<input type="hidden" id="PQSocialPopup_position" name="socialPopup[position]" value="<?php echo stripslashes($this->_options[socialPopup][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="socialPopupSettings('<?php echo stripslashes($this->_options[socialPopup][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQSocialPopup_nav_step_1" onclick="selectStep('PQSocialPopup', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQSocialPopup_nav_step_2" onclick="selectStep('PQSocialPopup', 2);enablePreviewIframe('PQSocialPopup');selectForm('PQSocialPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQSocialPopup_nav_step_3" onclick="selectStep('PQSocialPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQSocialPopup_nav_step_4" onclick="selectStep('PQSocialPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQSocialPopup_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQSocialPopup_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[socialPopup][theme]) $this->_options[socialPopup][theme] = $this->_default_themes[socialPopup];
				?>
				<input type="hidden" id="PQSocialPopup_Current_Theme" name="socialPopup[theme]" value="<?php echo stripslashes($this->_options[socialPopup][theme]);?>">
				<input type="hidden" id="PQSocialPopup_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[socialPopup] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[socialPopup][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQSocialPopup', 2);enablePreviewIframe('PQSocialPopup');selectForm('PQSocialPopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQSocialPopup','socialPopup','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQSocialPopup_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQSocialPopup_DesignToolSwitch_main" onclick="selectForm('PQSocialPopup', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQSocialPopup', 'main', 'pq_active',['main', 'thank']);socialPopupPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQSocialPopup_DesignToolSwitch_thank" onclick="selectForm('PQSocialPopup', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQSocialPopup', 'thank', 'pq_active',['main', 'thank']);socialPopupthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQSocialPopup_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQSocialPopup_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQSocialPopup', 'main', 'desktop');addClassToSwitcher('PQSocialPopup_main', 'desktop', 'pq_active',['desktop', 'mobile']);socialPopupPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQSocialPopup_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQSocialPopup', 'main', 'desktop');addClassToSwitcher('PQSocialPopup_main', 'mobile', 'pq_active',['desktop', 'mobile']);socialPopupPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>SOCIAL POPUP</h3>					
						<!-- DESKTOP -->
						<div id="PQSocialPopup_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',         
									  'iframe_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color',									  
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'socialPopup', $this->_options[socialPopup]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQSocialPopup_form_main_mobile" style="display:none;">
							<?php							
								
							?>
						</div>
					</div>
					<div id="PQSocialPopup_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQSocialPopup_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQSocialPopup_thank', 'desktop', 'pq_active',['desktop', 'mobile']);socialPopupthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQSocialPopup_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQSocialPopup_thank', 'mobile', 'pq_active',['desktop', 'mobile']);socialPopupthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="socialPopup[thank][enable]" id="socialPopup_thank_enable" <?php if($this->_options[socialPopup][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQSocialPopup_form_thank_proceed');socialPopupthankPreview();">
						<label for="socialPopup_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQSocialPopup_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'socialPopup[thank]', $this->_options[socialPopup][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="socialPopup[thank][socnet_block_type]" id="socialPopup_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQSocialPopup_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[socialPopup][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[socialPopup][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQSocialPopup_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('socialPopup[thank]', $this->_options[socialPopup][thank]);
								?>
							</div>					
							<br>
							<div id="PQSocialPopup_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('socialPopup[thank]', $this->_options[socialPopup][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[socialPopup][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'socialPopup[thank]', $this->_options[socialPopup][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[socialPopup][thank][socnet_block_type];?>', 'PQSocialPopup_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="socialPopup[thank][buttonBlock][type]" id="socialPopup_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQSocialPopup_thank_buttonBlock_', ['redirect']);socialPopupthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[socialPopup][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQSocialPopup_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'socialPopup[thank][buttonBlock]', $this->_options[socialPopup][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[socialPopup][thank][buttonBlock][type];?>', 'PQSocialPopup_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[socialPopup][thank][enable];?>', 'PQSocialPopup_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="socialPopup[whitelabel]" onclick="getPreviewByActiveForm('socialPopup', 'PQSocialPopup');" id="socialPopup_whitelabel" <?php if($this->_options[socialPopup][whitelabel] == 'on') echo 'checked';?>>
						<label for="socialPopup_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQSocialPopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('socialPopup');" <?php if(trim($this->_options[socialPopup][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>					
				</div>
				<div id="PQSocialPopup_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_social_popup.png', __FILE__);?>"> Social Popup</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('socialPopup', array('delay','exit','scrolling'),  $this->_options[socialPopup]);
					?>
					<?php
						echo $this->getPageOptions('socialPopup', $this->_options[socialPopup]);
					?>
					<?php
						echo $this->getGadgetRules('socialPopup', $this->_options[socialPopup]);
					?>
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQSocialPopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQSocialPopup_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_social_popup.png', __FILE__);?>"> Social Popup</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('socialPopup', $this->_options[socialPopup]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END socialPopup Div -->
				<script>				
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQSocialPopup\');
								selectStep(\'PQSocialPopup\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								socialPopupSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('socialPopup', 'PQSocialPopup');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

			if($_GET[s_t] == 'youtubePopup'){
				?>
				<!-- ********* youtubePopup Div -->
			<div id="PQYoutubePopup" style="display:none;" class="f_wrapper pq_li3">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="youtubePopupSave">
			<input type="hidden" id="PQYoutubePopup_position" name="youtubePopup[position]" value="<?php echo stripslashes($this->_options[youtubePopup][position]);?>">
				<div class="pq_nav" >				
					<span onclick="location.href='<?php echo $this->getSettingsPageUrl();?>';"><?php echo $this->_dictionary[navigation][index];?></span>							
					<span  onclick="youtubePopupSettings('<?php echo stripslashes($this->_options[youtubePopup][position]);?>');"><?php echo $this->_dictionary[navigation][location];?></span>
					<span id="PQYoutubePopup_nav_step_1" onclick="selectStep('PQYoutubePopup', 1);document.getElementById('PQIframePreviewBlock').style.display='none';"><?php echo $this->_dictionary[navigation][themes];?></span>
					<span id="PQYoutubePopup_nav_step_2" onclick="selectStep('PQYoutubePopup', 2);enablePreviewIframe('PQYoutubePopup');selectForm('PQYoutubePopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][design];?></span>
					<span id="PQYoutubePopup_nav_step_3" onclick="selectStep('PQYoutubePopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][display];?></span>
					<span id="PQYoutubePopup_nav_step_4" onclick="selectStep('PQYoutubePopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';"><?php echo $this->_dictionary[navigation][settings];?></span>
					<input type="submit" id="PQYoutubePopup_submit_button" disabled style="background-color:00e676;" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
				</div>
				<div id="PQYoutubePopup_step_1" style="display:none;" class="pq_themes">
				<?php
					if(!$this->_options[youtubePopup][theme]) $this->_options[youtubePopup][theme] = $this->_default_themes[youtubePopup];
				?>
				<input type="hidden" id="PQYoutubePopup_Current_Theme" name="youtubePopup[theme]" value="<?php echo stripslashes($this->_options[youtubePopup][theme]);?>">
				<input type="hidden" id="PQYoutubePopup_CurrentThemeForPreview" value="">
						<h1><?php echo $this->_dictionary[choseTheme][title];?></h1>										
						<p><?php echo $this->_dictionary[choseTheme][description];?></p>
						<div style="min-height: 200px; width: 90%; font-size: 0; position: absolute; left: 0; right: 0; top: 130px; margin: 0 auto; bottom: 0; overflow-y: auto; padding: 0 5%;">					
						<?php foreach((array)$this->_themes[youtubePopup] as $k => $v){
								$isActive = '';
								if($k == stripslashes($this->_options[youtubePopup][theme])){
									$isActive = 'pq_active';
								}
						?>
							<div <?php if((float)$v[price] > 0) echo 'class="'.$isActive.' pq_premium"'; else echo 'class="'.$isActive.'"';?> onclick="
								<?php
										if($isActive){
											echo "selectStep('PQYoutubePopup', 2);enablePreviewIframe('PQYoutubePopup');selectForm('PQYoutubePopup', 'main');document.getElementById('PQThemeSelectPreviewPopup').style.display = 'none';";
										}else{
											echo "previewThemeSelect('PQYoutubePopup','youtubePopup','".$k."', 'current');";
										}
									?>
							"/>
								<img width="200px" height="100px" src="<?php echo $v[preview_image_small];?>" />
							</div>
						<?php }?>
						</div>				
					<div class="pq_close"></div>							
				</div>
				<div id="PQYoutubePopup_step_2" style="display:block;" class="design_settings">
					
					<div class="pq_thank_select">
						<span id="PQYoutubePopup_DesignToolSwitch_main" onclick="selectForm('PQYoutubePopup', 'main', ['desktop', 'mobile']);addClassToSwitcher('PQYoutubePopup', 'main', 'pq_active',['main', 'thank']);youtubePopupPreview();" class="pq_active"><?php echo $this->_dictionary[navigation][your_tool];?></span>
						<span id="PQYoutubePopup_DesignToolSwitch_thank" onclick="selectForm('PQYoutubePopup', 'thank', ['desktop', 'mobile']);addClassToSwitcher('PQYoutubePopup', 'thank', 'pq_active',['main', 'thank']);youtubePopupthankPreview();"><?php echo $this->_dictionary[navigation][success_tool];?></span>					
						<span class="pq_disabled"><?php echo $this->_dictionary[navigation][email];?></span>
					</div>				
													
					<div id="PQYoutubePopup_form_main" style="display:block;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQYoutubePopup_main_DesignToolSwitch_desktop" onclick="selectSubForm('PQYoutubePopup', 'main', 'desktop');addClassToSwitcher('PQYoutubePopup_main', 'desktop', 'pq_active',['desktop', 'mobile']);youtubePopupPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQYoutubePopup_main_DesignToolSwitch_mobile" onclick="selectSubForm('PQYoutubePopup', 'main', 'desktop');addClassToSwitcher('PQYoutubePopup_main', 'mobile', 'pq_active',['desktop', 'mobile']);youtubePopupPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						<h3>YOUTUBE POPUP</h3>					
						<!-- DESKTOP -->
						<div id="PQYoutubePopup_form_main_desktop" style="display:block;">					
							<?php							
								echo $this->getFormCodeForTool(
								array(						
									  'size_window',
									  'popup_form',
									  'background_color',
									  'second_background',
									  'background_image_src',
									  'iframe_src',
									  'title',
									  'head_font',
									  'head_font_size',
									  'head_color',
									  'sub_title',
									  'text_font',
									  'font_size',
									  'text_color',									  
									  'header_image_src',          
									  'close_icon_type', 
									  'close_text',
									  'close_icon_color',
									  'close_icon_animation',
									  'border_type',
									  'border_depth', 
									  'border_color',
									  'animation',
									  'overlay_color'
									), 'youtubePopup', $this->_options[youtubePopup]);
							?>
							
						</div>
						<!-- MOBILE -->
						<div id="PQYoutubePopup_form_main_mobile" style="display:none;">
							<?php							
								
							?>
						</div>
					</div>
					<div id="PQYoutubePopup_form_thank" style="display:none;" class="pq_design">
						<div class="pq_desktop_mobile">
							<div id="PQYoutubePopup_thank_DesignToolSwitch_desktop" onclick="addClassToSwitcher('PQYoutubePopup_thank', 'desktop', 'pq_active',['desktop', 'mobile']);youtubePopupthankPreview();" class="pq_active" ><?php echo $this->_dictionary[navigation][desktop];?></div>
							<div id="PQYoutubePopup_thank_DesignToolSwitch_mobile" onclick="addClassToSwitcher('PQYoutubePopup_thank', 'mobile', 'pq_active',['desktop', 'mobile']);youtubePopupthankPreview();"><?php echo $this->_dictionary[navigation][mobile];?></div>					
						</div>
						
						<div class="pq_switch_for">
						<?php echo $this->_dictionary[action][enable];?>
						<input type="checkbox" class="pq_switch" name="youtubePopup[thank][enable]" id="youtubePopup_thank_enable" <?php if($this->_options[youtubePopup][thank][enable] == 'on') echo 'checked';?>  onclick="enableBlockByCheckboxClick(this.checked, 'PQYoutubePopup_form_thank_proceed');youtubePopupthankPreview();">
						<label for="youtubePopup_thank_enable" class="pq_switch_label"></label>
						</div>
						<div id="PQYoutubePopup_form_thank_proceed" style="display:none">						
							<?php					
								echo $this->getFormCodeForTool(
								array(
									'size_window',
										'popup_form',
										'background_color',
										'second_background',
										'background_image_src',
										'title',
										'head_font',
										'head_font_size',
										'head_color',
										'sub_title',
										'text_font',
										'font_size',
										'text_color',   
										'header_image_src',          
										'close_icon_type', 
										'close_text',
										'close_icon_color',
										'close_icon_animation',
										'border_type',
										'border_depth', 
										'border_color',   
										'animation',
										'overlay_color'	
								)
								, 'youtubePopup[thank]', $this->_options[youtubePopup][thank]);
							?>
							<h3><?php echo $this->_dictionary[thankPopupSettings][social_icon_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][social_icon_type];?></p>
							<select name="youtubePopup[thank][socnet_block_type]" id="youtubePopup_thank_socnet_block_type" onchange="enableAdditionalBlock(this.value, 'PQYoutubePopup_thank_socnetIconsBlock_',['follow', 'share']);">
								<option value="" selected>None</option>
								<option value="follow" <?php if($this->_options[youtubePopup][thank][socnet_block_type] == 'follow') echo 'selected';?>>Follow action</option>
								<option value="share" <?php if($this->_options[youtubePopup][thank][socnet_block_type] == 'share') echo 'selected';?>>Share action</option>								
							</select>
							<br>
							<div id="PQYoutubePopup_thank_socnetIconsBlock_follow" style="display:none">
								<?php
									echo $this->getFollowIcons('youtubePopup[thank]', $this->_options[youtubePopup][thank]);
								?>
							</div>					
							<br>
							<div id="PQYoutubePopup_thank_socnetIconsBlock_share" style="display:none">
								<?php
									echo $this->getSharingIcons('youtubePopup[thank]', $this->_options[youtubePopup][thank][socnet_with_pos], array('socnet_with_pos_error' =>  $sharingSocnetErrorArray[youtubePopup][thank]),1);
								?>
							</div>
							<?php
									echo $this->getFormCodeForTool(
									array(
										'design_icons',
										'form_icons',
										'size_icons',
										'space_icons',
										'animation_icons'
									)
									, 'youtubePopup[thank]', $this->_options[youtubePopup][thank]);
								?>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[youtubePopup][thank][socnet_block_type];?>', 'PQYoutubePopup_thank_socnetIconsBlock_',['follow', 'share']);
							</script>
							<h3><?php echo $this->_dictionary[thankPopupSettings][button_action_type_title];?></h3>
							
							<p><?php echo $this->_dictionary[thankPopupSettings][button_action_type];?></p>
							<select name="youtubePopup[thank][buttonBlock][type]" id="youtubePopup_thank_buttonBlock_type" onchange="enableAdditionalBlock(this.value, 'PQYoutubePopup_thank_buttonBlock_', ['redirect']);youtubePopupthankPreview();">
								<option value="" selected><?php echo $this->_dictionary[thankPopupSettings][button_action_type_0];?></option>
								<option value="redirect" <?php if($this->_options[youtubePopup][thank][buttonBlock][type] == 'redirect') echo 'selected';?>><?php echo $this->_dictionary[thankPopupSettings][button_action_type_1];?></option>								
							</select>
							<br>
							<div id="PQYoutubePopup_thank_buttonBlock_redirect" style="display:none">
								<?php
									echo $this->getFormCodeForTool(
									array(
										'url',
											'button_text',
											'button_font',
											'button_font_size',
											'button_text_color',
											'button_color'
									)
									, 'youtubePopup[thank][buttonBlock]', $this->_options[youtubePopup][thank][buttonBlock]);
								?>						
							</div>
							<script>
								enableAdditionalBlock('<?php echo $this->_options[youtubePopup][thank][buttonBlock][type];?>', 'PQYoutubePopup_thank_buttonBlock_', ['redirect']);
							</script>
						</div>
						<script>
							enableBlockByCheckboxClick('<?php echo $this->_options[youtubePopup][thank][enable];?>', 'PQYoutubePopup_form_thank_proceed');
						</script>
					</div>
					<div class="pq_design_buttons">
						<div></div>
						<p>						
							<?php echo $this->_dictionary[designOptions][_whitelabel];?>
						</p><span>pro</span>
						<input type="checkbox" class="pq_switch" name="youtubePopup[whitelabel]" onclick="getPreviewByActiveForm('youtubePopup', 'PQYoutubePopup');" id="youtubePopup_whitelabel" <?php if($this->_options[youtubePopup][whitelabel] == 'on') echo 'checked';?>>
						<label for="youtubePopup_whitelabel" class="pq_switch_label"></label>
						<input type="button" value="<?php echo $this->_dictionary[navigation][next];?>" onclick="selectStep('PQYoutubePopup', 3);document.getElementById('PQIframePreviewBlock').style.display='none';">
						<a onclick="clearLastChanges('youtubePopup');" <?php if(trim($this->_options[youtubePopup][position])=='') echo 'style="display:none;"';?>><?php echo $this->_dictionary[navigation][clear_changes];?></a>						
					</div>
					
				</div>
				<div id="PQYoutubePopup_step_3" style="display:none;" class="pq_rules">
					<h1><img src="<?php echo plugins_url('i/ico_youtube_popup.png', __FILE__);?>"> YouTube Popup</h1>
					<p><?php echo $this->_dictionary[displayRules][description];?></p>
					<div class="pq_clear"></div>
					<?php
						echo $this->getEventHandlerBlock('youtubePopup', array('delay', 'exit', 'scrolling'),  $this->_options[youtubePopup]);
					?>
					<?php
						echo $this->getGadgetRules('youtubePopup', $this->_options[youtubePopup]);
					?>
					<?php
						echo $this->getPageOptions('youtubePopup', $this->_options[youtubePopup]);
					?>				
					<input type="button" value="<?php echo $this->_dictionary[navigation][next_step];?>" onclick="selectStep('PQYoutubePopup', 4);document.getElementById('PQIframePreviewBlock').style.display='none';">
					<input type="submit" name="" value="<?php echo $this->_dictionary[navigation][save_button];?>">
					
				</div>
				<div id="PQYoutubePopup_step_4" style="display:none;" class="provider_settings">				
					<h1><img src="<?php echo plugins_url('i/ico_youtube_popup.png', __FILE__);?>"> YouTube Popup</h1>
					<p><?php echo $this->_dictionary[providerSettings][description];?></p>				
					<?php
						echo $this->getLockBlock('youtubePopup', $this->_options[youtubePopup]);
					?>
					<input type="submit" value="<?php echo $this->_dictionary[navigation][save_button];?>">				
				</div>			
			</form>	
			</div>
	<!-- ********* END youtubePopup Div -->
				<script>
					<?php
						if((int)$_GET[step]){
							echo '
								disableAllDialog();	
								startTool(\'PQYoutubePopup\');
								selectStep(\'PQYoutubePopup\', '.(int)$_GET[step].');
							';
						}else{
							echo '
								youtubePopupSettings(\''.stripslashes(urldecode($_GET[pos])).'\');
							';
						}
					?>
					checkTheme('youtubePopup', 'PQYoutubePopup');
					document.getElementById('PQLoader').style.display='none';
				</script>
				<?php
			}

		}
	?>

	<!-- ***********************************************************************END TOOLS*********************************************************************************** -->		
					
			
			<!-- CLEAR LAST CHANGES -->
			<div id="PQClearLastChangesPopup" style="display:none;">
				<input type="hidden" id="PQCLearLastChangesTool" value="">
				<h2><?php echo $this->_dictionary[clearChangesPopup][title];?></h2>
				<p><?php echo $this->_dictionary[clearChangesPopup][description];?></p>
				<input type="button" onclick="clearLastChangesProceed();" value="<?php echo $this->_dictionary[clearChangesPopup][button_yes];?>">
				<input type="button" onclick="document.getElementById('PQClearLastChangesPopup').style.display = 'none';" value="<?php echo $this->_dictionary[clearChangesPopup][button_no];?>"><br>
			</div>
			
			<!-- Preview Iframe -->
			<div class="frame" id="PQIframePreviewBlock" style="display:none">
				<iframe src="about:blank" id="PQPreviewID" width="100%" height="100%" class="pq_if"></iframe>
				
			</div>
			<!-- End Preview Iframe -->
			
			
			<!-- CONTINUE EXTEND PAY POPUP -->
			<?php
				//CHECKOUT
				if($_POST[action] == 'checkout')
				{
					$summ = 0;
					$discount = (int)$this->_plugin_settings[discounts][$_POST[checkoutMonthPeriod]];
					$collMonths = (int)$_POST[checkoutMonthPeriod];
					$optArray = array();
					$formFields = '';
					$fieldCnt = 0;
					$save_mail = 0;					
					if($_POST[type] == 'activate'){					
						foreach((array)$toolsArray as $tool => $data)
						{
							foreach((array)$data[proOptionsDetail] as $opt => $obj)
							{
								if($obj[status] != 'paid'){	
									if($opt == 'themes'){
										$summ += $obj[price];
										$optArray[strtolower($tool).'_theme_'.$obj[theme]]=1;							
										
										$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_name" value="'.$toolsArray[$tool][name][name].' '.$toolsArray[$tool][name][type].' Theme '.$obj[theme].'">';
										$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_price" value="'.$obj[price]*$collMonths.'">';
										$fieldCnt++;
									}elseif($opt == 'whitelabel'){
										$summ += $obj[price];
										$optArray[strtolower($tool).'_whitelabel']=1;
										
										$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_name" value="'.$toolsArray[$tool][name][name].' '.$toolsArray[$tool][name][type].' Whitelabel">';
										$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_price" value="'.$obj[price]*$collMonths.'">';
										$fieldCnt++;
									}elseif($opt == 'mail'){
										if(!$optArray['mail']){
											$optArray['mail'] = 1;
											$summ += $obj[price];
																				
										}else{
											$save_mail += $obj[price];
										}
										$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_name" value="'.$toolsArray[$tool][name][name].' '.$toolsArray[$tool][name][type].' Mail Service">';
										$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_price" value="'.$obj[price]*$collMonths.'">';
										$fieldCnt++;
									}
								}
							}
						}						
					}
					
					if($_POST[type] == 'extend'){					
						foreach((array)$_POST as $tool => $data){					
							if($toolsArray[$tool]){						
								foreach((array)$data as $opt => $v){
									$obj = $toolsArray[$tool][proOptionsDetail][$opt];
									if($obj[status] == 'paid'){								
										if($opt == 'themes'){
											$summ += $obj[price];
											$optArray[strtolower($tool).'_theme_'.$obj[theme]]=1;
											
											
											$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_name" value="'.$toolsArray[$tool][name][name].' '.$toolsArray[$tool][name][type].' Theme '.$obj[theme].'">';
											$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_price" value="'.$obj[price]*$collMonths.'">';
											$fieldCnt++;
										}elseif($opt == 'whitelabel'){
											$summ += $obj[price];
											$optArray[strtolower($tool).'_whitelabel']=1;
											
											$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_name" value="'.$toolsArray[$tool][name][name].' '.$toolsArray[$tool][name][type].' Whitelabel">';
											$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_price" value="'.$obj[price]*$collMonths.'">';
											$fieldCnt++;
										}elseif($opt == 'mail'){
											if(!$optArray['mail']){
												$optArray['mail'] = 1;
												$summ += $obj[price];
																					
											}else{
												$save_mail += $obj[price];
											}
											$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_name" value="'.$toolsArray[$tool][name][name].' '.$toolsArray[$tool][name][type].' Mail Service">';
											$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_price" value="'.$obj[price]*$collMonths.'">';
											$fieldCnt++;
										}
									}							
								}
							}
						}
					}					
					$save = 0;
					
					$allSumm = round(($summ+$save_mail)*$collMonths);											
					$summ = round(($summ - $summ*$discount/100)*$collMonths);
					$save = $allSumm - $summ;				
					if($save > 0){	
						$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_name" value="Profitquery Discount">';
						$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_price" value="'.$save.'">';
						$formFields .= '<input type="hidden" name="li_'.$fieldCnt.'_type" value="coupon">';
					}
					if($summ > 0){
						?>
						<script>
							try{document.getElementById('PQActivatePopup').style.display = 'none';}catch(err){}
							try{document.getElementById('PQExtandPopup').style.display = 'none';}catch(err){}
						</script>
						<div id="PQContinueCheckoutForm">
						<form action="https://www.2checkout.com/checkout/purchase" target="_" method="post" >
						<input name="sid" type="hidden" value="102567109">
						<input name="mode" type="hidden" value="2CO">  
						<input type="hidden" name="pq_period" value="<?php echo (int)$collMonths;?>">  
						<?php echo $formFields;?>
						  <h2><?php printf($this->_dictionary[checkoutConfirmPopup][title], $summ);?></h2>
						  <p><?php printf($this->_dictionary[checkoutConfirmPopup][description], $save, 'href="pramprampram"');?></p>
						  <label><?php echo $this->_dictionary[checkoutConfirmPopup][enter_email];?><input name="email" type="text" value="<?php echo stripslashes($this->_options[settings][email]);?>"></label>
						  <label><?php echo $this->_dictionary[checkoutConfirmPopup][enter_domain];?><input name="domain" type="text" value="<?php echo $this->getDomain();?>"></label>
						
						<?php
							$cnt = 0;
							foreach((array)$optArray as $k => $v){
								echo '<input name="pq_'.$cnt.'_art" type="hidden" value="'.$k.'">';
								$cnt++;
							}
						?>
						<input type="submit" name="sbmt" value="<?php echo $this->_dictionary[checkoutConfirmPopup][button];?>">
						<a class="pq_close"  href="javascript:void(0)" onclick="document.getElementById('PQContinueCheckoutForm').style.display='none';"></a>
						<img src="<?php echo plugins_url('i/basics.png', __FILE__);?>" />
						</div>
						<?php					
					}
				}
			?>
		</div>
		<div class="clear"></div>
	<script>		


	<?php
		if($_GET[act] == 'clearLastChanges' && $_GET[s_t]){
			echo 'returnToDesignStep("'.$_GET[s_t].'");';
		}
		if(trim($providerRedirectByError)){
			echo 'returnToProviderStep("'.$providerRedirectByError.'");';
		}
	?>
	</script>

	<!-- LANG -->						
				<div id="PQ_lang">
				<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
				<input type="hidden" name="action" value="setLang">
					<h2><?php echo $this->_dictionary[choseLangDialog][title_select];?></h2>
					<select name="lang">					
						<?php						
							foreach((array)$this->_plugin_settings[lang] as $code => $data){							
								echo '<option value="'.$code.'" '.sprintf("%s", (($code == $this->_options[settings][lang] ) ? "selected" : "")).'>'.stripslashes($data[name]).'</option>';
							}
						?>					
					</select>
					<input type="submit" value="<?php echo $this->_dictionary[choseLangDialog][button];?>">
					<p><?php printf($this->_dictionary[choseLangDialog][text_contact_us], 'href="javascript:void(0)" onclick="document.getElementById(\'PQNeedHelpPopup\').style.display=\'block\';"');?></p>
					<h3><?php echo $this->_dictionary[choseLangDialog][title_translate];?></h3>
					<p><?php echo $this->_dictionary[choseLangDialog][text_translate];?></p>
					<div class="pq_languages">
						<?php						
							foreach((array)$this->_plugin_settings[needTranslate] as $code => $data){
								echo '<label><input type="radio" name="lang"><div><img src="'.$data[icon].'">'.$data[name].'</div></label>';
							}
						?>					
					</div>
					<a href="http://profitquery-a.akamaihd.net/lib/plugins/lang/profitquery_plugin_dictionary.zip" target="_langDict"><input type="button" value="<?php echo $this->_dictionary[choseLangDialog][downloadZip];?>"></a>
					<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('PQ_lang').style.display='none';"></a>
					</form>
				</div>
				
				<!-- ACTIVATE POPUP  -->
			<div class="pq_upgrade" style="display:none" id="PQActivatePopup">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post" id="PQtoActivateForm">
			<input type="hidden" name="action" value="checkout">
			<input type="hidden" name="type" value="activate">
			<input type="hidden" id="PQActivatePopup_Period" value="12">
				<h2><?php echo $this->_dictionary[activatePopup][title];?></h2>
				<p><?php echo $this->_dictionary[activatePopup][description];?></p>
				<div class="pq_period">
				<label>
					<input type="radio"  name="checkoutMonthPeriod" value="1" onclick="document.getElementById('PQActivatePopup_Period').value=this.value;changeActivateCheckoutPeriod();">
					<div>
						<p>monthly</p>
					</div>
				</label>
				<label>
					<input type="radio" name="checkoutMonthPeriod" value="12" checked onclick="document.getElementById('PQActivatePopup_Period').value=this.value;changeActivateCheckoutPeriod();">
					<div>
						<p>12 months</p>
					</div>
				</label>
				<label>
					<input type="radio" name="checkoutMonthPeriod" value="6" onclick="document.getElementById('PQActivatePopup_Period').value=this.value;changeActivateCheckoutPeriod();">
					<div>
						<p>6 months</p>
					</div>
				</label>
				</div>
				<div style="width: auto;display: block;overflow-y: auto;    max-height: 45vh; margin: 10px auto 20px;">
				<table width="100%" cellspacing="0" style="margin:10px auto 0px; padding:0;border-spacing: 0px 5px;">
				<tbody>
				<?php
					$ifForActivateToolsExist = 0;
					if($toolsArray){				
						foreach((array)$toolsArray as $tools => $data)
						{							
							foreach((array)$data[proOptionsDetail] as $k => $v)
							{
								if($v[status] != 'paid')
								{
									$ifForActivateToolsExist = 1;
					?>
					<input type="hidden" id="PQToolsToActivate_<?php echo $tools."_".$k;?>" data-pq-mail-options="<?php if($k == 'mail') echo 1;else echo 0;?>" data-pq-price="<?php echo $v[price];?>">
					<tr>
						<td><img src="<?php echo plugins_url('i/'.$data[name][icon], __FILE__);?>"></td>
						<td><p><?php echo $data[name][name].' '.$data[name][type].' '; if($k == 'whitelabel') echo 'Whitelabel'; elseif($k == 'themes') echo 'Premium Theme "'.$v[theme].'"'; elseif($k=='mail') echo 'Profitquery Mail Service'; ?></td>
						<td><p><?php echo $v[price].'$/mo';?></p></td>
						<td><a onclick="disableCurrentOption('<?php echo $tools?>', '<?php echo $data[name][name].' '.$data[name][type].' '; if($k == 'whitelabel') echo 'Whitelabel'; elseif($k == 'themes') echo 'Premium Theme '.$v[theme].''; elseif($k=='mail') echo 'Profitquery Mail Service'; ?>', '<?php echo plugins_url('i/'.$data[name][icon], __FILE__);?>', '<?php echo $k;?>', '<?php echo $v[theme];?>', 'activate_popup');"><img src="<?php echo plugins_url('i/disable_tool.png', __FILE__);?>"></a></td>
					</tr>
				<?php
								}
							}
						}
					}
				?>				
				</tbody>
				</table>
				</div>
				<div class="pq_total">
					<p id="PQActivatePopup_Discount_Text"></p><p id="PQActivatePopup_Price"></p><p id="PQActivatePopup_Save_Text"></p>
				</div>
			
				<input type="submit" class="add_new_big" id="PQActivateForm_Submit" value="Pay">
				<div class="pq_close" onclick="document.getElementById('PQActivatePopup').style.display='none';"></div>
				<script>			
					changeActivateCheckoutPeriod();
				</script>
				</form>
			</div>
			<?php
				if((int)$needOpenActivatePopup && (int)$ifForActivateToolsExist == 1){
					echo '<script>document.getElementById("PQActivatePopup").style.display="block";</script>';
				}
			?>
			<!-- END ACTIVATE  -->
			
			
			<!-- EXTEND POPUP  -->		
			<div class="pq_upgrade pq_longer" id="PQExtendPopup" style="display:none">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post" id="PQtoExtendForm">
			<input type="hidden" id="PQExtendPopup_ExtendPeriod" value="12">
				<h2><?php echo $this->_dictionary[extendPopup][title];?></h2>
				<p><?php echo $this->_dictionary[extendPopup][description];?></p>
								
				<div class="pq_period">
				<label>
					<input type="radio" id="PQExtendPopup_ExtendTime_1month" onclick="document.getElementById('PQExtendPopup_ExtendPeriod').value=this.value;changeExtendCheckoutPeriod();" name="checkoutMonthPeriod" value="1">
					<div>
						<p>monthly</p>
					</div>
				</label>
				<label>
					<input type="radio" id="PQExtendPopup_ExtendTime_12month" onclick="document.getElementById('PQExtendPopup_ExtendPeriod').value=this.value;changeExtendCheckoutPeriod();" name="checkoutMonthPeriod" value="12" checked>
					<div>
						<p>12 months</p>
					</div>
				</label>
				<label>
					<input type="radio" id="PQExtendPopup_ExtendTime_3month" onclick="document.getElementById('PQExtendPopup_ExtendPeriod').value=this.value;changeExtendCheckoutPeriod();" name="checkoutMonthPeriod" value="6">
					<div>
						<p>6 months</p>
					</div>
				</label>
				</div>
				<div style="width: auto;display: block;overflow-y: auto;    max-height: 45vh; margin: 10px auto 20px;">
				<table width="100%" cellspacing="0" style="padding:0;border-spacing: 0px 5px;">
				<tbody>				
				<input type="hidden" name="action" value="checkout">
				<input type="hidden" name="type" value="extend">
					<?php						
						if($toolsArray){				
						foreach((array)$toolsArray as $tools => $data)
						{							
							foreach((array)$data[proOptionsDetail] as $k => $v)
							{
								if($v[status] == 'paid')
								{								
									if($v[attentionBlock] == 'yellow' && $v[diff][days]>0 && $v[diff][days]<=1){
										$v[attentionBlock] = 'red';
									}							
					?>
					<tr>
						<td><input type="checkbox" onclick="changeExtendCheckoutPeriod();" id="PQToolsToExtend_<?php echo $tools."_".$k;?>" data-pq-mail-options="<?php if($k == 'mail') echo 1;else echo 0;?>" data-pq-price="<?php echo $v[price];?>" name="<?php echo $tools;?>[<?php echo $k;?>]" checked></td>
						<td><img src="<?php echo plugins_url('i/'.$data[name][icon], __FILE__);?>"></td>
						<td><p><?php echo $data[name][name].' '.$data[name][type].' '; if($k == 'whitelabel') echo 'Whitelabel'; elseif($k == 'themes') echo 'Premium Theme "'.$v[theme].'"'; elseif($k=='mail') echo 'Profitquery Mail Service'; ?></p></td>
						<td><p><?php echo date('d/m').' - '.date('d/m', $v[dend]);?></p></td>
						<td><p><?php echo $v[price].'$/mo';?></p></td>
						<td><div class="pq_check <?php if($v[attentionBlock] == 'green') echo 'pq_active'; elseif($v[attentionBlock] == 'yellow') echo 'pq_trial'; elseif($v[attentionBlock] == 'red') echo 'pq_stop';?>"></div></td>
					</tr>
					<?php
								}
							}
						}
						}else{
							echo '<p>You use Free Plan</p>';
						}
					?>				
				</tbody>
			</table>	
			</div>
			<div class="pq_total">
				<p id="PQExtendPopup_Discount_Text"></p><p id="PQExtendPopup_Price"></p><p id="PQExtendPopup_Save_Text"></p>
			</div>	
			<input type="submit" id="PQtoExtendForm_Submit" class="add_new_big" value="Pay">		
			<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('PQExtendPopup').style.display='none';"></a>
			</form>
			<script>
				changeExtendCheckoutPeriod();
			</script>
			</div>		
			<!--  END EXTEND -->
			
			<!--  NOTIFICATION POPUP -->
			<div class="pq_info_popup" id="PQ_info_popup">
					<h2>Notification</h2>
					<a><p>text text text text text text text text text text text text text text text text text</p><img src="<?php echo plugins_url('i/read.png', __FILE__);?>"><span>31 july</span></a>
					<a><p>text text text text text text text text text text text text text text text text text</p><img src="<?php echo plugins_url('i/read.png', __FILE__);?>"><span>31 july</span></a>
					<a class="pq_readed"><p>text text text text text text text text text text text text text text text text text</p><img src="<?php echo plugins_url('i/read.png', __FILE__);?>"><span>31 july</span></a>
					<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('PQ_info_popup').style.display='none';"></a>
			</div>
			<!--  END NOTIFICATION POPUP -->
			<!-- Disable Option Popup -->	
				<div class="pq_popup_desable" id="PQ_Disable_Option_Popup" >
				<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
				<input type="hidden" id="PQ_Disable_Option_Popup_location" name="currentLocation" value="">			
				<input type="hidden" name="action" value="disableOption">			
				<input type="hidden" id="PQ_Disable_Option_optionName" name="optionName" value="">
				<input type="hidden" id="PQ_Disable_Option_themeName" name="themeName" value="">
				<input type="hidden" id="PQ_Disable_Option_toolID" name="tool" value="">
					<img id="PQ_Disable_Option_Popup_src" src="">
					<h2 id="PQ_Disable_Option_Popup_title"></h2>
					<input type="submit" value="<?php echo $this->_dictionary[disableDialog][button_option_yes];?>">
					<input type="button" value="<?php echo $this->_dictionary[disableDialog][button_no];?>" href="javascript:void(0)" onclick="document.getElementById('PQ_Disable_Option_Popup').style.display='none';"><br>
				</form>
				</div>
				
				<script>
					function disableCurrentOption(id, name, src, opt, theme, location){
						document.getElementById('PQ_Disable_Option_Popup').style.display = 'block';					
						var title = '<?php echo $this->_dictionary[disableDialog][title];?>';					
						document.getElementById('PQ_Disable_Option_toolID').value = id;
						document.getElementById('PQ_Disable_Option_optionName').value = opt;
						document.getElementById('PQ_Disable_Option_themeName').value = theme;
						document.getElementById('PQ_Disable_Option_Popup_title').innerHTML = title.replace(new RegExp("(\%s)",'g'),name);					
						document.getElementById('PQ_Disable_Option_Popup_src').src = src;
						document.getElementById('PQ_Disable_Option_Popup_location').value = location;
					}
				</script>
				
				<!-- Disable Popup -->	
				<div class="pq_popup_desable" id="PQ_Disable_Popup">
				<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
				<input type="hidden" name="action" value="disable">
				<input type="hidden" id="PQ_Disable_Popup_toolsID" name="toolsID" value="">
					<img id="PQ_Disable_Popup_src" src="">
					<h2 id="PQ_Disable_Popup_title"></h2>
					<input type="submit" value="<?php echo $this->_dictionary[disableDialog][button_yes];?>">
					<input type="button" value="<?php echo $this->_dictionary[disableDialog][button_no];?>" href="javascript:void(0)" onclick="document.getElementById('PQ_Disable_Popup').style.display='none';"><br>
				</form>
				</div>
			
			<!--  PRO OPTIONS POPUP -->
			<div class="pq_info_popup" id="PQ_pro_info">
					<h2><?php echo $this->_dictionary[proOptionsInfo][title];?></h2>
					<?php
						$ifProOptionsExist = 0;
						if($selectedProOptions){
							foreach((array)$selectedProOptions as $tool => $array){
								foreach((array)$array[pro_options] as $opt_name => $data){
									if($opt_name && (int)$data[price] > 0) $ifProOptionsExist = 1;
									?>
									<p>
									<img src="<?php echo plugins_url('i/'.$selectedProOptions[$tool][name][icon], __FILE__);?>">
									<?php echo $selectedProOptions[$tool][name][name].' '.$selectedProOptions[$tool][name][type].' '; if($opt_name == 'whitelabel') echo 'Whitelabel'; elseif($opt_name == 'themes') echo 'Premium Theme "'.$data[theme].'"'; elseif($opt_name=='mail') echo 'Profitquery Mail Service'; echo ' '.$data[price]."$/mo";?>  
									<a onclick="disableCurrentOption('<?php echo $tool?>', '<?php echo $selectedProOptions[$tool][name][name].' '.$selectedProOptions[$tool][name][type].' '; if($opt_name == 'whitelabel') echo 'Whitelabel'; elseif($opt_name == 'themes') echo 'Premium Theme '.$data[theme].''; elseif($opt_name=='mail') echo 'Profitquery Mail Service'; ?>', '<?php echo plugins_url('i/'.$selectedProOptions[$tool][name][icon], __FILE__);?>', '<?php echo $opt_name;?>', '<?php echo $data[theme];?>', 'pro_info');"><img src="<?php echo plugins_url('i/disable_tool.png', __FILE__);?>"></a>
									</p>
									<?php								
								}
							}
						}else{
							echo '<p>'.$this->_dictionary[proOptionsInfo][no_item].'</p>';
						}
					?>
					<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('PQ_pro_info').style.display='none';"></a>
			</div>
			<?php
				if((int)$needOpenProInfoPopup && (int)$ifProOptionsExist == 1){
					echo '<script>document.getElementById("PQ_pro_info").style.display="block";</script>';
				}
			?>
			<!--  END PRO OPTIONS POPUP -->
			
			<!-- NEED HELP -->			
			<div class="pq_info_popup" id="PQNeedHelpPopup">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="needHelp">
				<h2><?php echo $this->_dictionary[needHelp][title];?></h2>
				<p><?php echo $this->_dictionary[needHelp][description];?></p>				
				<label><p><?php echo $this->_dictionary[needHelp][enter_message];?></p><textarea name="message"></textarea></label>
				<div class="pq_clear"></div>
				<input type="submit" name="sbmt" value="<?php echo $this->_dictionary[action][send];?>">
				<p><?php printf($this->_dictionary[needHelp][support_text], '<a href="mailto:support@profitquery.com">support@profitquery.com</a>');?></p>
				<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('PQNeedHelpPopup').style.display='none';"></a>
			</form>
			</div>				
			<!-- END NEED HELP -->
			<div id="View_tools_map" style="display: none;">
				<div>
				<label onclick="document.getElementById('PQ_desctop').style.display='inline-block',document.getElementById('PQ_mobile').style.display='none';">
					<input type="radio" name="tools_map" checked="">
					<div><p>desktop</p></div>
				</label>
				<label onclick="document.getElementById('PQ_desctop').style.display='none', document.getElementById('PQ_mobile').style.display='inline-block';">
					<input type="radio" name="tools_map">
					<div><p>mobile</p></div>
				</label>
				</div>						
				<div id="PQ_desctop" style="display: inline-block; float: none; position: relative; max-width: 100%; margin: 10% auto 0; overflow: hidden;"  onclick="document.getElementById('View_tools_map_popup').style.display='block';"><img src="<?php echo plugins_url('i/map_descktop.png', __FILE__);?>">				
					<label class="bar_top"><input type="checkbox" <?php if($positionArray[desktop][BAR_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'BAR_TOP');">BAR TOP<div></div></label>
					<label class="bar_bottom"><input type="checkbox" <?php if($positionArray[desktop][BAR_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'BAR_BOTTOM');">BAR BOTTOM<div></div></label>
					<label class="side_left_top"><input type="checkbox" <?php if($positionArray[desktop][SIDE_LEFT_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'SIDE_LEFT_TOP');">SIDE_LEFT_TOP<div></div></label>
					<label class="side_left_middle"><input type="checkbox" <?php if($positionArray[desktop][SIDE_LEFT_MIDDLE]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'SIDE_LEFT_MIDDLE');">SIDE_LEFT_MIDDLE<div></div></label>
					<label class="side_left_bottom"><input type="checkbox" <?php if($positionArray[desktop][SIDE_LEFT_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'SIDE_LEFT_BOTTOM');">SIDE_LEFT_BOTTOM<div></div></label>
					<label class="side_right_top"><input type="checkbox" <?php if($positionArray[desktop][SIDE_RIGHT_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'SIDE_RIGHT_TOP');">SIDE_RIGHT_TOP<div></div></label>
					<label class="side_right_middle"><input type="checkbox" <?php if($positionArray[desktop][SIDE_RIGHT_MIDDLE]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'SIDE_RIGHT_MIDDLE');">SIDE_RIGHT_MIDDLE<div></div></label>
					<label class="side_right_bottom"><input type="checkbox" <?php if($positionArray[desktop][SIDE_RIGHT_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'SIDE_RIGHT_BOTTOM');">SIDE_RIGHT_BOTTOM<div></div></label>
					<label class="popup_center"><input type="checkbox" <?php if($positionArray[desktop][CENTER]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'CENTER');">CENTER<div></div></label>
					<label class="floating_left_top"><input type="checkbox" <?php if($positionArray[desktop][FLOATING_LEFT_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'FLOATING_LEFT_TOP');">FLOATING_LEFT_TOP<div></div></label>			
					<label class="floating_left_bottom"><input type="checkbox" <?php if($positionArray[desktop][FLOATING_LEFT_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'FLOATING_LEFT_BOTTOM');">FLOATING_LEFT_BOTTOM<div></div></label>
					<label class="floating_right_top"><input type="checkbox" <?php if($positionArray[desktop][FLOATING_RIGHT_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'FLOATING_RIGHT_TOP');">FLOATING_RIGHT_TOP<div></div></label>			
					<label class="floating_right_bottom"><input type="checkbox" <?php if($positionArray[desktop][FLOATING_RIGHT_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('desktop', 'FLOATING_RIGHT_BOTTOM');">FLOATING_RIGHT_BOTTOM<div></div></label>
				</div>
				<div id="PQ_mobile" style="float: none; position: relative; max-width: 100%; margin: 10% auto 0; overflow: hidden; display: none;"><img src="<?php echo plugins_url('i/map_mobile.png', __FILE__);?>">				
					<label class="bar_top"><input type="checkbox" <?php if($positionArray[mobile][BAR_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'BAR_TOP');">BAR TOP<div></div></label>
					<label class="bar_bottom"><input type="checkbox" <?php if($positionArray[mobile][BAR_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'BAR_BOTTOM');">BAR BOTTOM<div></div></label>
					<label class="side_left_top"><input type="checkbox" <?php if($positionArray[mobile][SIDE_LEFT_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'SIDE_LEFT_TOP');">SIDE_LEFT_TOP<div></div></label>
					<label class="side_left_middle"><input type="checkbox" <?php if($positionArray[mobile][SIDE_LEFT_MIDDLE]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'SIDE_LEFT_MIDDLE');">SIDE_LEFT_MIDDLE<div></div></label>
					<label class="side_left_bottom"><input type="checkbox" <?php if($positionArray[mobile][SIDE_LEFT_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'SIDE_LEFT_BOTTOM');">SIDE_LEFT_BOTTOM<div></div></label>
					<label class="side_right_top"><input type="checkbox" <?php if($positionArray[mobile][SIDE_RIGHT_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'SIDE_RIGHT_TOP');">SIDE_RIGHT_TOP<div></div></label>
					<label class="side_right_middle"><input type="checkbox" <?php if($positionArray[mobile][SIDE_RIGHT_MIDDLE]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'SIDE_RIGHT_MIDDLE');">SIDE_RIGHT_MIDDLE<div></div></label>
					<label class="side_right_bottom"><input type="checkbox" <?php if($positionArray[mobile][SIDE_RIGHT_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'SIDE_RIGHT_BOTTOM');">SIDE_RIGHT_BOTTOM<div></div></label>
					<label class="popup_center"><input type="checkbox" <?php if($positionArray[mobile][CENTER]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'CENTER');">CENTER<div></div></label>				
					<label class="floating_right_top"><input type="checkbox" <?php if($positionArray[mobile][FLOATING_TOP]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'FLOATING_TOP');">FLOATING_RIGHT_TOP<div></div></label>			
					<label class="floating_right_bottom"><input type="checkbox" <?php if($positionArray[mobile][FLOATING_BOTTOM]) echo 'class="pq_checked"';?>  onclick="viewPositionDetail('mobile', 'FLOATING_BOTTOM');">FLOATING_RIGHT_BOTTOM<div></div></label>
				</div>
				<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('View_tools_map').style.display='none';">close</a>
				
				<!-- VIEW TOOLS MAP POPUP -->
				<div id="View_tools_map_popup" style="display:none;" >
					<table width="100%" cellspacing="0" style="margin:10px auto 20px; padding:0;border-spacing: 0px 5px;">
					<tbody id="VIEW_TOOLS_MAP_CONTENT">					
					</tbody>
				</table>
				<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('View_tools_map_popup').style.display='none';"></a>
				</div>
				<script>
					function viewPositionDetail(type, pos){					
						var ret = '';
						var flag = false;
						var content = '';
						var ipath = '<?php echo plugins_url('i/', __FILE__);?>';
						var displayRules = 'Display Rules';
						var settingImg = '<?php echo plugins_url('i/settings_tool.png', __FILE__);?>';
						var settingUrl = '<?php echo $this->getSettingsPageUrl();?>';
						var header = 
						'<tr>'+
							'<td> </td>'+
							'<td> </td>'+
							'<td> </td>'+
							'<td> </td>'+						
							'<td> </td>'+
						'</tr>';
						for(var i in PQToolsPosition[type][pos]){
							if(typeof PQToolsPosition[type][pos][i] == 'object'){
								flag = true;
								content += 
								'<tr>'+
									'<td><img src="'+ipath+PQToolsPosition[type][pos][i].info.icon+'"></td>'+
									'<td><p>'+PQToolsPosition[type][pos][i].info.name+' '+PQToolsPosition[type][pos][i].info.type+'</p></td>'+
									'<td><p>'+PQToolsPosition[type][pos][i].eventHandler.name+' '+PQToolsPosition[type][pos][i].eventHandler.value+'</p></td>'+								
									'<td><a href="'+settingUrl+'&s_t='+PQToolsPosition[type][pos][i].code+'&step=3">'+displayRules+'</td>'+
									'<td><a href="'+settingUrl+'&s_t='+PQToolsPosition[type][pos][i].code+'&pos='+PQToolsPosition[type][pos][i].position+'"><img src="'+settingImg+'"></a></td>'+
								'</tr>';							
							}						
						}
						if(flag){
							ret = header+content;
						}else{
							ret = header+'<tr><td colspan="5">-</td></tr>';
						}
						document.getElementById('VIEW_TOOLS_MAP_CONTENT').innerHTML = ret;					
						document.getElementById('View_tools_map_popup').style.display = 'block';
					}
				</script>
				
				</div>
			<!-- SUCCESS SEND MAIL -->			
			<div class="pq_info_popup" id="PQSuccessSendMail">			
				<h2><?php echo $this->_dictionary[notification][send_mail];?></h2>							
				<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('PQSuccessSendMail').style.display='none';"></a>
			</form>
			</div>				
			<!-- END SUCCESS SEND MAIL -->
			
			<?php
				if((int)$successSendEmail == 1){
					echo '<script>document.getElementById("PQSuccessSendMail").style.display="block";setTimeout(function(){document.getElementById("PQSuccessSendMail").style.display="none";},3000);</script>';
				}
			?>
			
			<!--  SETTINGS POPUP -->
			<div class="pq_info_popup" id="PQSettingsPopup">
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="settingsSave">
				<h2><?php echo $this->_dictionary[globalSettings][title];?></h2>
				<p><?php echo $this->_dictionary[globalSettings][description];?></p>
				<label><p><?php echo $this->_dictionary[globalSettings][api_key];?> <a href="http://api.profitquery.com/cms-sign-in/?domain=<?php echo $this->getDomain();?>&cms=wp&ae=<?php echo get_settings('admin_email');?>&redirect=<?php echo str_replace(".", "%2E", urlencode($this->getSettingsPageUrl()));?>">Get Api Key</a></p><input type="text" name="settings[apiKey]" value="<?php echo stripslashes($this->_options[settings][apiKey]);?>"></label>
				<label><p><?php echo $this->_dictionary[globalSettings][pro_loader];?></p><input type="text" name="settings[pro_loader_filename]" value="<?php echo stripslashes($this->_options[settings][pro_loader_filename]);?>"></label>
				<label><p><?php echo $this->_dictionary[globalSettings][main_page];?></p><input type="text" name="settings[mainPage]" value="<?php echo stripslashes($this->_options[settings][mainPage]);?>"></label>
				<label><p><?php echo $this->_dictionary[globalSettings][admin_email];?></p><input type="text" name="settings[email]" value="<?php echo stripslashes($this->_options[settings][email]);?>"></label>
				<div class="pq_clear"></div>
				<label style="background-color: #F4F5F7; padding: 10px 20px; margin: 15px 0; position: relative;"><p>Use Google Analytics</p><input type="checkbox" id="1" class="pq_switch" name="settings[enableGA]" <?php if($this->_options[settings][enableGA] == 'on') echo 'checked';?> /><label for="1" class="pq_switch_label"></label></label>
				<input type="submit" name="sbmt" value="<?php echo $this->_dictionary[action][save];?>">
				<a class="pq_close" href="javascript:void(0)" onclick="document.getElementById('PQSettingsPopup').style.display='none';"></a>
			</form>
			</div>				
			<!--  END SETTINGS POPUP -->
			
	</div>
	<?php   
		}else{
			?>
			<form action="<?php echo $this->getSettingsPageUrl();?>" method="post">
			<input type="hidden" name="action" value="i_agree">
			<div id="pq_agree">
				<img src="<?php echo plugins_url('i/pq_logo_big.png', __FILE__);?>">
				<h1>Welcome to Profitquery</h1>
			<p>We've prepared over 20 tools for your website growth, all of them you can use for free. We have restructured all our tools and settings. Unfortunately, old plugin version have a structure that we cannot update automatically..</p>
			<div><img src="<?php echo plugins_url('i/pq_1.png', __FILE__);?>"><p>20 tools free</p></div>
			<div><img src="<?php echo plugins_url('i/pq_2.png', __FILE__);?>"><p>200+ themes</p></div>
			<div><img src="<?php echo plugins_url('i/pq_3.png', __FILE__);?>"><p>event handler</p></div>
			<input type="submit" name="i_agree" value="Get started">
			<p class="pq_policy">By clicking "Get Started" you agree to Profitquery's <a href="http://profitquery.com/terms.html" target="_blank">Terms of Service</a> and <a href="http://profitquery.com/privacy.html" target="_blank">Privacy Policy.</a></p>
			</div>
			</form>
			<?php
		}
	}
	
	function getFullDomain()
    {
        $url     = get_option('siteurl');
        $urlobj  = parse_url($url);
        $domain  = $urlobj['host'];        
        return $domain;
    }
	/**
     * Get the wp domain
     * 
     * @return string
     */
    function getDomain()
    {
        $url     = get_option('siteurl');
        $urlobj  = parse_url($url);
        $domain  = $urlobj['host'];
        $domain  = str_replace('www.', '', $domain);
        return $domain;
    }
}